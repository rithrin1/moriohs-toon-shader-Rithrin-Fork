# Morioh's Toon Shader Forked
# [Can't find the Download link? Click here](https://gitlab.com/rithrin1/moriohs-toon-shader/-/archive/master/moriohs-toon-shader-master.zip)
# [Link to moris orginal manual Dither is removed](https://gitlab.com/xMorioh/moriohs-toon-shader/-/wikis/Mori's-Toon-Shader-Manual)

# [Link to manual with Changes and extra things](https://gitlab.com/rithrin1/moriohs-toon-shader-Rithrin-Fork/-/wikis/Rithrin-Fork-Manual)


**About this Shader**
Forked Vertion of Moris Toon Shader

**Additional Features included**:
* Compleatly replaced dissolve for outfit swaping.
* Hair dyeing
* Skin tan
* Makeup masking
* Diffuse Color Swap

**Removed**
* Ignore Projector (personal preference)
* Dither


**Variants**:
* Opaque
* Cutout
* Outlined
* Transparent

<br>
//Disclaimer

This Shader is entirely made with the Amplify Shader Editor Asset.
<br>
This Shader does not support Deffered Rendering nor Lightmap Baking and should not be used for Worlds but rather for Avatars specifically.

Tested with Unity 2019.4.20f1