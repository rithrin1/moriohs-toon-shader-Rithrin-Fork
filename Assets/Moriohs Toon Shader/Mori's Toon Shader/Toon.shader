// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Moriohs Shaders/Moris Toon Shader/Toon"
{
	Properties
	{
		[ToggleUI]_MaterializeVertexColor("Materialize Vertex Color", Float) = 0
		[HideInInspector] _texcoord2( "", 2D ) = "white" {}
		[HideInInspector] _texcoord3( "", 2D ) = "white" {}
		[HideInInspector] _texcoord4( "", 2D ) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[ToggleUI]_DissolveVertexMultiplierAnimated1("_DissolveVertexMultiplier", Int) = 0
		[ToggleUI]_ToggleDissolveVertexOffsetAnimated("_Toggle DissolveVertexOffset", Int) = 0
		[ToggleUI]_EmissiveScaleAnimated("_EmissiveScale", Int) = 0
		[ToggleUI]_EmissiveDissolveColorAnimated1("_EmissiveDissolveColor", Int) = 0
		_DissolvePattern("Dissolve Pattern", 2D) = "black" {}
		_DissolveDensity("Dissolve Density", Range( 1 , 5)) = 1
		_DissolveRemapMin("Dissolve Remap Min", Range( -100 , 100)) = -1
		_DissolveRemapMax("Dissolve Remap Max", Range( -100 , 100)) = 0.5
		_DissolveVertexMultiplier("Dissolve Vertex Multiplier", Range( 0 , 1)) = 0.01
		[ToggleUI]_ToggleDissolveVertexOffset("Toggle DissolveVertexOffset", Float) = 0
		[ToggleUI]_ToggleDissolveEmission("Toggle DissolveEmission", Int) = 0
		[HDR]_EmissiveDissolveColor("Emissive Dissolve Color", Color) = (0.5019608,0,1,0)
		_EmissiveScale("EmissiveScale", Range( -1 , 1)) = 0.1354477
		[ToggleUI]_DissolveToggleDir33Animated("_DissolveToggleDir33", Int) = 0
		[ToggleUI]_DissolveToggleDir37Animated("_DissolveToggleDir37", Int) = 0
		[ToggleUI]_DissolveObject8Animated("_DissolveObject8", Int) = 0
		[ToggleUI]_DissolveObject9Animated("_DissolveObject9", Int) = 0
		[ToggleUI]_DissolveObject10Animated("_DissolveObject10", Int) = 0
		[ToggleUI]_DissolveToggleDir10Animated("_DissolveToggleDir10", Int) = 0
		[ToggleUI]_MaterializeVertexColorAnimated("_Materialize Vertex Color", Int) = 0
		[ToggleUI]_DissolveToggleDir1Animated("_Dissolve Toggle Dir1", Int) = 0
		[ToggleUI]_DissolveToggleDir2Animated("_DissolveToggleDir2", Int) = 0
		[ToggleUI]_DissolveToggleDir3Animated("_DissolveToggleDir3", Int) = 0
		[ToggleUI]_DissolveToggleDir4Animated("_DissolveToggleDir4", Int) = 0
		[ToggleUI]_DissolveToggleDir5Animated("_DissolveToggleDir5", Int) = 0
		[ToggleUI]_DissolveToggleDir6Animated("_DissolveToggleDir6", Int) = 0
		[ToggleUI]_DissolveToggleDir7Animated("_DissolveToggleDir7", Int) = 0
		[ToggleUI]_DissolveToggleDir8Animated("_DissolveToggleDir8", Int) = 0
		[ToggleUI]_DissolveToggleDir9Animated("_DissolveToggleDir9", Int) = 0
		[ToggleUI]_DissolveToggleDir20Animated("_DissolveToggleDir120", Int) = 0
		[ToggleUI]_DissolveObject6Animated("_DissolveObject6", Int) = 0
		[ToggleUI]_DissolveToggleDir19Animated("_DissolveToggleDir19", Int) = 0
		[ToggleUI]_DissolveToggleDir18Animated("_DissolveToggleDir18", Int) = 0
		[ToggleUI]_DissolveToggleDir17Animated("_DissolveToggleDir17", Int) = 0
		[ToggleUI]_DissolveToggleDir16Animated("_DissolveToggleDir16", Int) = 0
		[ToggleUI]_DissolveToggleDir34Animated("_DissolveToggleDir34", Int) = 0
		[ToggleUI]_DissolveToggleDir29Animated("_DissolveToggleDir29", Int) = 0
		[ToggleUI]_DissolveToggleDir39Animated("_DissolveToggleDir39", Int) = 0
		[ToggleUI]_DissolveToggleDir28Animated("_DissolveToggleDir28", Int) = 0
		[ToggleUI]_DissolveToggleDir22Animated("_DissolveToggleDir22", Int) = 0
		[ToggleUI]_DissolveObject30Animated("_DissolveObject30", Int) = 0
		[ToggleUI]_DissolveToggleDir27Animated("_DissolveToggleDir27", Int) = 0
		[ToggleUI]_DissolveToggleDir26Animated("_DissolveToggleDir26", Int) = 0
		[ToggleUI]_DissolveToggleDir25Animated("_DissolveToggleDir25", Int) = 0
		[ToggleUI]_DissolveToggleDir24Animated("_DissolveToggleDir24", Int) = 0
		[ToggleUI]_DissolveObject40Animated("_DissolveObject40", Int) = 0
		[ToggleUI]_DissolveToggleDir35Animated("_DissolveToggleDir35", Int) = 0
		[ToggleUI]_DissolveToggleDir40Animated("_DissolveToggleDir40", Int) = 0
		[ToggleUI]_DissolveToggleDir38Animated("_DissolveToggleDir38", Int) = 0
		[ToggleUI]_DissolveToggleDir30Animated("_DissolveToggleDir30", Int) = 0
		[ToggleUI]_DissolveToggleDir31Animated("_DissolveToggleDir31", Int) = 0
		[ToggleUI]_DissolveObject11Animated("_DissolveObject11", Int) = 0
		[ToggleUI]_DissolveToggleDir36Animated("_DissolveToggleDir36", Int) = 0
		[ToggleUI]_DissolveObject12Animated("_DissolveObject12", Int) = 0
		[ToggleUI]_DissolveToggleDir32Animated("_DissolveToggleDir32", Int) = 0
		[ToggleUI]_DissolveObject39Animated("_DissolveObject39", Int) = 0
		[ToggleUI]_DissolveObject24Animated("_DissolveObject24", Int) = 0
		[ToggleUI]_DissolveObject14Animated("_DissolveObject14", Int) = 0
		[ToggleUI]_DissolveObject1Animated("_DissolveObject1", Int) = 0
		[ToggleUI]_DissolveObject3Animated("_DissolveObject3", Int) = 0
		[ToggleUI]_DissolveObject5Animated("_DissolveObject5", Int) = 0
		[ToggleUI]_DissolveObject4Animated("_DissolveObject4", Int) = 0
		[ToggleUI]_DissolveToggleDir15Animated("_DissolveToggleDir15", Int) = 0
		[ToggleUI]_DissolveToggleDir14Animated("_DissolveToggleDir14", Int) = 0
		[ToggleUI]_DissolveObject7Animated("_DissolveObject7", Int) = 0
		[ToggleUI]_DissolveToggleDir12Animated("_DissolveToggleDir12", Int) = 0
		[ToggleUI]_DissolveToggleDir13Animated("_DissolveToggleDir13", Int) = 0
		[ToggleUI]_DissolveObject35Animated("_DissolveObject35", Int) = 0
		[ToggleUI]_DissolveObject34Animated("_DissolveObject34", Int) = 0
		[ToggleUI]_DissolveObject33Animated("_DissolveObject33", Int) = 0
		[ToggleUI]_DissolveObject32Animated("_DissolveObject32", Int) = 0
		[ToggleUI]_DissolveObject31Animated("_DissolveObject31", Int) = 0
		[ToggleUI]_DissolveObject2Animated("_DissolveObject2", Int) = 0
		[ToggleUI]_DissolveObject13Animated("_DissolveObject13", Int) = 0
		[ToggleUI]_DissolveObject29Animated("_DissolveObject29", Int) = 0
		[ToggleUI]_DissolveObject23Animated("_DissolveObject23", Int) = 0
		[ToggleUI]_DissolveObject26Animated("_DissolveObject26", Int) = 0
		[ToggleUI]_DissolveObject27Animated("_DissolveObject27", Int) = 0
		[ToggleUI]_DissolveObject28Animated("_DissolveObject28", Int) = 0
		[ToggleUI]_DissolveObject15Animated("_DissolveObject15", Int) = 0
		[ToggleUI]_DissolveToggleDir23Animated("_DissolveToggleDir23", Int) = 0
		[ToggleUI]_DissolveObject17Animated("_DissolveObject17", Int) = 0
		[ToggleUI]_DissolveObject18Animated("_DissolveObject18", Int) = 0
		[ToggleUI]_DissolveObject25Animated("_DissolveObject25", Int) = 0
		[ToggleUI]_DissolveObject19Animated("_DissolveObject19", Int) = 0
		[ToggleUI]_DissolveObject22Animated("_DissolveObject22", Int) = 0
		[ToggleUI]_DissolveObject21Animated("_DissolveObject21", Int) = 0
		[ToggleUI]_DissolveObject20Animated("_DissolveObject20", Int) = 0
		[ToggleUI]_DissolveObject36Animated("_DissolveObject36", Int) = 0
		[ToggleUI]_DissolveObject37Animated("_DissolveObject37", Int) = 0
		[ToggleUI]_DissolveToggleDir11Animated("_DissolveToggleDir11", Int) = 0
		[ToggleUI]_DissolveObject38Animated("_DissolveObject38", Int) = 0
		[ToggleUI]_GlobalDissolveAnimated("_GlobalDissolve", Int) = 0
		[ToggleUI]_DissolveObject16Animated("_DissolveObject16", Int) = 0
		[ToggleUI]_DissolveToggleDir21Animated("_DissolveToggleDir21", Int) = 0
		[Enum(Separate,0,Merge,1,Reverse Merge,2)]_MaterializeLayerModeR("Materialize Layer Mode R", Float) = 0
		_DissolveMask("DissolveMask", 2D) = "white" {}
		[ToggleUI]_DissolveToggleDir1("Dissolve Toggle Dir1", Float) = 0
		[ToggleUI]_DissolveToggleDir2("Dissolve Toggle Dir2", Float) = 0
		[ToggleUI]_DissolveToggleDir3("Dissolve Toggle Dir3", Float) = 0
		[ToggleUI]_DissolveToggleDir4("Dissolve Toggle Dir4", Float) = 0
		[ToggleUI]_DissolveToggleDir5("Dissolve Toggle Dir5", Float) = 0
		[ToggleUI]_DissolveToggleDir6("Dissolve Toggle Dir6", Float) = 0
		[ToggleUI]_DissolveToggleDir7("Dissolve Toggle Dir7", Float) = 0
		[ToggleUI]_DissolveToggleDir8("Dissolve Toggle Dir8", Float) = 0
		[ToggleUI]_DissolveToggleDir9("Dissolve Toggle Dir9", Float) = 0
		[ToggleUI]_DissolveToggleDir10("Dissolve Toggle Dir10", Float) = 0
		[ToggleUI]_DissolveToggleDir11("Dissolve Toggle Dir 11", Float) = 0
		[ToggleUI]_DissolveToggleDir12("Dissolve Toggle Dir 12", Float) = 0
		[ToggleUI]_DissolveToggleDir13("Dissolve Toggle Dir 13", Float) = 0
		[ToggleUI]_DissolveToggleDir14("Dissolve Toggle Dir 14", Float) = 0
		[ToggleUI]_DissolveToggleDir15("Dissolve Toggle Dir 15", Float) = 0
		[ToggleUI]_DissolveToggleDir16("Dissolve Toggle Dir 16", Float) = 0
		[ToggleUI]_DissolveToggleDir17("Dissolve Toggle Dir 17", Float) = 0
		[ToggleUI]_DissolveToggleDir18("Dissolve Toggle Dir 18", Float) = 0
		[ToggleUI]_DissolveToggleDir19("Dissolve Toggle Dir 19", Float) = 0
		[ToggleUI]_DissolveToggleDir20("Dissolve Toggle Dir 20", Float) = 0
		[ToggleUI]_DissolveToggleDir21("Dissolve Toggle Dir 21", Float) = 0
		[ToggleUI]_DissolveToggleDir22("Dissolve Toggle Dir 22", Float) = 0
		[ToggleUI]_DissolveToggleDir23("Dissolve Toggle Dir 23", Float) = 0
		[ToggleUI]_DissolveToggleDir24("Dissolve Toggle Dir 24", Float) = 0
		[ToggleUI]_DissolveToggleDir25("Dissolve Toggle Dir 25", Float) = 0
		[ToggleUI]_DissolveToggleDir26("Dissolve Toggle Dir 26", Float) = 0
		[ToggleUI]_DissolveToggleDir27("Dissolve Toggle Dir 27", Float) = 0
		[ToggleUI]_DissolveToggleDir28("Dissolve Toggle Dir 28", Float) = 0
		[ToggleUI]_DissolveToggleDir29("Dissolve Toggle Dir 29", Float) = 0
		[ToggleUI]_DissolveToggleDir30("Dissolve Toggle Dir 30", Float) = 0
		[ToggleUI]_DissolveToggleDir31("Dissolve Toggle Dir 31", Float) = 0
		[ToggleUI]_DissolveToggleDir32("Dissolve Toggle Dir 32", Float) = 0
		[ToggleUI]_DissolveToggleDir33("Dissolve Toggle Dir 33", Float) = 0
		[ToggleUI]_DissolveToggleDir34("Dissolve Toggle Dir 34", Float) = 0
		[ToggleUI]_DissolveToggleDir35("Dissolve Toggle Dir 35", Float) = 0
		[ToggleUI]_DissolveToggleDir36("Dissolve Toggle Dir 36", Float) = 0
		[ToggleUI]_DissolveToggleDir37("Dissolve Toggle Dir 37", Float) = 0
		[ToggleUI]_DissolveToggleDir38("Dissolve Toggle Dir 38", Float) = 0
		[ToggleUI]_DissolveToggleDir39("Dissolve Toggle Dir 39", Float) = 0
		[ToggleUI]_DissolveToggleDir40("Dissolve Toggle Dir 40", Float) = 0
		[ToggleUI]_GlobalDissolveDir("GlobalDissolveDir", Float) = 0
		_DissolveObject1("Dissolve Object1", Range( -2 , 2)) = 2
		_DissolveObject2("Dissolve Object2", Range( -2 , 2)) = 2
		_DissolveObject3("Dissolve Object3", Range( -2 , 2)) = 2
		_DissolveObject4("Dissolve Object4", Range( -2 , 2)) = 2
		_DissolveObject5("Dissolve Object5", Range( -2 , 2)) = 2
		_DissolveObject6("Dissolve Object6", Range( -2 , 2)) = 2
		_DissolveObject7("Dissolve Object7", Range( -2 , 2)) = 2
		_DissolveObject8("Dissolve Object8", Range( -2 , 2)) = 2
		_DissolveObject9("Dissolve Object9", Range( -2 , 2)) = 2
		_DissolveObject10("Dissolve Object 10", Range( -2 , 2)) = 2
		_DissolveObject11("Dissolve Object 11", Range( -2 , 2)) = 2
		_DissolveObject12("Dissolve Object 12", Range( -2 , 2)) = 2
		_DissolveObject13("Dissolve Object 13", Range( -2 , 2)) = 2
		_DissolveObject14("Dissolve Object 14", Range( -2 , 2)) = 2
		_DissolveObject15("Dissolve Object 15", Range( -2 , 2)) = 2
		_DissolveObject16("Dissolve Object 16", Range( -2 , 2)) = 2
		_DissolveObject17("Dissolve Object 17", Range( -2 , 2)) = 2
		_DissolveObject18("Dissolve Object 18", Range( -2 , 2)) = 2
		_DissolveObject19("Dissolve Object 19", Range( -2 , 2)) = 2
		_DissolveObject20("Dissolve Object 20", Range( -2 , 2)) = 2
		_DissolveObject21("Dissolve Object 21", Range( -2 , 2)) = 2
		_DissolveObject22("Dissolve Object 22", Range( -2 , 2)) = 2
		_DissolveObject23("Dissolve Object 23", Range( -2 , 2)) = 2
		_DissolveObject24("Dissolve Object 24", Range( -2 , 2)) = 2
		_DissolveObject25("Dissolve Object 25", Range( -2 , 2)) = 2
		_DissolveObject26("Dissolve Object 26", Range( -2 , 2)) = 2
		_DissolveObject27("Dissolve Object 27", Range( -2 , 2)) = 2
		_DissolveObject28("Dissolve Object 28", Range( -2 , 2)) = 2
		_DissolveObject29("Dissolve Object 29", Range( -2 , 2)) = 2
		_DissolveObject30("Dissolve Object 30", Range( -2 , 2)) = 2
		_DissolveObject31("Dissolve Object 31", Range( -2 , 2)) = 2
		_DissolveObject32("Dissolve Object 32", Range( -2 , 2)) = 2
		_DissolveObject33("Dissolve Object 33", Range( -2 , 2)) = 2
		_DissolveObject34("Dissolve Object 34", Range( -2 , 2)) = 2
		_DissolveObject35("Dissolve Object 35", Range( -2 , 2)) = 2
		_DissolveObject36("Dissolve Object 36", Range( -2 , 2)) = 2
		_DissolveObject37("Dissolve Object 37", Range( -2 , 2)) = 2
		_DissolveObject38("Dissolve Object 38", Range( -2 , 2)) = 2
		_DissolveObject39("Dissolve Object 39", Range( -2 , 2)) = 2
		_DissolveObject40("Dissolve Object 40", Range( -2 , 2)) = 2
		_GlobalDissolve("GlobalDissolve", Range( -2 , 2)) = 2
		[ToggleUI]_ToggleLipMatCapR("ToggleLipMatCapR", Float) = 0
		[ToggleUI]_ToggleLipMatCapB("ToggleLipMatCapB", Float) = 0
		[ToggleUI]_ToggleLipMatCapG("ToggleLipMatCapG", Float) = 0
		[ToggleUI]_ToggleMakeup("ToggleMakeup", Float) = 0
		[ToggleUI]_ToggleLipMatCapA("ToggleLipMatCapA", Float) = 0
		_MakeupMask("MakeupMask", 2D) = "white" {}
		_LipRotation("LipRotation", Range( -1 , 1)) = 0
		_MakeUpRedPower("MakeUpRedPower", Range( 0 , 1)) = 1
		_LipPosition("LipPosition", Vector) = (0,0,0,0)
		_MakeUpBluePower("MakeUpBluePower", Range( 0 , 1)) = 1
		_LipScale("LipScale", Vector) = (1,1,0,0)
		_MakeUpGreenPower("MakeUpGreenPower", Range( 0 , 1)) = 1
		_MakeUpGreenColor("MakeUpGreenColor", Color) = (0,1,0.110465,0)
		_MakeUpRedColor("MakeUpRedColor", Color) = (1,0,0.07831144,0)
		_MakeUpBlueColor("MakeUpBlueColor", Color) = (1,0,0.07831144,0)
		_MakeupLipThickness("MakeupLipThickness", Range( 0 , 1)) = 0
		[ToggleUI]_MakeUpBlueColorAnimated("_MakeUpBlueColor", Int) = 0
		[ToggleUI]_MakeUpBluePowerAnimated("_MakeUpBluePower", Int) = 0
		[ToggleUI]_MakeUpRedColorAnimated("_MakeUpRedColor", Int) = 0
		_MakeupLipThick("MakeupLipThick", 2D) = "white" {}
		[ToggleUI]_MakeUpRedPowerAnimated("_MakeUpRedPower", Int) = 0
		[ToggleUI]_MakeUpGreenColorAnimated("_MakeUpGreenColor", Int) = 0
		[ToggleUI]_MakeUpGreenPowerAnimated("_MakeUpGreenPower", Int) = 0
		[ToggleUI]_LipPositionAnimated("_LipPosition", Int) = 0
		[ToggleUI]_ToggleLipMatCapBAnimated("_ToggleLipMatCapB", Int) = 0
		[ToggleUI]_LipRotationAnimated("_LipRotation", Int) = 0
		[ToggleUI]_ToggleLipMatCapRAnimated("_ToggleLipMatCapR", Int) = 0
		[ToggleUI]_ToggleLipMatCapGAnimated("_ToggleLipMatCapG", Int) = 0
		[ToggleUI]_ToggleLipMatCapAAnimated("_ToggleLipMatCapA", Int) = 0
		[ToggleUI]_ToggleMakeupAnimated("_ToggleMakeup", Int) = 0
		[ToggleUI]_LipScaleAnimated("_LipScale", Int) = 0
		[ToggleUI]_MakeupLipThicknessAnimated("_MakeupLipThickness", Int) = 0
		_HairMask("HairMask", 2D) = "white" {}
		_HairTexture("HairTexture", 2D) = "white" {}
		[ToggleUI]_ToggleHair("ToggleHair", Float) = 0
		_HairColor("Hair Color", Color) = (0.3773585,0.2901388,0.2901388,0)
		[ToggleUI]_ToggleHairAnimated("_ToggleHair", Int) = 0
		[ToggleUI]_HairHighLightAnimated("_HairHighLight", Int) = 0
		[ToggleUI]_HairColorAnimated("_HairColor", Int) = 0
		[ToggleUI]_ShadowPowerAnimated("_ShadowPower", Int) = 0
		_ColorDiffuseSampler("ColorDiffuseSampler", 2D) = "white" {}
		_GreenMaskColor1("GreenMaskColor1", Color) = (1,0.5569075,0,0)
		[ToggleUI]_RedMaskColor4Animated("_RedMaskColor4", Int) = 0
		[ToggleUI]_BlueMaskColor4Animated("_BlueMaskColor4", Int) = 0
		[ToggleUI]_CustomDiffuseLerpAnimated("_CustomDiffuseLerp", Int) = 0
		[ToggleUI]_BlueMaskColor3Animated("_BlueMaskColor3", Int) = 0
		[ToggleUI]_BlueMaskColor2Animated("_BlueMaskColor2", Int) = 0
		[ToggleUI]_BlueMaskColor1Animated("_BlueMaskColor1", Int) = 0
		_BlueMaskColor1("BlueMaskColor1", Color) = (0,1,0.1528029,0)
		[ToggleUI]_GreenMaskColor3Animated("_GreenMaskColor3", Int) = 0
		[ToggleUI]_GreenMaskColor2Animated("_GreenMaskColor2", Int) = 0
		[ToggleUI]_GreenMaskColor1Animated("_GreenMaskColor1", Int) = 0
		[ToggleUI]_GreenMaskColor4Animated("_GreenMaskColor4", Int) = 0
		[ToggleUI]_RedMaskColor1Animated("_RedMaskColor1", Int) = 0
		[ToggleUI]_RedMaskColor2Animated("_RedMaskColor2", Int) = 0
		[ToggleUI]_RedMaskColor3Animated("_RedMaskColor3", Int) = 0
		_RedMaskColor1("RedMaskColor1", Color) = (0,0.04827571,1,0)
		_GreenMaskColor2("GreenMaskColor2", Color) = (1,0.9048885,0,0)
		_BlueMaskColor2("BlueMaskColor2", Color) = (1,0.9048885,0,0)
		_RedMaskColor2("RedMaskColor2", Color) = (1,0.9048885,0,0)
		_GreenMaskColor3("GreenMaskColor3", Color) = (0.1376267,0.8676471,0,0)
		_BlueMaskColor3("BlueMaskColor3", Color) = (0.1376267,0.8676471,0,0)
		_RedMaskColor3("RedMaskColor3", Color) = (0.1376267,0.8676471,0,0)
		_GreenMaskColor4("GreenMaskColor4", Color) = (0.8620691,0,1,0)
		_BlueMaskColor4("BlueMaskColor4", Color) = (0.8620691,0,1,0)
		_RedMaskColor4("RedMaskColor4", Color) = (0.8620691,0,1,0)
		[Toggle]_CustomDiffuseLerp("CustomDiffuseLerp", Float) = 0
		_TanPower("TanPower", Range( 0 , 1)) = 0
		_TanTexture("Tan Texture", 2D) = "white" {}
		[ToggleUI]_TanTextureAnimated("_TanTexture", Int) = 0
		[ToggleUI]_TanPowerAnimated("_TanPower", Int) = 0
		[ShaderOptimizerLockButton]_ShaderOptimizerEnabled("Shader Optimizer Enabled", Float) = 0
		[ToggleUI]_FlipbookTintAnimated("_FlipbookTint", Int) = 0
		[ToggleUI]_OptimizerExcludeFlipbook("OptimizerExcludeFlipbook", Int) = 0
		[ToggleUI]_RotateFlipbookAnimated("_RotateFlipbook", Int) = 0
		[ToggleUI]_ColumnsAnimated("_Columns", Int) = 0
		[ToggleUI]_RowsAnimated("_Rows", Int) = 0
		[ToggleUI]_SpeedAnimated("_Speed", Int) = 0
		[ToggleUI]_MaxFramesAnimated("_MaxFrames", Int) = 0
		[ToggleUI]_FlipbookToggleAnimated("_FlipbookToggle", Int) = 0
		[ToggleUI]_FlipbookColorAnimated("_FlipbookColor", Int) = 0
		[HDR]_FlipbookColor("FlipbookColor", Color) = (1,1,1,1)
		_RotateFlipbook("Rotate Flipbook", Range( -1 , 1)) = 0
		_Speed("Speed", Int) = 6
		_Columns("Columns", Int) = 0
		_Rows("Rows", Int) = 0
		_MaxFrames("Max Frames", Int) = 1
		_Flipbook("Flipbook", 2D) = "black" {}
		_FlipbookTint("Flipbook Tint", Range( 0 , 1)) = 0
		_MatcapR1("MatcapR1", 2D) = "white" {}
		_MatcapG2("MatcapG2", 2D) = "white" {}
		_MatcapB3("MatcapB3", 2D) = "white" {}
		_MatcapA4("MatcapA4", 2D) = "white" {}
		[ToggleUI]_MatcapG2smoothnessAnimated("_MatcapG2smoothness", Int) = 0
		[ToggleUI]_ReflectionR1TintAnimated("_ReflectionR1Tint", Int) = 0
		[ToggleUI]_MatcapG2ModeAnimated("_MatcapG2Mode", Int) = 0
		[ToggleUI]_MatcapR1smoothnessAnimated("_MatcapR1smoothness", Int) = 0
		[ToggleUI]_MatcapB3ColorAnimated("_MatcapB3Color", Int) = 0
		[ToggleUI]_MatcapA4ModeAnimated("_MatcapA4Mode", Int) = 0
		[ToggleUI]_MatcapB3smoothnessAnimated("_MatcapB3smoothness", Int) = 0
		[ToggleUI]_MatcapA4ToggleAnimated("_MatcapA4Toggle", Int) = 0
		[ToggleUI]_ReflectionA4TintAnimated("_ReflectionA4Tint", Int) = 0
		[ToggleUI]_ReflectionG2IntensityAnimated("_ReflectionG2Intensity", Int) = 0
		[ToggleUI]_MatcapB3ToggleAnimated("_MatcapB3Toggle", Int) = 0
		[ToggleUI]_MatcapA4smoothnessAnimated("_MatcapA4smoothness", Int) = 0
		[ToggleUI]_MatcapG2ColorAnimated("_MatcapG2Color", Int) = 0
		[ToggleUI]_ReflectionR1IntensityAnimated("_ReflectionR1Intensity", Int) = 0
		[ToggleUI]_MatcapViewDirAnimated("_MatcapViewDir", Int) = 0
		[ToggleUI]_MatcapA4BlendingAnimated("_MatcapA4Blending", Int) = 0
		[ToggleUI]_MatcapR1BlendingAnimated("_MatcapR1Blending", Int) = 0
		[ToggleUI]_MatcapB3ModeAnimated("_MatcapB3Mode", Int) = 0
		[ToggleUI]_ReflectionA4IntensityAnimated("_ReflectionA4Intensity", Int) = 0
		[ToggleUI]_ReflectionG2TintAnimated("_ReflectionG2Tint", Int) = 0
		[ToggleUI]_MatcapG2BlendingAnimated("_MatcapG2Blending", Int) = 0
		[ToggleUI]_ReflectionB3IntensityAnimated("_ReflectionB3Intensity", Int) = 0
		[ToggleUI]_MatcapG2ToggleAnimated("_MatcapG2Toggle", Int) = 0
		[ToggleUI]_ReflectionB3TintAnimated("_ReflectionB3Tint", Int) = 0
		[ToggleUI]_IgnoreNormalsMatcapAnimated("_IgnoreNormalsMatcap", Int) = 0
		[ToggleUI]_OptimizerExcludeMatcap("OptimizerExcludeMatcap", Int) = 0
		[ToggleUI]_MatcapR1ModeAnimated("_MatcapR1Mode", Int) = 0
		[ToggleUI]_MatcapR1ColorAnimated("_MatcapR1Color", Int) = 0
		[ToggleUI]_MatcapB3BlendingAnimated("_MatcapB3Blending", Int) = 0
		[ToggleUI]_MatcapR1ToggleAnimated("_MatcapR1Toggle", Int) = 0
		[ToggleUI]_MatcapA4ColorAnimated("_MatcapA4Color", Int) = 0
		_MatcapG2smoothness("MatcapG2smoothness", Range( 0 , 1)) = 1
		_MatcapA4smoothness("MatcapA4smoothness", Range( 0 , 1)) = 1
		_MatcapR1smoothness("MatcapR1smoothness", Range( 0 , 1)) = 1
		_MatcapB3smoothness("MatcapB3smoothness", Range( 0 , 1)) = 1
		_MatcapB3Blending("MatcapB3Blending", Range( 0 , 1)) = 1
		_MatcapR1Blending("MatcapR1Blending", Range( 0 , 1)) = 1
		_MatcapG2Blending("MatcapG2Blending", Range( 0 , 1)) = 1
		_MatcapA4Blending("MatcapA4Blending", Range( 0 , 1)) = 1
		_ReflectionG2Tint("ReflectionG2Tint", Range( 0 , 1)) = 0
		_ReflectionR1Tint("ReflectionR1Tint", Range( 0 , 1)) = 0
		_ReflectionB3Tint("ReflectionB3Tint", Range( 0 , 1)) = 0
		_ReflectionA4Tint("ReflectionA4Tint", Range( 0 , 1)) = 0
		_ReflectionR1Intensity("ReflectionR1Intensity", Range( 0 , 5)) = 1
		_ReflectionB3Intensity("ReflectionB3Intensity", Range( 0 , 5)) = 1
		_ReflectionG2Intensity("ReflectionG2Intensity", Range( 0 , 5)) = 1
		_ReflectionA4Intensity("ReflectionA4Intensity", Range( 0 , 5)) = 1
		_MatcapG2Color("MatcapG2Color", Color) = (1,1,1,1)
		_MatcapA4Color("MatcapA4Color", Color) = (1,1,1,1)
		_MatcapR1Color("MatcapR1Color", Color) = (1,1,1,1)
		_MatcapB3Color("MatcapB3Color", Color) = (1,1,1,1)
		_ReflectionMaskMatcap("Reflection Mask Matcap", 2D) = "white" {}
		[ToggleUI]_IgnoreNormalsMatcap("Ignore Normals Matcap", Float) = 0
		[Enum(Multiply,0,Add,1,Subtract,2)]_MatcapG2Mode("MatcapG2Mode", Float) = 0
		[Enum(Multiply,0,Add,1,Subtract,2)]_MatcapA4Mode("MatcapA4Mode", Float) = 0
		[Enum(Multiply,0,Add,1,Subtract,2)]_MatcapB3Mode("MatcapB3Mode", Float) = 0
		[Enum(Multiply,0,Add,1,Subtract,2)]_MatcapR1Mode("MatcapR1Mode", Float) = 0
		[ToggleUI]_MatcapR1Toggle("MatcapR1Toggle", Int) = 0
		[ToggleUI]_MatcapB3Toggle("MatcapB3Toggle", Int) = 0
		[ToggleUI]_MatcapG2Toggle("MatcapG2Toggle", Int) = 0
		[ToggleUI]_MatcapA4Toggle("MatcapA4Toggle", Int) = 0
		[Enum(View Dir Singularity,0,View Dir to Object Center,1)]_MatcapViewDir("MatcapViewDir", Float) = 0
		[Enum(UV0,0,UV1,1,UV2,2,UV3,3)]_ReflectionMaskMatcapUVSwitch("Reflection Mask Matcap UV Switch", Float) = 0
		[ToggleUI]_ReflectionMaskMatcapUVSwitchAnimated("_ReflectionMaskMatcapUVSwitch", Int) = 0
		[HideInInspector]_BumpMap("_BumpMap", 2D) = "white" {}
		[HideInInspector]_EmissionMap("_EmissionMap", 2D) = "white" {}
		[HideInInspector]_DetailMask("_DetailMask", 2D) = "white" {}
		[HideInInspector]_DetailNormalMap("_DetailNormalMap", 2D) = "white" {}
		[HideInInspector]_MetallicGlossMap("_MetallicGlossMap", 2D) = "white" {}
		[HideInInspector]_Color("_Color", Color) = (1,1,1,1)
		[OverrideTagToggle(IgnoreProjector)]_IgnoreProjector("IgnoreProjector", Int) = 0
		[Enum(UV0,0,UV1,1,UV2,2,UV3,3)]_EmissionScrollMaskUVSwitch("Emission Scroll Mask UV Switch", Float) = 0
		[Enum(UV0,0,UV1,1,UV2,2,UV3,3)]_EmissionUVSwitch("Emission UV Switch", Float) = 0
		[Enum(UV0,0,UV1,1,UV2,2,UV3,3)]_MainTexUVSwitch("Main Tex UV Switch", Float) = 0
		[ToggleUI]_DitherAlphaToggleAnimated("_DitherAlphaToggle", Int) = 0
		[ToggleUI]_StartDitheringFadeAnimated("_StartDitheringFade", Int) = 0
		[ToggleUI]_EndDitheringFadeAnimated("_EndDitheringFade", Int) = 0
		[ToggleUI]_DitherTextureTilingAnimated("_DitherTextureTiling", Int) = 0
		[ToggleUI]_OptimizerExcludeDither("OptimizerExcludeDither", Int) = 0
		[ToggleUI]_DitherTextureToggleAnimated("_DitherTextureToggle", Int) = 0
		[ToggleUI]_DitherMaskUVSwitchAnimated("_DitherMaskUVSwitch", Int) = 0
		[ToggleUI]_EmissionTintAnimated("_EmissionTint", Int) = 0
		[ToggleUI]_DissolveRemapMaxAnimated("_DissolveRemapMax", Int) = 0
		[ToggleUI]_DissolveRemapMinAnimated("_DissolveRemapMin", Int) = 0
		[ToggleUI]_DissolveVertexMultiplierAnimated("_DissolveVertexMultiplier", Int) = 0
		[ToggleUI]_MaterializeRAnimated("_MaterializeR", Int) = 0
		[ToggleUI]_MaterializeGAnimated("_MaterializeG", Int) = 0
		[ToggleUI]_OptimizerExcludeDissolve("OptimizerExcludeDissolve", Int) = 0
		[ToggleUI]_MaterializeLayerModeGAnimated("_MaterializeLayerModeG", Int) = 0
		[ToggleUI]_MaterializeLayerModeRAnimated("_MaterializeLayerModeR", Int) = 0
		[ToggleUI]_MaterializeLayerModeAAnimated("_MaterializeLayerModeA", Int) = 0
		[ToggleUI]_MaterializeLayerModeBAnimated("_MaterializeLayerModeB", Int) = 0
		[ToggleUI]_DissolveDensityAnimated("_DissolveDensity", Int) = 0
		[ToggleUI]_ToggleMaterializeDirInvAnimated("_ToggleMaterializeDirInv", Int) = 0
		[ToggleUI]_MaterializeAAnimated("_MaterializeA", Int) = 0
		[ToggleUI]_MaterializeColorLayerAAnimated("_MaterializeColorLayerA", Int) = 0
		[ToggleUI]_MaterializeColorLayerBAnimated("_MaterializeColorLayerB", Int) = 0
		[ToggleUI]_MaterializeColorLayerRAnimated("_MaterializeColorLayerR", Int) = 0
		[ToggleUI]_MaterializeColorLayerGAnimated("_MaterializeColorLayerG", Int) = 0
		[ToggleUI]_ToggleDissolveDirInvAnimated("_ToggleDissolveDirInv", Int) = 0
		[ToggleUI]_EmissiveDissolveColorAnimated("_EmissiveDissolveColor", Int) = 0
		[ToggleUI]_MaterializeBAnimated("_MaterializeB", Int) = 0
		[ToggleUI]_ToggleDissolveEmissionAnimated("_ToggleDissolveEmission", Int) = 0
		[ToggleUI]_MaterializeVertexColorAnimated("_MaterializeVertexColor", Int) = 0
		[ToggleUI]_ToggleDissolveVertexOffsetAnimated("_ToggleDissolveVertexOffset", Int) = 0
		[ToggleUI]_DissolveModifierAnimated("_DissolveModifier", Int) = 0
		[ToggleUI]_DissolvePatternUVSwitchAnimated("_DissolvePatternUVSwitch", Int) = 0
		[ToggleUI]_EmissionColorAnimated("_EmissionColor", Int) = 0
		[ToggleUI]_EmissionLightscaleAnimated("_EmissionLightscale", Int) = 0
		[ToggleUI]_OptimizerExcludeEmission("OptimizerExcludeEmission", Int) = 0
		[ToggleUI]_EmissionScrollToggleAnimated("_EmissionScrollToggle", Int) = 0
		[ToggleUI]_EmissionscrollColorAnimated("_EmissionscrollColor", Int) = 0
		[ToggleUI]_SSSToggleAnimated("_SSSToggle", Int) = 0
		[ToggleUI]_EmissionScrollMaskUVSwitchAnimated("_EmissionScrollMaskUVSwitch", Int) = 0
		[ToggleUI]_EmissionUVSwitchAnimated("_EmissionUVSwitch", Int) = 0
		[ToggleUI]_CutoutAnimated("_Cutout", Int) = 0
		[ToggleUI]_MainColorAnimated("_MainColor", Int) = 0
		[ToggleUI]_MainTexUVSwitchAnimated("_MainTexUVSwitch", Int) = 0
		[ToggleUI]_OptimizerExcludeMainSettings("OptimizerExcludeMainSettings", Int) = 0
		[ToggleUI]_ModeAnimated("_Mode", Int) = 0
		[ToggleUI]_ModeCustomAnimated("_ModeCustom", Int) = 0
		[ToggleUI]_SaturationAnimated("_Saturation", Int) = 0
		[ToggleUI]_SpecularToggleAnimated("_SpecularToggle", Int) = 0
		[ToggleUI]_ESCoordinatesAnimated("_ESCoordinates", Int) = 0
		[ToggleUI]_WaveformCoordinatesAnimated("_WaveformCoordinates", Int) = 0
		[ToggleUI]_WaveformRotationAnimated("_WaveformRotation", Int) = 0
		[ToggleUI]_AudioLinkSwitchAnimated("_AudioLinkSwitch", Int) = 0
		[ToggleUI]_AudioLinkColorAnimated("_AudioLinkColor", Int) = 0
		[ToggleUI]_AudioLinkBandHistoryAnimated("_AudioLinkBandHistory", Int) = 0
		[ToggleUI]_AudioLinkWaveformMirrorToggleAnimated("_AudioLinkWaveformMirrorToggle", Int) = 0
		[ToggleUI]_WaveformThicknessAnimated("_WaveformThickness", Int) = 0
		[ToggleUI]_ESRenderMethodAnimated("_ESRenderMethod", Int) = 0
		[ToggleUI]_AudioBandIntensityAnimated("_AudioBandIntensity", Int) = 0
		[ToggleUI]_EmissionscrollTintAnimated("_EmissionscrollTint", Int) = 0
		[ToggleUI]_ESSharpnessAnimated("_ESSharpness", Int) = 0
		[ToggleUI]_ESVoronoiScaleAnimated("_ESVoronoiScale", Int) = 0
		[ToggleUI]_ESSizeAnimated("_ESSize", Int) = 0
		[ToggleUI]_IgnoreNormalsESv2Animated("_IgnoreNormalsESv2", Int) = 0
		_AudioBandIntensity("AudioBandIntensity", Vector) = (1,0.25,0.25,0.25)
		[ToggleUI]_ESLevelOffsetAnimated("_ESLevelOffset", Int) = 0
		[ToggleUI]_ESSpeedAnimated("_ESSpeed", Int) = 0
		[ToggleUI]_ESScrollOffsetAnimated("_ESScrollOffset", Int) = 0
		[ToggleUI]_AudioHueSpeedAnimated("_AudioHueSpeed", Int) = 0
		[ToggleUI]_ESVoronoiSpeedAnimated("_ESVoronoiSpeed", Int) = 0
		[ToggleUI]_OptimizerExcludeESV2("OptimizerExcludeESV2", Int) = 0
		_AudioHueSpeed("Audio Hue Speed", Range( 0 , 1)) = 0.05
		_ESScrollOffset("ES Scroll Offset", Range( 0 , 1)) = 0
		[Enum(Vertex Normal based,0,Fresnel Camera based,1,Vertex Pos to World,2,Voronoi,3,Vertex UV based,4)]_ESRenderMethod("ES Render Method", Float) = 2
		[Enum(Off,0,On,1)]_IgnoreNormalsESv2("Ignore Normals ESv2", Float) = 0
		_ESSize("ES Size", Range( 0 , 1)) = 1
		_ESSpeed("ES Speed", Range( -10 , 10)) = 0.5
		_ESLevelOffset("ES Level Offset", Range( -1 , 1)) = 0
		_ESCoordinates("ES Coordinates", Vector) = (0,2,0,0)
		_ESSharpness("ES Sharpness", Range( 0 , 1)) = 0
		_EmissionscrollTint("Emissionscroll Tint", Range( 0 , 1)) = 1
		_ESVoronoiScale("ES Voronoi Scale", Float) = 10
		_ESVoronoiSpeed("ES Voronoi Speed", Range( -10 , 10)) = 1
		_WaveformRotation("Waveform Rotation", Range( -1 , 1)) = 0
		_WaveformCoordinates("Waveform Coordinates", Vector) = (1,1,0,0)
		[Enum(Off,0,Bands,1,Waveform,2,Bands plus Waveform,3)]_AudioLinkSwitch("AudioLink Switch", Float) = 1
		[HDR]_AudioLinkColor("AudioLink Color", Color) = (1,1,1,1)
		_AudioLinkBandHistory("AudioLink Band History", Range( 32 , 128)) = 80
		[Enum(Single,0,Mirrored,1,Mirror filled,2)]_AudioLinkWaveformMirrorToggle("AudioLink Waveform Mirror Toggle", Float) = 0
		_WaveformThickness("Waveform Thickness", Range( 0 , 1)) = 0.1
		[HelpBox(4)]_AudioLinkTooltip("AudioLinkTooltip", Float) = 0
		[ToggleUI]_COLORCOLORAnimated("_COLORCOLOR", Int) = 0
		[ToggleUI]_COLORADDSUBDIFFAnimated("_COLORADDSUBDIFF", Int) = 0
		[ToggleUI]_NoiseSpeedAnimated("_NoiseSpeed", Int) = 0
		[ToggleUI]_OptimizerExcludeESV1("OptimizerExcludeESV1", Int) = 0
		[ToggleUI]_NoiseVectorXYAnimated("_NoiseVectorXY", Int) = 0
		[ToggleUI]_VectorXYAnimated("_VectorXY", Int) = 0
		[ToggleUI]_EmiossionscrollspeedAnimated("_Emiossionscrollspeed", Int) = 0
		[Enum(UV0,0,UV1,1,UV2,2,UV3,3)]_EmissionscrollUVSwitch("Emission scroll UV Switch", Float) = 0
		[Enum(UV0,0,UV1,1,UV2,2,UV3,3)]_NoiseTextureUVSwitch("Noise Texture UV Switch", Float) = 0
		_NoiseVectorXY("Noise Vector X,Y", Vector) = (0,1,0,0)
		_NoiseSpeed("Noise Speed", Range( -2 , 2)) = 0.1
		_VectorXY("Vector X,Y", Vector) = (0,1,0,0)
		_Emiossionscrollspeed("Emiossion scroll speed", Range( -2 , 2)) = 0
		[ToggleUI]_EmissionscrollUVSwitchAnimated("_EmissionscrollUVSwitch", Int) = 0
		[ToggleUI]_NoiseTextureUVSwitchAnimated("_NoiseTextureUVSwitch", Int) = 0
		_NoiseTexture("Noise Texture", 2D) = "white" {}
		_Emissionscroll("Emission scroll", 2D) = "white" {}
		_OcclusionMap("Occlusion Map", 2D) = "white" {}
		_ShadowMask("Shadow Mask", 2D) = "white" {}
		[ToggleUI]_ColoringDirectEnvLightsAnimated("_ColoringDirectEnvLights", Int) = 0
		[ToggleUI]_ToggleStepsAnimated("_ToggleSteps", Int) = 0
		[ToggleUI]_PointSpotShadowIntensityAnimated("_PointSpotShadowIntensity", Int) = 0
		[ToggleUI]_AmbientBoostAnimated("_AmbientBoost", Int) = 0
		[ToggleUI]_ShadowMaskStrengthAnimated("_ShadowMaskStrength", Int) = 0
		[ToggleUI]_OptimizerExcludeLighting("OptimizerExcludeLighting", Int) = 0
		[ToggleUI]_RampOffsetAnimated("_RampOffset", Int) = 0
		[ToggleUI]_IndirectShadowIntensityAnimated("_IndirectShadowIntensity", Int) = 0
		[ToggleUI]_DirectShadowIntensityAnimated("_Direct Shadow Intensity", Int) = 0
		[ToggleUI]_NdLHalfingControlAnimated("_NdLHalfingControl", Int) = 0
		[ToggleUI]_ShadowMaskinvertAnimated("_ShadowMaskinvert", Int) = 0
		[ToggleUI]_ShadowColorMapStrengthAnimated("_ShadowColorMapStrength", Int) = 0
		[ToggleUI]_ToggleMonochromePixelLightAnimated("_ToggleMonochromePixelLight", Int) = 0
		[ToggleUI]_ExperimentalToggleAnimated("_ExperimentalToggle", Int) = 0
		[ToggleUI]_SelfCastShadowsAnimated("_SelfCastShadows", Int) = 0
		[ToggleUI]_ColoringPointLightsAnimated("_ColoringPointLights", Int) = 0
		[ToggleUI]_MaxLightDirectAnimated("_MaxLightDirect", Int) = 0
		[ToggleUI]_ToggleMonochromeEnvAnimated("_ToggleMonochromeEnv", Int) = 0
		[ToggleUI]_StepsAnimated("_Steps", Int) = 0
		[ToggleUI]_RampColorAnimated("_RampColor", Int) = 0
		_ShadowColorMap("Shadow Color Map", 2D) = "black" {}
		_ShadowColorMapStrength("Shadow Color Map Strength", Range( 0 , 1)) = 1
		_AmbientBoost("Ambient Boost", Range( 1 , 2)) = 1.5
		_RampOffset("RampOffset", Range( 0 , 1)) = 0.5
		[ToggleUI]_ShadowMaskinvert("Shadow Mask invert", Float) = 0
		[Enum(Off,0,On,1)]_ToggleSteps("Toggle Steps", Float) = 0
		[Enum(Off,0,On,1)]_ExperimentalToggle("Experimental Toggle", Float) = 1
		_ColoringDirectEnvLights("Coloring Direct Env Lights", Range( 0 , 1)) = 0
		_ColoringPointLights("Coloring Point Lights", Range( 0 , 1)) = 0
		_Steps("Steps", Int) = 3
		_DirectShadowIntensity("Direct Shadow Intensity", Range( 0 , 1)) = 1
		_RampColor("Ramp Color", Color) = (0.8588235,0.7647059,0.7098039,0)
		_Occlusion("Occlusion", Range( 0 , 1)) = 1
		_MaxLightDirect("Max Light Direct", Range( 0 , 1)) = 1
		_IndirectShadowIntensity("Indirect Shadow Intensity", Range( 0 , 1)) = 0.5
		_SelfCastShadows("SelfCastShadows", Range( 0 , 1)) = 1
		_PointSpotShadowIntensity("PointSpot Shadow Intensity", Range( 0 , 1)) = 1
		_ToonRamp("Toon Ramp", 2D) = "white" {}
		_ShadowMaskStrength("Shadow Mask Strength", Range( 0 , 1)) = 1
		_NdLHalfingControl("NdL Halfing Control", Range( 0.5 , 5)) = 0.5
		[ToggleUI]_ToggleMonochromeEnv("Toggle Monochrome Env", Float) = 0
		[ToggleUI]_ToggleMonochromePixelLight("Toggle Monochrome Pixel Light", Float) = 0
		_Cutout("Cutout", Range( 0 , 1)) = 0.5
		[HDR]_Cubemap("Cubemap", CUBE) = "white" {}
		_GSAAThreshold("GSAAThreshold", Range( 0 , 1)) = 0.1
		_GSAAVariance("GSAAVariance", Range( 0 , 1)) = 0.15
		[ToggleUI]_EnableGSAAAnimated("_EnableGSAA", Int) = 0
		[ToggleUI]_GSAAVarianceAnimated("_GSAAVariance", Int) = 0
		[ToggleUI]_CubemapsmoothnessAnimated("_Cubemapsmoothness", Int) = 0
		[ToggleUI]_CubemapIntensityAnimated("_CubemapIntensity", Int) = 0
		[ToggleUI]_MetallicAnimated("_Metallic", Int) = 0
		[ToggleUI]_CubemapSpecularToggleAnimated("_CubemapSpecularToggle", Int) = 0
		[ToggleUI]_WorkflowSwitchAnimated("_WorkflowSwitch", Int) = 0
		[ToggleUI]_OptimizerExcludeCubemap("OptimizerExcludeCubemap", Int) = 0
		[ToggleUI]_IgnoreNormalsCubemapAnimated("_IgnoreNormalsCubemap", Int) = 0
		[ToggleUI]_GSAAThresholdAnimated("_GSAAThreshold", Int) = 0
		_MetallicMap("Metallic Map", 2D) = "white" {}
		[Enum(UV0,0,UV1,1,UV2,2,UV3,3)]_ReflectionMaskUVSwitch("Reflection Mask UV Switch", Float) = 0
		_Cubemapsmoothness("Cubemap smoothness", Range( 0 , 1)) = 0.75
		[ToggleUI]_CubemapSpecularToggle("Cubemap Specular Toggle", Float) = 1
		[Enum(Dynamic,0,Baked Cubemap only,1,Unitys Metallic Workflow,2)]_WorkflowSwitch("Workflow Switch", Float) = 0
		_ReflectionMask("Reflection Mask", 2D) = "white" {}
		[ToggleUI]_IgnoreNormalsCubemap("Ignore Normals Cubemap", Float) = 0
		_CubemapIntensity("Cubemap Intensity", Range( 0 , 1)) = 1
		_Metallic("Metallic", Range( 0 , 1)) = 0.5
		[ToggleUI]_ReflectionMaskUVSwitchAnimated("_ReflectionMaskUVSwitch", Int) = 0
		[ToggleUI]_EnableGSAA("Enable GSAA", Float) = 1
		[ToggleUI]_HueMaskinverterAnimated("_HueMaskinverter", Int) = 0
		[ToggleUI]_HueShiftblendAnimated("_HueShiftblend", Int) = 0
		[ToggleUI]_OptimizerExcludeHueShift("OptimizerExcludeHueShift", Int) = 0
		[ToggleUI]_HueShiftRandomizerAnimated("_HueShiftRandomizer", Int) = 0
		[ToggleUI]_HueShiftSpeedAnimated("_HueShiftSpeed", Int) = 0
		[ToggleUI]_ToggleHueTexforSpeedAnimated("_ToggleHueTexforSpeed", Int) = 0
		[Enum(UV0,0,UV1,1,UV2,2,UV3,3)]_HueMaskUVSwitch("HueMask UV Switch", Float) = 0
		[ToggleUI]_HueMaskUVSwitchAnimated("_HueMaskUVSwitch", Int) = 0
		[Enum(Off,0,On,1)]_ToggleHueTexforSpeed("Toggle Hue Tex for Speed", Float) = 0
		_HueShiftSpeed("Hue Shift Speed", Range( 0 , 255)) = 0
		[Enum(Not Inverted,0,Inverted,1)]_HueMaskinverter("Hue Mask inverter", Int) = 0
		_HueShiftblend("Hue Shift blend", Range( 0 , 1)) = 0.5
		_HueMask("HueMask", 2D) = "white" {}
		_HueShiftRandomizer("Hue Shift Randomizer", Range( 0 , 1)) = 0
		[Normal]_NormalMap("Normal Map", 2D) = "bump" {}
		[Normal]_SecondaryNormal("Secondary Normal", 2D) = "bump" {}
		_SecondaryNormalMask("Secondary Normal Mask", 2D) = "white" {}
		[ToggleUI]_OptimizerExcludeNormals("OptimizerExcludeNormals", Int) = 0
		[ToggleUI]_NormalScaleAnimated("_NormalScale", Int) = 0
		[ToggleUI]_SecondaryNormalScaleAnimated("_SecondaryNormalScale", Int) = 0
		_NormalScale("Normal Scale", Range( -10 , 10)) = 0
		_SecondaryNormalScale("Secondary Normal Scale", Range( -10 , 10)) = 0
		[Enum(UV0,0,UV1,1,UV2,2,UV3,3)]_NormalMapUVSwitch("Normal Map UV Switch", Float) = 0
		[Enum(UV0,0,UV1,1,UV2,2,UV3,3)]_SecondaryNormalUVSwitch("Secondary Normal UV Switch", Float) = 0
		[Enum(UV0,0,UV1,1,UV2,2,UV3,3)]_SecondaryNormalMaskUVSwitch("Secondary Normal Mask UV Switch", Float) = 0
		[ToggleUI]_SecondaryNormalUVSwitchAnimated("_SecondaryNormalUVSwitch", Int) = 0
		[ToggleUI]_NormalMapUVSwitchAnimated("_NormalMapUVSwitch", Int) = 0
		[ToggleUI]_SecondaryNormalMaskUVSwitchAnimated("_SecondaryNormalMaskUVSwitch", Int) = 0
		_RimMask("Rim Mask", 2D) = "white" {}
		[ToggleUI]_RimSpecToggleAnimated("_RimSpecToggle", Int) = 0
		[ToggleUI]_RimToggleAnimated("_RimToggle", Int) = 0
		[ToggleUI]_OptimizerExcludeRimlight("OptimizerExcludeRimlight", Int) = 0
		[ToggleUI]_RimOffsetAnimated("_RimOffset", Int) = 0
		[ToggleUI]_RimTintAnimated("_RimTint", Int) = 0
		[ToggleUI]_RimColorAnimated("_RimColor", Int) = 0
		[ToggleUI]_RimOpacityAnimated("_RimOpacity", Int) = 0
		[ToggleUI]_RimFresnelBiasAnimated("_RimFresnelBias", Int) = 0
		[ToggleUI]_RimSpecLightsmoothnessAnimated("_RimSpecLightsmoothness", Int) = 0
		[ToggleUI]_RimDirectionToggleAnimated("_RimDirectionToggle", Int) = 0
		[ToggleUI]_RimFresnelScaleAnimated("_RimFresnelScale", Int) = 0
		[HDR]_RimColor("Rim Color", Color) = (1,1,1,0)
		[ToggleUI]_RimPowerAnimated("_RimPower", Int) = 0
		[ToggleUI]_RimFaceCullingAnimated("_RimFaceCulling", Int) = 0
		[ToggleUI]_RimLightMaskinvAnimated("_RimLightMaskinv", Int) = 0
		[ToggleUI]_RimFresnelPowerAnimated("_RimFresnelPower", Int) = 0
		_RimFresnelPower("Rim Fresnel Power", Range( 0 , 20)) = 5
		_RimOpacity("Rim Opacity", Range( 0 , 1)) = 0.25
		_RimFresnelScale("Rim Fresnel Scale", Range( 0 , 10)) = 1
		_RimFresnelBias("Rim Fresnel Bias", Range( 0 , 1)) = 0
		_RimPower("Rim Power", Range( 0 , 10)) = 5
		_RimTint("Rim Tint", Range( 0 , 1)) = 0.75
		_RimOffset("Rim Offset", Range( 0 , 1)) = 0
		_RimSpecLightsmoothness("Rim Spec Light smoothness", Range( 0 , 1)) = 0
		[Enum(UV0,0,UV1,1,UV2,2,UV3,3)]_RimMaskUVSwitch("Rim Mask UV Switch", Float) = 0
		[Enum(Off,0,On,1)]_RimSpecToggle("Rim Spec Toggle", Float) = 0
		[Enum(Not Inverted,0,Inverted,1)]_RimLightMaskinv("RimLightMaskinv", Float) = 0
		[Enum(Light Direction Based,0,Fresnel,1)]_RimDirectionToggle("Rim Direction Toggle", Float) = 0
		[ToggleUI]_RimMaskUVSwitchAnimated("_RimMaskUVSwitch", Int) = 0
		[Enum(Off,0,Front,1,Back,2)]_RimFaceCulling("Rim Face Culling", Float) = 0
		[Enum(Color Data,0,Modification Data,1)]_SSSMapMode("SSS Map Mode", Float) = 0
		_SSSThicknessMap("SSS Thickness Map", 2D) = "white" {}
		[ToggleUI]_SSSSettingAnimated("_SSSSetting", Int) = 0
		[ToggleUI]_SSSScaleAnimated("_SSSScale", Int) = 0
		[ToggleUI]_SSSPowerAnimated("_SSSPower", Int) = 0
		[ToggleUI]_SSSMapModeAnimated("_SSSMapMode", Int) = 0
		[ToggleUI]_SSSTintAnimated("_SSSTint", Int) = 0
		[ToggleUI]_OptimizerExcludeSSS("OptimizerExcludeSSS", Int) = 0
		[ToggleUI]_SubsurfaceDistortionModifierAnimated("_SubsurfaceDistortionModifier", Int) = 0
		[ToggleUI]_SSSColorAnimated("_SSSColor", Int) = 0
		[ToggleUI]_SSSThicknessinvAnimated("_SSSThicknessinv", Int) = 0
		[HDR]_SSSColor("SSS Color", Color) = (0.9997016,1,0.7028302,0)
		_SubsurfaceDistortionModifier("Subsurface Distortion Modifier", Float) = 1
		[Enum(UV0,0,UV1,1,UV2,2,UV3,3)]_SSSThicknessMapUVSwitch("SSS Thickness Map UV Switch", Float) = 0
		_SSSPower("SSS Power", Float) = 2.5
		_SSSTint("SSS Tint", Range( 0 , 1)) = 1
		[ToggleUI]_SSSThicknessinv("SSSThicknessinv", Float) = 0
		_SSSScale("SSS Scale", Float) = 1
		[ToggleUI]_SSSThicknessMapUVSwitchAnimated("_SSSThicknessMapUVSwitch", Int) = 0
		_BlinntoAniso("Blinn to Aniso", Range( 0 , 1)) = 0
		_AnisoF0Reflectance("Aniso F0 Reflectance", Range( 0 , 1)) = 0
		_AnisoDir("AnisoDir", 2D) = "bump" {}
		_SpecularMap("Specular Map", 2D) = "white" {}
		[ToggleUI]_HighlightSmoothnessAnimated("_HighlightSmoothness", Int) = 0
		[ToggleUI]_AnisotropyAnimated("_Anisotropy", Int) = 0
		[ToggleUI]_AnisoF0ReflectanceAnimated("_AnisoF0Reflectance", Int) = 0
		[ToggleUI]_AnisoFlickerFixAnimated("_AnisoFlickerFix", Int) = 0
		[ToggleUI]_HighlightOffsetAnimated("_HighlightOffset", Int) = 0
		[ToggleUI]_AnisoSharpeningAnimated("_AnisoSharpening", Int) = 0
		[ToggleUI]_OptimizerExcludeSpecularHighlights("OptimizerExcludeSpecularHighlights", Int) = 0
		[ToggleUI]_BlinntoAnisoAnimated("_BlinntoAniso", Int) = 0
		[ToggleUI]_SpecularColorAnimated("_SpecularColor", Int) = 0
		[ToggleUI]_SpecShadowMaskVarAnimated("_SpecShadowMaskVar", Int) = 0
		[ToggleUI]_SpecularSettingAnimated("_SpecularSetting", Int) = 0
		[ToggleUI]_SpecShadowMaskPowerAnimated("_SpecShadowMaskPower", Int) = 0
		[ToggleUI]_AnisoScaleAnimated("_AnisoScale", Int) = 0
		[ToggleUI]_SpecularTintAnimated("_SpecularTint", Int) = 0
		_Anisotropy("Anisotropy", Range( -1 , 1)) = 0.8
		_HighlightOffset("Highlight Offset", Range( -1 , 1)) = 0
		_HighlightSmoothness("Highlight Smoothness", Range( 0 , 1)) = 0
		[HDR]_SpecularColor("Specular Color", Color) = (1,1,1,1)
		_SpecularTint("Specular Tint", Range( 0 , 1)) = 1
		[Enum(UV0,0,UV1,1,UV2,2,UV3,3)]_SpecularMapUVSwitch("Specular Map UV Switch", Float) = 0
		[Enum(UV0,0,UV1,1,UV2,2,UV3,3)]_AnisoDirUVSwitch("AnisoDir UV Switch", Float) = 0
		[Enum(Off,0,Standard NdotL,1,Toon Ramp,2)]_SpecShadowMaskVar("Spec Shadow Mask Var", Float) = 2
		_AnisoScale("Aniso Scale", Range( 0 , 1)) = 1
		[ToggleUI]_AnisoFlickerFix("Aniso Flicker Fix", Float) = 0
		[ToggleUI]_AnisoSharpening("Aniso Sharpening", Float) = 0
		_SpecShadowMaskPower("Spec Shadow Mask Power", Range( -1 , 1)) = 0
		[ToggleUI]_SpecularMapUVSwitchAnimated("_SpecularMapUVSwitch", Int) = 0
		[ToggleUI]_AnisoDirUVSwitchAnimated("_AnisoDirUVSwitch", Int) = 0
		[ToggleUI]_RimHueSpeedAnimated("_RimHueSpeed", Int) = 0
		[ToggleUI]_ShadowRimSharpnessAnimated("_ShadowRimSharpness", Int) = 0
		[ToggleUI]_ShadowRimRangeAnimated("_ShadowRimRange", Int) = 0
		[ToggleUI]_ShadowRimOpacityAnimated("_ShadowRimOpacity", Int) = 0
		[ToggleUI]_OptimizerExcludeShadowRim("OptimizerExcludeShadowRim", Int) = 0
		[ToggleUI]_EmissiveRimColorAnimated("_EmissiveRimColor", Int) = 0
		[ToggleUI]_RimSwitchAnimated("_RimSwitch", Int) = 0
		_ShadowRimRange("ShadowRimRange", Range( 0 , 1)) = 0.75
		_ShadowRimSharpness("ShadowRimSharpness", Range( 0 , 1)) = 1
		_ShadowRimOpacity("ShadowRimOpacity", Range( 0 , 1)) = 0
		_RimHueSpeed("Rim Hue Speed", Range( 0 , 1)) = 0
		_EmissiveRimColor("Emissive Rim Color", Color) = (1,1,1,0)
		[Enum(UnityEngine.Rendering.BlendOp)]_BlendOpAlpha("Blend Op Alpha", Float) = 0
		[Enum(UnityEngine.Rendering.BlendMode)]_DestinationBlendAlpha("Destination Blend Alpha", Float) = 0
		[Enum(UnityEngine.Rendering.BlendOp)]_BlendOpRGB("Blend Op RGB", Float) = 0
		[Enum(UnityEngine.Rendering.BlendMode)]_DestinationBlendRGB("Destination Blend RGB", Float) = 0
		[HideInInspector][Enum(UnityEngine.Rendering.BlendMode)]_DstBlend("_DstBlend", Float) = 0
		[HideInInspector]_DetailNormalMapScale("_DetailNormalMapScale", Float) = 0
		[HideInInspector]_OcclusionStrength("_OcclusionStrength", Float) = 0
		[HideInInspector]_GlossMapScale("_GlossMapScale", Float) = 0
		[HideInInspector]_BumpScale("_BumpScale", Float) = 0
		[HideInInspector]_GlossyReflections("_GlossyReflections", Float) = 0
		[HideInInspector]_SpecularHighlights("_SpecularHighlights", Float) = 0
		[HideInInspector]_Cutoff("_Cutoff", Float) = 0
		[HideInInspector]_Glossiness("_Glossiness", Float) = 0
		[HideInInspector][Enum(UnityEngine.Rendering.BlendMode)]_SrcBlend("_SrcBlend", Float) = 0
		_StencilBufferWriteMask("Stencil Buffer Write Mask", Range( 0 , 255)) = 255
		_StencilBufferReadMask("Stencil Buffer Read Mask", Range( 0 , 255)) = 255
		_DepthOffsetFactor("Depth Offset Factor", Float) = 0
		_DepthOffsetUnits("Depth Offset Units", Float) = 0
		_StencilBufferReference("Stencil Buffer Reference", Range( 0 , 255)) = 0
		[Enum(UnityEngine.Rendering.StencilOp)]_StencilBufferFailFront("Stencil Buffer Fail Front", Float) = 0
		[Enum(UnityEngine.Rendering.StencilOp)]_StencilBufferZFailFront("Stencil Buffer ZFail Front", Float) = 0
		[Enum(UnityEngine.Rendering.CompareFunction)]_StencilBufferComparison("Stencil Buffer Comparison", Float) = 0
		[Enum(UnityEngine.Rendering.CompareFunction)]_ZTestMode("ZTest Mode", Float) = 4
		[Enum(UnityEngine.Rendering.StencilOp)]_StencilBufferPassFront("Stencil Buffer Pass Front", Float) = 0
		[Enum(UnityEngine.Rendering.BlendMode)]_SourceBlendAlpha("Source Blend Alpha", Float) = 1
		[Enum(UnityEngine.Rendering.BlendMode)]_SourceBlendRGB("Source Blend RGB", Float) = 1
		[Enum(UnityEngine.Rendering.CullMode)]_CullMode("Cull Mode", Float) = 2
		_Saturation("Saturation", Range( 0 , 10)) = 1
		_MainColor("Main Color", Color) = (1,1,1,1)
		_MainTex("Main Tex", 2D) = "white" {}
		[Enum(Basic,0,Advanced,1,Advanced Plus,2)]_AdvancedExperimentalToggle("Advanced Experimental Toggle", Float) = 0
		[HDR]_EmissionColor("Emission Color", Color) = (1,1,1,0)
		_Emission("Emission", 2D) = "black" {}
		[HDR]_EmissionscrollColor("Emission scroll Color", Color) = (1,1,1,1)
		_EmissionScrollMask("Emission Scroll Mask", 2D) = "white" {}
		_EmissionTint("Emission Tint", Range( 0 , 1)) = 1
		[ToggleUI]_COLORCOLOR("Toggle Advanced", Float) = 0
		[Enum(Light Based,0,Color Based,1,Mixed,2)]_SSSSetting("SSS Setting", Float) = 0
		[ToggleUI]_FlipbookToggle("Flipbook Toggle", Float) = 0
		[ToggleUI]_COLORADDSUBDIFF("Cubemap Toggle", Float) = 0
		[ToggleUI]_SSSToggle("SSS Toggle", Float) = 0
		[ToggleUI]_SpecularToggle("Specular Toggle", Float) = 0
		[Enum(Toon,0,Unity Standard GGX,1,Anisotropic by James OHare,2,Anisotropic GGX,3)]_SpecularSetting("Specular Setting", Float) = 0
		[ToggleUI]_RimToggle("Rim Toggle", Float) = 0
		[Enum(Off,0,ES v1,1,ES v2,2)]_EmissionScrollToggle("Emission Scroll Toggle", Float) = 0
		[Enum(Shadow,0,Emissive,1)]_RimSwitch("Rim Switch", Float) = 0
		[HideInInspector][Enum(Off,0,On,1)]_ZWrite("_ZWrite", Float) = 1
		[Enum(Off,0,On,1)]_ZWriteMode("ZWrite Mode", Float) = 1
		[Enum(Off,0,On,1)]_AlphatoCoverage("Alpha to Coverage", Float) = 0
		[Enum(UnityEngine.Rendering.ColorWriteMask)]_ColorMask("Color Mask", Float) = 15
		_MaskClipValue("Mask Clip Value", Range( 0 , 1)) = 0.5
		[Enum(Opaque,0,Cutout,1,Fade,2,Transparent,3)]_Mode("Mode", Float) = 0
		[ToggleUI]_ModeCustom("Mode Custom", Float) = 0
		[ToggleUI]_EmissionLightscale("Emission Lightscale", Float) = 0
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  "IgnoreProjectorPlaceholder"="True" }
		Cull [_CullMode]
		ZWrite [_ZWriteMode]
		ZTest [_ZTestMode]
		Offset  [_DepthOffsetFactor] , [_DepthOffsetUnits]
		Stencil
		{
			Ref [_StencilBufferReference]
			ReadMask [_StencilBufferReadMask]
			WriteMask [_StencilBufferWriteMask]
			Comp [_StencilBufferComparison]
			Pass [_StencilBufferPassFront]
			Fail [_StencilBufferFailFront]
			ZFail [_StencilBufferZFailFront]
		}
		Blend [_SourceBlendRGB] [_DestinationBlendRGB] , [_SourceBlendAlpha] [_DestinationBlendAlpha]
		BlendOp [_BlendOpRGB] , [_BlendOpAlpha]
		AlphaToMask [_AlphatoCoverage]
		ColorMask [_ColorMask]
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityStandardUtils.cginc"
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#pragma target 5.0
		#pragma multi_compile _ VERTEXLIGHT_ON
		#include "./cginc/AudioLink.cginc"
		#define ASE_USING_SAMPLING_MACROS 1
		#if defined(SHADER_API_D3D11) || defined(SHADER_API_XBOXONE) || defined(UNITY_COMPILER_HLSLCC) || defined(SHADER_API_PSSL) || (defined(SHADER_TARGET_SURFACE_ANALYSIS) && !defined(SHADER_TARGET_SURFACE_ANALYSIS_MOJOSHADER))//ASE Sampler Macros
		#define SAMPLE_TEXTURE2D(tex,samplerTex,coord) tex.Sample(samplerTex,coord)
		#define SAMPLE_TEXTURE2D_LOD(tex,samplerTex,coord,lod) tex.SampleLevel(samplerTex,coord, lod)
		#define SAMPLE_TEXTURE2D_BIAS(tex,samplerTex,coord,bias) tex.SampleBias(samplerTex,coord,bias)
		#define SAMPLE_TEXTURE2D_GRAD(tex,samplerTex,coord,ddx,ddy) tex.SampleGrad(samplerTex,coord,ddx,ddy)
		#define SAMPLE_TEXTURECUBE_LOD(tex,samplerTex,coord,lod) tex.SampleLevel(samplerTex,coord, lod)
		#else//ASE Sampling Macros
		#define SAMPLE_TEXTURE2D(tex,samplerTex,coord) tex2D(tex,coord)
		#define SAMPLE_TEXTURE2D_LOD(tex,samplerTex,coord,lod) tex2Dlod(tex,float4(coord,0,lod))
		#define SAMPLE_TEXTURE2D_BIAS(tex,samplerTex,coord,bias) tex2Dbias(tex,float4(coord,0,bias))
		#define SAMPLE_TEXTURE2D_GRAD(tex,samplerTex,coord,ddx,ddy) tex2Dgrad(tex,coord,ddx,ddy)
		#define SAMPLE_TEXTURECUBE_LOD(tex,samplertex,coord,lod) texCUBElod (tex,half4(coord,lod))
		#endif//ASE Sampling Macros

		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float2 uv2_texcoord2;
			float2 uv3_texcoord3;
			float2 uv4_texcoord4;
			half ASEVFace : VFACE;
			float3 worldNormal;
			INTERNAL_DATA
			float3 worldPos;
			float4 vertexColor : COLOR;
			float3 vertexToFrag2250_g22168;
			float3 vertexToFrag2251_g22168;
			float3 worldRefl;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform int _SecondaryNormalUVSwitchAnimated;
		uniform int _OptimizerExcludeNormals;
		uniform int _NormalMapUVSwitchAnimated;
		uniform int _SecondaryNormalMaskUVSwitchAnimated;
		uniform int _NormalScaleAnimated;
		uniform int _SecondaryNormalScaleAnimated;
		uniform int _HueMaskinverterAnimated;
		uniform int _HueShiftblendAnimated;
		uniform int _OptimizerExcludeHueShift;
		uniform int _HueShiftRandomizerAnimated;
		uniform int _HueShiftSpeedAnimated;
		uniform int _ToggleHueTexforSpeedAnimated;
		uniform int _HueMaskUVSwitchAnimated;
		uniform int _ToggleMonochromeEnvAnimated;
		uniform int _MaxLightDirectAnimated;
		uniform int _ColoringPointLightsAnimated;
		uniform int _SelfCastShadowsAnimated;
		uniform int _ToggleMonochromePixelLightAnimated;
		uniform int _StepsAnimated;
		uniform int _ShadowColorMapStrengthAnimated;
		uniform int _NdLHalfingControlAnimated;
		uniform int _DirectShadowIntensityAnimated;
		uniform int _IndirectShadowIntensityAnimated;
		uniform int _RampOffsetAnimated;
		uniform int _ShadowMaskStrengthAnimated;
		uniform int _ColoringDirectEnvLightsAnimated;
		uniform int _AmbientBoostAnimated;
		uniform int _PointSpotShadowIntensityAnimated;
		uniform int _ToggleStepsAnimated;
		uniform int _ShadowMaskinvertAnimated;
		uniform int _RampColorAnimated;
		uniform int _OptimizerExcludeLighting;
		uniform int _ExperimentalToggleAnimated;
		uniform int _RimHueSpeedAnimated;
		uniform int _ShadowRimSharpnessAnimated;
		uniform int _ShadowRimRangeAnimated;
		uniform int _ShadowRimOpacityAnimated;
		uniform int _OptimizerExcludeShadowRim;
		uniform int _EmissiveRimColorAnimated;
		uniform int _RimSwitchAnimated;
		uniform int _HighlightOffsetAnimated;
		uniform int _SpecShadowMaskPowerAnimated;
		uniform int _SpecularSettingAnimated;
		uniform int _AnisoSharpeningAnimated;
		uniform int _SpecShadowMaskVarAnimated;
		uniform int _SpecularColorAnimated;
		uniform int _OptimizerExcludeSpecularHighlights;
		uniform int _HighlightSmoothnessAnimated;
		uniform int _SpecularTintAnimated;
		uniform float _Anisotropy;
		uniform int _AnisotropyAnimated;
		uniform int _AnisoDirUVSwitchAnimated;
		uniform float _AnisoFlickerFix;
		uniform int _AnisoFlickerFixAnimated;
		uniform float _AnisoF0Reflectance;
		uniform int _SpecularMapUVSwitchAnimated;
		uniform int _AnisoF0ReflectanceAnimated;
		uniform int _BlinntoAnisoAnimated;
		uniform int _AnisoScaleAnimated;
		uniform int _OptimizerExcludeCubemap;
		uniform int _WorkflowSwitchAnimated;
		uniform int _CubemapSpecularToggleAnimated;
		uniform int _IgnoreNormalsCubemapAnimated;
		uniform int _CubemapIntensityAnimated;
		uniform int _CubemapsmoothnessAnimated;
		uniform int _MetallicAnimated;
		uniform float _EnableGSAA;
		uniform int _EnableGSAAAnimated;
		uniform float _GSAAVariance;
		uniform float _GSAAThreshold;
		uniform int _ReflectionMaskUVSwitchAnimated;
		uniform int _GSAAVarianceAnimated;
		uniform int _GSAAThresholdAnimated;
		uniform int _IgnoreNormalsMatcapAnimated;
		uniform int _MatcapG2ToggleAnimated;
		uniform int _MatcapG2BlendingAnimated;
		uniform int _MatcapA4ModeAnimated;
		uniform int _ReflectionB3IntensityAnimated;
		uniform int _MatcapR1ColorAnimated;
		uniform int _MatcapB3BlendingAnimated;
		uniform int _MatcapR1BlendingAnimated;
		uniform int _MatcapB3ToggleAnimated;
		uniform int _ReflectionA4IntensityAnimated;
		uniform int _MatcapR1ToggleAnimated;
		uniform int _MatcapR1smoothnessAnimated;
		uniform int _ReflectionMaskMatcapUVSwitchAnimated;
		uniform int _MatcapB3ModeAnimated;
		uniform int _MatcapA4ColorAnimated;
		uniform int _MatcapA4smoothnessAnimated;
		uniform int _ReflectionR1IntensityAnimated;
		uniform int _OptimizerExcludeMatcap;
		uniform int _ReflectionA4TintAnimated;
		uniform int _MatcapB3smoothnessAnimated;
		uniform int _MatcapA4BlendingAnimated;
		uniform int _MatcapA4ToggleAnimated;
		uniform int _ReflectionB3TintAnimated;
		uniform int _ReflectionR1TintAnimated;
		uniform int _ReflectionG2IntensityAnimated;
		uniform int _MatcapR1ModeAnimated;
		uniform int _ReflectionG2TintAnimated;
		uniform int _MatcapG2ModeAnimated;
		uniform int _MatcapViewDirAnimated;
		uniform int _MatcapG2ColorAnimated;
		uniform int _MatcapB3ColorAnimated;
		uniform int _MatcapG2smoothnessAnimated;
		uniform int _FlipbookToggleAnimated;
		uniform int _RotateFlipbookAnimated;
		uniform int _MaxFramesAnimated;
		uniform int _ColumnsAnimated;
		uniform int _FlipbookColorAnimated;
		uniform int _FlipbookTintAnimated;
		uniform int _OptimizerExcludeFlipbook;
		uniform int _RowsAnimated;
		uniform int _SpeedAnimated;
		uniform int _SSSColorAnimated;
		uniform int _SubsurfaceDistortionModifierAnimated;
		uniform int _OptimizerExcludeSSS;
		uniform int _SSSTintAnimated;
		uniform int _SSSThicknessinvAnimated;
		uniform int _SSSPowerAnimated;
		uniform int _SSSThicknessMapUVSwitchAnimated;
		uniform int _SSSScaleAnimated;
		uniform int _SSSSettingAnimated;
		uniform int _SSSMapModeAnimated;
		uniform int _RimSpecToggleAnimated;
		uniform int _RimFresnelPowerAnimated;
		uniform int _RimLightMaskinvAnimated;
		uniform int _RimPowerAnimated;
		uniform int _RimFaceCullingAnimated;
		uniform int _RimDirectionToggleAnimated;
		uniform int _RimSpecLightsmoothnessAnimated;
		uniform int _RimFresnelBiasAnimated;
		uniform int _RimOpacityAnimated;
		uniform int _RimColorAnimated;
		uniform int _RimTintAnimated;
		uniform int _RimOffsetAnimated;
		uniform int _OptimizerExcludeRimlight;
		uniform int _RimToggleAnimated;
		uniform int _RimMaskUVSwitchAnimated;
		uniform int _RimFresnelScaleAnimated;
		uniform float _ESRenderMethod;
		uniform int _ESCoordinatesAnimated;
		uniform int _AudioLinkWaveformMirrorToggleAnimated;
		uniform int _WaveformThicknessAnimated;
		uniform int _AudioHueSpeedAnimated;
		uniform int _ESSharpnessAnimated;
		uniform int _ESSizeAnimated;
		uniform int _AudioLinkColorAnimated;
		uniform int _ESLevelOffsetAnimated;
		uniform int _WaveformRotationAnimated;
		uniform int _WaveformCoordinatesAnimated;
		uniform int _EmissionscrollTintAnimated;
		uniform int _AudioLinkBandHistoryAnimated;
		uniform int _AudioBandIntensityAnimated;
		uniform int _AudioLinkSwitchAnimated;
		uniform float _AudioLinkTooltip;
		uniform int _ESVoronoiScaleAnimated;
		uniform int _ESRenderMethodAnimated;
		uniform int _IgnoreNormalsESv2Animated;
		uniform float _AudioHueSpeed;
		uniform int _ESSpeedAnimated;
		uniform int _ESVoronoiSpeedAnimated;
		uniform int _OptimizerExcludeESV2;
		uniform int _ESScrollOffsetAnimated;
		uniform int _NoiseSpeedAnimated;
		uniform int _OptimizerExcludeESV1;
		uniform int _EmissionscrollUVSwitchAnimated;
		uniform int _NoiseVectorXYAnimated;
		uniform int _VectorXYAnimated;
		uniform int _EmiossionscrollspeedAnimated;
		uniform int _NoiseTextureUVSwitchAnimated;
		uniform int _ToggleDissolveDirInvAnimated;
		uniform int _DissolveRemapMaxAnimated;
		uniform int _MaterializeBAnimated;
		uniform int _MaterializeAAnimated;
		uniform int _DissolveDensityAnimated;
		uniform int _MaterializeLayerModeAAnimated;
		uniform int _ToggleDissolveEmissionAnimated;
		uniform int _DissolveRemapMinAnimated;
		uniform int _MaterializeColorLayerGAnimated;
		uniform int _MaterializeLayerModeGAnimated;
		uniform int _MaterializeColorLayerBAnimated;
		uniform int _MaterializeColorLayerAAnimated;
		uniform int _MaterializeRAnimated;
		uniform int _MaterializeGAnimated;
		uniform int _ToggleMaterializeDirInvAnimated;
		uniform int _DissolveModifierAnimated;
		uniform int _ToggleDissolveVertexOffsetAnimated;
		uniform int _MaterializeLayerModeBAnimated;
		uniform int _OptimizerExcludeDissolve;
		uniform int _EmissiveDissolveColorAnimated;
		uniform int _MaterializeVertexColorAnimated;
		uniform int _MaterializeLayerModeRAnimated;
		uniform int _MaterializeColorLayerRAnimated;
		uniform int _DissolvePatternUVSwitchAnimated;
		uniform int _DissolveVertexMultiplierAnimated;
		uniform int _DitherAlphaToggleAnimated;
		uniform int _StartDitheringFadeAnimated;
		uniform int _EndDitheringFadeAnimated;
		uniform int _DitherMaskUVSwitchAnimated;
		uniform int _DitherTextureTilingAnimated;
		uniform int _OptimizerExcludeDither;
		uniform int _DitherTextureToggleAnimated;
		uniform float _AdvancedExperimentalToggle;
		uniform float _ZTestMode;
		uniform float _BlendOpRGB;
		uniform float _StencilBufferPassFront;
		uniform float _StencilBufferZFailFront;
		uniform float _ShaderOptimizerEnabled;
		uniform float _StencilBufferReadMask;
		uniform float _CullMode;
		uniform float _ColorMask;
		uniform half _SpecularSetting;
		uniform int _EmissionTintAnimated;
		uniform float _Cutoff;
		uniform int _ModeAnimated;
		uniform int _COLORADDSUBDIFFAnimated;
		uniform float _SourceBlendRGB;
		uniform int _OptimizerExcludeEmission;
		uniform float _DestinationBlendAlpha;
		uniform int _EmissionScrollMaskUVSwitchAnimated;
		uniform int _MainColorAnimated;
		uniform int _EmissionLightscaleAnimated;
		uniform float _ZWriteMode;
		uniform float _DestinationBlendRGB;
		uniform float _StencilBufferReference;
		uniform int _SSSToggleAnimated;
		uniform float _StencilBufferWriteMask;
		uniform float _SourceBlendAlpha;
		uniform int _EmissionUVSwitchAnimated;
		uniform int _SpecularToggleAnimated;
		uniform int _EmissionScrollToggleAnimated;
		uniform int _ModeCustomAnimated;
		uniform float _AlphatoCoverage;
		uniform int _SaturationAnimated;
		uniform int _CutoutAnimated;
		uniform float _BlendOpAlpha;
		uniform float _StencilBufferFailFront;
		uniform int _EmissionscrollColorAnimated;
		uniform int _EmissionColorAnimated;
		uniform float _StencilBufferComparison;
		uniform float _DepthOffsetFactor;
		uniform int _MainTexUVSwitchAnimated;
		uniform float _Glossiness;
		uniform int _COLORCOLORAnimated;
		uniform float _OcclusionStrength;
		uniform int _OptimizerExcludeMainSettings;
		uniform float _DepthOffsetUnits;
		uniform int _IgnoreProjector;
		uniform float _SpecularHighlights;
		uniform float _BumpScale;
		uniform float _DetailNormalMapScale;
		uniform float _DstBlend;
		uniform float _SrcBlend;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_BumpMap);
		SamplerState sampler_BumpMap;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_EmissionMap);
		SamplerState sampler_EmissionMap;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_DetailMask);
		SamplerState sampler_DetailMask;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_MetallicGlossMap);
		SamplerState sampler_MetallicGlossMap;
		uniform float _GlossMapScale;
		uniform float _ZWrite;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_DetailNormalMap);
		SamplerState sampler_DetailNormalMap;
		uniform float _GlossyReflections;
		uniform float4 _Color;
		uniform int _DissolveObject28Animated;
		uniform int _DissolveObject21Animated;
		uniform int _DissolveToggleDir18Animated;
		uniform int _DissolveToggleDir1Animated;
		uniform int _DissolveObject36Animated;
		uniform int _DissolveObject26Animated;
		uniform int _GlobalDissolveAnimated;
		uniform int _DissolveObject15Animated;
		uniform int _DissolveObject38Animated;
		uniform int _DissolveObject27Animated;
		uniform int _DissolveObject34Animated;
		uniform int _DissolveObject33Animated;
		uniform int _DissolveObject32Animated;
		uniform int _DissolveObject31Animated;
		uniform int _DissolveObject2Animated;
		uniform int _DissolveToggleDir21Animated;
		uniform int _DissolveToggleDir11Animated;
		uniform int _DissolveToggleDir10Animated;
		uniform int _DissolveObject8Animated;
		uniform int _DissolveObject37Animated;
		uniform int _DissolveObject16Animated;
		uniform int _DissolveObject17Animated;
		uniform int _DissolveObject18Animated;
		uniform int _DissolveObject25Animated;
		uniform int _DissolveObject19Animated;
		uniform int _DissolveObject29Animated;
		uniform int _DissolveObject13Animated;
		uniform int _DissolveObject4Animated;
		uniform int _DissolveToggleDir7Animated;
		uniform int _DissolveToggleDir12Animated;
		uniform int _DissolveToggleDir13Animated;
		uniform int _DissolveToggleDir14Animated;
		uniform int _DissolveToggleDir15Animated;
		uniform int _DissolveToggleDir16Animated;
		uniform int _DissolveToggleDir6Animated;
		uniform int _DissolveObject6Animated;
		uniform int _DissolveToggleDir8Animated;
		uniform int _DissolveToggleDir9Animated;
		uniform int _DissolveObject10Animated;
		uniform int _DissolveToggleDir5Animated;
		uniform int _DissolveToggleDir4Animated;
		uniform int _DissolveToggleDir3Animated;
		uniform int _DissolveToggleDir2Animated;
		uniform int _DissolveToggleDir17Animated;
		uniform int _DissolveObject9Animated;
		uniform int _DissolveToggleDir30Animated;
		uniform int _DissolveToggleDir20Animated;
		uniform int _DissolveToggleDir34Animated;
		uniform int _DissolveObject35Animated;
		uniform int _DissolveObject30Animated;
		uniform int _DissolveObject11Animated;
		uniform int _DissolveToggleDir28Animated;
		uniform int _DissolveToggleDir33Animated;
		uniform int _DissolveToggleDir29Animated;
		uniform int _DissolveObject20Animated;
		uniform int _DissolveToggleDir22Animated;
		uniform int _DissolveObject5Animated;
		uniform int _DissolveObject3Animated;
		uniform int _DissolveObject1Animated;
		uniform int _DissolveObject14Animated;
		uniform int _DissolveObject39Animated;
		uniform int _DissolveToggleDir32Animated;
		uniform int _EmissiveDissolveColorAnimated1;
		uniform int _DissolveVertexMultiplierAnimated1;
		uniform int _EmissiveScaleAnimated;
		uniform int _DissolveToggleDir39Animated;
		uniform int _DissolveToggleDir38Animated;
		uniform int _DissolveToggleDir36Animated;
		uniform int _DissolveObject7Animated;
		uniform int _DissolveToggleDir19Animated;
		uniform int _DissolveToggleDir37Animated;
		uniform int _DissolveToggleDir40Animated;
		uniform int _DissolveToggleDir35Animated;
		uniform int _DissolveObject40Animated;
		uniform int _DissolveToggleDir24Animated;
		uniform int _DissolveToggleDir25Animated;
		uniform int _DissolveToggleDir26Animated;
		uniform int _DissolveToggleDir27Animated;
		uniform int _DissolveObject24Animated;
		uniform int _DissolveToggleDir31Animated;
		uniform int _DissolveObject12Animated;
		uniform int _DissolveToggleDir23Animated;
		uniform int _DissolveObject23Animated;
		uniform int _DissolveObject22Animated;
		uniform int _TanTextureAnimated;
		uniform int _TanPowerAnimated;
		uniform int _RedMaskColor2Animated;
		uniform int _GreenMaskColor3Animated;
		uniform int _BlueMaskColor1Animated;
		uniform int _BlueMaskColor2Animated;
		uniform int _BlueMaskColor3Animated;
		uniform int _BlueMaskColor4Animated;
		uniform int _GreenMaskColor4Animated;
		uniform int _GreenMaskColor2Animated;
		uniform int _GreenMaskColor1Animated;
		uniform int _RedMaskColor4Animated;
		uniform int _RedMaskColor3Animated;
		uniform int _RedMaskColor1Animated;
		uniform int _CustomDiffuseLerpAnimated;
		uniform int _LipPositionAnimated;
		uniform int _MakeUpRedColorAnimated;
		uniform int _MakeUpGreenColorAnimated;
		uniform int _MakeUpRedPowerAnimated;
		uniform int _MakeUpBluePowerAnimated;
		uniform int _LipRotationAnimated;
		uniform int _ToggleMakeupAnimated;
		uniform int _ToggleLipMatCapGAnimated;
		uniform int _ToggleLipMatCapRAnimated;
		uniform int _LipScaleAnimated;
		uniform int _MakeUpBlueColorAnimated;
		uniform int _MakeUpGreenPowerAnimated;
		uniform int _ToggleLipMatCapAAnimated;
		uniform int _ToggleLipMatCapBAnimated;
		uniform int _MakeupLipThicknessAnimated;
		uniform int _HairHighLightAnimated;
		uniform int _ToggleHairAnimated;
		uniform int _ShadowPowerAnimated;
		uniform int _HairColorAnimated;
		uniform float _ToggleDissolveVertexOffset;
		uniform float _DissolveObject31;
		uniform float _DissolveRemapMin;
		uniform float _DissolveRemapMax;
		uniform float _DissolveToggleDir31;
		uniform float _DissolveDensity;
		uniform float _EmissiveScale;
		uniform float _DissolveVertexMultiplier;
		uniform float _DissolveObject32;
		uniform float _DissolveToggleDir32;
		uniform float _DissolveObject33;
		uniform float _DissolveToggleDir33;
		uniform float _DissolveObject34;
		uniform float _DissolveToggleDir34;
		uniform float _DissolveObject35;
		uniform float _DissolveToggleDir35;
		uniform float _DissolveObject36;
		uniform float _DissolveToggleDir36;
		uniform float _DissolveObject37;
		uniform float _DissolveToggleDir37;
		uniform float _DissolveObject38;
		uniform float _DissolveToggleDir38;
		uniform float _DissolveObject39;
		uniform float _DissolveToggleDir39;
		uniform float _DissolveObject40;
		uniform float _DissolveToggleDir40;
		uniform float _DissolveObject21;
		uniform float _DissolveToggleDir21;
		uniform float _DissolveObject22;
		uniform float _DissolveToggleDir22;
		uniform float _DissolveObject23;
		uniform float _DissolveToggleDir23;
		uniform float _DissolveObject24;
		uniform float _DissolveToggleDir24;
		uniform float _DissolveObject25;
		uniform float _DissolveToggleDir25;
		uniform float _DissolveObject26;
		uniform float _DissolveToggleDir26;
		uniform float _DissolveObject27;
		uniform float _DissolveToggleDir27;
		uniform float _DissolveObject28;
		uniform float _DissolveToggleDir28;
		uniform float _DissolveObject29;
		uniform float _DissolveToggleDir29;
		uniform float _DissolveObject30;
		uniform float _DissolveToggleDir30;
		uniform float _DissolveObject11;
		uniform float _DissolveToggleDir11;
		uniform float _DissolveObject12;
		uniform float _DissolveToggleDir12;
		uniform float _DissolveObject13;
		uniform float _DissolveToggleDir13;
		uniform float _DissolveObject14;
		uniform float _DissolveToggleDir14;
		uniform float _DissolveObject15;
		uniform float _DissolveToggleDir15;
		uniform float _DissolveObject16;
		uniform float _DissolveToggleDir16;
		uniform float _DissolveObject17;
		uniform float _DissolveToggleDir17;
		uniform float _DissolveObject18;
		uniform float _DissolveToggleDir18;
		uniform float _DissolveObject19;
		uniform float _DissolveToggleDir19;
		uniform float _DissolveObject20;
		uniform float _DissolveToggleDir20;
		uniform float _DissolveObject1;
		uniform float _DissolveToggleDir1;
		uniform float _DissolveObject2;
		uniform float _DissolveToggleDir2;
		uniform float _DissolveObject3;
		uniform float _DissolveToggleDir3;
		uniform float _DissolveObject4;
		uniform float _DissolveToggleDir4;
		uniform float _DissolveObject5;
		uniform float _DissolveToggleDir5;
		uniform float _DissolveObject6;
		uniform float _DissolveToggleDir6;
		uniform float _DissolveObject7;
		uniform float _DissolveToggleDir7;
		uniform float _DissolveObject8;
		uniform float _DissolveToggleDir8;
		uniform float _DissolveObject9;
		uniform float _DissolveToggleDir9;
		uniform float _DissolveObject10;
		uniform float _DissolveToggleDir10;
		uniform float _GlobalDissolve;
		uniform float _GlobalDissolveDir;
		uniform float _EmissionScrollToggle;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_NoiseTexture);
		uniform float _NoiseSpeed;
		uniform float2 _NoiseVectorXY;
		uniform float4 _NoiseTexture_ST;
		uniform float _NoiseTextureUVSwitch;
		SamplerState sampler_trilinear_repeat;
		SamplerState sampler_NoiseTexture;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_Emissionscroll);
		uniform float _Emiossionscrollspeed;
		uniform float2 _VectorXY;
		uniform float4 _Emissionscroll_ST;
		uniform float _EmissionscrollUVSwitch;
		uniform float4 _EmissionscrollColor;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_EmissionScrollMask);
		uniform float4 _EmissionScrollMask_ST;
		uniform float _EmissionScrollMaskUVSwitch;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_MainTex);
		SamplerState sampler_MainTex;
		uniform float _AudioLinkSwitch;
		uniform float _ESSpeed;
		uniform float _ESScrollOffset;
		uniform float _IgnoreNormalsESv2;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_NormalMap);
		uniform float4 _NormalMap_ST;
		uniform float _NormalMapUVSwitch;
		uniform float _NormalScale;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_SecondaryNormalMask);
		uniform float4 _SecondaryNormalMask_ST;
		uniform float _SecondaryNormalMaskUVSwitch;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_SecondaryNormal);
		uniform float4 _SecondaryNormal_ST;
		uniform float _SecondaryNormalUVSwitch;
		uniform float _SecondaryNormalScale;
		uniform float _ESVoronoiScale;
		uniform float _ESVoronoiSpeed;
		uniform float2 _ESCoordinates;
		uniform float _ESSize;
		uniform float _ESSharpness;
		uniform float _ESLevelOffset;
		uniform float _AudioLinkBandHistory;
		uniform float4 _AudioBandIntensity;
		uniform float _AudioLinkWaveformMirrorToggle;
		uniform float4 _WaveformCoordinates;
		uniform float _WaveformRotation;
		uniform float _WaveformThickness;
		uniform float4 _AudioLinkColor;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_TanTexture);
		uniform float4 _TanTexture_ST;
		uniform float4 _MainTex_ST;
		uniform float _MainTexUVSwitch;
		uniform float _TanPower;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_ColorDiffuseSampler);
		uniform float4 _ColorDiffuseSampler_ST;
		uniform float4 _RedMaskColor1;
		uniform float4 _RedMaskColor2;
		uniform float4 _RedMaskColor3;
		uniform float4 _RedMaskColor4;
		uniform float4 _GreenMaskColor1;
		uniform float4 _GreenMaskColor2;
		uniform float4 _GreenMaskColor3;
		uniform float4 _GreenMaskColor4;
		uniform float4 _BlueMaskColor1;
		uniform float4 _BlueMaskColor2;
		uniform float4 _BlueMaskColor3;
		uniform float4 _BlueMaskColor4;
		uniform float _CustomDiffuseLerp;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_MakeupLipThick);
		uniform float2 _LipScale;
		uniform float2 _LipPosition;
		uniform float _LipRotation;
		uniform float _ToggleMakeup;
		uniform float _MakeupLipThickness;
		uniform float4 _MakeUpGreenColor;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_MakeupMask);
		uniform float4 _MakeupMask_ST;
		uniform float4 _MakeUpBlueColor;
		uniform float4 _MakeUpRedColor;
		uniform float _MakeUpRedPower;
		uniform float _MakeUpGreenPower;
		uniform float _MakeUpBluePower;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_HairTexture);
		uniform float4 _HairTexture_ST;
		uniform float4 _HairColor;
		uniform float _ToggleHair;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_HairMask);
		uniform float4 _HairMask_ST;
		uniform float _HueShiftSpeed;
		uniform float _HueShiftRandomizer;
		uniform float _ToggleHueTexforSpeed;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_HueMask);
		uniform float4 _HueMask_ST;
		uniform float _HueMaskUVSwitch;
		SamplerState sampler_point_clamp;
		uniform int _HueMaskinverter;
		uniform float _HueShiftblend;
		uniform float4 _MainColor;
		uniform float _Cutout;
		uniform float _Saturation;
		uniform float _EmissionscrollTint;
		uniform float _COLORCOLOR;
		uniform float _RimSwitch;
		uniform float _ShadowRimSharpness;
		uniform float _ShadowRimRange;
		uniform float _ShadowRimOpacity;
		uniform float4 _EmissiveRimColor;
		uniform float _RimHueSpeed;
		uniform int _ToggleDissolveEmission;
		uniform half4 _EmissiveDissolveColor;
		uniform float _ModeCustom;
		uniform float _Mode;
		uniform half _Metallic;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_MetallicMap);
		uniform float4 _MetallicMap_ST;
		uniform float _COLORADDSUBDIFF;
		uniform float _MaterializeVertexColor;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_DissolveMask);
		uniform float4 _DissolveMask_ST;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_DissolvePattern);
		uniform float4 _DissolvePattern_ST;
		uniform float _MaterializeLayerModeR;
		uniform float _ToggleMonochromePixelLight;
		uniform float _ToggleSteps;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_ToonRamp);
		uniform float _NdLHalfingControl;
		uniform float _RampOffset;
		SamplerState sampler_linear_clamp;
		uniform float4 _RampColor;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_ShadowColorMap);
		uniform float4 _ShadowColorMap_ST;
		SamplerState sampler_trilinear_clamp;
		uniform float _ShadowColorMapStrength;
		uniform float _ColoringPointLights;
		uniform float _PointSpotShadowIntensity;
		uniform int _Steps;
		uniform float _MaxLightDirect;
		uniform float _AmbientBoost;
		uniform float _ExperimentalToggle;
		uniform float _DirectShadowIntensity;
		uniform float _SelfCastShadows;
		uniform float _ShadowMaskinvert;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_ShadowMask);
		uniform float4 _ShadowMask_ST;
		uniform float _ShadowMaskStrength;
		uniform float _ColoringDirectEnvLights;
		uniform float _ToggleMonochromeEnv;
		uniform float _IndirectShadowIntensity;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_OcclusionMap);
		uniform float4 _OcclusionMap_ST;
		uniform float _Occlusion;
		uniform float _CubemapIntensity;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_ReflectionMask);
		uniform float4 _ReflectionMask_ST;
		uniform float _ReflectionMaskUVSwitch;
		uniform float _MatcapR1Mode;
		uniform int _MatcapR1Toggle;
		uniform float _MatcapR1Blending;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_ReflectionMaskMatcap);
		uniform float4 _ReflectionMaskMatcap_ST;
		uniform float _ReflectionMaskMatcapUVSwitch;
		uniform float _ToggleLipMatCapR;
		uniform float _ToggleLipMatCapG;
		uniform float _ToggleLipMatCapB;
		uniform float _ToggleLipMatCapA;
		uniform float _MatcapG2Mode;
		uniform int _MatcapG2Toggle;
		uniform float _MatcapG2Blending;
		uniform float _MatcapB3Mode;
		uniform int _MatcapB3Toggle;
		uniform float _MatcapB3Blending;
		uniform float _MatcapA4Mode;
		uniform int _MatcapA4Toggle;
		uniform float _MatcapA4Blending;
		uniform float _HighlightOffset;
		uniform float _HighlightSmoothness;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_SpecularMap);
		uniform float4 _SpecularMap_ST;
		uniform float _SpecularMapUVSwitch;
		uniform float4 _SpecularColor;
		uniform float _SpecularTint;
		uniform float _SpecShadowMaskVar;
		uniform float _SpecShadowMaskPower;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_AnisoDir);
		uniform float4 _AnisoDir_ST;
		uniform float _AnisoDirUVSwitch;
		uniform float _BlinntoAniso;
		uniform float _AnisoScale;
		uniform float _AnisoSharpening;
		uniform float _SpecularToggle;
		uniform float _IgnoreNormalsCubemap;
		uniform float _Cubemapsmoothness;
		uniform float _CubemapSpecularToggle;
		uniform float _WorkflowSwitch;
		UNITY_DECLARE_TEXCUBE_NOSAMPLER(_Cubemap);
		SamplerState sampler_Cubemap;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_MatcapR1);
		uniform float _MatcapViewDir;
		uniform float _IgnoreNormalsMatcap;
		uniform float _MatcapR1smoothness;
		uniform float4 _MatcapR1Color;
		uniform float _ReflectionR1Intensity;
		uniform float _ReflectionR1Tint;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_MatcapG2);
		uniform float _MatcapG2smoothness;
		uniform float4 _MatcapG2Color;
		uniform float _ReflectionG2Intensity;
		uniform float _ReflectionG2Tint;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_MatcapB3);
		uniform float _MatcapB3smoothness;
		uniform float4 _MatcapB3Color;
		uniform float _ReflectionB3Intensity;
		uniform float _ReflectionB3Tint;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_MatcapA4);
		uniform float _MatcapA4smoothness;
		uniform float4 _MatcapA4Color;
		uniform float _ReflectionA4Intensity;
		uniform float _ReflectionA4Tint;
		uniform float _RimToggle;
		uniform float _RimFaceCulling;
		uniform float _RimDirectionToggle;
		uniform float _RimOffset;
		uniform float _RimPower;
		uniform float _RimFresnelBias;
		uniform float _RimFresnelScale;
		uniform float _RimFresnelPower;
		uniform float _RimTint;
		uniform float4 _RimColor;
		uniform float _RimLightMaskinv;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_RimMask);
		uniform float4 _RimMask_ST;
		uniform float _RimMaskUVSwitch;
		uniform float _RimSpecToggle;
		uniform float _RimSpecLightsmoothness;
		uniform float _RimOpacity;
		uniform float _FlipbookToggle;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_Flipbook);
		uniform float4 _Flipbook_ST;
		uniform float _RotateFlipbook;
		uniform int _Columns;
		uniform int _Rows;
		uniform int _Speed;
		uniform int _MaxFrames;
		SamplerState sampler_Flipbook;
		uniform half4 _FlipbookColor;
		uniform float _FlipbookTint;
		uniform float _SSSSetting;
		uniform float _SubsurfaceDistortionModifier;
		uniform float _SSSMapMode;
		uniform float _SSSThicknessinv;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_SSSThicknessMap);
		uniform float4 _SSSThicknessMap_ST;
		uniform float _SSSThicknessMapUVSwitch;
		uniform float _SSSPower;
		uniform float _SSSScale;
		uniform float _SSSTint;
		uniform float4 _SSSColor;
		uniform float _SSSToggle;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_Emission);
		uniform float4 _Emission_ST;
		uniform float _EmissionUVSwitch;
		uniform float4 _EmissionColor;
		uniform float _EmissionTint;
		uniform float _EmissionLightscale;
		uniform float _MaskClipValue;


		void SourceDeclaration(  )
		{
			//This Shader was made possible by Moriohs Toon Shader (https://gitlab.com/xMorioh/moriohs-toon-shader)
		}


		float V_SmithGGXCorrelated_Anisotropic( float at, float ab, float ToV, float BoV, float ToL, float BoL, float NoV, float NoL )
		{
			float lambdaV = NoL * length(float3(at * ToV, ab * BoV, NoV));
				float lambdaL = NoV * length(float3(at * ToL, ab * BoL, NoL));
				float v = 0.5 / (lambdaV + lambdaL);
				return saturate(v);
		}


		float D_GGX_Anisotropic( float NoH, float3 h, float3 t, float3 b, float at, float ab )
		{
				const float ToH = dot(t, h);
				const float BoH = dot(b, h);
				float a2 = at * ab;
				const float3 v = float3(ab * ToH, at * BoH, a2 * NoH);
				const float v2 = dot(v, v);
				const float w2 = a2 / v2;
				return a2 * w2 * w2 * (1.0 / UNITY_PI);
		}


		float3 F_Schlick( float u, float f0 )
		{
			return f0 + (1.0 - f0) * pow(1.0 - u, 5.0);
		}


		void CentroidSampling(  )
		{
			// This is a workaround for Surface Shaders and Amplify, Source (https://twitter.com/kurotori4423/status/1381253671799824386) (https://twitter.com/Silent0264/status/1383139686055497728/photo/1)
			#if defined(SHADER_STAGE_VERTEX) || defined(SHADER_STAGE_FRAGMENT) || defined(SHADER_STAGE_DOMAIN) || defined(SHADER_STAGE_HULL) || defined(SHADER_STAGE_GEOMETRY)
			#if !defined(UNITY_PASS_DEFERRED)
			#define TEXCOORD1 TEXCOORD1_Centroid
			#endif
			#endif
		}


		float2 UVSwitch( float UVSwitchProp, float2 UV0, float2 UV1, float2 UV2, float2 UV3 )
		{
			if (UVSwitchProp == 0)
				return UV0;
			else if (UVSwitchProp == 1)
				return UV1;
			else if (UVSwitchProp == 2)
				return UV2;
			else
				return UV3;
		}


		float ASEOr( float A, float B )
		{
			float result = A || B;
			return result;
		}


		float2 voronoihash64_g22346( float2 p )
		{
			
			p = float2( dot( p, float2( 127.1, 311.7 ) ), dot( p, float2( 269.5, 183.3 ) ) );
			return frac( sin( p ) *43758.5453);
		}


		float voronoi64_g22346( float2 v, float time, inout float2 id, inout float2 mr, float smoothness, inout float2 smoothId )
		{
			float2 n = floor( v );
			float2 f = frac( v );
			float F1 = 8.0;
			float F2 = 8.0; float2 mg = 0;
			for ( int j = -1; j <= 1; j++ )
			{
				for ( int i = -1; i <= 1; i++ )
			 	{
			 		float2 g = float2( i, j );
			 		float2 o = voronoihash64_g22346( n + g );
					o = ( sin( time + o * 6.2831 ) * 0.5 + 0.5 ); float2 r = f - g - o;
					float d = 0.5 * dot( r, r );
			 		if( d<F1 ) {
			 			F2 = F1;
			 			F1 = d; mg = g; mr = r; id = o;
			 		} else if( d<F2 ) {
			 			F2 = d;
			
			 		}
			 	}
			}
			return F1;
		}


		inline float AudioLinkLerp( int Band, float Delay )
		{
			return AudioLinkLerp( ALPASS_AUDIOLINK + float2( Delay, Band ) ).r;
		}


		float2 AudioLinkWaveformsample( float2 uv, float thickness )
		{
			//Source: "https://github.com/llealloo/vrc-udon-audio-link"
			float noteno = uv.x*ETOTALBINS;
			                float4 spectrum_value = -AudioLinkLerpMultiline( ALPASS_DFT + float2( noteno, 0. ) ) * 0.5 + 0.5;
				if( uv.y < spectrum_value.z )
			                    return 0;
				else if( uv.y < spectrum_value.z + 0.1 * thickness )
			                    return 1.;
			                return 0;
		}


		float2 AudioLinkWaveformsampleMirror( float2 uv )
		{
			//Source: "https://github.com/llealloo/vrc-udon-audio-link"
			float noteno = uv.x*ETOTALBINS;
			                float4 spectrum_value = -AudioLinkLerpMultiline( ALPASS_DFT + float2( noteno, 0. ) ) * 0.5 + 0.5;
				if( uv.y < spectrum_value.z )
			                    return 0;
				else if( uv.y < 1-spectrum_value.z )
			                    return 1.;
			                return 0;
		}


		float3 HSVToRGB( float3 c )
		{
			float4 K = float4( 1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0 );
			float3 p = abs( frac( c.xxx + K.xyz ) * 6.0 - K.www );
			return c.z * lerp( K.xxx, saturate( p - K.xxx ), c.y );
		}


		float getGammaToLinearSpaceExact( float LinearIn )
		{
			return GammaToLinearSpaceExact(LinearIn);
		}


		float4 FourLightPosX(  )
		{
			return unity_4LightPosX0;
		}


		float4 FourLightPosY(  )
		{
			return unity_4LightPosY0;
		}


		float4 FourLightPosZ(  )
		{
			return unity_4LightPosZ0;
		}


		half getLinearRgbToLuminance( half3 linearRgb )
		{
			return LinearRgbToLuminance(linearRgb);
		}


		float3 LightColorZero(  )
		{
			return unity_LightColor[0];
		}


		float4 FourLightAtten(  )
		{
			return unity_4LightAtten0;
		}


		float3 LightColorZOne(  )
		{
			return unity_LightColor[1];
		}


		float3 LightColorZTwo(  )
		{
			return unity_LightColor[2];
		}


		float3 LightColorZThree(  )
		{
			return unity_LightColor[3];
		}


		half3 Ambient(  )
		{
			return float3(unity_SHAr.w, unity_SHAg.w, unity_SHAb.w) + float3(unity_SHBr.z, unity_SHBg.z, unity_SHBb.z) / 3.0;
		}


		float3 ambientDir(  )
		{
			//Source: "https://web.archive.org/web/20160313132301/http://www.geomerics.com/wp-content/uploads/2015/08/CEDEC_Geomerics_ReconstructingDiffuseLighting1.pdf" Page 18
			return normalize(unity_SHAr.xyz + unity_SHAg.xyz + unity_SHAb.xyz);
		}


		inline float3 ASESafeNormalize(float3 inVec)
		{
			float dp3 = max( 0.001f , dot( inVec , inVec ) );
			return inVec* rsqrt( dp3);
		}


		float GSAA_Filament( float3 worldNormal, float smoothness )
		{
			// Kaplanyan 2016, "Stable specular highlights"
			// Tokuyoshi 2017, "Error Reduction and Simplification for Shading Anti-Aliasing"
			// Tokuyoshi and Kaplanyan 2019, "Improved Geometric Specular Antialiasing"
			// This implementation is meant for deferred rendering in the original paper but
			// we use it in forward rendering as well (as discussed in Tokuyoshi and Kaplanyan
			// 2019). The main reason is that the forward version requires an expensive transform
			// of the float vector by the tangent frame for every light. This is therefore an
			// approximation but it works well enough for our needs and provides an improvement
			// over our original implementation based on Vlachos 2015, "Advanced VR Rendering".
			if (_EnableGSAA == 1)
			{
			    float3 du = ddx(worldNormal);
			    float3 dv = ddy(worldNormal);
			    float variance = _GSAAVariance * (dot(du, du) + dot(dv, dv));
			    float perceptualRoughness = 1-smoothness;
			    float roughness = perceptualRoughness * perceptualRoughness;
			    float kernelRoughness = min(2.0 * variance, _GSAAThreshold);
			    float squareRoughness = saturate(roughness * roughness + kernelRoughness);
			    return 1-sqrt(sqrt(squareRoughness));
			}
			else
			{
			    return smoothness;
			}
		}


		float CorrectNegativeNdotV( float3 viewDir, float3 normal )
		{
			#define UNITY_HANDLE_CORRECTLY_NEGATIVE_NDOTV 0
			#if UNITY_HANDLE_CORRECTLY_NEGATIVE_NDOTV
			    // The amount we shift the normal toward the view vector is defined by the dot product.
			    half shiftAmount = dot(normal, viewDir);
			    normal = shiftAmount < 0.0f ? normal + viewDir * (-shiftAmount + 1e-5f) : normal;
			    // A re-normalization should be applied here but as the shift is small we don't do it to save ALU.
			    //normal = normalize(normal);
			    float nv = saturate(dot(normal, viewDir)); // TODO: this saturate should no be necessary here
			#else
			    half nv = abs(dot(normal, viewDir));    // This abs allow to limit artifact
			#endif
			return nv;
		}


		float getSmithJointGGXVisibilityTerm( float NdotL, float NdotV, float roughness )
		{
			return SmithJointGGXVisibilityTerm (NdotL, NdotV, roughness);
		}


		float getGGXTerm( float NdotH, float roughness )
		{
			return GGXTerm (NdotH, roughness);
		}


		float3 calcSpecularTerm( float GGXVisibilityTerm, float GGXTerm, float NdotL, float LdotH, float roughness, float3 specColor, float3 lightcolor, float specularTermToggle )
		{
			// "GGX with roughness to 0 would mean no specular at all, using max(roughness, 0.002) here to match HDrenderloop roughness remapping."
			float specularTerm = GGXVisibilityTerm * GGXTerm * UNITY_PI; // Torrance-Sparrow model, Fresnel is applied later
			// Gamma Space support
			#   ifdef UNITY_COLORSPACE_GAMMA
			        specularTerm = sqrt(max(1e-4h, specularTerm));
			#   endif
			// specularTerm * nl can be NaN on Metal in some cases, use max() to make sure it's a sane value
			specularTerm = max(0, specularTerm * NdotL);
			//Toggle specularTerm
			if (specularTermToggle == 1) {
			// To provide true Lambert lighting, we need to be able to kill specular completely.
				specularTerm *= any(specColor) ? 1.0 : 0.0;
			}
			else {
				specularTerm = 0;
			}
			return
			specularTerm * lightcolor * FresnelTerm(specColor, LdotH);
		}


		float3 getUnityObjectToWorldNormal2226_g22168( float3 In0 )
		{
			return UnityObjectToWorldNormal(In0);
		}


		float3 getUnityObjectToWorldDir2223_g22168( float3 In0 )
		{
			return UnityObjectToWorldDir(In0);
		}


		float3 calcGGXAniso( float ndl, float ndh, float vdn, float ldh, float3 lightCol, float3 halfVector, float smoothness, float3 tangent, float3 bitangent, float3 diffuseColor, float4 SpecularMap, float3 LightDir, float3 ViewDir )
		{
			    const half specularIntensity = _SpecularColor.a * SpecularMap.r;
			    
			    const float3 t = tangent;
			    const float3 b = bitangent;
			    const float ToV = dot(t, ViewDir);
			    const float BoV = dot(b, ViewDir);
			    const float ToL = dot(t, LightDir);
			    const float BoL = dot(b, LightDir);
			    float perceptualRoughness = SmoothnessToPerceptualRoughness(smoothness);
			    perceptualRoughness = clamp(perceptualRoughness, 0.089, 1.0);
			    float roughness = PerceptualRoughnessToRoughness(perceptualRoughness);
			    float rough = roughness;
			    
			    float anisotropy = _Anisotropy * SpecularMap.b;
			    const float at = max(rough * (1.0 + anisotropy), 0.001);
			    const float ab = max(rough * (1.0 - anisotropy), 0.001);
			    const float V = V_SmithGGXCorrelated_Anisotropic(at, ab, ToV, BoV, ToL, BoL, vdn, ndl);
			    const float D = D_GGX_Anisotropic(ndh, halfVector, tangent, bitangent, at, ab);
			          float F = 1-F_Schlick(ldh,1-lerp(unity_ColorSpaceDielectricSpec.r, 1, _AnisoF0Reflectance));
			    if (_AnisoFlickerFix == 1)
			    {
			        F = 1-F_Schlick(ldh,1-lerp(unity_ColorSpaceDielectricSpec.r, 1, _AnisoF0Reflectance) * ndl * ndh); //this is not mathematically correct, only use when needed
			    }
			    
			    half3 specular = max(0, D * V * F * ndl * UNITY_PI);
			    specular = lerp(specular, smoothstep(0.25, 0.26, specular), _AnisoSharpening * SpecularMap.g) * lightCol * specularIntensity * diffuseColor;
			    return specular;
		}


		float3 calcSpecularBase( float3 specularTerm, float NdotV, float3 specColor, float roughness, float oneMinusReflectivity, float3 indirectspecular, float smoothness, float perceptualRoughness )
		{
			half surfaceReduction;
			// Gamma Space support
			#   ifdef UNITY_COLORSPACE_GAMMA
			        surfaceReduction = 1.0-0.28*roughness*perceptualRoughness;      // 1-0.28*x^3 as approximation for (1/(x^4+1))^(1/2.2) on the domain [0;1]
			#   else
			        surfaceReduction = 1.0 / (roughness*roughness + 1.0);           // fade \in [0.5;1]
			#   endif
			half grazingTerm = saturate(smoothness + (1-oneMinusReflectivity));
			return
			specularTerm + surfaceReduction * indirectspecular.rgb * FresnelLerp(specColor, grazingTerm, NdotV);
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float DissolveModifier30_g22441 = _DissolveObject31;
			float3 ase_vertex3Pos = v.vertex.xyz;
			float4 transform4_g22441 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22441 = transform4_g22441.y;
			float ifLocalVar46_g22441 = 0;
			if( _DissolveToggleDir31 > 0.0 )
				ifLocalVar46_g22441 = -Space24_g22441;
			else if( _DissolveToggleDir31 == 0.0 )
				ifLocalVar46_g22441 = Space24_g22441;
			float3 ase_objectScale = float3( length( unity_ObjectToWorld[ 0 ].xyz ), length( unity_ObjectToWorld[ 1 ].xyz ), length( unity_ObjectToWorld[ 2 ].xyz ) );
			float ObjectScale23_g22441 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22441 = exp2( _DissolveDensity );
			float temp_output_70_0_g22441 = ( ( (-1.0 + (DissolveModifier30_g22441 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22441 / ObjectScale23_g22441 ) ) * DissolveDensity67_g22441 );
			float DissolveOutputNoPattern286_g22441 = temp_output_70_0_g22441;
			float lerpResult91_g22441 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22441 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22441 - _EmissiveScale ) ) ));
			float3 ase_vertexNormal = v.normal.xyz;
			float3 ifLocalVar96_g22441 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22441 = ( ( ( lerpResult91_g22441 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22448 = _DissolveObject32;
			float4 transform4_g22448 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22448 = transform4_g22448.y;
			float ifLocalVar46_g22448 = 0;
			if( _DissolveToggleDir32 > 0.0 )
				ifLocalVar46_g22448 = -Space24_g22448;
			else if( _DissolveToggleDir32 == 0.0 )
				ifLocalVar46_g22448 = Space24_g22448;
			float ObjectScale23_g22448 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22448 = exp2( _DissolveDensity );
			float temp_output_70_0_g22448 = ( ( (-1.0 + (DissolveModifier30_g22448 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22448 / ObjectScale23_g22448 ) ) * DissolveDensity67_g22448 );
			float DissolveOutputNoPattern286_g22448 = temp_output_70_0_g22448;
			float lerpResult91_g22448 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22448 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22448 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22448 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22448 = ( ( ( lerpResult91_g22448 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22447 = _DissolveObject33;
			float4 transform4_g22447 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22447 = transform4_g22447.y;
			float ifLocalVar46_g22447 = 0;
			if( _DissolveToggleDir33 > 0.0 )
				ifLocalVar46_g22447 = -Space24_g22447;
			else if( _DissolveToggleDir33 == 0.0 )
				ifLocalVar46_g22447 = Space24_g22447;
			float ObjectScale23_g22447 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22447 = exp2( _DissolveDensity );
			float temp_output_70_0_g22447 = ( ( (-1.0 + (DissolveModifier30_g22447 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22447 / ObjectScale23_g22447 ) ) * DissolveDensity67_g22447 );
			float DissolveOutputNoPattern286_g22447 = temp_output_70_0_g22447;
			float lerpResult91_g22447 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22447 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22447 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22447 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22447 = ( ( ( lerpResult91_g22447 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22442 = _DissolveObject34;
			float4 transform4_g22442 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22442 = transform4_g22442.y;
			float ifLocalVar46_g22442 = 0;
			if( _DissolveToggleDir34 > 0.0 )
				ifLocalVar46_g22442 = -Space24_g22442;
			else if( _DissolveToggleDir34 == 0.0 )
				ifLocalVar46_g22442 = Space24_g22442;
			float ObjectScale23_g22442 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22442 = exp2( _DissolveDensity );
			float temp_output_70_0_g22442 = ( ( (-1.0 + (DissolveModifier30_g22442 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22442 / ObjectScale23_g22442 ) ) * DissolveDensity67_g22442 );
			float DissolveOutputNoPattern286_g22442 = temp_output_70_0_g22442;
			float lerpResult91_g22442 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22442 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22442 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22442 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22442 = ( ( ( lerpResult91_g22442 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22430 = _DissolveObject35;
			float4 transform4_g22430 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22430 = transform4_g22430.y;
			float ifLocalVar46_g22430 = 0;
			if( _DissolveToggleDir35 > 0.0 )
				ifLocalVar46_g22430 = -Space24_g22430;
			else if( _DissolveToggleDir35 == 0.0 )
				ifLocalVar46_g22430 = Space24_g22430;
			float ObjectScale23_g22430 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22430 = exp2( _DissolveDensity );
			float temp_output_70_0_g22430 = ( ( (-1.0 + (DissolveModifier30_g22430 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22430 / ObjectScale23_g22430 ) ) * DissolveDensity67_g22430 );
			float DissolveOutputNoPattern286_g22430 = temp_output_70_0_g22430;
			float lerpResult91_g22430 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22430 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22430 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22430 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22430 = ( ( ( lerpResult91_g22430 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22427 = _DissolveObject36;
			float4 transform4_g22427 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22427 = transform4_g22427.y;
			float ifLocalVar46_g22427 = 0;
			if( _DissolveToggleDir36 > 0.0 )
				ifLocalVar46_g22427 = -Space24_g22427;
			else if( _DissolveToggleDir36 == 0.0 )
				ifLocalVar46_g22427 = Space24_g22427;
			float ObjectScale23_g22427 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22427 = exp2( _DissolveDensity );
			float temp_output_70_0_g22427 = ( ( (-1.0 + (DissolveModifier30_g22427 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22427 / ObjectScale23_g22427 ) ) * DissolveDensity67_g22427 );
			float DissolveOutputNoPattern286_g22427 = temp_output_70_0_g22427;
			float lerpResult91_g22427 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22427 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22427 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22427 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22427 = ( ( ( lerpResult91_g22427 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22435 = _DissolveObject37;
			float4 transform4_g22435 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22435 = transform4_g22435.y;
			float ifLocalVar46_g22435 = 0;
			if( _DissolveToggleDir37 > 0.0 )
				ifLocalVar46_g22435 = -Space24_g22435;
			else if( _DissolveToggleDir37 == 0.0 )
				ifLocalVar46_g22435 = Space24_g22435;
			float ObjectScale23_g22435 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22435 = exp2( _DissolveDensity );
			float temp_output_70_0_g22435 = ( ( (-1.0 + (DissolveModifier30_g22435 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22435 / ObjectScale23_g22435 ) ) * DissolveDensity67_g22435 );
			float DissolveOutputNoPattern286_g22435 = temp_output_70_0_g22435;
			float lerpResult91_g22435 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22435 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22435 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22435 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22435 = ( ( ( lerpResult91_g22435 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22438 = _DissolveObject38;
			float4 transform4_g22438 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22438 = transform4_g22438.y;
			float ifLocalVar46_g22438 = 0;
			if( _DissolveToggleDir38 > 0.0 )
				ifLocalVar46_g22438 = -Space24_g22438;
			else if( _DissolveToggleDir38 == 0.0 )
				ifLocalVar46_g22438 = Space24_g22438;
			float ObjectScale23_g22438 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22438 = exp2( _DissolveDensity );
			float temp_output_70_0_g22438 = ( ( (-1.0 + (DissolveModifier30_g22438 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22438 / ObjectScale23_g22438 ) ) * DissolveDensity67_g22438 );
			float DissolveOutputNoPattern286_g22438 = temp_output_70_0_g22438;
			float lerpResult91_g22438 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22438 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22438 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22438 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22438 = ( ( ( lerpResult91_g22438 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22456 = _DissolveObject39;
			float4 transform4_g22456 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22456 = transform4_g22456.y;
			float ifLocalVar46_g22456 = 0;
			if( _DissolveToggleDir39 > 0.0 )
				ifLocalVar46_g22456 = -Space24_g22456;
			else if( _DissolveToggleDir39 == 0.0 )
				ifLocalVar46_g22456 = Space24_g22456;
			float ObjectScale23_g22456 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22456 = exp2( _DissolveDensity );
			float temp_output_70_0_g22456 = ( ( (-1.0 + (DissolveModifier30_g22456 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22456 / ObjectScale23_g22456 ) ) * DissolveDensity67_g22456 );
			float DissolveOutputNoPattern286_g22456 = temp_output_70_0_g22456;
			float lerpResult91_g22456 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22456 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22456 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22456 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22456 = ( ( ( lerpResult91_g22456 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22411 = _DissolveObject40;
			float4 transform4_g22411 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22411 = transform4_g22411.y;
			float ifLocalVar46_g22411 = 0;
			if( _DissolveToggleDir40 > 0.0 )
				ifLocalVar46_g22411 = -Space24_g22411;
			else if( _DissolveToggleDir40 == 0.0 )
				ifLocalVar46_g22411 = Space24_g22411;
			float ObjectScale23_g22411 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22411 = exp2( _DissolveDensity );
			float temp_output_70_0_g22411 = ( ( (-1.0 + (DissolveModifier30_g22411 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22411 / ObjectScale23_g22411 ) ) * DissolveDensity67_g22411 );
			float DissolveOutputNoPattern286_g22411 = temp_output_70_0_g22411;
			float lerpResult91_g22411 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22411 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22411 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22411 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22411 = ( ( ( lerpResult91_g22411 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22458 = _DissolveObject21;
			float4 transform4_g22458 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22458 = transform4_g22458.y;
			float ifLocalVar46_g22458 = 0;
			if( _DissolveToggleDir21 > 0.0 )
				ifLocalVar46_g22458 = -Space24_g22458;
			else if( _DissolveToggleDir21 == 0.0 )
				ifLocalVar46_g22458 = Space24_g22458;
			float ObjectScale23_g22458 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22458 = exp2( _DissolveDensity );
			float temp_output_70_0_g22458 = ( ( (-1.0 + (DissolveModifier30_g22458 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22458 / ObjectScale23_g22458 ) ) * DissolveDensity67_g22458 );
			float DissolveOutputNoPattern286_g22458 = temp_output_70_0_g22458;
			float lerpResult91_g22458 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22458 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22458 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22458 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22458 = ( ( ( lerpResult91_g22458 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22450 = _DissolveObject22;
			float4 transform4_g22450 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22450 = transform4_g22450.y;
			float ifLocalVar46_g22450 = 0;
			if( _DissolveToggleDir22 > 0.0 )
				ifLocalVar46_g22450 = -Space24_g22450;
			else if( _DissolveToggleDir22 == 0.0 )
				ifLocalVar46_g22450 = Space24_g22450;
			float ObjectScale23_g22450 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22450 = exp2( _DissolveDensity );
			float temp_output_70_0_g22450 = ( ( (-1.0 + (DissolveModifier30_g22450 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22450 / ObjectScale23_g22450 ) ) * DissolveDensity67_g22450 );
			float DissolveOutputNoPattern286_g22450 = temp_output_70_0_g22450;
			float lerpResult91_g22450 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22450 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22450 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22450 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22450 = ( ( ( lerpResult91_g22450 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22451 = _DissolveObject23;
			float4 transform4_g22451 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22451 = transform4_g22451.y;
			float ifLocalVar46_g22451 = 0;
			if( _DissolveToggleDir23 > 0.0 )
				ifLocalVar46_g22451 = -Space24_g22451;
			else if( _DissolveToggleDir23 == 0.0 )
				ifLocalVar46_g22451 = Space24_g22451;
			float ObjectScale23_g22451 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22451 = exp2( _DissolveDensity );
			float temp_output_70_0_g22451 = ( ( (-1.0 + (DissolveModifier30_g22451 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22451 / ObjectScale23_g22451 ) ) * DissolveDensity67_g22451 );
			float DissolveOutputNoPattern286_g22451 = temp_output_70_0_g22451;
			float lerpResult91_g22451 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22451 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22451 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22451 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22451 = ( ( ( lerpResult91_g22451 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22396 = _DissolveObject24;
			float4 transform4_g22396 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22396 = transform4_g22396.y;
			float ifLocalVar46_g22396 = 0;
			if( _DissolveToggleDir24 > 0.0 )
				ifLocalVar46_g22396 = -Space24_g22396;
			else if( _DissolveToggleDir24 == 0.0 )
				ifLocalVar46_g22396 = Space24_g22396;
			float ObjectScale23_g22396 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22396 = exp2( _DissolveDensity );
			float temp_output_70_0_g22396 = ( ( (-1.0 + (DissolveModifier30_g22396 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22396 / ObjectScale23_g22396 ) ) * DissolveDensity67_g22396 );
			float DissolveOutputNoPattern286_g22396 = temp_output_70_0_g22396;
			float lerpResult91_g22396 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22396 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22396 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22396 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22396 = ( ( ( lerpResult91_g22396 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22436 = _DissolveObject25;
			float4 transform4_g22436 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22436 = transform4_g22436.y;
			float ifLocalVar46_g22436 = 0;
			if( _DissolveToggleDir25 > 0.0 )
				ifLocalVar46_g22436 = -Space24_g22436;
			else if( _DissolveToggleDir25 == 0.0 )
				ifLocalVar46_g22436 = Space24_g22436;
			float ObjectScale23_g22436 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22436 = exp2( _DissolveDensity );
			float temp_output_70_0_g22436 = ( ( (-1.0 + (DissolveModifier30_g22436 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22436 / ObjectScale23_g22436 ) ) * DissolveDensity67_g22436 );
			float DissolveOutputNoPattern286_g22436 = temp_output_70_0_g22436;
			float lerpResult91_g22436 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22436 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22436 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22436 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22436 = ( ( ( lerpResult91_g22436 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22437 = _DissolveObject26;
			float4 transform4_g22437 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22437 = transform4_g22437.y;
			float ifLocalVar46_g22437 = 0;
			if( _DissolveToggleDir26 > 0.0 )
				ifLocalVar46_g22437 = -Space24_g22437;
			else if( _DissolveToggleDir26 == 0.0 )
				ifLocalVar46_g22437 = Space24_g22437;
			float ObjectScale23_g22437 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22437 = exp2( _DissolveDensity );
			float temp_output_70_0_g22437 = ( ( (-1.0 + (DissolveModifier30_g22437 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22437 / ObjectScale23_g22437 ) ) * DissolveDensity67_g22437 );
			float DissolveOutputNoPattern286_g22437 = temp_output_70_0_g22437;
			float lerpResult91_g22437 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22437 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22437 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22437 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22437 = ( ( ( lerpResult91_g22437 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22459 = _DissolveObject27;
			float4 transform4_g22459 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22459 = transform4_g22459.y;
			float ifLocalVar46_g22459 = 0;
			if( _DissolveToggleDir27 > 0.0 )
				ifLocalVar46_g22459 = -Space24_g22459;
			else if( _DissolveToggleDir27 == 0.0 )
				ifLocalVar46_g22459 = Space24_g22459;
			float ObjectScale23_g22459 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22459 = exp2( _DissolveDensity );
			float temp_output_70_0_g22459 = ( ( (-1.0 + (DissolveModifier30_g22459 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22459 / ObjectScale23_g22459 ) ) * DissolveDensity67_g22459 );
			float DissolveOutputNoPattern286_g22459 = temp_output_70_0_g22459;
			float lerpResult91_g22459 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22459 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22459 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22459 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22459 = ( ( ( lerpResult91_g22459 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22424 = _DissolveObject28;
			float4 transform4_g22424 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22424 = transform4_g22424.y;
			float ifLocalVar46_g22424 = 0;
			if( _DissolveToggleDir28 > 0.0 )
				ifLocalVar46_g22424 = -Space24_g22424;
			else if( _DissolveToggleDir28 == 0.0 )
				ifLocalVar46_g22424 = Space24_g22424;
			float ObjectScale23_g22424 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22424 = exp2( _DissolveDensity );
			float temp_output_70_0_g22424 = ( ( (-1.0 + (DissolveModifier30_g22424 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22424 / ObjectScale23_g22424 ) ) * DissolveDensity67_g22424 );
			float DissolveOutputNoPattern286_g22424 = temp_output_70_0_g22424;
			float lerpResult91_g22424 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22424 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22424 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22424 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22424 = ( ( ( lerpResult91_g22424 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22422 = _DissolveObject29;
			float4 transform4_g22422 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22422 = transform4_g22422.y;
			float ifLocalVar46_g22422 = 0;
			if( _DissolveToggleDir29 > 0.0 )
				ifLocalVar46_g22422 = -Space24_g22422;
			else if( _DissolveToggleDir29 == 0.0 )
				ifLocalVar46_g22422 = Space24_g22422;
			float ObjectScale23_g22422 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22422 = exp2( _DissolveDensity );
			float temp_output_70_0_g22422 = ( ( (-1.0 + (DissolveModifier30_g22422 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22422 / ObjectScale23_g22422 ) ) * DissolveDensity67_g22422 );
			float DissolveOutputNoPattern286_g22422 = temp_output_70_0_g22422;
			float lerpResult91_g22422 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22422 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22422 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22422 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22422 = ( ( ( lerpResult91_g22422 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22440 = _DissolveObject30;
			float4 transform4_g22440 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22440 = transform4_g22440.y;
			float ifLocalVar46_g22440 = 0;
			if( _DissolveToggleDir30 > 0.0 )
				ifLocalVar46_g22440 = -Space24_g22440;
			else if( _DissolveToggleDir30 == 0.0 )
				ifLocalVar46_g22440 = Space24_g22440;
			float ObjectScale23_g22440 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22440 = exp2( _DissolveDensity );
			float temp_output_70_0_g22440 = ( ( (-1.0 + (DissolveModifier30_g22440 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22440 / ObjectScale23_g22440 ) ) * DissolveDensity67_g22440 );
			float DissolveOutputNoPattern286_g22440 = temp_output_70_0_g22440;
			float lerpResult91_g22440 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22440 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22440 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22440 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22440 = ( ( ( lerpResult91_g22440 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22454 = _DissolveObject11;
			float4 transform4_g22454 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22454 = transform4_g22454.y;
			float ifLocalVar46_g22454 = 0;
			if( _DissolveToggleDir11 > 0.0 )
				ifLocalVar46_g22454 = -Space24_g22454;
			else if( _DissolveToggleDir11 == 0.0 )
				ifLocalVar46_g22454 = Space24_g22454;
			float ObjectScale23_g22454 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22454 = exp2( _DissolveDensity );
			float temp_output_70_0_g22454 = ( ( (-1.0 + (DissolveModifier30_g22454 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22454 / ObjectScale23_g22454 ) ) * DissolveDensity67_g22454 );
			float DissolveOutputNoPattern286_g22454 = temp_output_70_0_g22454;
			float lerpResult91_g22454 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22454 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22454 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22454 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22454 = ( ( ( lerpResult91_g22454 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22460 = _DissolveObject12;
			float4 transform4_g22460 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22460 = transform4_g22460.y;
			float ifLocalVar46_g22460 = 0;
			if( _DissolveToggleDir12 > 0.0 )
				ifLocalVar46_g22460 = -Space24_g22460;
			else if( _DissolveToggleDir12 == 0.0 )
				ifLocalVar46_g22460 = Space24_g22460;
			float ObjectScale23_g22460 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22460 = exp2( _DissolveDensity );
			float temp_output_70_0_g22460 = ( ( (-1.0 + (DissolveModifier30_g22460 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22460 / ObjectScale23_g22460 ) ) * DissolveDensity67_g22460 );
			float DissolveOutputNoPattern286_g22460 = temp_output_70_0_g22460;
			float lerpResult91_g22460 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22460 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22460 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22460 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22460 = ( ( ( lerpResult91_g22460 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22428 = _DissolveObject13;
			float4 transform4_g22428 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22428 = transform4_g22428.y;
			float ifLocalVar46_g22428 = 0;
			if( _DissolveToggleDir13 > 0.0 )
				ifLocalVar46_g22428 = -Space24_g22428;
			else if( _DissolveToggleDir13 == 0.0 )
				ifLocalVar46_g22428 = Space24_g22428;
			float ObjectScale23_g22428 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22428 = exp2( _DissolveDensity );
			float temp_output_70_0_g22428 = ( ( (-1.0 + (DissolveModifier30_g22428 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22428 / ObjectScale23_g22428 ) ) * DissolveDensity67_g22428 );
			float DissolveOutputNoPattern286_g22428 = temp_output_70_0_g22428;
			float lerpResult91_g22428 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22428 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22428 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22428 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22428 = ( ( ( lerpResult91_g22428 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22431 = _DissolveObject14;
			float4 transform4_g22431 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22431 = transform4_g22431.y;
			float ifLocalVar46_g22431 = 0;
			if( _DissolveToggleDir14 > 0.0 )
				ifLocalVar46_g22431 = -Space24_g22431;
			else if( _DissolveToggleDir14 == 0.0 )
				ifLocalVar46_g22431 = Space24_g22431;
			float ObjectScale23_g22431 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22431 = exp2( _DissolveDensity );
			float temp_output_70_0_g22431 = ( ( (-1.0 + (DissolveModifier30_g22431 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22431 / ObjectScale23_g22431 ) ) * DissolveDensity67_g22431 );
			float DissolveOutputNoPattern286_g22431 = temp_output_70_0_g22431;
			float lerpResult91_g22431 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22431 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22431 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22431 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22431 = ( ( ( lerpResult91_g22431 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22457 = _DissolveObject15;
			float4 transform4_g22457 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22457 = transform4_g22457.y;
			float ifLocalVar46_g22457 = 0;
			if( _DissolveToggleDir15 > 0.0 )
				ifLocalVar46_g22457 = -Space24_g22457;
			else if( _DissolveToggleDir15 == 0.0 )
				ifLocalVar46_g22457 = Space24_g22457;
			float ObjectScale23_g22457 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22457 = exp2( _DissolveDensity );
			float temp_output_70_0_g22457 = ( ( (-1.0 + (DissolveModifier30_g22457 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22457 / ObjectScale23_g22457 ) ) * DissolveDensity67_g22457 );
			float DissolveOutputNoPattern286_g22457 = temp_output_70_0_g22457;
			float lerpResult91_g22457 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22457 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22457 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22457 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22457 = ( ( ( lerpResult91_g22457 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22455 = _DissolveObject16;
			float4 transform4_g22455 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22455 = transform4_g22455.y;
			float ifLocalVar46_g22455 = 0;
			if( _DissolveToggleDir16 > 0.0 )
				ifLocalVar46_g22455 = -Space24_g22455;
			else if( _DissolveToggleDir16 == 0.0 )
				ifLocalVar46_g22455 = Space24_g22455;
			float ObjectScale23_g22455 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22455 = exp2( _DissolveDensity );
			float temp_output_70_0_g22455 = ( ( (-1.0 + (DissolveModifier30_g22455 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22455 / ObjectScale23_g22455 ) ) * DissolveDensity67_g22455 );
			float DissolveOutputNoPattern286_g22455 = temp_output_70_0_g22455;
			float lerpResult91_g22455 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22455 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22455 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22455 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22455 = ( ( ( lerpResult91_g22455 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22439 = _DissolveObject17;
			float4 transform4_g22439 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22439 = transform4_g22439.y;
			float ifLocalVar46_g22439 = 0;
			if( _DissolveToggleDir17 > 0.0 )
				ifLocalVar46_g22439 = -Space24_g22439;
			else if( _DissolveToggleDir17 == 0.0 )
				ifLocalVar46_g22439 = Space24_g22439;
			float ObjectScale23_g22439 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22439 = exp2( _DissolveDensity );
			float temp_output_70_0_g22439 = ( ( (-1.0 + (DissolveModifier30_g22439 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22439 / ObjectScale23_g22439 ) ) * DissolveDensity67_g22439 );
			float DissolveOutputNoPattern286_g22439 = temp_output_70_0_g22439;
			float lerpResult91_g22439 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22439 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22439 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22439 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22439 = ( ( ( lerpResult91_g22439 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22446 = _DissolveObject18;
			float4 transform4_g22446 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22446 = transform4_g22446.y;
			float ifLocalVar46_g22446 = 0;
			if( _DissolveToggleDir18 > 0.0 )
				ifLocalVar46_g22446 = -Space24_g22446;
			else if( _DissolveToggleDir18 == 0.0 )
				ifLocalVar46_g22446 = Space24_g22446;
			float ObjectScale23_g22446 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22446 = exp2( _DissolveDensity );
			float temp_output_70_0_g22446 = ( ( (-1.0 + (DissolveModifier30_g22446 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22446 / ObjectScale23_g22446 ) ) * DissolveDensity67_g22446 );
			float DissolveOutputNoPattern286_g22446 = temp_output_70_0_g22446;
			float lerpResult91_g22446 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22446 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22446 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22446 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22446 = ( ( ( lerpResult91_g22446 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22443 = _DissolveObject19;
			float4 transform4_g22443 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22443 = transform4_g22443.y;
			float ifLocalVar46_g22443 = 0;
			if( _DissolveToggleDir19 > 0.0 )
				ifLocalVar46_g22443 = -Space24_g22443;
			else if( _DissolveToggleDir19 == 0.0 )
				ifLocalVar46_g22443 = Space24_g22443;
			float ObjectScale23_g22443 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22443 = exp2( _DissolveDensity );
			float temp_output_70_0_g22443 = ( ( (-1.0 + (DissolveModifier30_g22443 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22443 / ObjectScale23_g22443 ) ) * DissolveDensity67_g22443 );
			float DissolveOutputNoPattern286_g22443 = temp_output_70_0_g22443;
			float lerpResult91_g22443 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22443 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22443 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22443 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22443 = ( ( ( lerpResult91_g22443 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22445 = _DissolveObject20;
			float4 transform4_g22445 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22445 = transform4_g22445.y;
			float ifLocalVar46_g22445 = 0;
			if( _DissolveToggleDir20 > 0.0 )
				ifLocalVar46_g22445 = -Space24_g22445;
			else if( _DissolveToggleDir20 == 0.0 )
				ifLocalVar46_g22445 = Space24_g22445;
			float ObjectScale23_g22445 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22445 = exp2( _DissolveDensity );
			float temp_output_70_0_g22445 = ( ( (-1.0 + (DissolveModifier30_g22445 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22445 / ObjectScale23_g22445 ) ) * DissolveDensity67_g22445 );
			float DissolveOutputNoPattern286_g22445 = temp_output_70_0_g22445;
			float lerpResult91_g22445 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22445 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22445 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22445 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22445 = ( ( ( lerpResult91_g22445 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22390 = _DissolveObject1;
			float4 transform4_g22390 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22390 = transform4_g22390.y;
			float ifLocalVar46_g22390 = 0;
			if( _DissolveToggleDir1 > 0.0 )
				ifLocalVar46_g22390 = -Space24_g22390;
			else if( _DissolveToggleDir1 == 0.0 )
				ifLocalVar46_g22390 = Space24_g22390;
			float ObjectScale23_g22390 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22390 = exp2( _DissolveDensity );
			float temp_output_70_0_g22390 = ( ( (-1.0 + (DissolveModifier30_g22390 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22390 / ObjectScale23_g22390 ) ) * DissolveDensity67_g22390 );
			float DissolveOutputNoPattern286_g22390 = temp_output_70_0_g22390;
			float lerpResult91_g22390 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22390 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22390 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22390 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22390 = ( ( ( lerpResult91_g22390 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22429 = _DissolveObject2;
			float4 transform4_g22429 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22429 = transform4_g22429.y;
			float ifLocalVar46_g22429 = 0;
			if( _DissolveToggleDir2 > 0.0 )
				ifLocalVar46_g22429 = -Space24_g22429;
			else if( _DissolveToggleDir2 == 0.0 )
				ifLocalVar46_g22429 = Space24_g22429;
			float ObjectScale23_g22429 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22429 = exp2( _DissolveDensity );
			float temp_output_70_0_g22429 = ( ( (-1.0 + (DissolveModifier30_g22429 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22429 / ObjectScale23_g22429 ) ) * DissolveDensity67_g22429 );
			float DissolveOutputNoPattern286_g22429 = temp_output_70_0_g22429;
			float lerpResult91_g22429 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22429 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22429 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22429 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22429 = ( ( ( lerpResult91_g22429 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22452 = _DissolveObject3;
			float4 transform4_g22452 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22452 = transform4_g22452.y;
			float ifLocalVar46_g22452 = 0;
			if( _DissolveToggleDir3 > 0.0 )
				ifLocalVar46_g22452 = -Space24_g22452;
			else if( _DissolveToggleDir3 == 0.0 )
				ifLocalVar46_g22452 = Space24_g22452;
			float ObjectScale23_g22452 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22452 = exp2( _DissolveDensity );
			float temp_output_70_0_g22452 = ( ( (-1.0 + (DissolveModifier30_g22452 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22452 / ObjectScale23_g22452 ) ) * DissolveDensity67_g22452 );
			float DissolveOutputNoPattern286_g22452 = temp_output_70_0_g22452;
			float lerpResult91_g22452 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22452 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22452 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22452 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22452 = ( ( ( lerpResult91_g22452 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22432 = _DissolveObject4;
			float4 transform4_g22432 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22432 = transform4_g22432.y;
			float ifLocalVar46_g22432 = 0;
			if( _DissolveToggleDir4 > 0.0 )
				ifLocalVar46_g22432 = -Space24_g22432;
			else if( _DissolveToggleDir4 == 0.0 )
				ifLocalVar46_g22432 = Space24_g22432;
			float ObjectScale23_g22432 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22432 = exp2( _DissolveDensity );
			float temp_output_70_0_g22432 = ( ( (-1.0 + (DissolveModifier30_g22432 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22432 / ObjectScale23_g22432 ) ) * DissolveDensity67_g22432 );
			float DissolveOutputNoPattern286_g22432 = temp_output_70_0_g22432;
			float lerpResult91_g22432 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22432 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22432 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22432 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22432 = ( ( ( lerpResult91_g22432 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22449 = _DissolveObject5;
			float4 transform4_g22449 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22449 = transform4_g22449.y;
			float ifLocalVar46_g22449 = 0;
			if( _DissolveToggleDir5 > 0.0 )
				ifLocalVar46_g22449 = -Space24_g22449;
			else if( _DissolveToggleDir5 == 0.0 )
				ifLocalVar46_g22449 = Space24_g22449;
			float ObjectScale23_g22449 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22449 = exp2( _DissolveDensity );
			float temp_output_70_0_g22449 = ( ( (-1.0 + (DissolveModifier30_g22449 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22449 / ObjectScale23_g22449 ) ) * DissolveDensity67_g22449 );
			float DissolveOutputNoPattern286_g22449 = temp_output_70_0_g22449;
			float lerpResult91_g22449 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22449 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22449 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22449 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22449 = ( ( ( lerpResult91_g22449 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22434 = _DissolveObject6;
			float4 transform4_g22434 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22434 = transform4_g22434.y;
			float ifLocalVar46_g22434 = 0;
			if( _DissolveToggleDir6 > 0.0 )
				ifLocalVar46_g22434 = -Space24_g22434;
			else if( _DissolveToggleDir6 == 0.0 )
				ifLocalVar46_g22434 = Space24_g22434;
			float ObjectScale23_g22434 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22434 = exp2( _DissolveDensity );
			float temp_output_70_0_g22434 = ( ( (-1.0 + (DissolveModifier30_g22434 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22434 / ObjectScale23_g22434 ) ) * DissolveDensity67_g22434 );
			float DissolveOutputNoPattern286_g22434 = temp_output_70_0_g22434;
			float lerpResult91_g22434 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22434 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22434 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22434 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22434 = ( ( ( lerpResult91_g22434 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22453 = _DissolveObject7;
			float4 transform4_g22453 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22453 = transform4_g22453.y;
			float ifLocalVar46_g22453 = 0;
			if( _DissolveToggleDir7 > 0.0 )
				ifLocalVar46_g22453 = -Space24_g22453;
			else if( _DissolveToggleDir7 == 0.0 )
				ifLocalVar46_g22453 = Space24_g22453;
			float ObjectScale23_g22453 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22453 = exp2( _DissolveDensity );
			float temp_output_70_0_g22453 = ( ( (-1.0 + (DissolveModifier30_g22453 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22453 / ObjectScale23_g22453 ) ) * DissolveDensity67_g22453 );
			float DissolveOutputNoPattern286_g22453 = temp_output_70_0_g22453;
			float lerpResult91_g22453 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22453 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22453 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22453 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22453 = ( ( ( lerpResult91_g22453 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22444 = _DissolveObject8;
			float4 transform4_g22444 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22444 = transform4_g22444.y;
			float ifLocalVar46_g22444 = 0;
			if( _DissolveToggleDir8 > 0.0 )
				ifLocalVar46_g22444 = -Space24_g22444;
			else if( _DissolveToggleDir8 == 0.0 )
				ifLocalVar46_g22444 = Space24_g22444;
			float ObjectScale23_g22444 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22444 = exp2( _DissolveDensity );
			float temp_output_70_0_g22444 = ( ( (-1.0 + (DissolveModifier30_g22444 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22444 / ObjectScale23_g22444 ) ) * DissolveDensity67_g22444 );
			float DissolveOutputNoPattern286_g22444 = temp_output_70_0_g22444;
			float lerpResult91_g22444 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22444 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22444 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22444 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22444 = ( ( ( lerpResult91_g22444 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22426 = _DissolveObject9;
			float4 transform4_g22426 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22426 = transform4_g22426.y;
			float ifLocalVar46_g22426 = 0;
			if( _DissolveToggleDir9 > 0.0 )
				ifLocalVar46_g22426 = -Space24_g22426;
			else if( _DissolveToggleDir9 == 0.0 )
				ifLocalVar46_g22426 = Space24_g22426;
			float ObjectScale23_g22426 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22426 = exp2( _DissolveDensity );
			float temp_output_70_0_g22426 = ( ( (-1.0 + (DissolveModifier30_g22426 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22426 / ObjectScale23_g22426 ) ) * DissolveDensity67_g22426 );
			float DissolveOutputNoPattern286_g22426 = temp_output_70_0_g22426;
			float lerpResult91_g22426 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22426 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22426 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22426 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22426 = ( ( ( lerpResult91_g22426 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22425 = _DissolveObject10;
			float4 transform4_g22425 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22425 = transform4_g22425.y;
			float ifLocalVar46_g22425 = 0;
			if( _DissolveToggleDir10 > 0.0 )
				ifLocalVar46_g22425 = -Space24_g22425;
			else if( _DissolveToggleDir10 == 0.0 )
				ifLocalVar46_g22425 = Space24_g22425;
			float ObjectScale23_g22425 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22425 = exp2( _DissolveDensity );
			float temp_output_70_0_g22425 = ( ( (-1.0 + (DissolveModifier30_g22425 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22425 / ObjectScale23_g22425 ) ) * DissolveDensity67_g22425 );
			float DissolveOutputNoPattern286_g22425 = temp_output_70_0_g22425;
			float lerpResult91_g22425 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22425 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22425 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22425 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22425 = ( ( ( lerpResult91_g22425 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float DissolveModifier30_g22433 = _GlobalDissolve;
			float4 transform4_g22433 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22433 = transform4_g22433.y;
			float ifLocalVar46_g22433 = 0;
			if( _GlobalDissolveDir > 0.0 )
				ifLocalVar46_g22433 = -Space24_g22433;
			else if( _GlobalDissolveDir == 0.0 )
				ifLocalVar46_g22433 = Space24_g22433;
			float ObjectScale23_g22433 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22433 = exp2( _DissolveDensity );
			float temp_output_70_0_g22433 = ( ( (-1.0 + (DissolveModifier30_g22433 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22433 / ObjectScale23_g22433 ) ) * DissolveDensity67_g22433 );
			float DissolveOutputNoPattern286_g22433 = temp_output_70_0_g22433;
			float lerpResult91_g22433 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22433 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22433 - _EmissiveScale ) ) ));
			float3 ifLocalVar96_g22433 = 0;
			if( _ToggleDissolveVertexOffset == 1.0 )
				ifLocalVar96_g22433 = ( ( ( lerpResult91_g22433 * 4.0 ) * _DissolveVertexMultiplier ) * ase_vertexNormal );
			float3 GlobalVertexOffsetDissolve940_g22378 = ifLocalVar96_g22433;
			float3 VertexOffsetDissolve21892 = ( ( ifLocalVar96_g22441 + ifLocalVar96_g22448 + ifLocalVar96_g22447 + ifLocalVar96_g22442 + ifLocalVar96_g22430 + ifLocalVar96_g22427 + ifLocalVar96_g22435 + ifLocalVar96_g22438 + ifLocalVar96_g22456 + ifLocalVar96_g22411 ) + ( ifLocalVar96_g22458 + ifLocalVar96_g22450 + ifLocalVar96_g22451 + ifLocalVar96_g22396 + ifLocalVar96_g22436 + ifLocalVar96_g22437 + ifLocalVar96_g22459 + ifLocalVar96_g22424 + ifLocalVar96_g22422 + ifLocalVar96_g22440 ) + ( ifLocalVar96_g22454 + ifLocalVar96_g22460 + ifLocalVar96_g22428 + ifLocalVar96_g22431 + ifLocalVar96_g22457 + ifLocalVar96_g22455 + ifLocalVar96_g22439 + ifLocalVar96_g22446 + ifLocalVar96_g22443 + ifLocalVar96_g22445 ) + ( ifLocalVar96_g22390 + ifLocalVar96_g22429 + ifLocalVar96_g22452 + ifLocalVar96_g22432 + ifLocalVar96_g22449 + ifLocalVar96_g22434 + ifLocalVar96_g22453 + ifLocalVar96_g22444 + ifLocalVar96_g22426 + ifLocalVar96_g22425 ) + GlobalVertexOffsetDissolve940_g22378 );
			v.vertex.xyz += VertexOffsetDissolve21892;
			v.vertex.w = 1;
			float3 In02226_g22168 = ase_vertexNormal;
			float3 localgetUnityObjectToWorldNormal2226_g22168 = getUnityObjectToWorldNormal2226_g22168( In02226_g22168 );
			float4 ase_vertexTangent = v.tangent;
			float3 In02223_g22168 = ase_vertexTangent.xyz;
			float3 localgetUnityObjectToWorldDir2223_g22168 = getUnityObjectToWorldDir2223_g22168( In02223_g22168 );
			float3 temp_output_2222_0_g22168 = (localgetUnityObjectToWorldDir2223_g22168).xyz;
			float ase_vertexTangentSign = v.tangent.w;
			float3 normalizeResult2248_g22168 = normalize( ( cross( localgetUnityObjectToWorldNormal2226_g22168 , temp_output_2222_0_g22168 ) * ase_vertexTangentSign ) );
			o.vertexToFrag2250_g22168 = normalizeResult2248_g22168;
			float3 normalizeResult2249_g22168 = normalize( temp_output_2222_0_g22168 );
			o.vertexToFrag2251_g22168 = normalizeResult2249_g22168;
		}

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			float ifLocalVar25147 = 0;
			if( _ModeCustom == 1.0 )
				ifLocalVar25147 = ( _ModeCustom + 3.0 );
			else if( _ModeCustom < 1.0 )
				ifLocalVar25147 = _Mode;
			float UVSwitchProp24660 = _MainTexUVSwitch;
			float2 UV024660 = i.uv_texcoord;
			float2 UV124660 = i.uv2_texcoord2;
			float2 UV224660 = i.uv3_texcoord3;
			float2 UV324660 = i.uv4_texcoord4;
			float2 localUVSwitch24660 = UVSwitch( UVSwitchProp24660 , UV024660 , UV124660 , UV224660 , UV324660 );
			float2 MainTexUVSwitch24638 = ( ( _MainTex_ST.xy * localUVSwitch24660 ) + _MainTex_ST.zw );
			float4 tex2DNode43 = SAMPLE_TEXTURE2D( _MainTex, sampler_MainTex, MainTexUVSwitch24638 );
			float AlphaChannelMul9091 = ( tex2DNode43.a * _MainColor.a );
			float2 uv_TanTexture = i.uv_texcoord * _TanTexture_ST.xy + _TanTexture_ST.zw;
			float4 blendOpSrc166_g352 = SAMPLE_TEXTURE2D( _TanTexture, sampler_trilinear_repeat, uv_TanTexture );
			float4 blendOpDest166_g352 = tex2DNode43;
			float4 lerpBlendMode166_g352 = lerp(blendOpDest166_g352,( blendOpSrc166_g352 * blendOpDest166_g352 ),_TanPower);
			float4 temp_output_163_0_g353 = ( saturate( lerpBlendMode166_g352 ));
			float2 uv3_ColorDiffuseSampler = i.uv3_texcoord3 * _ColorDiffuseSampler_ST.xy + _ColorDiffuseSampler_ST.zw;
			float4 tex2DNode36_g353 = SAMPLE_TEXTURE2D( _ColorDiffuseSampler, sampler_trilinear_repeat, uv3_ColorDiffuseSampler );
			float4 lerpResult191_g353 = lerp( temp_output_163_0_g353 , float4( 0,0,0,0 ) , ( 1.0 - tex2DNode36_g353.a ));
			float MaskRed100_g353 = tex2DNode36_g353.r;
			float temp_output_25_0_g363 = 0.0;
			float temp_output_25_0_g368 = ( temp_output_25_0_g363 + 0.2 );
			float temp_output_22_0_g368 = step( MaskRed100_g353 , temp_output_25_0_g368 );
			float temp_output_22_0_g363 = step( MaskRed100_g353 , temp_output_25_0_g363 );
			float temp_output_25_0_g361 = ( temp_output_25_0_g368 + 0.2 );
			float temp_output_22_0_g361 = step( MaskRed100_g353 , temp_output_25_0_g361 );
			float temp_output_25_0_g360 = ( temp_output_25_0_g361 + 0.2 );
			float temp_output_22_0_g360 = step( MaskRed100_g353 , temp_output_25_0_g360 );
			float temp_output_25_0_g364 = ( temp_output_25_0_g360 + 0.2 );
			float temp_output_22_0_g364 = step( MaskRed100_g353 , temp_output_25_0_g364 );
			float MaskGreen103_g353 = tex2DNode36_g353.g;
			float temp_output_25_0_g356 = 0.0;
			float temp_output_25_0_g354 = ( temp_output_25_0_g356 + 0.2 );
			float temp_output_22_0_g354 = step( MaskGreen103_g353 , temp_output_25_0_g354 );
			float temp_output_22_0_g356 = step( MaskGreen103_g353 , temp_output_25_0_g356 );
			float temp_output_25_0_g358 = ( temp_output_25_0_g354 + 0.2 );
			float temp_output_22_0_g358 = step( MaskGreen103_g353 , temp_output_25_0_g358 );
			float temp_output_25_0_g355 = ( temp_output_25_0_g358 + 0.2 );
			float temp_output_22_0_g355 = step( MaskGreen103_g353 , temp_output_25_0_g355 );
			float temp_output_25_0_g366 = ( temp_output_25_0_g355 + 0.2 );
			float temp_output_22_0_g366 = step( MaskRed100_g353 , temp_output_25_0_g366 );
			float MaskBlue102_g353 = tex2DNode36_g353.b;
			float temp_output_25_0_g357 = 0.0;
			float temp_output_25_0_g365 = ( temp_output_25_0_g357 + 0.2 );
			float temp_output_22_0_g365 = step( MaskBlue102_g353 , temp_output_25_0_g365 );
			float temp_output_22_0_g357 = step( MaskBlue102_g353 , temp_output_25_0_g357 );
			float temp_output_25_0_g367 = ( temp_output_25_0_g365 + 0.2 );
			float temp_output_22_0_g367 = step( MaskBlue102_g353 , temp_output_25_0_g367 );
			float temp_output_25_0_g362 = ( temp_output_25_0_g367 + 0.2 );
			float temp_output_22_0_g362 = step( MaskBlue102_g353 , temp_output_25_0_g362 );
			float temp_output_25_0_g359 = ( temp_output_25_0_g362 + 0.2 );
			float temp_output_22_0_g359 = step( MaskBlue102_g353 , temp_output_25_0_g359 );
			float4 ColorOutPut161_g353 = ( ( ( ( temp_output_22_0_g368 - temp_output_22_0_g363 ) * _RedMaskColor1 ) + ( _RedMaskColor2 * ( temp_output_22_0_g361 - temp_output_22_0_g368 ) ) + ( _RedMaskColor3 * ( temp_output_22_0_g360 - temp_output_22_0_g361 ) ) + ( _RedMaskColor4 * ( temp_output_22_0_g364 - temp_output_22_0_g360 ) ) ) + ( ( ( temp_output_22_0_g354 - temp_output_22_0_g356 ) * _GreenMaskColor1 ) + ( _GreenMaskColor2 * ( temp_output_22_0_g358 - temp_output_22_0_g354 ) ) + ( _GreenMaskColor3 * ( temp_output_22_0_g355 - temp_output_22_0_g358 ) ) + ( _GreenMaskColor4 * ( temp_output_22_0_g366 - temp_output_22_0_g355 ) ) ) + ( ( ( temp_output_22_0_g365 - temp_output_22_0_g357 ) * _BlueMaskColor1 ) + ( _BlueMaskColor2 * ( temp_output_22_0_g367 - temp_output_22_0_g365 ) ) + ( _BlueMaskColor3 * ( temp_output_22_0_g362 - temp_output_22_0_g367 ) ) + ( _BlueMaskColor4 * ( temp_output_22_0_g359 - temp_output_22_0_g362 ) ) ) );
			float4 lerpResult158_g353 = lerp( temp_output_163_0_g353 , ColorOutPut161_g353 , _CustomDiffuseLerp);
			float4 lerpResult194_g353 = lerp( temp_output_163_0_g353 , ( lerpResult191_g353 + lerpResult158_g353 ) , _CustomDiffuseLerp);
			float2 uv_TexCoord130_g369 = i.uv_texcoord * _LipScale + _LipPosition;
			float cos133_g369 = cos( ( _LipRotation * UNITY_PI ) );
			float sin133_g369 = sin( ( _LipRotation * UNITY_PI ) );
			float2 rotator133_g369 = mul( uv_TexCoord130_g369 - float2( 0.5,0.5 ) , float2x2( cos133_g369 , -sin133_g369 , sin133_g369 , cos133_g369 )) + float2( 0.5,0.5 );
			float2 _Vec001 = float2(0,0);
			float2 temp_output_119_0_g369 = ( 1.0 - float2( 1,1 ) );
			float2 temp_output_120_0_g369 = ( 2.0 + float2( 0,0 ) );
			float2 _Vec111 = float2(1,1);
			float4 tex2DNode99_g369 = SAMPLE_TEXTURE2D( _MakeupLipThick, sampler_trilinear_repeat, (_Vec001 + (rotator133_g369 - ( _Vec001 + ( temp_output_119_0_g369 / temp_output_120_0_g369 ) )) * (_Vec111 - _Vec001) / (( _Vec111 - ( temp_output_119_0_g369 / temp_output_120_0_g369 ) ) - ( _Vec001 + ( temp_output_119_0_g369 / temp_output_120_0_g369 ) ))) );
			float4 temp_cast_63 = (( _MakeupLipThickness * tex2DNode99_g369.a )).xxxx;
			float4 color112_g369 = IsGammaSpace() ? float4(0,0,0,0) : float4(0,0,0,0);
			float4 ifLocalVar115_g369 = 0;
			if( _ToggleMakeup > 0.0 )
				ifLocalVar115_g369 = temp_cast_63;
			else if( _ToggleMakeup == 0.0 )
				ifLocalVar115_g369 = color112_g369;
			float4 lerpResult102_g369 = lerp( float4( lerpResult194_g353.rgb , 0.0 ) , tex2DNode99_g369 , ifLocalVar115_g369);
			float2 uv_MakeupMask = i.uv_texcoord * _MakeupMask_ST.xy + _MakeupMask_ST.zw;
			float4 tex2DNode39_g369 = SAMPLE_TEXTURE2D( _MakeupMask, sampler_trilinear_repeat, uv_MakeupMask );
			float MakeUpGreen42_g369 = tex2DNode39_g369.g;
			float4 MakeUpGreenOut66_g369 = ( _MakeUpGreenColor * MakeUpGreen42_g369 );
			float MakeUpBlue45_g369 = tex2DNode39_g369.b;
			float4 MajkeUpBlueOut73_g369 = ( _MakeUpBlueColor * MakeUpBlue45_g369 );
			float MakeUpRed41_g369 = tex2DNode39_g369.r;
			float MakeUpLipThickness101_g369 = tex2DNode99_g369.a;
			float lerpResult104_g369 = lerp( MakeUpRed41_g369 , MakeUpLipThickness101_g369 , _MakeupLipThickness);
			float4 MajkeUpRedOut71_g369 = ( _MakeUpRedColor * lerpResult104_g369 );
			float MakeUPRedAlpha63_g369 = ( lerpResult104_g369 * _MakeUpRedPower );
			float MakeUpGreenAlpha60_g369 = ( MakeUpGreen42_g369 * _MakeUpGreenPower );
			float MakeUPBlueAlpha61_g369 = ( MakeUpBlue45_g369 * _MakeUpBluePower );
			float MakeUpAlphaMask76_g369 = ( MakeUPRedAlpha63_g369 + MakeUpGreenAlpha60_g369 + MakeUPBlueAlpha61_g369 );
			float4 temp_cast_64 = (MakeUpAlphaMask76_g369).xxxx;
			float4 ifLocalVar113_g369 = 0;
			if( _ToggleMakeup > 0.0 )
				ifLocalVar113_g369 = temp_cast_64;
			else if( _ToggleMakeup == 0.0 )
				ifLocalVar113_g369 = color112_g369;
			float4 lerpResult84_g369 = lerp( lerpResult102_g369 , ( MakeUpGreenOut66_g369 + MajkeUpBlueOut73_g369 + MajkeUpRedOut71_g369 ) , ifLocalVar113_g369);
			float2 uv_HairTexture = i.uv_texcoord * _HairTexture_ST.xy + _HairTexture_ST.zw;
			float4 tex2DNode108_g370 = SAMPLE_TEXTURE2D( _HairTexture, sampler_trilinear_repeat, uv_HairTexture );
			float4 blendOpSrc136_g370 = tex2DNode108_g370;
			float4 blendOpDest136_g370 = _HairColor;
			float2 uv_HairMask = i.uv_texcoord * _HairMask_ST.xy + _HairMask_ST.zw;
			float4 HairMask28_g370 = SAMPLE_TEXTURE2D( _HairMask, sampler_trilinear_repeat, uv_HairMask );
			float4 ifLocalVar19_g370 = 0;
			if( _ToggleHair > 0.0 )
				ifLocalVar19_g370 = HairMask28_g370;
			float4 lerpResult15_g370 = lerp( lerpResult84_g369 , (( blendOpDest136_g370 > 0.5 ) ? ( 1.0 - 2.0 * ( 1.0 - blendOpDest136_g370 ) * ( 1.0 - blendOpSrc136_g370 ) ) : ( 2.0 * blendOpDest136_g370 * blendOpSrc136_g370 ) ) , ifLocalVar19_g370);
			float3 Texture18_g21920 = lerpResult15_g370.rgb;
			float grayscale5_g21920 = (Texture18_g21920.r + Texture18_g21920.g + Texture18_g21920.b) / 3;
			float UVSwitchProp257_g21920 = _HueMaskUVSwitch;
			float2 UV0257_g21920 = i.uv_texcoord;
			float2 UV1257_g21920 = i.uv2_texcoord2;
			float2 UV2257_g21920 = i.uv3_texcoord3;
			float2 UV3257_g21920 = i.uv4_texcoord4;
			float2 localUVSwitch257_g21920 = UVSwitch( UVSwitchProp257_g21920 , UV0257_g21920 , UV1257_g21920 , UV2257_g21920 , UV3257_g21920 );
			float2 temp_output_252_0_g21920 = ( ( _HueMask_ST.xy * localUVSwitch257_g21920 ) + _HueMask_ST.zw );
			float HueMaskG53_g21920 = SAMPLE_TEXTURE2D( _HueMask, sampler_point_clamp, temp_output_252_0_g21920 ).g;
			float ifLocalVar218_g21920 = 0;
			if( 1.0 > _ToggleHueTexforSpeed )
				ifLocalVar218_g21920 = 1.0;
			else if( 1.0 == _ToggleHueTexforSpeed )
				ifLocalVar218_g21920 = HueMaskG53_g21920;
			float mulTime2_g21920 = _Time.y * ( _HueShiftSpeed * ifLocalVar218_g21920 );
			float3 hsvTorgb3_g21921 = HSVToRGB( float3(( mulTime2_g21920 + _HueShiftRandomizer ),1.0,1.0) );
			float3 ifLocalVar13_g21920 = 0;
			if( ( _HueShiftSpeed + _HueShiftRandomizer ) > 0.0 )
				ifLocalVar13_g21920 = ( grayscale5_g21920 * hsvTorgb3_g21921 );
			else if( ( _HueShiftSpeed + _HueShiftRandomizer ) == 0.0 )
				ifLocalVar13_g21920 = Texture18_g21920;
			float HueMaskR52_g21920 = SAMPLE_TEXTURE2D( _HueMask, sampler_trilinear_repeat, temp_output_252_0_g21920 ).r;
			float ifLocalVar9_g21920 = 0;
			if( 1.0 > _HueMaskinverter )
				ifLocalVar9_g21920 = HueMaskR52_g21920;
			else if( 1.0 == _HueMaskinverter )
				ifLocalVar9_g21920 = ( 1.0 - HueMaskR52_g21920 );
			float lerpResult15_g21920 = lerp( 0.0 , ifLocalVar9_g21920 , _HueShiftblend);
			float3 lerpResult16_g21920 = lerp( Texture18_g21920 , ifLocalVar13_g21920 , lerpResult15_g21920);
			clip( AlphaChannelMul9091 - _Cutout);
			float3 desaturateInitialColor626 = ( lerpResult16_g21920 * (_MainColor).rgb );
			float desaturateDot626 = dot( desaturateInitialColor626, float3( 0.299, 0.587, 0.114 ));
			float3 desaturateVar626 = lerp( desaturateInitialColor626, desaturateDot626.xxx, ( 1.0 - _Saturation ) );
			float3 MainTexSaturate2197 = desaturateVar626;
			float3 MainTex312_g22174 = MainTexSaturate2197;
			float LinearIn2768_g22174 = _Metallic;
			float localgetGammaToLinearSpaceExact2768_g22174 = getGammaToLinearSpaceExact( LinearIn2768_g22174 );
			float Metallic302_g22174 = localgetGammaToLinearSpaceExact2768_g22174;
			float2 uv_MetallicMap = i.uv_texcoord * _MetallicMap_ST.xy + _MetallicMap_ST.zw;
			float4 tex2DNode2688_g22174 = SAMPLE_TEXTURE2D( _MetallicMap, sampler_trilinear_repeat, uv_MetallicMap );
			float MetallicTex289_g22174 = ( Metallic302_g22174 * tex2DNode2688_g22174.r );
			half3 specColor2324_g22174 = (0).xxx;
			half oneMinusReflectivity2324_g22174 = 0;
			half3 diffuseAndSpecularFromMetallic2324_g22174 = DiffuseAndSpecularFromMetallic(MainTex312_g22174,MetallicTex289_g22174,specColor2324_g22174,oneMinusReflectivity2324_g22174);
			float OneMinusReflectivity2718_g22174 = oneMinusReflectivity2324_g22174;
			float OneMinusReflectivity25129 = OneMinusReflectivity2718_g22174;
			float ToggleAdvanced21493 = _COLORCOLOR;
			float ToggleCubemap21525 = _COLORADDSUBDIFF;
			float lerpResult25114 = lerp( 1.0 , OneMinusReflectivity25129 , ( ToggleAdvanced21493 * ToggleCubemap21525 ));
			float ifLocalVar25111 = 0;
			if( ifLocalVar25147 > 2.0 )
				ifLocalVar25111 = ( ( AlphaChannelMul9091 * lerpResult25114 ) + ( 1.0 - lerpResult25114 ) );
			else if( ifLocalVar25147 == 2.0 )
				ifLocalVar25111 = AlphaChannelMul9091;
			else if( ifLocalVar25147 < 2.0 )
				ifLocalVar25111 = 1.0;
			float FinalAlphaOut25135 = ifLocalVar25111;
			float4 color905_g22378 = IsGammaSpace() ? float4(1,1,1,0) : float4(1,1,1,0);
			float2 uv3_DissolveMask = i.uv3_texcoord3 * _DissolveMask_ST.xy + _DissolveMask_ST.zw;
			float4 temp_cast_68 = (SAMPLE_TEXTURE2D( _DissolveMask, sampler_trilinear_repeat, uv3_DissolveMask ).r).xxxx;
			float4 ifLocalVar161_g22378 = 0;
			if( _MaterializeVertexColor > 0.0 )
				ifLocalVar161_g22378 = i.vertexColor;
			else if( _MaterializeVertexColor == 0.0 )
				ifLocalVar161_g22378 = temp_cast_68;
			float4 MaterialzeMask165_g22378 = ifLocalVar161_g22378;
			float DissolveModifier30_g22441 = _DissolveObject31;
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float4 transform4_g22441 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22441 = transform4_g22441.y;
			float ifLocalVar46_g22441 = 0;
			if( _DissolveToggleDir31 > 0.0 )
				ifLocalVar46_g22441 = -Space24_g22441;
			else if( _DissolveToggleDir31 == 0.0 )
				ifLocalVar46_g22441 = Space24_g22441;
			float3 ase_objectScale = float3( length( unity_ObjectToWorld[ 0 ].xyz ), length( unity_ObjectToWorld[ 1 ].xyz ), length( unity_ObjectToWorld[ 2 ].xyz ) );
			float ObjectScale23_g22441 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22441 = exp2( _DissolveDensity );
			float temp_output_70_0_g22441 = ( ( (-1.0 + (DissolveModifier30_g22441 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22441 / ObjectScale23_g22441 ) ) * DissolveDensity67_g22441 );
			float2 uv_DissolvePattern = i.uv_texcoord * _DissolvePattern_ST.xy + _DissolvePattern_ST.zw;
			float4 tex2DNode61_g22441 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22441 = max( max( tex2DNode61_g22441.r , tex2DNode61_g22441.g ) , tex2DNode61_g22441.b );
			float temp_output_36_0_g22441 = saturate( ( temp_output_70_0_g22441 + DissolvePattern58_g22441 ) );
			float temp_output_19_0_g22414 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22414 = ( 31.0 / 100.0 );
			float temp_output_8_0_g22414 = ( temp_output_6_0_g22414 - 0.005 );
			float temp_output_7_0_g22414 = ( temp_output_6_0_g22414 + 0.005 );
			float2 appendResult4_g22414 = (float2(temp_output_8_0_g22414 , temp_output_7_0_g22414));
			float2 appendResult13_g22414 = (float2(-0.005 , temp_output_7_0_g22414));
			float2 appendResult14_g22414 = (float2(temp_output_8_0_g22414 , 1.005));
			float2 ifLocalVar11_g22414 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22414 = appendResult4_g22414;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22414 = appendResult13_g22414;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22414 = appendResult14_g22414;
			float2 break15_g22414 = ifLocalVar11_g22414;
			float4 appendResult3_g22414 = (float4(break15_g22414.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22414 = (float4(break15_g22414.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22441 = lerp( temp_output_36_0_g22441 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22414 >= appendResult3_g22414.x && temp_output_19_0_g22414 <= appendResult16_g22414.x ) ? temp_output_19_0_g22414 :  0.0 ) ) ));
			float DissolveModifier30_g22448 = _DissolveObject32;
			float4 transform4_g22448 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22448 = transform4_g22448.y;
			float ifLocalVar46_g22448 = 0;
			if( _DissolveToggleDir32 > 0.0 )
				ifLocalVar46_g22448 = -Space24_g22448;
			else if( _DissolveToggleDir32 == 0.0 )
				ifLocalVar46_g22448 = Space24_g22448;
			float ObjectScale23_g22448 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22448 = exp2( _DissolveDensity );
			float temp_output_70_0_g22448 = ( ( (-1.0 + (DissolveModifier30_g22448 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22448 / ObjectScale23_g22448 ) ) * DissolveDensity67_g22448 );
			float4 tex2DNode61_g22448 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22448 = max( max( tex2DNode61_g22448.r , tex2DNode61_g22448.g ) , tex2DNode61_g22448.b );
			float temp_output_36_0_g22448 = saturate( ( temp_output_70_0_g22448 + DissolvePattern58_g22448 ) );
			float temp_output_19_0_g22398 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22398 = ( 32.0 / 100.0 );
			float temp_output_8_0_g22398 = ( temp_output_6_0_g22398 - 0.005 );
			float temp_output_7_0_g22398 = ( temp_output_6_0_g22398 + 0.005 );
			float2 appendResult4_g22398 = (float2(temp_output_8_0_g22398 , temp_output_7_0_g22398));
			float2 appendResult13_g22398 = (float2(-0.005 , temp_output_7_0_g22398));
			float2 appendResult14_g22398 = (float2(temp_output_8_0_g22398 , 1.005));
			float2 ifLocalVar11_g22398 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22398 = appendResult4_g22398;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22398 = appendResult13_g22398;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22398 = appendResult14_g22398;
			float2 break15_g22398 = ifLocalVar11_g22398;
			float4 appendResult3_g22398 = (float4(break15_g22398.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22398 = (float4(break15_g22398.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22448 = lerp( temp_output_36_0_g22448 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22398 >= appendResult3_g22398.x && temp_output_19_0_g22398 <= appendResult16_g22398.x ) ? temp_output_19_0_g22398 :  0.0 ) ) ));
			float DissolveModifier30_g22447 = _DissolveObject33;
			float4 transform4_g22447 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22447 = transform4_g22447.y;
			float ifLocalVar46_g22447 = 0;
			if( _DissolveToggleDir33 > 0.0 )
				ifLocalVar46_g22447 = -Space24_g22447;
			else if( _DissolveToggleDir33 == 0.0 )
				ifLocalVar46_g22447 = Space24_g22447;
			float ObjectScale23_g22447 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22447 = exp2( _DissolveDensity );
			float temp_output_70_0_g22447 = ( ( (-1.0 + (DissolveModifier30_g22447 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22447 / ObjectScale23_g22447 ) ) * DissolveDensity67_g22447 );
			float4 tex2DNode61_g22447 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22447 = max( max( tex2DNode61_g22447.r , tex2DNode61_g22447.g ) , tex2DNode61_g22447.b );
			float temp_output_36_0_g22447 = saturate( ( temp_output_70_0_g22447 + DissolvePattern58_g22447 ) );
			float temp_output_19_0_g22399 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22399 = ( 33.0 / 100.0 );
			float temp_output_8_0_g22399 = ( temp_output_6_0_g22399 - 0.005 );
			float temp_output_7_0_g22399 = ( temp_output_6_0_g22399 + 0.005 );
			float2 appendResult4_g22399 = (float2(temp_output_8_0_g22399 , temp_output_7_0_g22399));
			float2 appendResult13_g22399 = (float2(-0.005 , temp_output_7_0_g22399));
			float2 appendResult14_g22399 = (float2(temp_output_8_0_g22399 , 1.005));
			float2 ifLocalVar11_g22399 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22399 = appendResult4_g22399;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22399 = appendResult13_g22399;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22399 = appendResult14_g22399;
			float2 break15_g22399 = ifLocalVar11_g22399;
			float4 appendResult3_g22399 = (float4(break15_g22399.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22399 = (float4(break15_g22399.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22447 = lerp( temp_output_36_0_g22447 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22399 >= appendResult3_g22399.x && temp_output_19_0_g22399 <= appendResult16_g22399.x ) ? temp_output_19_0_g22399 :  0.0 ) ) ));
			float DissolveModifier30_g22442 = _DissolveObject34;
			float4 transform4_g22442 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22442 = transform4_g22442.y;
			float ifLocalVar46_g22442 = 0;
			if( _DissolveToggleDir34 > 0.0 )
				ifLocalVar46_g22442 = -Space24_g22442;
			else if( _DissolveToggleDir34 == 0.0 )
				ifLocalVar46_g22442 = Space24_g22442;
			float ObjectScale23_g22442 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22442 = exp2( _DissolveDensity );
			float temp_output_70_0_g22442 = ( ( (-1.0 + (DissolveModifier30_g22442 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22442 / ObjectScale23_g22442 ) ) * DissolveDensity67_g22442 );
			float4 tex2DNode61_g22442 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22442 = max( max( tex2DNode61_g22442.r , tex2DNode61_g22442.g ) , tex2DNode61_g22442.b );
			float temp_output_36_0_g22442 = saturate( ( temp_output_70_0_g22442 + DissolvePattern58_g22442 ) );
			float temp_output_19_0_g22397 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22397 = ( 34.0 / 100.0 );
			float temp_output_8_0_g22397 = ( temp_output_6_0_g22397 - 0.005 );
			float temp_output_7_0_g22397 = ( temp_output_6_0_g22397 + 0.005 );
			float2 appendResult4_g22397 = (float2(temp_output_8_0_g22397 , temp_output_7_0_g22397));
			float2 appendResult13_g22397 = (float2(-0.005 , temp_output_7_0_g22397));
			float2 appendResult14_g22397 = (float2(temp_output_8_0_g22397 , 1.005));
			float2 ifLocalVar11_g22397 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22397 = appendResult4_g22397;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22397 = appendResult13_g22397;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22397 = appendResult14_g22397;
			float2 break15_g22397 = ifLocalVar11_g22397;
			float4 appendResult3_g22397 = (float4(break15_g22397.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22397 = (float4(break15_g22397.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22442 = lerp( temp_output_36_0_g22442 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22397 >= appendResult3_g22397.x && temp_output_19_0_g22397 <= appendResult16_g22397.x ) ? temp_output_19_0_g22397 :  0.0 ) ) ));
			float DissolveModifier30_g22430 = _DissolveObject35;
			float4 transform4_g22430 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22430 = transform4_g22430.y;
			float ifLocalVar46_g22430 = 0;
			if( _DissolveToggleDir35 > 0.0 )
				ifLocalVar46_g22430 = -Space24_g22430;
			else if( _DissolveToggleDir35 == 0.0 )
				ifLocalVar46_g22430 = Space24_g22430;
			float ObjectScale23_g22430 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22430 = exp2( _DissolveDensity );
			float temp_output_70_0_g22430 = ( ( (-1.0 + (DissolveModifier30_g22430 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22430 / ObjectScale23_g22430 ) ) * DissolveDensity67_g22430 );
			float4 tex2DNode61_g22430 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22430 = max( max( tex2DNode61_g22430.r , tex2DNode61_g22430.g ) , tex2DNode61_g22430.b );
			float temp_output_36_0_g22430 = saturate( ( temp_output_70_0_g22430 + DissolvePattern58_g22430 ) );
			float temp_output_19_0_g22413 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22413 = ( 35.0 / 100.0 );
			float temp_output_8_0_g22413 = ( temp_output_6_0_g22413 - 0.005 );
			float temp_output_7_0_g22413 = ( temp_output_6_0_g22413 + 0.005 );
			float2 appendResult4_g22413 = (float2(temp_output_8_0_g22413 , temp_output_7_0_g22413));
			float2 appendResult13_g22413 = (float2(-0.005 , temp_output_7_0_g22413));
			float2 appendResult14_g22413 = (float2(temp_output_8_0_g22413 , 1.005));
			float2 ifLocalVar11_g22413 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22413 = appendResult4_g22413;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22413 = appendResult13_g22413;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22413 = appendResult14_g22413;
			float2 break15_g22413 = ifLocalVar11_g22413;
			float4 appendResult3_g22413 = (float4(break15_g22413.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22413 = (float4(break15_g22413.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22430 = lerp( temp_output_36_0_g22430 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22413 >= appendResult3_g22413.x && temp_output_19_0_g22413 <= appendResult16_g22413.x ) ? temp_output_19_0_g22413 :  0.0 ) ) ));
			float DissolveModifier30_g22427 = _DissolveObject36;
			float4 transform4_g22427 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22427 = transform4_g22427.y;
			float ifLocalVar46_g22427 = 0;
			if( _DissolveToggleDir36 > 0.0 )
				ifLocalVar46_g22427 = -Space24_g22427;
			else if( _DissolveToggleDir36 == 0.0 )
				ifLocalVar46_g22427 = Space24_g22427;
			float ObjectScale23_g22427 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22427 = exp2( _DissolveDensity );
			float temp_output_70_0_g22427 = ( ( (-1.0 + (DissolveModifier30_g22427 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22427 / ObjectScale23_g22427 ) ) * DissolveDensity67_g22427 );
			float4 tex2DNode61_g22427 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22427 = max( max( tex2DNode61_g22427.r , tex2DNode61_g22427.g ) , tex2DNode61_g22427.b );
			float temp_output_36_0_g22427 = saturate( ( temp_output_70_0_g22427 + DissolvePattern58_g22427 ) );
			float temp_output_19_0_g22395 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22395 = ( 36.0 / 100.0 );
			float temp_output_8_0_g22395 = ( temp_output_6_0_g22395 - 0.005 );
			float temp_output_7_0_g22395 = ( temp_output_6_0_g22395 + 0.005 );
			float2 appendResult4_g22395 = (float2(temp_output_8_0_g22395 , temp_output_7_0_g22395));
			float2 appendResult13_g22395 = (float2(-0.005 , temp_output_7_0_g22395));
			float2 appendResult14_g22395 = (float2(temp_output_8_0_g22395 , 1.005));
			float2 ifLocalVar11_g22395 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22395 = appendResult4_g22395;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22395 = appendResult13_g22395;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22395 = appendResult14_g22395;
			float2 break15_g22395 = ifLocalVar11_g22395;
			float4 appendResult3_g22395 = (float4(break15_g22395.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22395 = (float4(break15_g22395.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22427 = lerp( temp_output_36_0_g22427 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22395 >= appendResult3_g22395.x && temp_output_19_0_g22395 <= appendResult16_g22395.x ) ? temp_output_19_0_g22395 :  0.0 ) ) ));
			float DissolveModifier30_g22435 = _DissolveObject37;
			float4 transform4_g22435 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22435 = transform4_g22435.y;
			float ifLocalVar46_g22435 = 0;
			if( _DissolveToggleDir37 > 0.0 )
				ifLocalVar46_g22435 = -Space24_g22435;
			else if( _DissolveToggleDir37 == 0.0 )
				ifLocalVar46_g22435 = Space24_g22435;
			float ObjectScale23_g22435 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22435 = exp2( _DissolveDensity );
			float temp_output_70_0_g22435 = ( ( (-1.0 + (DissolveModifier30_g22435 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22435 / ObjectScale23_g22435 ) ) * DissolveDensity67_g22435 );
			float4 tex2DNode61_g22435 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22435 = max( max( tex2DNode61_g22435.r , tex2DNode61_g22435.g ) , tex2DNode61_g22435.b );
			float temp_output_36_0_g22435 = saturate( ( temp_output_70_0_g22435 + DissolvePattern58_g22435 ) );
			float temp_output_19_0_g22392 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22392 = ( 37.0 / 100.0 );
			float temp_output_8_0_g22392 = ( temp_output_6_0_g22392 - 0.005 );
			float temp_output_7_0_g22392 = ( temp_output_6_0_g22392 + 0.005 );
			float2 appendResult4_g22392 = (float2(temp_output_8_0_g22392 , temp_output_7_0_g22392));
			float2 appendResult13_g22392 = (float2(-0.005 , temp_output_7_0_g22392));
			float2 appendResult14_g22392 = (float2(temp_output_8_0_g22392 , 1.005));
			float2 ifLocalVar11_g22392 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22392 = appendResult4_g22392;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22392 = appendResult13_g22392;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22392 = appendResult14_g22392;
			float2 break15_g22392 = ifLocalVar11_g22392;
			float4 appendResult3_g22392 = (float4(break15_g22392.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22392 = (float4(break15_g22392.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22435 = lerp( temp_output_36_0_g22435 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22392 >= appendResult3_g22392.x && temp_output_19_0_g22392 <= appendResult16_g22392.x ) ? temp_output_19_0_g22392 :  0.0 ) ) ));
			float DissolveModifier30_g22438 = _DissolveObject38;
			float4 transform4_g22438 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22438 = transform4_g22438.y;
			float ifLocalVar46_g22438 = 0;
			if( _DissolveToggleDir38 > 0.0 )
				ifLocalVar46_g22438 = -Space24_g22438;
			else if( _DissolveToggleDir38 == 0.0 )
				ifLocalVar46_g22438 = Space24_g22438;
			float ObjectScale23_g22438 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22438 = exp2( _DissolveDensity );
			float temp_output_70_0_g22438 = ( ( (-1.0 + (DissolveModifier30_g22438 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22438 / ObjectScale23_g22438 ) ) * DissolveDensity67_g22438 );
			float4 tex2DNode61_g22438 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22438 = max( max( tex2DNode61_g22438.r , tex2DNode61_g22438.g ) , tex2DNode61_g22438.b );
			float temp_output_36_0_g22438 = saturate( ( temp_output_70_0_g22438 + DissolvePattern58_g22438 ) );
			float temp_output_19_0_g22407 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22407 = ( 38.0 / 100.0 );
			float temp_output_8_0_g22407 = ( temp_output_6_0_g22407 - 0.005 );
			float temp_output_7_0_g22407 = ( temp_output_6_0_g22407 + 0.005 );
			float2 appendResult4_g22407 = (float2(temp_output_8_0_g22407 , temp_output_7_0_g22407));
			float2 appendResult13_g22407 = (float2(-0.005 , temp_output_7_0_g22407));
			float2 appendResult14_g22407 = (float2(temp_output_8_0_g22407 , 1.005));
			float2 ifLocalVar11_g22407 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22407 = appendResult4_g22407;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22407 = appendResult13_g22407;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22407 = appendResult14_g22407;
			float2 break15_g22407 = ifLocalVar11_g22407;
			float4 appendResult3_g22407 = (float4(break15_g22407.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22407 = (float4(break15_g22407.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22438 = lerp( temp_output_36_0_g22438 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22407 >= appendResult3_g22407.x && temp_output_19_0_g22407 <= appendResult16_g22407.x ) ? temp_output_19_0_g22407 :  0.0 ) ) ));
			float DissolveModifier30_g22456 = _DissolveObject39;
			float4 transform4_g22456 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22456 = transform4_g22456.y;
			float ifLocalVar46_g22456 = 0;
			if( _DissolveToggleDir39 > 0.0 )
				ifLocalVar46_g22456 = -Space24_g22456;
			else if( _DissolveToggleDir39 == 0.0 )
				ifLocalVar46_g22456 = Space24_g22456;
			float ObjectScale23_g22456 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22456 = exp2( _DissolveDensity );
			float temp_output_70_0_g22456 = ( ( (-1.0 + (DissolveModifier30_g22456 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22456 / ObjectScale23_g22456 ) ) * DissolveDensity67_g22456 );
			float4 tex2DNode61_g22456 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22456 = max( max( tex2DNode61_g22456.r , tex2DNode61_g22456.g ) , tex2DNode61_g22456.b );
			float temp_output_36_0_g22456 = saturate( ( temp_output_70_0_g22456 + DissolvePattern58_g22456 ) );
			float temp_output_19_0_g22388 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22388 = ( 39.0 / 100.0 );
			float temp_output_8_0_g22388 = ( temp_output_6_0_g22388 - 0.005 );
			float temp_output_7_0_g22388 = ( temp_output_6_0_g22388 + 0.005 );
			float2 appendResult4_g22388 = (float2(temp_output_8_0_g22388 , temp_output_7_0_g22388));
			float2 appendResult13_g22388 = (float2(-0.005 , temp_output_7_0_g22388));
			float2 appendResult14_g22388 = (float2(temp_output_8_0_g22388 , 1.005));
			float2 ifLocalVar11_g22388 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22388 = appendResult4_g22388;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22388 = appendResult13_g22388;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22388 = appendResult14_g22388;
			float2 break15_g22388 = ifLocalVar11_g22388;
			float4 appendResult3_g22388 = (float4(break15_g22388.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22388 = (float4(break15_g22388.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22456 = lerp( temp_output_36_0_g22456 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22388 >= appendResult3_g22388.x && temp_output_19_0_g22388 <= appendResult16_g22388.x ) ? temp_output_19_0_g22388 :  0.0 ) ) ));
			float DissolveModifier30_g22411 = _DissolveObject40;
			float4 transform4_g22411 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22411 = transform4_g22411.y;
			float ifLocalVar46_g22411 = 0;
			if( _DissolveToggleDir40 > 0.0 )
				ifLocalVar46_g22411 = -Space24_g22411;
			else if( _DissolveToggleDir40 == 0.0 )
				ifLocalVar46_g22411 = Space24_g22411;
			float ObjectScale23_g22411 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22411 = exp2( _DissolveDensity );
			float temp_output_70_0_g22411 = ( ( (-1.0 + (DissolveModifier30_g22411 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22411 / ObjectScale23_g22411 ) ) * DissolveDensity67_g22411 );
			float4 tex2DNode61_g22411 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22411 = max( max( tex2DNode61_g22411.r , tex2DNode61_g22411.g ) , tex2DNode61_g22411.b );
			float temp_output_36_0_g22411 = saturate( ( temp_output_70_0_g22411 + DissolvePattern58_g22411 ) );
			float temp_output_19_0_g22386 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22386 = ( 40.0 / 100.0 );
			float temp_output_8_0_g22386 = ( temp_output_6_0_g22386 - 0.005 );
			float temp_output_7_0_g22386 = ( temp_output_6_0_g22386 + 0.005 );
			float2 appendResult4_g22386 = (float2(temp_output_8_0_g22386 , temp_output_7_0_g22386));
			float2 appendResult13_g22386 = (float2(-0.005 , temp_output_7_0_g22386));
			float2 appendResult14_g22386 = (float2(temp_output_8_0_g22386 , 1.005));
			float2 ifLocalVar11_g22386 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22386 = appendResult4_g22386;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22386 = appendResult13_g22386;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22386 = appendResult14_g22386;
			float2 break15_g22386 = ifLocalVar11_g22386;
			float4 appendResult3_g22386 = (float4(break15_g22386.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22386 = (float4(break15_g22386.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22411 = lerp( temp_output_36_0_g22411 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22386 >= appendResult3_g22386.x && temp_output_19_0_g22386 <= appendResult16_g22386.x ) ? temp_output_19_0_g22386 :  0.0 ) ) ));
			float DissolveModifier30_g22458 = _DissolveObject21;
			float4 transform4_g22458 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22458 = transform4_g22458.y;
			float ifLocalVar46_g22458 = 0;
			if( _DissolveToggleDir21 > 0.0 )
				ifLocalVar46_g22458 = -Space24_g22458;
			else if( _DissolveToggleDir21 == 0.0 )
				ifLocalVar46_g22458 = Space24_g22458;
			float ObjectScale23_g22458 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22458 = exp2( _DissolveDensity );
			float temp_output_70_0_g22458 = ( ( (-1.0 + (DissolveModifier30_g22458 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22458 / ObjectScale23_g22458 ) ) * DissolveDensity67_g22458 );
			float4 tex2DNode61_g22458 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22458 = max( max( tex2DNode61_g22458.r , tex2DNode61_g22458.g ) , tex2DNode61_g22458.b );
			float temp_output_36_0_g22458 = saturate( ( temp_output_70_0_g22458 + DissolvePattern58_g22458 ) );
			float temp_output_19_0_g22403 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22403 = ( 21.0 / 100.0 );
			float temp_output_8_0_g22403 = ( temp_output_6_0_g22403 - 0.005 );
			float temp_output_7_0_g22403 = ( temp_output_6_0_g22403 + 0.005 );
			float2 appendResult4_g22403 = (float2(temp_output_8_0_g22403 , temp_output_7_0_g22403));
			float2 appendResult13_g22403 = (float2(-0.005 , temp_output_7_0_g22403));
			float2 appendResult14_g22403 = (float2(temp_output_8_0_g22403 , 1.005));
			float2 ifLocalVar11_g22403 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22403 = appendResult4_g22403;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22403 = appendResult13_g22403;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22403 = appendResult14_g22403;
			float2 break15_g22403 = ifLocalVar11_g22403;
			float4 appendResult3_g22403 = (float4(break15_g22403.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22403 = (float4(break15_g22403.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22458 = lerp( temp_output_36_0_g22458 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22403 >= appendResult3_g22403.x && temp_output_19_0_g22403 <= appendResult16_g22403.x ) ? temp_output_19_0_g22403 :  0.0 ) ) ));
			float DissolveModifier30_g22450 = _DissolveObject22;
			float4 transform4_g22450 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22450 = transform4_g22450.y;
			float ifLocalVar46_g22450 = 0;
			if( _DissolveToggleDir22 > 0.0 )
				ifLocalVar46_g22450 = -Space24_g22450;
			else if( _DissolveToggleDir22 == 0.0 )
				ifLocalVar46_g22450 = Space24_g22450;
			float ObjectScale23_g22450 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22450 = exp2( _DissolveDensity );
			float temp_output_70_0_g22450 = ( ( (-1.0 + (DissolveModifier30_g22450 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22450 / ObjectScale23_g22450 ) ) * DissolveDensity67_g22450 );
			float4 tex2DNode61_g22450 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22450 = max( max( tex2DNode61_g22450.r , tex2DNode61_g22450.g ) , tex2DNode61_g22450.b );
			float temp_output_36_0_g22450 = saturate( ( temp_output_70_0_g22450 + DissolvePattern58_g22450 ) );
			float temp_output_19_0_g22410 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22410 = ( 22.0 / 100.0 );
			float temp_output_8_0_g22410 = ( temp_output_6_0_g22410 - 0.005 );
			float temp_output_7_0_g22410 = ( temp_output_6_0_g22410 + 0.005 );
			float2 appendResult4_g22410 = (float2(temp_output_8_0_g22410 , temp_output_7_0_g22410));
			float2 appendResult13_g22410 = (float2(-0.005 , temp_output_7_0_g22410));
			float2 appendResult14_g22410 = (float2(temp_output_8_0_g22410 , 1.005));
			float2 ifLocalVar11_g22410 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22410 = appendResult4_g22410;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22410 = appendResult13_g22410;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22410 = appendResult14_g22410;
			float2 break15_g22410 = ifLocalVar11_g22410;
			float4 appendResult3_g22410 = (float4(break15_g22410.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22410 = (float4(break15_g22410.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22450 = lerp( temp_output_36_0_g22450 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22410 >= appendResult3_g22410.x && temp_output_19_0_g22410 <= appendResult16_g22410.x ) ? temp_output_19_0_g22410 :  0.0 ) ) ));
			float DissolveModifier30_g22451 = _DissolveObject23;
			float4 transform4_g22451 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22451 = transform4_g22451.y;
			float ifLocalVar46_g22451 = 0;
			if( _DissolveToggleDir23 > 0.0 )
				ifLocalVar46_g22451 = -Space24_g22451;
			else if( _DissolveToggleDir23 == 0.0 )
				ifLocalVar46_g22451 = Space24_g22451;
			float ObjectScale23_g22451 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22451 = exp2( _DissolveDensity );
			float temp_output_70_0_g22451 = ( ( (-1.0 + (DissolveModifier30_g22451 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22451 / ObjectScale23_g22451 ) ) * DissolveDensity67_g22451 );
			float4 tex2DNode61_g22451 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22451 = max( max( tex2DNode61_g22451.r , tex2DNode61_g22451.g ) , tex2DNode61_g22451.b );
			float temp_output_36_0_g22451 = saturate( ( temp_output_70_0_g22451 + DissolvePattern58_g22451 ) );
			float temp_output_19_0_g22393 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22393 = ( 23.0 / 100.0 );
			float temp_output_8_0_g22393 = ( temp_output_6_0_g22393 - 0.005 );
			float temp_output_7_0_g22393 = ( temp_output_6_0_g22393 + 0.005 );
			float2 appendResult4_g22393 = (float2(temp_output_8_0_g22393 , temp_output_7_0_g22393));
			float2 appendResult13_g22393 = (float2(-0.005 , temp_output_7_0_g22393));
			float2 appendResult14_g22393 = (float2(temp_output_8_0_g22393 , 1.005));
			float2 ifLocalVar11_g22393 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22393 = appendResult4_g22393;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22393 = appendResult13_g22393;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22393 = appendResult14_g22393;
			float2 break15_g22393 = ifLocalVar11_g22393;
			float4 appendResult3_g22393 = (float4(break15_g22393.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22393 = (float4(break15_g22393.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22451 = lerp( temp_output_36_0_g22451 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22393 >= appendResult3_g22393.x && temp_output_19_0_g22393 <= appendResult16_g22393.x ) ? temp_output_19_0_g22393 :  0.0 ) ) ));
			float DissolveModifier30_g22396 = _DissolveObject24;
			float4 transform4_g22396 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22396 = transform4_g22396.y;
			float ifLocalVar46_g22396 = 0;
			if( _DissolveToggleDir24 > 0.0 )
				ifLocalVar46_g22396 = -Space24_g22396;
			else if( _DissolveToggleDir24 == 0.0 )
				ifLocalVar46_g22396 = Space24_g22396;
			float ObjectScale23_g22396 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22396 = exp2( _DissolveDensity );
			float temp_output_70_0_g22396 = ( ( (-1.0 + (DissolveModifier30_g22396 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22396 / ObjectScale23_g22396 ) ) * DissolveDensity67_g22396 );
			float4 tex2DNode61_g22396 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22396 = max( max( tex2DNode61_g22396.r , tex2DNode61_g22396.g ) , tex2DNode61_g22396.b );
			float temp_output_36_0_g22396 = saturate( ( temp_output_70_0_g22396 + DissolvePattern58_g22396 ) );
			float temp_output_19_0_g22419 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22419 = ( 24.0 / 100.0 );
			float temp_output_8_0_g22419 = ( temp_output_6_0_g22419 - 0.005 );
			float temp_output_7_0_g22419 = ( temp_output_6_0_g22419 + 0.005 );
			float2 appendResult4_g22419 = (float2(temp_output_8_0_g22419 , temp_output_7_0_g22419));
			float2 appendResult13_g22419 = (float2(-0.005 , temp_output_7_0_g22419));
			float2 appendResult14_g22419 = (float2(temp_output_8_0_g22419 , 1.005));
			float2 ifLocalVar11_g22419 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22419 = appendResult4_g22419;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22419 = appendResult13_g22419;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22419 = appendResult14_g22419;
			float2 break15_g22419 = ifLocalVar11_g22419;
			float4 appendResult3_g22419 = (float4(break15_g22419.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22419 = (float4(break15_g22419.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22396 = lerp( temp_output_36_0_g22396 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22419 >= appendResult3_g22419.x && temp_output_19_0_g22419 <= appendResult16_g22419.x ) ? temp_output_19_0_g22419 :  0.0 ) ) ));
			float DissolveModifier30_g22436 = _DissolveObject25;
			float4 transform4_g22436 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22436 = transform4_g22436.y;
			float ifLocalVar46_g22436 = 0;
			if( _DissolveToggleDir25 > 0.0 )
				ifLocalVar46_g22436 = -Space24_g22436;
			else if( _DissolveToggleDir25 == 0.0 )
				ifLocalVar46_g22436 = Space24_g22436;
			float ObjectScale23_g22436 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22436 = exp2( _DissolveDensity );
			float temp_output_70_0_g22436 = ( ( (-1.0 + (DissolveModifier30_g22436 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22436 / ObjectScale23_g22436 ) ) * DissolveDensity67_g22436 );
			float4 tex2DNode61_g22436 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22436 = max( max( tex2DNode61_g22436.r , tex2DNode61_g22436.g ) , tex2DNode61_g22436.b );
			float temp_output_36_0_g22436 = saturate( ( temp_output_70_0_g22436 + DissolvePattern58_g22436 ) );
			float temp_output_19_0_g22394 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22394 = ( 25.0 / 100.0 );
			float temp_output_8_0_g22394 = ( temp_output_6_0_g22394 - 0.005 );
			float temp_output_7_0_g22394 = ( temp_output_6_0_g22394 + 0.005 );
			float2 appendResult4_g22394 = (float2(temp_output_8_0_g22394 , temp_output_7_0_g22394));
			float2 appendResult13_g22394 = (float2(-0.005 , temp_output_7_0_g22394));
			float2 appendResult14_g22394 = (float2(temp_output_8_0_g22394 , 1.005));
			float2 ifLocalVar11_g22394 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22394 = appendResult4_g22394;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22394 = appendResult13_g22394;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22394 = appendResult14_g22394;
			float2 break15_g22394 = ifLocalVar11_g22394;
			float4 appendResult3_g22394 = (float4(break15_g22394.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22394 = (float4(break15_g22394.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22436 = lerp( temp_output_36_0_g22436 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22394 >= appendResult3_g22394.x && temp_output_19_0_g22394 <= appendResult16_g22394.x ) ? temp_output_19_0_g22394 :  0.0 ) ) ));
			float DissolveModifier30_g22437 = _DissolveObject26;
			float4 transform4_g22437 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22437 = transform4_g22437.y;
			float ifLocalVar46_g22437 = 0;
			if( _DissolveToggleDir26 > 0.0 )
				ifLocalVar46_g22437 = -Space24_g22437;
			else if( _DissolveToggleDir26 == 0.0 )
				ifLocalVar46_g22437 = Space24_g22437;
			float ObjectScale23_g22437 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22437 = exp2( _DissolveDensity );
			float temp_output_70_0_g22437 = ( ( (-1.0 + (DissolveModifier30_g22437 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22437 / ObjectScale23_g22437 ) ) * DissolveDensity67_g22437 );
			float4 tex2DNode61_g22437 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22437 = max( max( tex2DNode61_g22437.r , tex2DNode61_g22437.g ) , tex2DNode61_g22437.b );
			float temp_output_36_0_g22437 = saturate( ( temp_output_70_0_g22437 + DissolvePattern58_g22437 ) );
			float temp_output_19_0_g22391 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22391 = ( 26.0 / 100.0 );
			float temp_output_8_0_g22391 = ( temp_output_6_0_g22391 - 0.005 );
			float temp_output_7_0_g22391 = ( temp_output_6_0_g22391 + 0.005 );
			float2 appendResult4_g22391 = (float2(temp_output_8_0_g22391 , temp_output_7_0_g22391));
			float2 appendResult13_g22391 = (float2(-0.005 , temp_output_7_0_g22391));
			float2 appendResult14_g22391 = (float2(temp_output_8_0_g22391 , 1.005));
			float2 ifLocalVar11_g22391 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22391 = appendResult4_g22391;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22391 = appendResult13_g22391;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22391 = appendResult14_g22391;
			float2 break15_g22391 = ifLocalVar11_g22391;
			float4 appendResult3_g22391 = (float4(break15_g22391.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22391 = (float4(break15_g22391.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22437 = lerp( temp_output_36_0_g22437 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22391 >= appendResult3_g22391.x && temp_output_19_0_g22391 <= appendResult16_g22391.x ) ? temp_output_19_0_g22391 :  0.0 ) ) ));
			float DissolveModifier30_g22459 = _DissolveObject27;
			float4 transform4_g22459 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22459 = transform4_g22459.y;
			float ifLocalVar46_g22459 = 0;
			if( _DissolveToggleDir27 > 0.0 )
				ifLocalVar46_g22459 = -Space24_g22459;
			else if( _DissolveToggleDir27 == 0.0 )
				ifLocalVar46_g22459 = Space24_g22459;
			float ObjectScale23_g22459 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22459 = exp2( _DissolveDensity );
			float temp_output_70_0_g22459 = ( ( (-1.0 + (DissolveModifier30_g22459 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22459 / ObjectScale23_g22459 ) ) * DissolveDensity67_g22459 );
			float4 tex2DNode61_g22459 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22459 = max( max( tex2DNode61_g22459.r , tex2DNode61_g22459.g ) , tex2DNode61_g22459.b );
			float temp_output_36_0_g22459 = saturate( ( temp_output_70_0_g22459 + DissolvePattern58_g22459 ) );
			float temp_output_19_0_g22420 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22420 = ( 27.0 / 100.0 );
			float temp_output_8_0_g22420 = ( temp_output_6_0_g22420 - 0.005 );
			float temp_output_7_0_g22420 = ( temp_output_6_0_g22420 + 0.005 );
			float2 appendResult4_g22420 = (float2(temp_output_8_0_g22420 , temp_output_7_0_g22420));
			float2 appendResult13_g22420 = (float2(-0.005 , temp_output_7_0_g22420));
			float2 appendResult14_g22420 = (float2(temp_output_8_0_g22420 , 1.005));
			float2 ifLocalVar11_g22420 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22420 = appendResult4_g22420;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22420 = appendResult13_g22420;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22420 = appendResult14_g22420;
			float2 break15_g22420 = ifLocalVar11_g22420;
			float4 appendResult3_g22420 = (float4(break15_g22420.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22420 = (float4(break15_g22420.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22459 = lerp( temp_output_36_0_g22459 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22420 >= appendResult3_g22420.x && temp_output_19_0_g22420 <= appendResult16_g22420.x ) ? temp_output_19_0_g22420 :  0.0 ) ) ));
			float DissolveModifier30_g22424 = _DissolveObject28;
			float4 transform4_g22424 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22424 = transform4_g22424.y;
			float ifLocalVar46_g22424 = 0;
			if( _DissolveToggleDir28 > 0.0 )
				ifLocalVar46_g22424 = -Space24_g22424;
			else if( _DissolveToggleDir28 == 0.0 )
				ifLocalVar46_g22424 = Space24_g22424;
			float ObjectScale23_g22424 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22424 = exp2( _DissolveDensity );
			float temp_output_70_0_g22424 = ( ( (-1.0 + (DissolveModifier30_g22424 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22424 / ObjectScale23_g22424 ) ) * DissolveDensity67_g22424 );
			float4 tex2DNode61_g22424 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22424 = max( max( tex2DNode61_g22424.r , tex2DNode61_g22424.g ) , tex2DNode61_g22424.b );
			float temp_output_36_0_g22424 = saturate( ( temp_output_70_0_g22424 + DissolvePattern58_g22424 ) );
			float temp_output_19_0_g22417 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22417 = ( 28.0 / 100.0 );
			float temp_output_8_0_g22417 = ( temp_output_6_0_g22417 - 0.005 );
			float temp_output_7_0_g22417 = ( temp_output_6_0_g22417 + 0.005 );
			float2 appendResult4_g22417 = (float2(temp_output_8_0_g22417 , temp_output_7_0_g22417));
			float2 appendResult13_g22417 = (float2(-0.005 , temp_output_7_0_g22417));
			float2 appendResult14_g22417 = (float2(temp_output_8_0_g22417 , 1.005));
			float2 ifLocalVar11_g22417 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22417 = appendResult4_g22417;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22417 = appendResult13_g22417;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22417 = appendResult14_g22417;
			float2 break15_g22417 = ifLocalVar11_g22417;
			float4 appendResult3_g22417 = (float4(break15_g22417.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22417 = (float4(break15_g22417.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22424 = lerp( temp_output_36_0_g22424 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22417 >= appendResult3_g22417.x && temp_output_19_0_g22417 <= appendResult16_g22417.x ) ? temp_output_19_0_g22417 :  0.0 ) ) ));
			float DissolveModifier30_g22422 = _DissolveObject29;
			float4 transform4_g22422 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22422 = transform4_g22422.y;
			float ifLocalVar46_g22422 = 0;
			if( _DissolveToggleDir29 > 0.0 )
				ifLocalVar46_g22422 = -Space24_g22422;
			else if( _DissolveToggleDir29 == 0.0 )
				ifLocalVar46_g22422 = Space24_g22422;
			float ObjectScale23_g22422 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22422 = exp2( _DissolveDensity );
			float temp_output_70_0_g22422 = ( ( (-1.0 + (DissolveModifier30_g22422 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22422 / ObjectScale23_g22422 ) ) * DissolveDensity67_g22422 );
			float4 tex2DNode61_g22422 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22422 = max( max( tex2DNode61_g22422.r , tex2DNode61_g22422.g ) , tex2DNode61_g22422.b );
			float temp_output_36_0_g22422 = saturate( ( temp_output_70_0_g22422 + DissolvePattern58_g22422 ) );
			float temp_output_19_0_g22412 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22412 = ( 29.0 / 100.0 );
			float temp_output_8_0_g22412 = ( temp_output_6_0_g22412 - 0.005 );
			float temp_output_7_0_g22412 = ( temp_output_6_0_g22412 + 0.005 );
			float2 appendResult4_g22412 = (float2(temp_output_8_0_g22412 , temp_output_7_0_g22412));
			float2 appendResult13_g22412 = (float2(-0.005 , temp_output_7_0_g22412));
			float2 appendResult14_g22412 = (float2(temp_output_8_0_g22412 , 1.005));
			float2 ifLocalVar11_g22412 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22412 = appendResult4_g22412;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22412 = appendResult13_g22412;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22412 = appendResult14_g22412;
			float2 break15_g22412 = ifLocalVar11_g22412;
			float4 appendResult3_g22412 = (float4(break15_g22412.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22412 = (float4(break15_g22412.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22422 = lerp( temp_output_36_0_g22422 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22412 >= appendResult3_g22412.x && temp_output_19_0_g22412 <= appendResult16_g22412.x ) ? temp_output_19_0_g22412 :  0.0 ) ) ));
			float DissolveModifier30_g22440 = _DissolveObject30;
			float4 transform4_g22440 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22440 = transform4_g22440.y;
			float ifLocalVar46_g22440 = 0;
			if( _DissolveToggleDir30 > 0.0 )
				ifLocalVar46_g22440 = -Space24_g22440;
			else if( _DissolveToggleDir30 == 0.0 )
				ifLocalVar46_g22440 = Space24_g22440;
			float ObjectScale23_g22440 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22440 = exp2( _DissolveDensity );
			float temp_output_70_0_g22440 = ( ( (-1.0 + (DissolveModifier30_g22440 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22440 / ObjectScale23_g22440 ) ) * DissolveDensity67_g22440 );
			float4 tex2DNode61_g22440 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22440 = max( max( tex2DNode61_g22440.r , tex2DNode61_g22440.g ) , tex2DNode61_g22440.b );
			float temp_output_36_0_g22440 = saturate( ( temp_output_70_0_g22440 + DissolvePattern58_g22440 ) );
			float temp_output_19_0_g22380 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22380 = ( 30.0 / 100.0 );
			float temp_output_8_0_g22380 = ( temp_output_6_0_g22380 - 0.005 );
			float temp_output_7_0_g22380 = ( temp_output_6_0_g22380 + 0.005 );
			float2 appendResult4_g22380 = (float2(temp_output_8_0_g22380 , temp_output_7_0_g22380));
			float2 appendResult13_g22380 = (float2(-0.005 , temp_output_7_0_g22380));
			float2 appendResult14_g22380 = (float2(temp_output_8_0_g22380 , 1.005));
			float2 ifLocalVar11_g22380 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22380 = appendResult4_g22380;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22380 = appendResult13_g22380;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22380 = appendResult14_g22380;
			float2 break15_g22380 = ifLocalVar11_g22380;
			float4 appendResult3_g22380 = (float4(break15_g22380.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22380 = (float4(break15_g22380.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22440 = lerp( temp_output_36_0_g22440 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22380 >= appendResult3_g22380.x && temp_output_19_0_g22380 <= appendResult16_g22380.x ) ? temp_output_19_0_g22380 :  0.0 ) ) ));
			float DissolveModifier30_g22454 = _DissolveObject11;
			float4 transform4_g22454 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22454 = transform4_g22454.y;
			float ifLocalVar46_g22454 = 0;
			if( _DissolveToggleDir11 > 0.0 )
				ifLocalVar46_g22454 = -Space24_g22454;
			else if( _DissolveToggleDir11 == 0.0 )
				ifLocalVar46_g22454 = Space24_g22454;
			float ObjectScale23_g22454 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22454 = exp2( _DissolveDensity );
			float temp_output_70_0_g22454 = ( ( (-1.0 + (DissolveModifier30_g22454 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22454 / ObjectScale23_g22454 ) ) * DissolveDensity67_g22454 );
			float4 tex2DNode61_g22454 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22454 = max( max( tex2DNode61_g22454.r , tex2DNode61_g22454.g ) , tex2DNode61_g22454.b );
			float temp_output_36_0_g22454 = saturate( ( temp_output_70_0_g22454 + DissolvePattern58_g22454 ) );
			float temp_output_19_0_g22379 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22379 = ( 11.0 / 100.0 );
			float temp_output_8_0_g22379 = ( temp_output_6_0_g22379 - 0.005 );
			float temp_output_7_0_g22379 = ( temp_output_6_0_g22379 + 0.005 );
			float2 appendResult4_g22379 = (float2(temp_output_8_0_g22379 , temp_output_7_0_g22379));
			float2 appendResult13_g22379 = (float2(-0.005 , temp_output_7_0_g22379));
			float2 appendResult14_g22379 = (float2(temp_output_8_0_g22379 , 1.005));
			float2 ifLocalVar11_g22379 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22379 = appendResult4_g22379;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22379 = appendResult13_g22379;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22379 = appendResult14_g22379;
			float2 break15_g22379 = ifLocalVar11_g22379;
			float4 appendResult3_g22379 = (float4(break15_g22379.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22379 = (float4(break15_g22379.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22454 = lerp( temp_output_36_0_g22454 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22379 >= appendResult3_g22379.x && temp_output_19_0_g22379 <= appendResult16_g22379.x ) ? temp_output_19_0_g22379 :  0.0 ) ) ));
			float DissolveModifier30_g22460 = _DissolveObject12;
			float4 transform4_g22460 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22460 = transform4_g22460.y;
			float ifLocalVar46_g22460 = 0;
			if( _DissolveToggleDir12 > 0.0 )
				ifLocalVar46_g22460 = -Space24_g22460;
			else if( _DissolveToggleDir12 == 0.0 )
				ifLocalVar46_g22460 = Space24_g22460;
			float ObjectScale23_g22460 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22460 = exp2( _DissolveDensity );
			float temp_output_70_0_g22460 = ( ( (-1.0 + (DissolveModifier30_g22460 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22460 / ObjectScale23_g22460 ) ) * DissolveDensity67_g22460 );
			float4 tex2DNode61_g22460 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22460 = max( max( tex2DNode61_g22460.r , tex2DNode61_g22460.g ) , tex2DNode61_g22460.b );
			float temp_output_36_0_g22460 = saturate( ( temp_output_70_0_g22460 + DissolvePattern58_g22460 ) );
			float temp_output_19_0_g22385 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22385 = ( 12.0 / 100.0 );
			float temp_output_8_0_g22385 = ( temp_output_6_0_g22385 - 0.005 );
			float temp_output_7_0_g22385 = ( temp_output_6_0_g22385 + 0.005 );
			float2 appendResult4_g22385 = (float2(temp_output_8_0_g22385 , temp_output_7_0_g22385));
			float2 appendResult13_g22385 = (float2(-0.005 , temp_output_7_0_g22385));
			float2 appendResult14_g22385 = (float2(temp_output_8_0_g22385 , 1.005));
			float2 ifLocalVar11_g22385 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22385 = appendResult4_g22385;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22385 = appendResult13_g22385;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22385 = appendResult14_g22385;
			float2 break15_g22385 = ifLocalVar11_g22385;
			float4 appendResult3_g22385 = (float4(break15_g22385.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22385 = (float4(break15_g22385.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22460 = lerp( temp_output_36_0_g22460 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22385 >= appendResult3_g22385.x && temp_output_19_0_g22385 <= appendResult16_g22385.x ) ? temp_output_19_0_g22385 :  0.0 ) ) ));
			float DissolveModifier30_g22428 = _DissolveObject13;
			float4 transform4_g22428 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22428 = transform4_g22428.y;
			float ifLocalVar46_g22428 = 0;
			if( _DissolveToggleDir13 > 0.0 )
				ifLocalVar46_g22428 = -Space24_g22428;
			else if( _DissolveToggleDir13 == 0.0 )
				ifLocalVar46_g22428 = Space24_g22428;
			float ObjectScale23_g22428 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22428 = exp2( _DissolveDensity );
			float temp_output_70_0_g22428 = ( ( (-1.0 + (DissolveModifier30_g22428 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22428 / ObjectScale23_g22428 ) ) * DissolveDensity67_g22428 );
			float4 tex2DNode61_g22428 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22428 = max( max( tex2DNode61_g22428.r , tex2DNode61_g22428.g ) , tex2DNode61_g22428.b );
			float temp_output_36_0_g22428 = saturate( ( temp_output_70_0_g22428 + DissolvePattern58_g22428 ) );
			float temp_output_19_0_g22384 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22384 = ( 13.0 / 100.0 );
			float temp_output_8_0_g22384 = ( temp_output_6_0_g22384 - 0.005 );
			float temp_output_7_0_g22384 = ( temp_output_6_0_g22384 + 0.005 );
			float2 appendResult4_g22384 = (float2(temp_output_8_0_g22384 , temp_output_7_0_g22384));
			float2 appendResult13_g22384 = (float2(-0.005 , temp_output_7_0_g22384));
			float2 appendResult14_g22384 = (float2(temp_output_8_0_g22384 , 1.005));
			float2 ifLocalVar11_g22384 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22384 = appendResult4_g22384;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22384 = appendResult13_g22384;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22384 = appendResult14_g22384;
			float2 break15_g22384 = ifLocalVar11_g22384;
			float4 appendResult3_g22384 = (float4(break15_g22384.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22384 = (float4(break15_g22384.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22428 = lerp( temp_output_36_0_g22428 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22384 >= appendResult3_g22384.x && temp_output_19_0_g22384 <= appendResult16_g22384.x ) ? temp_output_19_0_g22384 :  0.0 ) ) ));
			float DissolveModifier30_g22431 = _DissolveObject14;
			float4 transform4_g22431 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22431 = transform4_g22431.y;
			float ifLocalVar46_g22431 = 0;
			if( _DissolveToggleDir14 > 0.0 )
				ifLocalVar46_g22431 = -Space24_g22431;
			else if( _DissolveToggleDir14 == 0.0 )
				ifLocalVar46_g22431 = Space24_g22431;
			float ObjectScale23_g22431 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22431 = exp2( _DissolveDensity );
			float temp_output_70_0_g22431 = ( ( (-1.0 + (DissolveModifier30_g22431 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22431 / ObjectScale23_g22431 ) ) * DissolveDensity67_g22431 );
			float4 tex2DNode61_g22431 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22431 = max( max( tex2DNode61_g22431.r , tex2DNode61_g22431.g ) , tex2DNode61_g22431.b );
			float temp_output_36_0_g22431 = saturate( ( temp_output_70_0_g22431 + DissolvePattern58_g22431 ) );
			float temp_output_19_0_g22406 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22406 = ( 14.0 / 100.0 );
			float temp_output_8_0_g22406 = ( temp_output_6_0_g22406 - 0.005 );
			float temp_output_7_0_g22406 = ( temp_output_6_0_g22406 + 0.005 );
			float2 appendResult4_g22406 = (float2(temp_output_8_0_g22406 , temp_output_7_0_g22406));
			float2 appendResult13_g22406 = (float2(-0.005 , temp_output_7_0_g22406));
			float2 appendResult14_g22406 = (float2(temp_output_8_0_g22406 , 1.005));
			float2 ifLocalVar11_g22406 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22406 = appendResult4_g22406;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22406 = appendResult13_g22406;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22406 = appendResult14_g22406;
			float2 break15_g22406 = ifLocalVar11_g22406;
			float4 appendResult3_g22406 = (float4(break15_g22406.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22406 = (float4(break15_g22406.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22431 = lerp( temp_output_36_0_g22431 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22406 >= appendResult3_g22406.x && temp_output_19_0_g22406 <= appendResult16_g22406.x ) ? temp_output_19_0_g22406 :  0.0 ) ) ));
			float DissolveModifier30_g22457 = _DissolveObject15;
			float4 transform4_g22457 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22457 = transform4_g22457.y;
			float ifLocalVar46_g22457 = 0;
			if( _DissolveToggleDir15 > 0.0 )
				ifLocalVar46_g22457 = -Space24_g22457;
			else if( _DissolveToggleDir15 == 0.0 )
				ifLocalVar46_g22457 = Space24_g22457;
			float ObjectScale23_g22457 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22457 = exp2( _DissolveDensity );
			float temp_output_70_0_g22457 = ( ( (-1.0 + (DissolveModifier30_g22457 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22457 / ObjectScale23_g22457 ) ) * DissolveDensity67_g22457 );
			float4 tex2DNode61_g22457 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22457 = max( max( tex2DNode61_g22457.r , tex2DNode61_g22457.g ) , tex2DNode61_g22457.b );
			float temp_output_36_0_g22457 = saturate( ( temp_output_70_0_g22457 + DissolvePattern58_g22457 ) );
			float temp_output_19_0_g22405 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22405 = ( 15.0 / 100.0 );
			float temp_output_8_0_g22405 = ( temp_output_6_0_g22405 - 0.005 );
			float temp_output_7_0_g22405 = ( temp_output_6_0_g22405 + 0.005 );
			float2 appendResult4_g22405 = (float2(temp_output_8_0_g22405 , temp_output_7_0_g22405));
			float2 appendResult13_g22405 = (float2(-0.005 , temp_output_7_0_g22405));
			float2 appendResult14_g22405 = (float2(temp_output_8_0_g22405 , 1.005));
			float2 ifLocalVar11_g22405 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22405 = appendResult4_g22405;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22405 = appendResult13_g22405;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22405 = appendResult14_g22405;
			float2 break15_g22405 = ifLocalVar11_g22405;
			float4 appendResult3_g22405 = (float4(break15_g22405.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22405 = (float4(break15_g22405.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22457 = lerp( temp_output_36_0_g22457 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22405 >= appendResult3_g22405.x && temp_output_19_0_g22405 <= appendResult16_g22405.x ) ? temp_output_19_0_g22405 :  0.0 ) ) ));
			float DissolveModifier30_g22455 = _DissolveObject16;
			float4 transform4_g22455 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22455 = transform4_g22455.y;
			float ifLocalVar46_g22455 = 0;
			if( _DissolveToggleDir16 > 0.0 )
				ifLocalVar46_g22455 = -Space24_g22455;
			else if( _DissolveToggleDir16 == 0.0 )
				ifLocalVar46_g22455 = Space24_g22455;
			float ObjectScale23_g22455 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22455 = exp2( _DissolveDensity );
			float temp_output_70_0_g22455 = ( ( (-1.0 + (DissolveModifier30_g22455 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22455 / ObjectScale23_g22455 ) ) * DissolveDensity67_g22455 );
			float4 tex2DNode61_g22455 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22455 = max( max( tex2DNode61_g22455.r , tex2DNode61_g22455.g ) , tex2DNode61_g22455.b );
			float temp_output_36_0_g22455 = saturate( ( temp_output_70_0_g22455 + DissolvePattern58_g22455 ) );
			float temp_output_19_0_g22409 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22409 = ( 16.0 / 100.0 );
			float temp_output_8_0_g22409 = ( temp_output_6_0_g22409 - 0.005 );
			float temp_output_7_0_g22409 = ( temp_output_6_0_g22409 + 0.005 );
			float2 appendResult4_g22409 = (float2(temp_output_8_0_g22409 , temp_output_7_0_g22409));
			float2 appendResult13_g22409 = (float2(-0.005 , temp_output_7_0_g22409));
			float2 appendResult14_g22409 = (float2(temp_output_8_0_g22409 , 1.005));
			float2 ifLocalVar11_g22409 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22409 = appendResult4_g22409;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22409 = appendResult13_g22409;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22409 = appendResult14_g22409;
			float2 break15_g22409 = ifLocalVar11_g22409;
			float4 appendResult3_g22409 = (float4(break15_g22409.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22409 = (float4(break15_g22409.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22455 = lerp( temp_output_36_0_g22455 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22409 >= appendResult3_g22409.x && temp_output_19_0_g22409 <= appendResult16_g22409.x ) ? temp_output_19_0_g22409 :  0.0 ) ) ));
			float DissolveModifier30_g22439 = _DissolveObject17;
			float4 transform4_g22439 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22439 = transform4_g22439.y;
			float ifLocalVar46_g22439 = 0;
			if( _DissolveToggleDir17 > 0.0 )
				ifLocalVar46_g22439 = -Space24_g22439;
			else if( _DissolveToggleDir17 == 0.0 )
				ifLocalVar46_g22439 = Space24_g22439;
			float ObjectScale23_g22439 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22439 = exp2( _DissolveDensity );
			float temp_output_70_0_g22439 = ( ( (-1.0 + (DissolveModifier30_g22439 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22439 / ObjectScale23_g22439 ) ) * DissolveDensity67_g22439 );
			float4 tex2DNode61_g22439 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22439 = max( max( tex2DNode61_g22439.r , tex2DNode61_g22439.g ) , tex2DNode61_g22439.b );
			float temp_output_36_0_g22439 = saturate( ( temp_output_70_0_g22439 + DissolvePattern58_g22439 ) );
			float temp_output_19_0_g22415 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22415 = ( 17.0 / 100.0 );
			float temp_output_8_0_g22415 = ( temp_output_6_0_g22415 - 0.005 );
			float temp_output_7_0_g22415 = ( temp_output_6_0_g22415 + 0.005 );
			float2 appendResult4_g22415 = (float2(temp_output_8_0_g22415 , temp_output_7_0_g22415));
			float2 appendResult13_g22415 = (float2(-0.005 , temp_output_7_0_g22415));
			float2 appendResult14_g22415 = (float2(temp_output_8_0_g22415 , 1.005));
			float2 ifLocalVar11_g22415 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22415 = appendResult4_g22415;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22415 = appendResult13_g22415;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22415 = appendResult14_g22415;
			float2 break15_g22415 = ifLocalVar11_g22415;
			float4 appendResult3_g22415 = (float4(break15_g22415.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22415 = (float4(break15_g22415.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22439 = lerp( temp_output_36_0_g22439 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22415 >= appendResult3_g22415.x && temp_output_19_0_g22415 <= appendResult16_g22415.x ) ? temp_output_19_0_g22415 :  0.0 ) ) ));
			float DissolveModifier30_g22446 = _DissolveObject18;
			float4 transform4_g22446 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22446 = transform4_g22446.y;
			float ifLocalVar46_g22446 = 0;
			if( _DissolveToggleDir18 > 0.0 )
				ifLocalVar46_g22446 = -Space24_g22446;
			else if( _DissolveToggleDir18 == 0.0 )
				ifLocalVar46_g22446 = Space24_g22446;
			float ObjectScale23_g22446 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22446 = exp2( _DissolveDensity );
			float temp_output_70_0_g22446 = ( ( (-1.0 + (DissolveModifier30_g22446 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22446 / ObjectScale23_g22446 ) ) * DissolveDensity67_g22446 );
			float4 tex2DNode61_g22446 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22446 = max( max( tex2DNode61_g22446.r , tex2DNode61_g22446.g ) , tex2DNode61_g22446.b );
			float temp_output_36_0_g22446 = saturate( ( temp_output_70_0_g22446 + DissolvePattern58_g22446 ) );
			float temp_output_19_0_g22404 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22404 = ( 18.0 / 100.0 );
			float temp_output_8_0_g22404 = ( temp_output_6_0_g22404 - 0.005 );
			float temp_output_7_0_g22404 = ( temp_output_6_0_g22404 + 0.005 );
			float2 appendResult4_g22404 = (float2(temp_output_8_0_g22404 , temp_output_7_0_g22404));
			float2 appendResult13_g22404 = (float2(-0.005 , temp_output_7_0_g22404));
			float2 appendResult14_g22404 = (float2(temp_output_8_0_g22404 , 1.005));
			float2 ifLocalVar11_g22404 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22404 = appendResult4_g22404;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22404 = appendResult13_g22404;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22404 = appendResult14_g22404;
			float2 break15_g22404 = ifLocalVar11_g22404;
			float4 appendResult3_g22404 = (float4(break15_g22404.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22404 = (float4(break15_g22404.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22446 = lerp( temp_output_36_0_g22446 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22404 >= appendResult3_g22404.x && temp_output_19_0_g22404 <= appendResult16_g22404.x ) ? temp_output_19_0_g22404 :  0.0 ) ) ));
			float DissolveModifier30_g22443 = _DissolveObject19;
			float4 transform4_g22443 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22443 = transform4_g22443.y;
			float ifLocalVar46_g22443 = 0;
			if( _DissolveToggleDir19 > 0.0 )
				ifLocalVar46_g22443 = -Space24_g22443;
			else if( _DissolveToggleDir19 == 0.0 )
				ifLocalVar46_g22443 = Space24_g22443;
			float ObjectScale23_g22443 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22443 = exp2( _DissolveDensity );
			float temp_output_70_0_g22443 = ( ( (-1.0 + (DissolveModifier30_g22443 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22443 / ObjectScale23_g22443 ) ) * DissolveDensity67_g22443 );
			float4 tex2DNode61_g22443 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22443 = max( max( tex2DNode61_g22443.r , tex2DNode61_g22443.g ) , tex2DNode61_g22443.b );
			float temp_output_36_0_g22443 = saturate( ( temp_output_70_0_g22443 + DissolvePattern58_g22443 ) );
			float temp_output_19_0_g22401 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22401 = ( 19.0 / 100.0 );
			float temp_output_8_0_g22401 = ( temp_output_6_0_g22401 - 0.005 );
			float temp_output_7_0_g22401 = ( temp_output_6_0_g22401 + 0.005 );
			float2 appendResult4_g22401 = (float2(temp_output_8_0_g22401 , temp_output_7_0_g22401));
			float2 appendResult13_g22401 = (float2(-0.005 , temp_output_7_0_g22401));
			float2 appendResult14_g22401 = (float2(temp_output_8_0_g22401 , 1.005));
			float2 ifLocalVar11_g22401 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22401 = appendResult4_g22401;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22401 = appendResult13_g22401;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22401 = appendResult14_g22401;
			float2 break15_g22401 = ifLocalVar11_g22401;
			float4 appendResult3_g22401 = (float4(break15_g22401.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22401 = (float4(break15_g22401.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22443 = lerp( temp_output_36_0_g22443 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22401 >= appendResult3_g22401.x && temp_output_19_0_g22401 <= appendResult16_g22401.x ) ? temp_output_19_0_g22401 :  0.0 ) ) ));
			float DissolveModifier30_g22445 = _DissolveObject20;
			float4 transform4_g22445 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22445 = transform4_g22445.y;
			float ifLocalVar46_g22445 = 0;
			if( _DissolveToggleDir20 > 0.0 )
				ifLocalVar46_g22445 = -Space24_g22445;
			else if( _DissolveToggleDir20 == 0.0 )
				ifLocalVar46_g22445 = Space24_g22445;
			float ObjectScale23_g22445 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22445 = exp2( _DissolveDensity );
			float temp_output_70_0_g22445 = ( ( (-1.0 + (DissolveModifier30_g22445 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22445 / ObjectScale23_g22445 ) ) * DissolveDensity67_g22445 );
			float4 tex2DNode61_g22445 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22445 = max( max( tex2DNode61_g22445.r , tex2DNode61_g22445.g ) , tex2DNode61_g22445.b );
			float temp_output_36_0_g22445 = saturate( ( temp_output_70_0_g22445 + DissolvePattern58_g22445 ) );
			float temp_output_19_0_g22423 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22423 = ( 20.0 / 100.0 );
			float temp_output_8_0_g22423 = ( temp_output_6_0_g22423 - 0.005 );
			float temp_output_7_0_g22423 = ( temp_output_6_0_g22423 + 0.005 );
			float2 appendResult4_g22423 = (float2(temp_output_8_0_g22423 , temp_output_7_0_g22423));
			float2 appendResult13_g22423 = (float2(-0.005 , temp_output_7_0_g22423));
			float2 appendResult14_g22423 = (float2(temp_output_8_0_g22423 , 1.005));
			float2 ifLocalVar11_g22423 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22423 = appendResult4_g22423;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22423 = appendResult13_g22423;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22423 = appendResult14_g22423;
			float2 break15_g22423 = ifLocalVar11_g22423;
			float4 appendResult3_g22423 = (float4(break15_g22423.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22423 = (float4(break15_g22423.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22445 = lerp( temp_output_36_0_g22445 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22423 >= appendResult3_g22423.x && temp_output_19_0_g22423 <= appendResult16_g22423.x ) ? temp_output_19_0_g22423 :  0.0 ) ) ));
			float DissolveModifier30_g22390 = _DissolveObject1;
			float4 transform4_g22390 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22390 = transform4_g22390.y;
			float ifLocalVar46_g22390 = 0;
			if( _DissolveToggleDir1 > 0.0 )
				ifLocalVar46_g22390 = -Space24_g22390;
			else if( _DissolveToggleDir1 == 0.0 )
				ifLocalVar46_g22390 = Space24_g22390;
			float ObjectScale23_g22390 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22390 = exp2( _DissolveDensity );
			float temp_output_70_0_g22390 = ( ( (-1.0 + (DissolveModifier30_g22390 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22390 / ObjectScale23_g22390 ) ) * DissolveDensity67_g22390 );
			float4 tex2DNode61_g22390 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22390 = max( max( tex2DNode61_g22390.r , tex2DNode61_g22390.g ) , tex2DNode61_g22390.b );
			float temp_output_36_0_g22390 = saturate( ( temp_output_70_0_g22390 + DissolvePattern58_g22390 ) );
			float temp_output_19_0_g22408 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22408 = ( 1.0 / 100.0 );
			float temp_output_8_0_g22408 = ( temp_output_6_0_g22408 - 0.005 );
			float temp_output_7_0_g22408 = ( temp_output_6_0_g22408 + 0.005 );
			float2 appendResult4_g22408 = (float2(temp_output_8_0_g22408 , temp_output_7_0_g22408));
			float2 appendResult13_g22408 = (float2(-0.005 , temp_output_7_0_g22408));
			float2 appendResult14_g22408 = (float2(temp_output_8_0_g22408 , 1.005));
			float2 ifLocalVar11_g22408 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22408 = appendResult4_g22408;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22408 = appendResult13_g22408;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22408 = appendResult14_g22408;
			float2 break15_g22408 = ifLocalVar11_g22408;
			float4 appendResult3_g22408 = (float4(break15_g22408.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22408 = (float4(break15_g22408.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22390 = lerp( temp_output_36_0_g22390 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22408 >= appendResult3_g22408.x && temp_output_19_0_g22408 <= appendResult16_g22408.x ) ? temp_output_19_0_g22408 :  0.0 ) ) ));
			float DissolveModifier30_g22429 = _DissolveObject2;
			float4 transform4_g22429 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22429 = transform4_g22429.y;
			float ifLocalVar46_g22429 = 0;
			if( _DissolveToggleDir2 > 0.0 )
				ifLocalVar46_g22429 = -Space24_g22429;
			else if( _DissolveToggleDir2 == 0.0 )
				ifLocalVar46_g22429 = Space24_g22429;
			float ObjectScale23_g22429 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22429 = exp2( _DissolveDensity );
			float temp_output_70_0_g22429 = ( ( (-1.0 + (DissolveModifier30_g22429 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22429 / ObjectScale23_g22429 ) ) * DissolveDensity67_g22429 );
			float4 tex2DNode61_g22429 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22429 = max( max( tex2DNode61_g22429.r , tex2DNode61_g22429.g ) , tex2DNode61_g22429.b );
			float temp_output_36_0_g22429 = saturate( ( temp_output_70_0_g22429 + DissolvePattern58_g22429 ) );
			float temp_output_19_0_g22382 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22382 = ( 2.0 / 100.0 );
			float temp_output_8_0_g22382 = ( temp_output_6_0_g22382 - 0.005 );
			float temp_output_7_0_g22382 = ( temp_output_6_0_g22382 + 0.005 );
			float2 appendResult4_g22382 = (float2(temp_output_8_0_g22382 , temp_output_7_0_g22382));
			float2 appendResult13_g22382 = (float2(-0.005 , temp_output_7_0_g22382));
			float2 appendResult14_g22382 = (float2(temp_output_8_0_g22382 , 1.005));
			float2 ifLocalVar11_g22382 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22382 = appendResult4_g22382;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22382 = appendResult13_g22382;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22382 = appendResult14_g22382;
			float2 break15_g22382 = ifLocalVar11_g22382;
			float4 appendResult3_g22382 = (float4(break15_g22382.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22382 = (float4(break15_g22382.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22429 = lerp( temp_output_36_0_g22429 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22382 >= appendResult3_g22382.x && temp_output_19_0_g22382 <= appendResult16_g22382.x ) ? temp_output_19_0_g22382 :  0.0 ) ) ));
			float DissolveModifier30_g22452 = _DissolveObject3;
			float4 transform4_g22452 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22452 = transform4_g22452.y;
			float ifLocalVar46_g22452 = 0;
			if( _DissolveToggleDir3 > 0.0 )
				ifLocalVar46_g22452 = -Space24_g22452;
			else if( _DissolveToggleDir3 == 0.0 )
				ifLocalVar46_g22452 = Space24_g22452;
			float ObjectScale23_g22452 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22452 = exp2( _DissolveDensity );
			float temp_output_70_0_g22452 = ( ( (-1.0 + (DissolveModifier30_g22452 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22452 / ObjectScale23_g22452 ) ) * DissolveDensity67_g22452 );
			float4 tex2DNode61_g22452 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22452 = max( max( tex2DNode61_g22452.r , tex2DNode61_g22452.g ) , tex2DNode61_g22452.b );
			float temp_output_36_0_g22452 = saturate( ( temp_output_70_0_g22452 + DissolvePattern58_g22452 ) );
			float temp_output_19_0_g22389 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22389 = ( 3.0 / 100.0 );
			float temp_output_8_0_g22389 = ( temp_output_6_0_g22389 - 0.005 );
			float temp_output_7_0_g22389 = ( temp_output_6_0_g22389 + 0.005 );
			float2 appendResult4_g22389 = (float2(temp_output_8_0_g22389 , temp_output_7_0_g22389));
			float2 appendResult13_g22389 = (float2(-0.005 , temp_output_7_0_g22389));
			float2 appendResult14_g22389 = (float2(temp_output_8_0_g22389 , 1.005));
			float2 ifLocalVar11_g22389 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22389 = appendResult4_g22389;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22389 = appendResult13_g22389;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22389 = appendResult14_g22389;
			float2 break15_g22389 = ifLocalVar11_g22389;
			float4 appendResult3_g22389 = (float4(break15_g22389.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22389 = (float4(break15_g22389.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22452 = lerp( temp_output_36_0_g22452 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22389 >= appendResult3_g22389.x && temp_output_19_0_g22389 <= appendResult16_g22389.x ) ? temp_output_19_0_g22389 :  0.0 ) ) ));
			float DissolveModifier30_g22432 = _DissolveObject4;
			float4 transform4_g22432 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22432 = transform4_g22432.y;
			float ifLocalVar46_g22432 = 0;
			if( _DissolveToggleDir4 > 0.0 )
				ifLocalVar46_g22432 = -Space24_g22432;
			else if( _DissolveToggleDir4 == 0.0 )
				ifLocalVar46_g22432 = Space24_g22432;
			float ObjectScale23_g22432 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22432 = exp2( _DissolveDensity );
			float temp_output_70_0_g22432 = ( ( (-1.0 + (DissolveModifier30_g22432 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22432 / ObjectScale23_g22432 ) ) * DissolveDensity67_g22432 );
			float4 tex2DNode61_g22432 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22432 = max( max( tex2DNode61_g22432.r , tex2DNode61_g22432.g ) , tex2DNode61_g22432.b );
			float temp_output_36_0_g22432 = saturate( ( temp_output_70_0_g22432 + DissolvePattern58_g22432 ) );
			float temp_output_19_0_g22387 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22387 = ( 4.0 / 100.0 );
			float temp_output_8_0_g22387 = ( temp_output_6_0_g22387 - 0.005 );
			float temp_output_7_0_g22387 = ( temp_output_6_0_g22387 + 0.005 );
			float2 appendResult4_g22387 = (float2(temp_output_8_0_g22387 , temp_output_7_0_g22387));
			float2 appendResult13_g22387 = (float2(-0.005 , temp_output_7_0_g22387));
			float2 appendResult14_g22387 = (float2(temp_output_8_0_g22387 , 1.005));
			float2 ifLocalVar11_g22387 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22387 = appendResult4_g22387;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22387 = appendResult13_g22387;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22387 = appendResult14_g22387;
			float2 break15_g22387 = ifLocalVar11_g22387;
			float4 appendResult3_g22387 = (float4(break15_g22387.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22387 = (float4(break15_g22387.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22432 = lerp( temp_output_36_0_g22432 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22387 >= appendResult3_g22387.x && temp_output_19_0_g22387 <= appendResult16_g22387.x ) ? temp_output_19_0_g22387 :  0.0 ) ) ));
			float DissolveModifier30_g22449 = _DissolveObject5;
			float4 transform4_g22449 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22449 = transform4_g22449.y;
			float ifLocalVar46_g22449 = 0;
			if( _DissolveToggleDir5 > 0.0 )
				ifLocalVar46_g22449 = -Space24_g22449;
			else if( _DissolveToggleDir5 == 0.0 )
				ifLocalVar46_g22449 = Space24_g22449;
			float ObjectScale23_g22449 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22449 = exp2( _DissolveDensity );
			float temp_output_70_0_g22449 = ( ( (-1.0 + (DissolveModifier30_g22449 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22449 / ObjectScale23_g22449 ) ) * DissolveDensity67_g22449 );
			float4 tex2DNode61_g22449 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22449 = max( max( tex2DNode61_g22449.r , tex2DNode61_g22449.g ) , tex2DNode61_g22449.b );
			float temp_output_36_0_g22449 = saturate( ( temp_output_70_0_g22449 + DissolvePattern58_g22449 ) );
			float temp_output_19_0_g22416 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22416 = ( 5.0 / 100.0 );
			float temp_output_8_0_g22416 = ( temp_output_6_0_g22416 - 0.005 );
			float temp_output_7_0_g22416 = ( temp_output_6_0_g22416 + 0.005 );
			float2 appendResult4_g22416 = (float2(temp_output_8_0_g22416 , temp_output_7_0_g22416));
			float2 appendResult13_g22416 = (float2(-0.005 , temp_output_7_0_g22416));
			float2 appendResult14_g22416 = (float2(temp_output_8_0_g22416 , 1.005));
			float2 ifLocalVar11_g22416 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22416 = appendResult4_g22416;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22416 = appendResult13_g22416;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22416 = appendResult14_g22416;
			float2 break15_g22416 = ifLocalVar11_g22416;
			float4 appendResult3_g22416 = (float4(break15_g22416.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22416 = (float4(break15_g22416.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22449 = lerp( temp_output_36_0_g22449 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22416 >= appendResult3_g22416.x && temp_output_19_0_g22416 <= appendResult16_g22416.x ) ? temp_output_19_0_g22416 :  0.0 ) ) ));
			float temp_output_1103_0_g22378 = lerpResult53_g22449;
			float DissolveModifier30_g22434 = _DissolveObject6;
			float4 transform4_g22434 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22434 = transform4_g22434.y;
			float ifLocalVar46_g22434 = 0;
			if( _DissolveToggleDir6 > 0.0 )
				ifLocalVar46_g22434 = -Space24_g22434;
			else if( _DissolveToggleDir6 == 0.0 )
				ifLocalVar46_g22434 = Space24_g22434;
			float ObjectScale23_g22434 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22434 = exp2( _DissolveDensity );
			float temp_output_70_0_g22434 = ( ( (-1.0 + (DissolveModifier30_g22434 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22434 / ObjectScale23_g22434 ) ) * DissolveDensity67_g22434 );
			float4 tex2DNode61_g22434 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22434 = max( max( tex2DNode61_g22434.r , tex2DNode61_g22434.g ) , tex2DNode61_g22434.b );
			float temp_output_36_0_g22434 = saturate( ( temp_output_70_0_g22434 + DissolvePattern58_g22434 ) );
			float temp_output_19_0_g22418 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22418 = ( 6.0 / 100.0 );
			float temp_output_8_0_g22418 = ( temp_output_6_0_g22418 - 0.005 );
			float temp_output_7_0_g22418 = ( temp_output_6_0_g22418 + 0.005 );
			float2 appendResult4_g22418 = (float2(temp_output_8_0_g22418 , temp_output_7_0_g22418));
			float2 appendResult13_g22418 = (float2(-0.005 , temp_output_7_0_g22418));
			float2 appendResult14_g22418 = (float2(temp_output_8_0_g22418 , 1.005));
			float2 ifLocalVar11_g22418 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22418 = appendResult4_g22418;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22418 = appendResult13_g22418;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22418 = appendResult14_g22418;
			float2 break15_g22418 = ifLocalVar11_g22418;
			float4 appendResult3_g22418 = (float4(break15_g22418.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22418 = (float4(break15_g22418.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22434 = lerp( temp_output_36_0_g22434 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22418 >= appendResult3_g22418.x && temp_output_19_0_g22418 <= appendResult16_g22418.x ) ? temp_output_19_0_g22418 :  0.0 ) ) ));
			float DissolveModifier30_g22453 = _DissolveObject7;
			float4 transform4_g22453 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22453 = transform4_g22453.y;
			float ifLocalVar46_g22453 = 0;
			if( _DissolveToggleDir7 > 0.0 )
				ifLocalVar46_g22453 = -Space24_g22453;
			else if( _DissolveToggleDir7 == 0.0 )
				ifLocalVar46_g22453 = Space24_g22453;
			float ObjectScale23_g22453 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22453 = exp2( _DissolveDensity );
			float temp_output_70_0_g22453 = ( ( (-1.0 + (DissolveModifier30_g22453 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22453 / ObjectScale23_g22453 ) ) * DissolveDensity67_g22453 );
			float4 tex2DNode61_g22453 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22453 = max( max( tex2DNode61_g22453.r , tex2DNode61_g22453.g ) , tex2DNode61_g22453.b );
			float temp_output_36_0_g22453 = saturate( ( temp_output_70_0_g22453 + DissolvePattern58_g22453 ) );
			float temp_output_19_0_g22383 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22383 = ( 7.0 / 100.0 );
			float temp_output_8_0_g22383 = ( temp_output_6_0_g22383 - 0.005 );
			float temp_output_7_0_g22383 = ( temp_output_6_0_g22383 + 0.005 );
			float2 appendResult4_g22383 = (float2(temp_output_8_0_g22383 , temp_output_7_0_g22383));
			float2 appendResult13_g22383 = (float2(-0.005 , temp_output_7_0_g22383));
			float2 appendResult14_g22383 = (float2(temp_output_8_0_g22383 , 1.005));
			float2 ifLocalVar11_g22383 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22383 = appendResult4_g22383;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22383 = appendResult13_g22383;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22383 = appendResult14_g22383;
			float2 break15_g22383 = ifLocalVar11_g22383;
			float4 appendResult3_g22383 = (float4(break15_g22383.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22383 = (float4(break15_g22383.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22453 = lerp( temp_output_36_0_g22453 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22383 >= appendResult3_g22383.x && temp_output_19_0_g22383 <= appendResult16_g22383.x ) ? temp_output_19_0_g22383 :  0.0 ) ) ));
			float DissolveModifier30_g22444 = _DissolveObject8;
			float4 transform4_g22444 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22444 = transform4_g22444.y;
			float ifLocalVar46_g22444 = 0;
			if( _DissolveToggleDir8 > 0.0 )
				ifLocalVar46_g22444 = -Space24_g22444;
			else if( _DissolveToggleDir8 == 0.0 )
				ifLocalVar46_g22444 = Space24_g22444;
			float ObjectScale23_g22444 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22444 = exp2( _DissolveDensity );
			float temp_output_70_0_g22444 = ( ( (-1.0 + (DissolveModifier30_g22444 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22444 / ObjectScale23_g22444 ) ) * DissolveDensity67_g22444 );
			float4 tex2DNode61_g22444 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22444 = max( max( tex2DNode61_g22444.r , tex2DNode61_g22444.g ) , tex2DNode61_g22444.b );
			float temp_output_36_0_g22444 = saturate( ( temp_output_70_0_g22444 + DissolvePattern58_g22444 ) );
			float temp_output_19_0_g22421 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22421 = ( 8.0 / 100.0 );
			float temp_output_8_0_g22421 = ( temp_output_6_0_g22421 - 0.005 );
			float temp_output_7_0_g22421 = ( temp_output_6_0_g22421 + 0.005 );
			float2 appendResult4_g22421 = (float2(temp_output_8_0_g22421 , temp_output_7_0_g22421));
			float2 appendResult13_g22421 = (float2(-0.005 , temp_output_7_0_g22421));
			float2 appendResult14_g22421 = (float2(temp_output_8_0_g22421 , 1.005));
			float2 ifLocalVar11_g22421 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22421 = appendResult4_g22421;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22421 = appendResult13_g22421;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22421 = appendResult14_g22421;
			float2 break15_g22421 = ifLocalVar11_g22421;
			float4 appendResult3_g22421 = (float4(break15_g22421.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22421 = (float4(break15_g22421.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22444 = lerp( temp_output_36_0_g22444 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22421 >= appendResult3_g22421.x && temp_output_19_0_g22421 <= appendResult16_g22421.x ) ? temp_output_19_0_g22421 :  0.0 ) ) ));
			float temp_output_1100_0_g22378 = lerpResult53_g22444;
			float DissolveModifier30_g22426 = _DissolveObject9;
			float4 transform4_g22426 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22426 = transform4_g22426.y;
			float ifLocalVar46_g22426 = 0;
			if( _DissolveToggleDir9 > 0.0 )
				ifLocalVar46_g22426 = -Space24_g22426;
			else if( _DissolveToggleDir9 == 0.0 )
				ifLocalVar46_g22426 = Space24_g22426;
			float ObjectScale23_g22426 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22426 = exp2( _DissolveDensity );
			float temp_output_70_0_g22426 = ( ( (-1.0 + (DissolveModifier30_g22426 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22426 / ObjectScale23_g22426 ) ) * DissolveDensity67_g22426 );
			float4 tex2DNode61_g22426 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22426 = max( max( tex2DNode61_g22426.r , tex2DNode61_g22426.g ) , tex2DNode61_g22426.b );
			float temp_output_36_0_g22426 = saturate( ( temp_output_70_0_g22426 + DissolvePattern58_g22426 ) );
			float temp_output_19_0_g22402 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22402 = ( 9.0 / 100.0 );
			float temp_output_8_0_g22402 = ( temp_output_6_0_g22402 - 0.005 );
			float temp_output_7_0_g22402 = ( temp_output_6_0_g22402 + 0.005 );
			float2 appendResult4_g22402 = (float2(temp_output_8_0_g22402 , temp_output_7_0_g22402));
			float2 appendResult13_g22402 = (float2(-0.005 , temp_output_7_0_g22402));
			float2 appendResult14_g22402 = (float2(temp_output_8_0_g22402 , 1.005));
			float2 ifLocalVar11_g22402 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22402 = appendResult4_g22402;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22402 = appendResult13_g22402;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22402 = appendResult14_g22402;
			float2 break15_g22402 = ifLocalVar11_g22402;
			float4 appendResult3_g22402 = (float4(break15_g22402.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22402 = (float4(break15_g22402.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22426 = lerp( temp_output_36_0_g22426 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22402 >= appendResult3_g22402.x && temp_output_19_0_g22402 <= appendResult16_g22402.x ) ? temp_output_19_0_g22402 :  0.0 ) ) ));
			float DissolveModifier30_g22425 = _DissolveObject10;
			float4 transform4_g22425 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22425 = transform4_g22425.y;
			float ifLocalVar46_g22425 = 0;
			if( _DissolveToggleDir10 > 0.0 )
				ifLocalVar46_g22425 = -Space24_g22425;
			else if( _DissolveToggleDir10 == 0.0 )
				ifLocalVar46_g22425 = Space24_g22425;
			float ObjectScale23_g22425 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22425 = exp2( _DissolveDensity );
			float temp_output_70_0_g22425 = ( ( (-1.0 + (DissolveModifier30_g22425 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22425 / ObjectScale23_g22425 ) ) * DissolveDensity67_g22425 );
			float4 tex2DNode61_g22425 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22425 = max( max( tex2DNode61_g22425.r , tex2DNode61_g22425.g ) , tex2DNode61_g22425.b );
			float temp_output_36_0_g22425 = saturate( ( temp_output_70_0_g22425 + DissolvePattern58_g22425 ) );
			float temp_output_19_0_g22381 = MaterialzeMask165_g22378.r;
			float temp_output_6_0_g22381 = ( 10.0 / 100.0 );
			float temp_output_8_0_g22381 = ( temp_output_6_0_g22381 - 0.005 );
			float temp_output_7_0_g22381 = ( temp_output_6_0_g22381 + 0.005 );
			float2 appendResult4_g22381 = (float2(temp_output_8_0_g22381 , temp_output_7_0_g22381));
			float2 appendResult13_g22381 = (float2(-0.005 , temp_output_7_0_g22381));
			float2 appendResult14_g22381 = (float2(temp_output_8_0_g22381 , 1.005));
			float2 ifLocalVar11_g22381 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22381 = appendResult4_g22381;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22381 = appendResult13_g22381;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22381 = appendResult14_g22381;
			float2 break15_g22381 = ifLocalVar11_g22381;
			float4 appendResult3_g22381 = (float4(break15_g22381.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22381 = (float4(break15_g22381.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22425 = lerp( temp_output_36_0_g22425 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22381 >= appendResult3_g22381.x && temp_output_19_0_g22381 <= appendResult16_g22381.x ) ? temp_output_19_0_g22381 :  0.0 ) ) ));
			float4 temp_cast_229 = (( 0.0 + ( ( lerpResult53_g22441 + lerpResult53_g22448 + lerpResult53_g22447 + lerpResult53_g22442 + lerpResult53_g22430 + lerpResult53_g22427 + lerpResult53_g22435 + lerpResult53_g22438 + lerpResult53_g22456 + lerpResult53_g22411 ) + ( lerpResult53_g22458 + lerpResult53_g22450 + lerpResult53_g22451 + lerpResult53_g22396 + lerpResult53_g22436 + lerpResult53_g22437 + lerpResult53_g22459 + lerpResult53_g22424 + lerpResult53_g22422 + lerpResult53_g22440 ) + ( lerpResult53_g22454 + lerpResult53_g22460 + lerpResult53_g22428 + lerpResult53_g22431 + lerpResult53_g22457 + lerpResult53_g22455 + lerpResult53_g22439 + lerpResult53_g22446 + lerpResult53_g22443 + lerpResult53_g22445 ) + ( lerpResult53_g22390 + lerpResult53_g22429 + lerpResult53_g22452 + lerpResult53_g22432 + temp_output_1103_0_g22378 + lerpResult53_g22434 + lerpResult53_g22453 + temp_output_1100_0_g22378 + lerpResult53_g22426 + lerpResult53_g22425 ) ) )).xxxx;
			float DissolveModifier30_g22433 = _GlobalDissolve;
			float4 transform4_g22433 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22433 = transform4_g22433.y;
			float ifLocalVar46_g22433 = 0;
			if( _GlobalDissolveDir > 0.0 )
				ifLocalVar46_g22433 = -Space24_g22433;
			else if( _GlobalDissolveDir == 0.0 )
				ifLocalVar46_g22433 = Space24_g22433;
			float ObjectScale23_g22433 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22433 = exp2( _DissolveDensity );
			float temp_output_70_0_g22433 = ( ( (-1.0 + (DissolveModifier30_g22433 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22433 / ObjectScale23_g22433 ) ) * DissolveDensity67_g22433 );
			float4 tex2DNode61_g22433 = SAMPLE_TEXTURE2D( _DissolvePattern, sampler_trilinear_repeat, uv_DissolvePattern );
			float DissolvePattern58_g22433 = max( max( tex2DNode61_g22433.r , tex2DNode61_g22433.g ) , tex2DNode61_g22433.b );
			float temp_output_36_0_g22433 = saturate( ( temp_output_70_0_g22433 + DissolvePattern58_g22433 ) );
			float4 color938_g22378 = IsGammaSpace() ? float4(1,1,1,0) : float4(1,1,1,0);
			float temp_output_19_0_g22400 = color938_g22378.r;
			float temp_output_6_0_g22400 = ( 100.0 / 100.0 );
			float temp_output_8_0_g22400 = ( temp_output_6_0_g22400 - 0.005 );
			float temp_output_7_0_g22400 = ( temp_output_6_0_g22400 + 0.005 );
			float2 appendResult4_g22400 = (float2(temp_output_8_0_g22400 , temp_output_7_0_g22400));
			float2 appendResult13_g22400 = (float2(-0.005 , temp_output_7_0_g22400));
			float2 appendResult14_g22400 = (float2(temp_output_8_0_g22400 , 1.005));
			float2 ifLocalVar11_g22400 = 0;
			if( 1.0 > _MaterializeLayerModeR )
				ifLocalVar11_g22400 = appendResult4_g22400;
			else if( 1.0 == _MaterializeLayerModeR )
				ifLocalVar11_g22400 = appendResult13_g22400;
			else if( 1.0 < _MaterializeLayerModeR )
				ifLocalVar11_g22400 = appendResult14_g22400;
			float2 break15_g22400 = ifLocalVar11_g22400;
			float4 appendResult3_g22400 = (float4(break15_g22400.x , 0.0 , 0.0 , 0.0));
			float4 appendResult16_g22400 = (float4(break15_g22400.y , 0.0 , 0.0 , 0.0));
			float lerpResult53_g22433 = lerp( temp_output_36_0_g22433 , 0.0 , ( 1.0 - ceil( (( temp_output_19_0_g22400 >= appendResult3_g22400.x && temp_output_19_0_g22400 <= appendResult16_g22400.x ) ? temp_output_19_0_g22400 :  0.0 ) ) ));
			float4 temp_cast_234 = (( 1.0 - lerpResult53_g22433 )).xxxx;
			float3 temp_output_150_0_g22165 = MainTexSaturate2197;
			float MonochromeTogglePixelLight1337_g22165 = _ToggleMonochromePixelLight;
			float ToggleSteps66_g22165 = _ToggleSteps;
			float4 localFourLightPosX340_g21922 = FourLightPosX();
			float3 ase_worldPos = i.worldPos;
			float4 temp_cast_236 = (ase_worldPos.x).xxxx;
			float4 FourLightPosX0WorldPos286_g21922 = ( localFourLightPosX340_g21922 - temp_cast_236 );
			float4 localFourLightPosY342_g21922 = FourLightPosY();
			float4 temp_cast_237 = (ase_worldPos.y).xxxx;
			float4 FourLightPosY0WorldPos291_g21922 = ( localFourLightPosY342_g21922 - temp_cast_237 );
			float4 localFourLightPosZ296_g21922 = FourLightPosZ();
			float4 temp_cast_238 = (ase_worldPos.z).xxxx;
			float4 FourLightPosZ0WorldPos325_g21922 = ( localFourLightPosZ296_g21922 - temp_cast_238 );
			float4 temp_cast_239 = (1E-06).xxxx;
			float4 temp_output_328_0_g21922 = max( ( ( FourLightPosX0WorldPos286_g21922 * FourLightPosX0WorldPos286_g21922 ) + ( FourLightPosY0WorldPos291_g21922 * FourLightPosY0WorldPos291_g21922 ) + ( FourLightPosZ0WorldPos325_g21922 * FourLightPosZ0WorldPos325_g21922 ) ) , temp_cast_239 );
			float3 _DefaultTangentVector = float3(0,0,1);
			float UVSwitchProp88_g21919 = _NormalMapUVSwitch;
			float2 UV088_g21919 = i.uv_texcoord;
			float2 UV188_g21919 = i.uv2_texcoord2;
			float2 UV288_g21919 = i.uv3_texcoord3;
			float2 UV388_g21919 = i.uv4_texcoord4;
			float2 localUVSwitch88_g21919 = UVSwitch( UVSwitchProp88_g21919 , UV088_g21919 , UV188_g21919 , UV288_g21919 , UV388_g21919 );
			float UVSwitchProp107_g21919 = _SecondaryNormalMaskUVSwitch;
			float2 UV0107_g21919 = i.uv_texcoord;
			float2 UV1107_g21919 = i.uv2_texcoord2;
			float2 UV2107_g21919 = i.uv3_texcoord3;
			float2 UV3107_g21919 = i.uv4_texcoord4;
			float2 localUVSwitch107_g21919 = UVSwitch( UVSwitchProp107_g21919 , UV0107_g21919 , UV1107_g21919 , UV2107_g21919 , UV3107_g21919 );
			float4 tex2DNode65_g21919 = SAMPLE_TEXTURE2D( _SecondaryNormalMask, sampler_trilinear_repeat, ( ( _SecondaryNormalMask_ST.xy * localUVSwitch107_g21919 ) + _SecondaryNormalMask_ST.zw ) );
			float3 lerpResult63_g21919 = lerp( _DefaultTangentVector , UnpackScaleNormal( SAMPLE_TEXTURE2D( _NormalMap, sampler_trilinear_repeat, ( ( _NormalMap_ST.xy * localUVSwitch88_g21919 ) + _NormalMap_ST.zw ) ), _NormalScale ) , tex2DNode65_g21919.a);
			float UVSwitchProp98_g21919 = _SecondaryNormalUVSwitch;
			float2 UV098_g21919 = i.uv_texcoord;
			float2 UV198_g21919 = i.uv2_texcoord2;
			float2 UV298_g21919 = i.uv3_texcoord3;
			float2 UV398_g21919 = i.uv4_texcoord4;
			float2 localUVSwitch98_g21919 = UVSwitch( UVSwitchProp98_g21919 , UV098_g21919 , UV198_g21919 , UV298_g21919 , UV398_g21919 );
			float3 lerpResult58_g21919 = lerp( _DefaultTangentVector , UnpackScaleNormal( SAMPLE_TEXTURE2D( _SecondaryNormal, sampler_trilinear_repeat, ( ( _SecondaryNormal_ST.xy * localUVSwitch98_g21919 ) + _SecondaryNormal_ST.zw ) ), _SecondaryNormalScale ) , tex2DNode65_g21919.g);
			float3 temp_output_54_0_g21919 = BlendNormals( lerpResult63_g21919 , lerpResult58_g21919 );
			float3 newWorldNormal50_g21919 = (WorldNormalVector( i , temp_output_54_0_g21919 ));
			float localOutlineSwitch128_g21919 = ( 0.0 );
			float3 true128_g21919 = newWorldNormal50_g21919;
			float3 false128_g21919 = -newWorldNormal50_g21919;
			float3 Out0128_g21919 = float3( 0,0,0 );
			{
			#ifdef FOROUTLINE
			Out0128_g21919 = true128_g21919; //Outline
			#else
			Out0128_g21919 = false128_g21919; //Not Outline
			#endif
			#define FOROUTLINE
			}
			float3 switchResult119_g21919 = (((i.ASEVFace>0)?(newWorldNormal50_g21919):(Out0128_g21919)));
			float3 normalizeResult53_g21919 = normalize( switchResult119_g21919 );
			float3 worldnormals2351 = normalizeResult53_g21919;
			float3 WorldNormals20_g21922 = worldnormals2351;
			float3 break295_g21922 = WorldNormals20_g21922;
			float4 temp_output_366_0_g21922 = ( rsqrt( temp_output_328_0_g21922 ) * ( ( FourLightPosX0WorldPos286_g21922 * break295_g21922.x ) + ( FourLightPosY0WorldPos291_g21922 * break295_g21922.y ) + ( FourLightPosZ0WorldPos325_g21922 * break295_g21922.z ) ) );
			float4 VertexLightNdLNONMAX21088 = temp_output_366_0_g21922;
			float NdLHalfingControl704_g22165 = _NdLHalfingControl;
			float RampOffset167_g22165 = _RampOffset;
			float4 temp_output_1224_0_g22165 = saturate( ( ( VertexLightNdLNONMAX21088 * NdLHalfingControl704_g22165 ) + RampOffset167_g22165 ) );
			float4 VertexLightUV349_g22165 = temp_output_1224_0_g22165;
			float4 break548_g22165 = VertexLightUV349_g22165;
			float2 temp_cast_240 = (break548_g22165.x).xx;
			float4 tex2DNode647_g22165 = SAMPLE_TEXTURE2D( _ToonRamp, sampler_linear_clamp, temp_cast_240 );
			float3 ToonRampTexVLOne2100_g22165 = (tex2DNode647_g22165).rgb;
			float2 uv_ShadowColorMap = i.uv_texcoord * _ShadowColorMap_ST.xy + _ShadowColorMap_ST.zw;
			float4 tex2DNode1489_g22165 = SAMPLE_TEXTURE2D( _ShadowColorMap, sampler_trilinear_clamp, uv_ShadowColorMap );
			float3 lerpResult1511_g22165 = lerp( (_RampColor).rgb , (tex2DNode1489_g22165).rgb , ( max( max( tex2DNode1489_g22165.r , tex2DNode1489_g22165.g ) , tex2DNode1489_g22165.b ) * _ShadowColorMapStrength ));
			float3 RampColorRGB42_g22165 = lerpResult1511_g22165;
			float ColoringPointLights1080_g22165 = _ColoringPointLights;
			float3 lerpResult2003_g22165 = lerp( ToonRampTexVLOne2100_g22165 , RampColorRGB42_g22165 , ColoringPointLights1080_g22165);
			half3 linearRgb1700_g22165 = tex2DNode647_g22165.rgb;
			half localgetLinearRgbToLuminance1700_g22165 = getLinearRgbToLuminance( linearRgb1700_g22165 );
			float2 temp_cast_242 = (break548_g22165.y).xx;
			float4 tex2DNode648_g22165 = SAMPLE_TEXTURE2D( _ToonRamp, sampler_linear_clamp, temp_cast_242 );
			half3 linearRgb1701_g22165 = tex2DNode648_g22165.rgb;
			half localgetLinearRgbToLuminance1701_g22165 = getLinearRgbToLuminance( linearRgb1701_g22165 );
			float2 temp_cast_244 = (break548_g22165.z).xx;
			float4 tex2DNode649_g22165 = SAMPLE_TEXTURE2D( _ToonRamp, sampler_linear_clamp, temp_cast_244 );
			half3 linearRgb1702_g22165 = tex2DNode649_g22165.rgb;
			half localgetLinearRgbToLuminance1702_g22165 = getLinearRgbToLuminance( linearRgb1702_g22165 );
			float2 temp_cast_246 = (break548_g22165.w).xx;
			float4 tex2DNode650_g22165 = SAMPLE_TEXTURE2D( _ToonRamp, sampler_linear_clamp, temp_cast_246 );
			half3 linearRgb1703_g22165 = tex2DNode650_g22165.rgb;
			half localgetLinearRgbToLuminance1703_g22165 = getLinearRgbToLuminance( linearRgb1703_g22165 );
			float4 appendResult538_g22165 = (float4(localgetLinearRgbToLuminance1700_g22165 , localgetLinearRgbToLuminance1701_g22165 , localgetLinearRgbToLuminance1702_g22165 , localgetLinearRgbToLuminance1703_g22165));
			float4 ToonRampTexVertexLightLuminanced352_g22165 = appendResult538_g22165;
			float PointSpotShadows2112_g22165 = _PointSpotShadowIntensity;
			float4 break390_g22165 = saturate( ( ToonRampTexVertexLightLuminanced352_g22165 + ( 1.0 - PointSpotShadows2112_g22165 ) ) );
			float3 lerpResult2004_g22165 = lerp( lerpResult2003_g22165 , float3( 1,1,1 ) , break390_g22165.x);
			float3 localLightColorZero385_g22165 = LightColorZero();
			float4 localFourLightAtten305_g21922 = FourLightAtten();
			float4 temp_cast_248 = (1E-06).xxxx;
			float4 temp_output_272_0_g21922 = ( localFourLightAtten305_g21922 * temp_output_328_0_g21922 );
			float4 temp_cast_249 = (1E-06).xxxx;
			float4 temp_output_343_0_g21922 = saturate( ( 1.0 - ( temp_output_272_0_g21922 / 25.0 ) ) );
			float4 temp_output_320_0_g21922 = min( ( 1.0 / ( 1.0 + temp_output_272_0_g21922 ) ) , ( temp_output_343_0_g21922 * temp_output_343_0_g21922 ) );
			float4 VertexLightAtten21183 = temp_output_320_0_g21922;
			float4 temp_output_1306_0_g22165 = VertexLightAtten21183;
			float4 break2039_g22165 = temp_output_1306_0_g22165;
			float3 ToonRampTexVLTwo2103_g22165 = (tex2DNode648_g22165).rgb;
			float3 lerpResult2029_g22165 = lerp( ToonRampTexVLTwo2103_g22165 , RampColorRGB42_g22165 , ColoringPointLights1080_g22165);
			float3 lerpResult2028_g22165 = lerp( lerpResult2029_g22165 , float3( 1,1,1 ) , break390_g22165.y);
			float3 localLightColorZOne1302_g22165 = LightColorZOne();
			float3 ToonRampTexVLThree2105_g22165 = (tex2DNode649_g22165).rgb;
			float3 lerpResult2033_g22165 = lerp( ToonRampTexVLThree2105_g22165 , RampColorRGB42_g22165 , ColoringPointLights1080_g22165);
			float3 lerpResult2032_g22165 = lerp( lerpResult2033_g22165 , float3( 1,1,1 ) , break390_g22165.z);
			float3 localLightColorZTwo1303_g22165 = LightColorZTwo();
			float3 ToonRampTexVLFour2107_g22165 = (tex2DNode650_g22165).rgb;
			float3 lerpResult2037_g22165 = lerp( ToonRampTexVLFour2107_g22165 , RampColorRGB42_g22165 , ColoringPointLights1080_g22165);
			float3 lerpResult2036_g22165 = lerp( lerpResult2037_g22165 , float3( 1,1,1 ) , break390_g22165.w);
			float3 localLightColorZThree1304_g22165 = LightColorZThree();
			int Steps30_g22165 = _Steps;
			float4 VertexLightNdLStepped2141_g22165 = saturate( ( floor( ( temp_output_1224_0_g22165 * Steps30_g22165 ) ) / ( Steps30_g22165 - 1 ) ) );
			float4 break2090_g22165 = saturate( ( VertexLightNdLStepped2141_g22165 + ( 1.0 - PointSpotShadows2112_g22165 ) ) );
			float3 temp_cast_250 = (break2090_g22165.x).xxx;
			float3 lerpResult2097_g22165 = lerp( temp_cast_250 , RampColorRGB42_g22165 , ColoringPointLights1080_g22165);
			float3 lerpResult2089_g22165 = lerp( lerpResult2097_g22165 , float3( 1,1,1 ) , break2090_g22165.x);
			float3 localLightColorZero2091_g22165 = LightColorZero();
			float4 break2074_g22165 = temp_output_1306_0_g22165;
			float3 temp_cast_251 = (break2090_g22165.y).xxx;
			float3 lerpResult2084_g22165 = lerp( temp_cast_251 , RampColorRGB42_g22165 , ColoringPointLights1080_g22165);
			float3 lerpResult2087_g22165 = lerp( lerpResult2084_g22165 , float3( 1,1,1 ) , break2090_g22165.y);
			float3 localLightColorZOne2083_g22165 = LightColorZOne();
			float3 temp_cast_252 = (break2090_g22165.z).xxx;
			float3 lerpResult2081_g22165 = lerp( temp_cast_252 , RampColorRGB42_g22165 , ColoringPointLights1080_g22165);
			float3 lerpResult2073_g22165 = lerp( lerpResult2081_g22165 , float3( 1,1,1 ) , break2090_g22165.z);
			float3 localLightColorZTwo2077_g22165 = LightColorZTwo();
			float3 temp_cast_253 = (break2090_g22165.w).xxx;
			float3 lerpResult2093_g22165 = lerp( temp_cast_253 , RampColorRGB42_g22165 , ColoringPointLights1080_g22165);
			float3 lerpResult2095_g22165 = lerp( lerpResult2093_g22165 , float3( 1,1,1 ) , break2090_g22165.w);
			float3 localLightColorZThree2080_g22165 = LightColorZThree();
			float3 ifLocalVar553_g22165 = 0;
			if( 1.0 > ToggleSteps66_g22165 )
				ifLocalVar553_g22165 = ( ( lerpResult2004_g22165 * localLightColorZero385_g22165 * break2039_g22165.x ) + ( lerpResult2028_g22165 * localLightColorZOne1302_g22165 * break2039_g22165.y ) + ( lerpResult2032_g22165 * localLightColorZTwo1303_g22165 * break2039_g22165.z ) + ( lerpResult2036_g22165 * localLightColorZThree1304_g22165 * break2039_g22165.w ) );
			else if( 1.0 == ToggleSteps66_g22165 )
				ifLocalVar553_g22165 = ( ( lerpResult2089_g22165 * localLightColorZero2091_g22165 * break2074_g22165.x ) + ( lerpResult2087_g22165 * localLightColorZOne2083_g22165 * break2074_g22165.y ) + ( lerpResult2073_g22165 * localLightColorZTwo2077_g22165 * break2074_g22165.z ) + ( lerpResult2095_g22165 * localLightColorZThree2080_g22165 * break2074_g22165.w ) );
			half3 linearRgb1433_g22165 = ifLocalVar553_g22165;
			half localgetLinearRgbToLuminance1433_g22165 = getLinearRgbToLuminance( linearRgb1433_g22165 );
			float3 temp_cast_254 = (localgetLinearRgbToLuminance1433_g22165).xxx;
			float3 ifLocalVar1341_g22165 = 0;
			if( 1.0 > MonochromeTogglePixelLight1337_g22165 )
				ifLocalVar1341_g22165 = ifLocalVar553_g22165;
			else if( 1.0 == MonochromeTogglePixelLight1337_g22165 )
				ifLocalVar1341_g22165 = temp_cast_254;
			#ifdef UNITY_PASS_FORWARDBASE
				float3 staticSwitch1927_g22165 = ifLocalVar1341_g22165;
			#else
				float3 staticSwitch1927_g22165 = float3( 0,0,0 );
			#endif
			#ifdef VERTEXLIGHT_ON
				float3 staticSwitch1928_g22165 = staticSwitch1927_g22165;
			#else
				float3 staticSwitch1928_g22165 = float3( 0,0,0 );
			#endif
			float3 DiffuseVertexLighting354_g22165 = staticSwitch1928_g22165;
			float3 temp_output_1997_0_g22165 = ( DiffuseVertexLighting354_g22165 * _MaxLightDirect );
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			#ifdef DIRECTIONAL
				float3 staticSwitch952_g22165 = ase_lightColor.rgb;
			#else
				float3 staticSwitch952_g22165 = float3( 0,0,0 );
			#endif
			half3 localAmbient1767_g22165 = Ambient();
			half3 localAmbient1820_g22165 = Ambient();
			float3 break1831_g22165 = localAmbient1820_g22165;
			float3 lerpResult1766_g22165 = lerp( ( localAmbient1767_g22165 * _AmbientBoost ) , localAmbient1767_g22165 , saturate( max( max( break1831_g22165.x , break1831_g22165.y ) , break1831_g22165.z ) ));
			float3 AmbientLightBoosted1782_g22165 = lerpResult1766_g22165;
			half3 linearRgb1430_g22165 = ( staticSwitch952_g22165 + AmbientLightBoosted1782_g22165 );
			half localgetLinearRgbToLuminance1430_g22165 = getLinearRgbToLuminance( linearRgb1430_g22165 );
			half3 linearRgb1431_g22165 = ase_lightColor.rgb;
			half localgetLinearRgbToLuminance1431_g22165 = getLinearRgbToLuminance( linearRgb1431_g22165 );
			float3 temp_cast_255 = (localgetLinearRgbToLuminance1431_g22165).xxx;
			float3 ifLocalVar1331_g22165 = 0;
			if( 1.0 > MonochromeTogglePixelLight1337_g22165 )
				ifLocalVar1331_g22165 = ase_lightColor.rgb;
			else if( 1.0 == MonochromeTogglePixelLight1337_g22165 )
				ifLocalVar1331_g22165 = temp_cast_255;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float dotResult3_g21922 = dot( WorldNormals20_g21922 , ase_worldlightDir );
			float NdotL19801 = dotResult3_g21922;
			float NdotLHalfed204_g22165 = saturate( ( ( NdotL19801 * NdLHalfingControl704_g22165 ) + RampOffset167_g22165 ) );
			float2 temp_cast_256 = (NdotLHalfed204_g22165).xx;
			float3 ToonRampTexNDL207_g22165 = (SAMPLE_TEXTURE2D( _ToonRamp, sampler_linear_clamp, temp_cast_256 )).rgb;
			float DirectShadowIntensity163_g22165 = _DirectShadowIntensity;
			float3 lerpResult1929_g22165 = lerp( float3( 1,1,1 ) , ToonRampTexNDL207_g22165 , DirectShadowIntensity163_g22165);
			float3 lerpResult2115_g22165 = lerp( float3( 1,1,1 ) , ToonRampTexNDL207_g22165 , PointSpotShadows2112_g22165);
			#ifdef UNITY_PASS_FORWARDADD
				float3 staticSwitch2068_g22165 = lerpResult2115_g22165;
			#else
				float3 staticSwitch2068_g22165 = lerpResult1929_g22165;
			#endif
			float3 _egg = float3(1,1,1);
			float TangentNormalAtten1259_g22165 = ase_lightAtten;
			float2 temp_cast_257 = (TangentNormalAtten1259_g22165).xx;
			float3 ToonRampTexATTEN216_g22165 = (SAMPLE_TEXTURE2D( _ToonRamp, sampler_linear_clamp, temp_cast_257 )).rgb;
			#ifdef DIRECTIONAL
				float3 staticSwitch1968_g22165 = ToonRampTexATTEN216_g22165;
			#else
				float3 staticSwitch1968_g22165 = _egg;
			#endif
			float3 DirectionalAttenuationRamp24_g22165 = staticSwitch1968_g22165;
			float SelfCastShadows1958_g22165 = _SelfCastShadows;
			float3 lerpResult1941_g22165 = lerp( float3( 1,1,1 ) , DirectionalAttenuationRamp24_g22165 , SelfCastShadows1958_g22165);
			float temp_output_708_0_g22165 = saturate( ( floor( ( NdotLHalfed204_g22165 * Steps30_g22165 ) ) / ( Steps30_g22165 - 1 ) ) );
			float lerpResult1945_g22165 = lerp( 1.0 , temp_output_708_0_g22165 , DirectShadowIntensity163_g22165);
			float lerpResult2131_g22165 = lerp( 1.0 , temp_output_708_0_g22165 , PointSpotShadows2112_g22165);
			#ifdef UNITY_PASS_FORWARDADD
				float staticSwitch2129_g22165 = lerpResult2131_g22165;
			#else
				float staticSwitch2129_g22165 = lerpResult1945_g22165;
			#endif
			#ifdef DIRECTIONAL
				float staticSwitch1967_g22165 = TangentNormalAtten1259_g22165;
			#else
				float staticSwitch1967_g22165 = 1.0;
			#endif
			float DirectionalAttenuationSteps1969_g22165 = staticSwitch1967_g22165;
			float temp_output_1936_0_g22165 = saturate( ( floor( ( DirectionalAttenuationSteps1969_g22165 * Steps30_g22165 ) ) / ( Steps30_g22165 - 1 ) ) );
			float lerpResult1942_g22165 = lerp( 1.0 , temp_output_1936_0_g22165 , SelfCastShadows1958_g22165);
			float3 temp_cast_258 = (min( staticSwitch2129_g22165 , lerpResult1942_g22165 )).xxx;
			float3 ifLocalVar71_g22165 = 0;
			UNITY_BRANCH 
			if( 1.0 > ToggleSteps66_g22165 )
				ifLocalVar71_g22165 = min( staticSwitch2068_g22165 , lerpResult1941_g22165 );
			else if( 1.0 == ToggleSteps66_g22165 )
				ifLocalVar71_g22165 = temp_cast_258;
			float2 uv_ShadowMask = i.uv_texcoord * _ShadowMask_ST.xy + _ShadowMask_ST.zw;
			float2 temp_output_698_0_g22165 = (SAMPLE_TEXTURE2D( _ShadowMask, sampler_trilinear_clamp, uv_ShadowMask )).rg;
			float2 ifLocalVar597_g22165 = 0;
			if( 1.0 > _ShadowMaskinvert )
				ifLocalVar597_g22165 = temp_output_698_0_g22165;
			else if( 1.0 == _ShadowMaskinvert )
				ifLocalVar597_g22165 = ( 1.0 - temp_output_698_0_g22165 );
			float2 break699_g22165 = ( ifLocalVar597_g22165 * _ShadowMaskStrength );
			float PixelShadowMask279_g22165 = break699_g22165.x;
			float3 lerpResult290_g22165 = lerp( float3( 1,1,1 ) , ifLocalVar71_g22165 , PixelShadowMask279_g22165);
			float3 LightRamp85_g22165 = lerpResult290_g22165;
			float3 lerpResult1624_g22165 = lerp( LightRamp85_g22165 , RampColorRGB42_g22165 , ColoringPointLights1080_g22165);
			half3 linearRgb2055_g22165 = LightRamp85_g22165;
			half localgetLinearRgbToLuminance2055_g22165 = getLinearRgbToLuminance( linearRgb2055_g22165 );
			float3 lerpResult1623_g22165 = lerp( lerpResult1624_g22165 , float3( 1,1,1 ) , localgetLinearRgbToLuminance2055_g22165);
			#ifdef UNITY_PASS_FORWARDADD
				float3 staticSwitch1036_g22165 = ( lerpResult1623_g22165 * ase_lightAtten );
			#else
				float3 staticSwitch1036_g22165 = float3( 0,0,0 );
			#endif
			float ColoringDirectEnvLights1082_g22165 = _ColoringDirectEnvLights;
			float3 lerpResult1544_g22165 = lerp( LightRamp85_g22165 , RampColorRGB42_g22165 , ColoringDirectEnvLights1082_g22165);
			float3 lerpResult1538_g22165 = lerp( lerpResult1544_g22165 , float3( 1,1,1 ) , localgetLinearRgbToLuminance2055_g22165);
			#ifdef DIRECTIONAL
				float3 staticSwitch1035_g22165 = lerpResult1538_g22165;
			#else
				float3 staticSwitch1035_g22165 = staticSwitch1036_g22165;
			#endif
			float MonochromeToggleEnv1327_g22165 = _ToggleMonochromeEnv;
			half3 linearRgb1432_g22165 = AmbientLightBoosted1782_g22165;
			half localgetLinearRgbToLuminance1432_g22165 = getLinearRgbToLuminance( linearRgb1432_g22165 );
			float3 temp_cast_259 = (localgetLinearRgbToLuminance1432_g22165).xxx;
			float3 ifLocalVar1325_g22165 = 0;
			if( 1.0 > MonochromeToggleEnv1327_g22165 )
				ifLocalVar1325_g22165 = AmbientLightBoosted1782_g22165;
			else if( 1.0 == MonochromeToggleEnv1327_g22165 )
				ifLocalVar1325_g22165 = temp_cast_259;
			float3 localambientDir475_g21922 = ambientDir();
			float dotResult67_g21922 = dot( localambientDir475_g21922 , WorldNormals20_g21922 );
			float NdotAmbientL19820 = dotResult67_g21922;
			float AmbientUV224_g22165 = saturate( ( ( NdotAmbientL19820 * NdLHalfingControl704_g22165 ) + RampOffset167_g22165 ) );
			float2 temp_cast_260 = (AmbientUV224_g22165).xx;
			float3 ToonRampTexAmbient220_g22165 = (SAMPLE_TEXTURE2D( _ToonRamp, sampler_linear_clamp, temp_cast_260 )).rgb;
			float temp_output_709_0_g22165 = saturate( ( floor( ( AmbientUV224_g22165 * Steps30_g22165 ) ) / ( Steps30_g22165 - 1 ) ) );
			float3 temp_cast_261 = (temp_output_709_0_g22165).xxx;
			float3 ifLocalVar92_g22165 = 0;
			UNITY_BRANCH 
			if( 1.0 > ToggleSteps66_g22165 )
				ifLocalVar92_g22165 = ToonRampTexAmbient220_g22165;
			else if( 1.0 == ToggleSteps66_g22165 )
				ifLocalVar92_g22165 = temp_cast_261;
			float IndirectShadowMask688_g22165 = break699_g22165.y;
			float3 lerpResult1684_g22165 = lerp( float3( 1,1,1 ) , ifLocalVar92_g22165 , IndirectShadowMask688_g22165);
			float3 lerpResult1626_g22165 = lerp( lerpResult1684_g22165 , RampColorRGB42_g22165 , ColoringDirectEnvLights1082_g22165);
			half3 linearRgb2058_g22165 = lerpResult1684_g22165;
			half localgetLinearRgbToLuminance2058_g22165 = getLinearRgbToLuminance( linearRgb2058_g22165 );
			float3 lerpResult1627_g22165 = lerp( lerpResult1626_g22165 , float3( 1,1,1 ) , localgetLinearRgbToLuminance2058_g22165);
			float3 lerpResult284_g22165 = lerp( float3( 1,1,1 ) , lerpResult1627_g22165 , max( _IndirectShadowIntensity , 1E-06 ));
			half3 linearRgb1953_g22165 = ( ase_lightColor.rgb * staticSwitch1035_g22165 );
			half localgetLinearRgbToLuminance1953_g22165 = getLinearRgbToLuminance( linearRgb1953_g22165 );
			float3 lerpResult1661_g22165 = lerp( lerpResult284_g22165 , float3( 1,1,1 ) , saturate( localgetLinearRgbToLuminance1953_g22165 ));
			float2 uv_OcclusionMap = i.uv_texcoord * _OcclusionMap_ST.xy + _OcclusionMap_ST.zw;
			float lerpResult54_g22165 = lerp( 1.0 , SAMPLE_TEXTURE2D( _OcclusionMap, sampler_trilinear_repeat, uv_OcclusionMap ).g , _Occlusion);
			float3 FinalAmbientBakedLight132_g22165 = ( ifLocalVar1325_g22165 * lerpResult1661_g22165 * lerpResult54_g22165 );
			float3 ForFinalLightCalculation993_g22165 = ( ( ifLocalVar1331_g22165 * staticSwitch1035_g22165 ) + FinalAmbientBakedLight132_g22165 );
			float3 ifLocalVar944_g22165 = 0;
			if( ( localgetLinearRgbToLuminance1430_g22165 * _ExperimentalToggle ) <= 1.0 )
				ifLocalVar944_g22165 = ForFinalLightCalculation993_g22165;
			else
				ifLocalVar944_g22165 = ( max( float3( 0,0,0 ) , ForFinalLightCalculation993_g22165 ) / localgetLinearRgbToLuminance1430_g22165 );
			float3 temp_output_782_0_g22165 = ( ifLocalVar944_g22165 * _MaxLightDirect );
			float3 ase_worldViewDir = Unity_SafeNormalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float dotResult442_g21922 = dot( WorldNormals20_g21922 , ase_worldViewDir );
			float NdotV19803 = dotResult442_g21922;
			float smoothstepResult10_g22166 = smoothstep( min( _ShadowRimSharpness , ( 1.0 - 1E-06 ) ) , 1.0 , ( NdotV19803 + _ShadowRimRange ));
			float lerpResult11_g22166 = lerp( 1.0 , smoothstepResult10_g22166 , _ShadowRimOpacity);
			float ifLocalVar8684 = 0;
			if( 1.0 > _RimSwitch )
				ifLocalVar8684 = lerpResult11_g22166;
			else if( 1.0 == _RimSwitch )
				ifLocalVar8684 = 1.0;
			float ShadowRim2907 = ifLocalVar8684;
			float3 FinalLight2205 = ( ( ( temp_output_150_0_g22165 * temp_output_1997_0_g22165 ) + ( temp_output_782_0_g22165 * temp_output_150_0_g22165 ) ) * ShadowRim2907 );
			float3 ifLocalVar25104 = 0;
			if( ifLocalVar25147 <= 2.0 )
				ifLocalVar25104 = FinalLight2205;
			else
				ifLocalVar25104 = ( FinalLight2205 * AlphaChannelMul9091 );
			float temp_output_21533_0 = ( ToggleAdvanced21493 * ToggleCubemap21525 );
			float Intensity285_g22174 = _CubemapIntensity;
			float UVSwitchProp2886_g22174 = _ReflectionMaskUVSwitch;
			float2 UV02886_g22174 = i.uv_texcoord;
			float2 UV12886_g22174 = i.uv2_texcoord2;
			float2 UV22886_g22174 = i.uv3_texcoord3;
			float2 UV32886_g22174 = i.uv4_texcoord4;
			float2 localUVSwitch2886_g22174 = UVSwitch( UVSwitchProp2886_g22174 , UV02886_g22174 , UV12886_g22174 , UV22886_g22174 , UV32886_g22174 );
			float2 ReflectionMaskUVSwitch2896_g22174 = ( ( _ReflectionMask_ST.xy * localUVSwitch2886_g22174 ) + _ReflectionMask_ST.zw );
			float temp_output_80_0_g22174 = ( Intensity285_g22174 * SAMPLE_TEXTURE2D( _ReflectionMask, sampler_trilinear_repeat, ReflectionMaskUVSwitch2896_g22174 ).r );
			float ifLocalVar22474 = 0;
			if( temp_output_21533_0 > 0.0 )
				ifLocalVar22474 = ( temp_output_80_0_g22174 * MetallicTex289_g22174 );
			float CubemapLightAbsorbtion22506 = ifLocalVar22474;
			float AdvancedToggle555_g22227 = ToggleAdvanced21493;
			float temp_output_570_0_g22227 = ( _MatcapR1Toggle * AdvancedToggle555_g22227 );
			float MatcapR1Blending703_g22227 = _MatcapR1Blending;
			float UVSwitchProp912_g22227 = _ReflectionMaskMatcapUVSwitch;
			float2 UV0912_g22227 = i.uv_texcoord;
			float2 UV1912_g22227 = i.uv2_texcoord2;
			float2 UV2912_g22227 = i.uv3_texcoord3;
			float2 UV3912_g22227 = i.uv4_texcoord4;
			float2 localUVSwitch912_g22227 = UVSwitch( UVSwitchProp912_g22227 , UV0912_g22227 , UV1912_g22227 , UV2912_g22227 , UV3912_g22227 );
			float2 ReflectionMaskMatcapUVSwitch914_g22227 = ( ( _ReflectionMaskMatcap_ST.xy * localUVSwitch912_g22227 ) + _ReflectionMaskMatcap_ST.zw );
			float4 break646_g22227 = SAMPLE_TEXTURE2D( _ReflectionMaskMatcap, sampler_trilinear_repeat, ReflectionMaskMatcapUVSwitch914_g22227 );
			float ifLocalVar148_g369 = 0;
			if( ( 1.0 - _MakeupLipThickness ) > 0.0 )
				ifLocalVar148_g369 = MakeUpRed41_g369;
			float ifLocalVar156_g369 = 0;
			if( _MakeupLipThickness > 0.0 )
				ifLocalVar156_g369 = tex2DNode99_g369.a;
			float temp_output_147_0_g369 = ( ifLocalVar148_g369 + ifLocalVar156_g369 );
			float ifLocalVar138_g369 = 0;
			if( _ToggleLipMatCapR > 0.0 )
				ifLocalVar138_g369 = temp_output_147_0_g369;
			float ifLocalVar143_g369 = 0;
			if( _ToggleLipMatCapG > 0.0 )
				ifLocalVar143_g369 = temp_output_147_0_g369;
			float ifLocalVar144_g369 = 0;
			if( _ToggleLipMatCapB > 0.0 )
				ifLocalVar144_g369 = temp_output_147_0_g369;
			float ifLocalVar145_g369 = 0;
			if( _ToggleLipMatCapA > 0.0 )
				ifLocalVar145_g369 = temp_output_147_0_g369;
			float4 appendResult154_g369 = (float4(ifLocalVar138_g369 , ifLocalVar143_g369 , ifLocalVar144_g369 , ifLocalVar145_g369));
			float4 LipMatCap25172 = appendResult154_g369;
			float4 break930_g22227 = LipMatCap25172;
			float ReflectionMaskR199_g22227 = ( break646_g22227.r + break930_g22227.r );
			float ifLocalVar677_g22227 = 0;
			if( 1.0 > ( _MatcapR1Mode + ( 1.0 - temp_output_570_0_g22227 ) ) )
				ifLocalVar677_g22227 = ( MatcapR1Blending703_g22227 * ReflectionMaskR199_g22227 );
			float MatcapR1LightAbsorbtion731_g22227 = ifLocalVar677_g22227;
			float temp_output_573_0_g22227 = ( _MatcapG2Toggle * AdvancedToggle555_g22227 );
			float MatcapG2Blending706_g22227 = _MatcapG2Blending;
			float ReflectionMaskG200_g22227 = ( break646_g22227.g + break930_g22227.g );
			float ifLocalVar712_g22227 = 0;
			if( 1.0 > ( _MatcapG2Mode + ( 1.0 - temp_output_573_0_g22227 ) ) )
				ifLocalVar712_g22227 = ( MatcapG2Blending706_g22227 * ReflectionMaskG200_g22227 );
			float MatcapG2LightAbsorbtion732_g22227 = ifLocalVar712_g22227;
			float temp_output_576_0_g22227 = ( _MatcapB3Toggle * AdvancedToggle555_g22227 );
			float MatcapB3Blending708_g22227 = _MatcapB3Blending;
			float ReflectionMaskB201_g22227 = ( break646_g22227.b + break930_g22227.b );
			float ifLocalVar715_g22227 = 0;
			if( 1.0 > ( _MatcapB3Mode + ( 1.0 - temp_output_576_0_g22227 ) ) )
				ifLocalVar715_g22227 = ( MatcapB3Blending708_g22227 * ReflectionMaskB201_g22227 );
			float MatcapB3LightAbsorbtion733_g22227 = ifLocalVar715_g22227;
			float temp_output_579_0_g22227 = ( _MatcapA4Toggle * AdvancedToggle555_g22227 );
			float MatcapA4Blending710_g22227 = _MatcapA4Blending;
			float ReflectionMaskA202_g22227 = ( break646_g22227.a + break930_g22227.a );
			float ifLocalVar718_g22227 = 0;
			if( 1.0 > ( _MatcapA4Mode + ( 1.0 - temp_output_579_0_g22227 ) ) )
				ifLocalVar718_g22227 = ( MatcapA4Blending710_g22227 * ReflectionMaskA202_g22227 );
			float MatcapA4LightAbsorbtion734_g22227 = ifLocalVar718_g22227;
			float MatcapLightAbsorbtion24109 = saturate( ( MatcapR1LightAbsorbtion731_g22227 + MatcapG2LightAbsorbtion732_g22227 + MatcapB3LightAbsorbtion733_g22227 + MatcapA4LightAbsorbtion734_g22227 ) );
			float3 lerpResult22467 = lerp( ifLocalVar25104 , float3( 0,0,0 ) , saturate( ( CubemapLightAbsorbtion22506 + MatcapLightAbsorbtion24109 ) ));
			float3 FinalBase25138 = lerpResult22467;
			float locallongIF24841 = ( 0.0 );
			float3 Out24841 = float3( 0,0,0 );
			float4 break24_g21922 = ase_lightColor;
			float GrayscaledLight19806 = saturate( ( max( max( break24_g21922.r , break24_g21922.g ) , break24_g21922.b ) * break24_g21922.a ) );
			float temp_output_1739_0_g22168 = GrayscaledLight19806;
			float3 normalizeResult464_g21922 = ASESafeNormalize( ( ase_worldViewDir + ase_worldlightDir ) );
			float3 HalfVectorUnityNormalized457_g21922 = normalizeResult464_g21922;
			float dotResult42_g21922 = dot( WorldNormals20_g21922 , HalfVectorUnityNormalized457_g21922 );
			float NdotH19802 = dotResult42_g21922;
			float NdotH583_g22168 = NdotH19802;
			float HighlightOffset127_g22168 = _HighlightOffset;
			float3 WorldNormals2285_g22168 = worldnormals2351;
			float3 worldNormal2417_g22168 = WorldNormals2285_g22168;
			float UVSwitchProp2003_g22168 = _SpecularMapUVSwitch;
			float2 UV02003_g22168 = i.uv_texcoord;
			float2 UV12003_g22168 = i.uv2_texcoord2;
			float2 UV22003_g22168 = i.uv3_texcoord3;
			float2 UV32003_g22168 = i.uv4_texcoord4;
			float2 localUVSwitch2003_g22168 = UVSwitch( UVSwitchProp2003_g22168 , UV02003_g22168 , UV12003_g22168 , UV22003_g22168 , UV32003_g22168 );
			float4 tex2DNode1752_g22168 = SAMPLE_TEXTURE2D( _SpecularMap, sampler_trilinear_repeat, ( ( _SpecularMap_ST.xy * localUVSwitch2003_g22168 ) + _SpecularMap_ST.zw ) );
			float4 break380_g22168 = tex2DNode1752_g22168;
			float SpecularMapa1649_g22168 = break380_g22168.a;
			float smoothness2417_g22168 = ( _HighlightSmoothness * SpecularMapa1649_g22168 );
			float localGSAA_Filament2417_g22168 = GSAA_Filament( worldNormal2417_g22168 , smoothness2417_g22168 );
			float HighlightSmoothness128_g22168 = localGSAA_Filament2417_g22168;
			float3 ColorRGB141_g22168 = (_SpecularColor).rgb;
			float temp_output_2_0_g22169 = _SpecularTint;
			float temp_output_3_0_g22169 = ( 1.0 - temp_output_2_0_g22169 );
			float3 appendResult7_g22169 = (float3(temp_output_3_0_g22169 , temp_output_3_0_g22169 , temp_output_3_0_g22169));
			float3 DiffuseTint148_g22168 = ( ( MainTexSaturate2197 * temp_output_2_0_g22169 ) + appendResult7_g22169 );
			float ColorA142_g22168 = _SpecularColor.a;
			float4 SpecularMapRGBA2289_g22168 = tex2DNode1752_g22168;
			float3 SpecularMapRGB160_g22168 = (SpecularMapRGBA2289_g22168).rgb;
			float SpecShadowMaskVar279_g22168 = _SpecShadowMaskVar;
			#ifdef UNITY_PASS_FORWARDADD
				float staticSwitch1892_g22168 = ase_lightAtten;
			#else
				float staticSwitch1892_g22168 = 1.0;
			#endif
			float NdotL595_g22168 = NdotL19801;
			half3 linearRgb2056_g22165 = min( ToonRampTexNDL207_g22165 , DirectionalAttenuationRamp24_g22165 );
			half localgetLinearRgbToLuminance2056_g22165 = getLinearRgbToLuminance( linearRgb2056_g22165 );
			float ifLocalVar1946_g22165 = 0;
			UNITY_BRANCH 
			if( 1.0 > ToggleSteps66_g22165 )
				ifLocalVar1946_g22165 = localgetLinearRgbToLuminance2056_g22165;
			else if( 1.0 == ToggleSteps66_g22165 )
				ifLocalVar1946_g22165 = min( temp_output_708_0_g22165 , temp_output_1936_0_g22165 );
			float DirectLightRamp14556 = ifLocalVar1946_g22165;
			float SpecShadowMaskPower286_g22168 = _SpecShadowMaskPower;
			float temp_output_1_0_g22170 = -max( SpecShadowMaskPower286_g22168 , -0.99 );
			float temp_output_2363_0_g22168 = ( saturate( SpecShadowMaskPower286_g22168 ) * 0.5 );
			float lerpResult2345_g22168 = lerp( ( ( DirectLightRamp14556 - temp_output_1_0_g22170 ) / ( 1.0 - temp_output_1_0_g22170 ) ) , 1.0 , ( ( SpecShadowMaskPower286_g22168 * temp_output_2363_0_g22168 ) + temp_output_2363_0_g22168 ));
			float temp_output_1878_0_g22168 = saturate( lerpResult2345_g22168 );
			#ifdef UNITY_PASS_FORWARDADD
				float staticSwitch621_g22168 = ( temp_output_1878_0_g22168 * ase_lightAtten );
			#else
				float staticSwitch621_g22168 = 0.0;
			#endif
			#ifdef DIRECTIONAL
				float staticSwitch620_g22168 = temp_output_1878_0_g22168;
			#else
				float staticSwitch620_g22168 = staticSwitch621_g22168;
			#endif
			float AdditionalShadowRamp280_g22168 = staticSwitch620_g22168;
			float ifLocalVar1904_g22168 = 0;
			UNITY_BRANCH 
			if( 1.0 > SpecShadowMaskVar279_g22168 )
				ifLocalVar1904_g22168 = staticSwitch1892_g22168;
			else if( 1.0 == SpecShadowMaskVar279_g22168 )
				ifLocalVar1904_g22168 = saturate( ( ase_lightAtten * NdotL595_g22168 ) );
			else if( 1.0 < SpecShadowMaskVar279_g22168 )
				ifLocalVar1904_g22168 = AdditionalShadowRamp280_g22168;
			float ShadowsToonAniso1905_g22168 = ifLocalVar1904_g22168;
			float3 localambientDir468_g21922 = ambientDir();
			float3 normalizeResult467_g21922 = ASESafeNormalize( ( ase_worldViewDir + localambientDir468_g21922 ) );
			float3 AmbientHalfVectorUnityNormalized469_g21922 = normalizeResult467_g21922;
			float dotResult75_g21922 = dot( WorldNormals20_g21922 , AmbientHalfVectorUnityNormalized469_g21922 );
			float NdotAmbientH19817 = dotResult75_g21922;
			float NdotAmbientH591_g22168 = NdotAmbientH19817;
			half3 localAmbient1730_g22168 = Ambient();
			float NdotAmbientL597_g22168 = NdotAmbientL19820;
			half3 linearRgb2057_g22165 = ToonRampTexAmbient220_g22165;
			half localgetLinearRgbToLuminance2057_g22165 = getLinearRgbToLuminance( linearRgb2057_g22165 );
			float ifLocalVar2059_g22165 = 0;
			UNITY_BRANCH 
			if( 1.0 > ToggleSteps66_g22165 )
				ifLocalVar2059_g22165 = localgetLinearRgbToLuminance2057_g22165;
			else if( 1.0 == ToggleSteps66_g22165 )
				ifLocalVar2059_g22165 = temp_output_709_0_g22165;
			float AmbientRamp14557 = ifLocalVar2059_g22165;
			float temp_output_1_0_g22172 = -max( SpecShadowMaskPower286_g22168 , -0.99 );
			float temp_output_2375_0_g22168 = ( saturate( SpecShadowMaskPower286_g22168 ) * 0.5 );
			float lerpResult2369_g22168 = lerp( ( ( AmbientRamp14557 - temp_output_1_0_g22172 ) / ( 1.0 - temp_output_1_0_g22172 ) ) , 1.0 , ( ( SpecShadowMaskPower286_g22168 * temp_output_2375_0_g22168 ) + temp_output_2375_0_g22168 ));
			float ifLocalVar1824_g22168 = 0;
			UNITY_BRANCH 
			if( 1.0 > SpecShadowMaskVar279_g22168 )
				ifLocalVar1824_g22168 = 1.0;
			else if( 1.0 == SpecShadowMaskVar279_g22168 )
				ifLocalVar1824_g22168 = saturate( NdotAmbientL597_g22168 );
			else if( 1.0 < SpecShadowMaskVar279_g22168 )
				ifLocalVar1824_g22168 = saturate( lerpResult2369_g22168 );
			float ShadowsIndirectNdL289_g22168 = ifLocalVar1824_g22168;
			float3 ifLocalVar1740_g22168 = 0;
			if( temp_output_1739_0_g22168 > 0.0 )
				ifLocalVar1740_g22168 = ( saturate( ( ( NdotH583_g22168 + HighlightOffset127_g22168 ) / max( HighlightSmoothness128_g22168 , 1E-06 ) ) ) * ColorRGB141_g22168 * DiffuseTint148_g22168 * ColorA142_g22168 * SpecularMapRGB160_g22168 * ase_lightColor.rgb * ShadowsToonAniso1905_g22168 );
			else if( temp_output_1739_0_g22168 == 0.0 )
				ifLocalVar1740_g22168 = ( saturate( ( ( NdotAmbientH591_g22168 + HighlightOffset127_g22168 ) / max( HighlightSmoothness128_g22168 , 1E-06 ) ) ) * ColorRGB141_g22168 * DiffuseTint148_g22168 * ColorA142_g22168 * SpecularMapRGB160_g22168 * localAmbient1730_g22168 * ShadowsIndirectNdL289_g22168 );
			float3 Toon24841 = ( ifLocalVar1740_g22168 + float3( 0,0,0 ) );
			float ifLocalVar1769_g22168 = 0;
			UNITY_BRANCH 
			if( 1.0 > SpecShadowMaskVar279_g22168 )
				ifLocalVar1769_g22168 = 1.0;
			else if( 1.0 == SpecShadowMaskVar279_g22168 )
				ifLocalVar1769_g22168 = NdotL595_g22168;
			else if( 1.0 < SpecShadowMaskVar279_g22168 )
				ifLocalVar1769_g22168 = temp_output_1878_0_g22168;
			float NdLGGX171_g22168 = ifLocalVar1769_g22168;
			float temp_output_694_0_g22168 = max( 0.0 , NdLGGX171_g22168 );
			float NdotL688_g22168 = temp_output_694_0_g22168;
			float3 viewDir443_g21922 = ase_worldViewDir;
			float3 normal443_g21922 = WorldNormals20_g21922;
			float localCorrectNegativeNdotV443_g21922 = CorrectNegativeNdotV( viewDir443_g21922 , normal443_g21922 );
			float NdotVCorr21432 = localCorrectNegativeNdotV443_g21922;
			float CorrNdotV626_g22168 = NdotVCorr21432;
			float NdotV688_g22168 = CorrNdotV626_g22168;
			float temp_output_566_0_g22168 = ( 1.0 - HighlightSmoothness128_g22168 );
			float temp_output_643_0_g22168 = max( ( temp_output_566_0_g22168 * temp_output_566_0_g22168 ) , 0.002 );
			float roughness688_g22168 = temp_output_643_0_g22168;
			float localgetSmithJointGGXVisibilityTerm688_g22168 = getSmithJointGGXVisibilityTerm( NdotL688_g22168 , NdotV688_g22168 , roughness688_g22168 );
			float GGXVisibilityTerm630_g22168 = localgetSmithJointGGXVisibilityTerm688_g22168;
			float NdotH689_g22168 = max( 0.0 , NdotH583_g22168 );
			float roughness689_g22168 = temp_output_643_0_g22168;
			float localgetGGXTerm689_g22168 = getGGXTerm( NdotH689_g22168 , roughness689_g22168 );
			float GGXTerm630_g22168 = localgetGGXTerm689_g22168;
			float NdotL630_g22168 = temp_output_694_0_g22168;
			float dotResult50_g21922 = dot( ase_worldlightDir , HalfVectorUnityNormalized457_g21922 );
			float LdotH19804 = dotResult50_g21922;
			float LdotH2104_g22168 = LdotH19804;
			float LdotH630_g22168 = max( 0.0 , LdotH2104_g22168 );
			float roughness630_g22168 = temp_output_643_0_g22168;
			float3 specColor630_g22168 = ( ColorRGB141_g22168 * DiffuseTint148_g22168 );
			float ifLocalVar1908_g22168 = 0;
			UNITY_BRANCH 
			if( 1.0 == SpecShadowMaskVar279_g22168 )
				ifLocalVar1908_g22168 = ase_lightAtten;
			else
				ifLocalVar1908_g22168 = staticSwitch1892_g22168;
			float AttenGGX1911_g22168 = ifLocalVar1908_g22168;
			float3 lightcolor630_g22168 = ( AttenGGX1911_g22168 * ase_lightColor.rgb );
			float specularTermToggle630_g22168 = 1.0;
			float3 localcalcSpecularTerm630_g22168 = calcSpecularTerm( GGXVisibilityTerm630_g22168 , GGXTerm630_g22168 , NdotL630_g22168 , LdotH630_g22168 , roughness630_g22168 , specColor630_g22168 , lightcolor630_g22168 , specularTermToggle630_g22168 );
			float temp_output_695_0_g22168 = max( 0.0 , ShadowsIndirectNdL289_g22168 );
			float NdotL690_g22168 = temp_output_695_0_g22168;
			float NdotV690_g22168 = CorrNdotV626_g22168;
			float temp_output_650_0_g22168 = ( 1.0 - HighlightSmoothness128_g22168 );
			float temp_output_648_0_g22168 = max( ( temp_output_650_0_g22168 * temp_output_650_0_g22168 ) , 0.002 );
			float roughness690_g22168 = temp_output_648_0_g22168;
			float localgetSmithJointGGXVisibilityTerm690_g22168 = getSmithJointGGXVisibilityTerm( NdotL690_g22168 , NdotV690_g22168 , roughness690_g22168 );
			float GGXVisibilityTerm666_g22168 = localgetSmithJointGGXVisibilityTerm690_g22168;
			float NdotH691_g22168 = max( 0.0 , NdotAmbientH591_g22168 );
			float roughness691_g22168 = temp_output_648_0_g22168;
			float localgetGGXTerm691_g22168 = getGGXTerm( NdotH691_g22168 , roughness691_g22168 );
			float GGXTerm666_g22168 = localgetGGXTerm691_g22168;
			float NdotL666_g22168 = temp_output_695_0_g22168;
			float3 localambientDir101_g21922 = ambientDir();
			float dotResult82_g21922 = dot( localambientDir101_g21922 , AmbientHalfVectorUnityNormalized469_g21922 );
			float AmbientLdotAmbientH19902 = dotResult82_g21922;
			float AmbientLdotAmbientH2157_g22168 = AmbientLdotAmbientH19902;
			float LdotH666_g22168 = max( 0.0 , AmbientLdotAmbientH2157_g22168 );
			float roughness666_g22168 = temp_output_648_0_g22168;
			float3 specColor666_g22168 = ( ColorRGB141_g22168 * DiffuseTint148_g22168 );
			half3 localAmbient1728_g22168 = Ambient();
			float3 lightcolor666_g22168 = localAmbient1728_g22168;
			float specularTermToggle666_g22168 = 1.0;
			float3 localcalcSpecularTerm666_g22168 = calcSpecularTerm( GGXVisibilityTerm666_g22168 , GGXTerm666_g22168 , NdotL666_g22168 , LdotH666_g22168 , roughness666_g22168 , specColor666_g22168 , lightcolor666_g22168 , specularTermToggle666_g22168 );
			float3 ifLocalVar1741_g22168 = 0;
			if( temp_output_1739_0_g22168 > 0.0 )
				ifLocalVar1741_g22168 = ( localcalcSpecularTerm630_g22168 * SpecularMapRGB160_g22168 * ColorA142_g22168 );
			else if( temp_output_1739_0_g22168 == 0.0 )
				ifLocalVar1741_g22168 = ( localcalcSpecularTerm666_g22168 * SpecularMapRGB160_g22168 * ColorA142_g22168 );
			float3 GGX24841 = ( ifLocalVar1741_g22168 + float3( 0,0,0 ) );
			float UVSwitchProp2002_g22168 = _AnisoDirUVSwitch;
			float2 UV02002_g22168 = i.uv_texcoord;
			float2 UV12002_g22168 = i.uv2_texcoord2;
			float2 UV22002_g22168 = i.uv3_texcoord3;
			float2 UV32002_g22168 = i.uv4_texcoord4;
			float2 localUVSwitch2002_g22168 = UVSwitch( UVSwitchProp2002_g22168 , UV02002_g22168 , UV12002_g22168 , UV22002_g22168 , UV32002_g22168 );
			float2 AnisoDirUVSwitch2009_g22168 = ( ( _AnisoDir_ST.xy * localUVSwitch2002_g22168 ) + _AnisoDir_ST.zw );
			float3 normalizeResult77_g22168 = normalize( ( UnpackNormal( SAMPLE_TEXTURE2D( _AnisoDir, sampler_trilinear_repeat, AnisoDirUVSwitch2009_g22168 ) ) + WorldNormals2285_g22168 ) );
			float3 HdotA1339_g22168 = normalizeResult77_g22168;
			float3 normalizeResult4_g22171 = normalize( ( ase_worldViewDir + ase_worldlightDir ) );
			float dotResult76_g22168 = dot( HdotA1339_g22168 , normalizeResult4_g22171 );
			float BlinnToAniso1426_g22168 = _BlinntoAniso;
			float SpecularMapb383_g22168 = break380_g22168.b;
			float lerpResult97_g22168 = lerp( saturate( NdotH583_g22168 ) , max( 0.0 , sin( radians( ( ( dotResult76_g22168 + HighlightOffset127_g22168 ) * 180.0 ) ) ) ) , ( BlinnToAniso1426_g22168 * SpecularMapb383_g22168 ));
			float AnisoScale1344_g22168 = _AnisoScale;
			float SpecularMapg162_g22168 = break380_g22168.g;
			float AnisoSharpening1345_g22168 = _AnisoSharpening;
			float lerpResult1579_g22168 = lerp( 128.0 , 4096.0 , AnisoSharpening1345_g22168);
			float SpecularMapr161_g22168 = break380_g22168.r;
			float3 localambientDir2162_g22168 = ambientDir();
			float3 normalizeResult2164_g22168 = normalize( ( ase_worldViewDir + localambientDir2162_g22168 ) );
			float3 AmbientHalfVector2165_g22168 = normalizeResult2164_g22168;
			float dotResult341_g22168 = dot( HdotA1339_g22168 , AmbientHalfVector2165_g22168 );
			float lerpResult1443_g22168 = lerp( saturate( NdotAmbientH591_g22168 ) , max( 0.0 , sin( radians( ( ( dotResult341_g22168 + HighlightOffset127_g22168 ) * 180.0 ) ) ) ) , ( BlinnToAniso1426_g22168 * SpecularMapb383_g22168 ));
			float lerpResult1595_g22168 = lerp( 128.0 , 4096.0 , AnisoSharpening1345_g22168);
			half3 localAmbient1729_g22168 = Ambient();
			float3 ifLocalVar1738_g22168 = 0;
			if( temp_output_1739_0_g22168 > 0.0 )
				ifLocalVar1738_g22168 = ( ( saturate( ( ( pow( lerpResult97_g22168 , ( ( 1.0 - AnisoScale1344_g22168 ) * SpecularMapg162_g22168 * lerpResult1579_g22168 ) ) * SpecularMapr161_g22168 ) / ( 1.0 - AnisoSharpening1345_g22168 ) ) ) * ColorA142_g22168 ) * ( ColorRGB141_g22168 * DiffuseTint148_g22168 ) * ase_lightColor.rgb * max( ( ShadowsToonAniso1905_g22168 * 2.0 ) , 0.0 ) );
			else if( temp_output_1739_0_g22168 == 0.0 )
				ifLocalVar1738_g22168 = ( ( saturate( ( ( pow( lerpResult1443_g22168 , ( ( 1.0 - AnisoScale1344_g22168 ) * SpecularMapg162_g22168 * lerpResult1595_g22168 ) ) * SpecularMapr161_g22168 ) / ( 1.0 - AnisoSharpening1345_g22168 ) ) ) * ColorA142_g22168 ) * ( ColorRGB141_g22168 * DiffuseTint148_g22168 ) * localAmbient1729_g22168 * max( ( ShadowsIndirectNdL289_g22168 * 2.0 ) , 0.0 ) );
			float3 Anisotropic24841 = ( ifLocalVar1738_g22168 + float3( 0,0,0 ) );
			float ndl2022_g22168 = max( NdLGGX171_g22168 , 0.0 );
			float ndh2022_g22168 = max( NdotH583_g22168 , 0.0 );
			float vdn2022_g22168 = CorrNdotV626_g22168;
			float ldh2022_g22168 = max( LdotH2104_g22168 , 0.0 );
			float3 lightCol2022_g22168 = ( ShadowsToonAniso1905_g22168 * ase_lightColor.rgb );
			float3 normalizeResult4_g22173 = normalize( ( ase_worldViewDir + ase_worldlightDir ) );
			float3 halfVector2022_g22168 = normalizeResult4_g22173;
			float smoothness2022_g22168 = HighlightSmoothness128_g22168;
			float localOutlineSwitch127_g21919 = ( 0.0 );
			float3 true127_g21919 = temp_output_54_0_g21919;
			float3 false127_g21919 = -temp_output_54_0_g21919;
			float3 Out0127_g21919 = float3( 0,0,0 );
			{
			#ifdef FOROUTLINE
			Out0127_g21919 = true127_g21919; //Outline
			#else
			Out0127_g21919 = false127_g21919; //Not Outline
			#endif
			#define FOROUTLINE
			}
			float3 switchResult121_g21919 = (((i.ASEVFace>0)?(temp_output_54_0_g21919):(Out0127_g21919)));
			float3 normalizeResult52_g21919 = normalize( switchResult121_g21919 );
			float3 normals2353 = normalizeResult52_g21919;
			float3 temp_output_2254_0_g22168 = normals2353;
			float3 break2257_g22168 = temp_output_2254_0_g22168;
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float3 ase_vertexNormal = mul( unity_WorldToObject, float4( ase_worldNormal, 0 ) );
			ase_vertexNormal = normalize( ase_vertexNormal );
			float3 In02226_g22168 = ase_vertexNormal;
			float3 localgetUnityObjectToWorldNormal2226_g22168 = getUnityObjectToWorldNormal2226_g22168( In02226_g22168 );
			float3 normalizeResult2262_g22168 = normalize( ( ( break2257_g22168.x * i.vertexToFrag2251_g22168 ) + ( break2257_g22168.y * i.vertexToFrag2250_g22168 ) + ( break2257_g22168.z * localgetUnityObjectToWorldNormal2226_g22168 ) ) );
			float3 temp_output_2252_0_g22168 = cross( i.vertexToFrag2250_g22168 , normalizeResult2262_g22168 );
			float3 normalizeResult2265_g22168 = normalize( temp_output_2252_0_g22168 );
			float3 bumpedTangent2278_g22168 = normalizeResult2265_g22168;
			float3 tangent2022_g22168 = bumpedTangent2278_g22168;
			float3 normalizeResult2266_g22168 = normalize( cross( normalizeResult2262_g22168 , temp_output_2252_0_g22168 ) );
			float3 bumpedBitangent2277_g22168 = normalizeResult2266_g22168;
			float3 bitangent2022_g22168 = bumpedBitangent2277_g22168;
			float3 diffuseColor2022_g22168 = ( ColorRGB141_g22168 * DiffuseTint148_g22168 );
			float4 SpecularMap2022_g22168 = SpecularMapRGBA2289_g22168;
			float3 LightDir2022_g22168 = ase_worldlightDir;
			float3 ViewDir2022_g22168 = ase_worldViewDir;
			float3 localcalcGGXAniso2022_g22168 = calcGGXAniso( ndl2022_g22168 , ndh2022_g22168 , vdn2022_g22168 , ldh2022_g22168 , lightCol2022_g22168 , halfVector2022_g22168 , smoothness2022_g22168 , tangent2022_g22168 , bitangent2022_g22168 , diffuseColor2022_g22168 , SpecularMap2022_g22168 , LightDir2022_g22168 , ViewDir2022_g22168 );
			float ndl2410_g22168 = max( ShadowsIndirectNdL289_g22168 , 0.0 );
			float ndh2410_g22168 = max( NdotAmbientH591_g22168 , 0.0 );
			float vdn2410_g22168 = CorrNdotV626_g22168;
			float ldh2410_g22168 = max( AmbientLdotAmbientH2157_g22168 , 0.0 );
			half3 localAmbient2155_g22168 = Ambient();
			float3 lightCol2410_g22168 = ( ShadowsIndirectNdL289_g22168 * localAmbient2155_g22168 );
			float3 halfVector2410_g22168 = AmbientHalfVector2165_g22168;
			float smoothness2410_g22168 = HighlightSmoothness128_g22168;
			float3 tangent2410_g22168 = bumpedTangent2278_g22168;
			float3 bitangent2410_g22168 = bumpedBitangent2277_g22168;
			float3 diffuseColor2410_g22168 = ( ColorRGB141_g22168 * DiffuseTint148_g22168 );
			float4 SpecularMap2410_g22168 = SpecularMapRGBA2289_g22168;
			float3 localambientDir2411_g22168 = ambientDir();
			float3 LightDir2410_g22168 = localambientDir2411_g22168;
			float3 ViewDir2410_g22168 = ase_worldViewDir;
			float3 localcalcGGXAniso2410_g22168 = calcGGXAniso( ndl2410_g22168 , ndh2410_g22168 , vdn2410_g22168 , ldh2410_g22168 , lightCol2410_g22168 , halfVector2410_g22168 , smoothness2410_g22168 , tangent2410_g22168 , bitangent2410_g22168 , diffuseColor2410_g22168 , SpecularMap2410_g22168 , LightDir2410_g22168 , ViewDir2410_g22168 );
			float3 ifLocalVar2153_g22168 = 0;
			if( temp_output_1739_0_g22168 > 0.0 )
				ifLocalVar2153_g22168 = localcalcGGXAniso2022_g22168;
			else if( temp_output_1739_0_g22168 == 0.0 )
				ifLocalVar2153_g22168 = localcalcGGXAniso2410_g22168;
			float3 GGXAnisotropic24841 = ifLocalVar2153_g22168;
			{
			UNITY_BRANCH
			if (_SpecularSetting == 0)
				Out24841 = Toon24841;
			else if (_SpecularSetting == 1)
				Out24841 = GGX24841;
			else if (_SpecularSetting == 2)
				Out24841 = Anisotropic24841;
			else
				Out24841 = GGXAnisotropic24841;
			}
			float3 SpecularHighlight2223 = Out24841;
			float3 appendResult1896_g22168 = (float3(NdLGGX171_g22168 , AttenGGX1911_g22168 , ShadowsIndirectNdL289_g22168));
			float3 PixelAmbientShadows24166 = appendResult1896_g22168;
			float3 break2823_g22174 = PixelAmbientShadows24166;
			float NdLGGX2357_g22174 = break2823_g22174.x;
			float temp_output_2418_0_g22174 = max( 0.0 , NdLGGX2357_g22174 );
			float NdotL2412_g22174 = temp_output_2418_0_g22174;
			float CorrectedNdotV2507_g22174 = NdotVCorr21432;
			float NdotV2412_g22174 = CorrectedNdotV2507_g22174;
			float IgnoreNormalsToggle323_g22174 = _IgnoreNormalsCubemap;
			float3 normalizeResult28_g22174 = normalize( ase_worldNormal );
			float3 ifLocalVar46_g22174 = 0;
			if( 1.0 > IgnoreNormalsToggle323_g22174 )
				ifLocalVar46_g22174 = worldnormals2351;
			else if( 1.0 == IgnoreNormalsToggle323_g22174 )
				ifLocalVar46_g22174 = normalizeResult28_g22174;
			float3 WorldNormals305_g22174 = ifLocalVar46_g22174;
			float3 worldNormal2910_g22174 = WorldNormals305_g22174;
			float Smoothness300_g22174 = _Cubemapsmoothness;
			float smoothness2910_g22174 = ( tex2DNode2688_g22174.a * Smoothness300_g22174 );
			float localGSAA_Filament2910_g22174 = GSAA_Filament( worldNormal2910_g22174 , smoothness2910_g22174 );
			float SmoothnessTex290_g22174 = localGSAA_Filament2910_g22174;
			float perceptualRoughness2761_g22174 = ( 1.0 - SmoothnessTex290_g22174 );
			float roughness2729_g22174 = max( ( perceptualRoughness2761_g22174 * perceptualRoughness2761_g22174 ) , 0.002 );
			float roughness2412_g22174 = roughness2729_g22174;
			float localgetSmithJointGGXVisibilityTerm2412_g22174 = getSmithJointGGXVisibilityTerm( NdotL2412_g22174 , NdotV2412_g22174 , roughness2412_g22174 );
			float GGXVisibilityTerm2305_g22174 = localgetSmithJointGGXVisibilityTerm2412_g22174;
			float NdotH2416_g22174 = max( 0.0 , NdotH19802 );
			float roughness2416_g22174 = roughness2729_g22174;
			float localgetGGXTerm2416_g22174 = getGGXTerm( NdotH2416_g22174 , roughness2416_g22174 );
			float GGXTerm2305_g22174 = localgetGGXTerm2416_g22174;
			float NdotL2305_g22174 = temp_output_2418_0_g22174;
			float LdotH2305_g22174 = max( 0.0 , LdotH19804 );
			float roughness2305_g22174 = roughness2729_g22174;
			float3 SpecColor2715_g22174 = specColor2324_g22174;
			float3 specColor2305_g22174 = SpecColor2715_g22174;
			float AttenGGX2831_g22174 = break2823_g22174.y;
			float3 lightcolor2305_g22174 = ( ase_lightColor.rgb * AttenGGX2831_g22174 );
			float SpecularHighlightToggle2498_g22174 = _CubemapSpecularToggle;
			float specularTermToggle2305_g22174 = SpecularHighlightToggle2498_g22174;
			float3 localcalcSpecularTerm2305_g22174 = calcSpecularTerm( GGXVisibilityTerm2305_g22174 , GGXTerm2305_g22174 , NdotL2305_g22174 , LdotH2305_g22174 , roughness2305_g22174 , specColor2305_g22174 , lightcolor2305_g22174 , specularTermToggle2305_g22174 );
			float temp_output_2490_0_g22174 = max( 0.0 , NdotAmbientL19820 );
			float NdotL2496_g22174 = temp_output_2490_0_g22174;
			float NdotV2496_g22174 = CorrectedNdotV2507_g22174;
			float roughness2496_g22174 = roughness2729_g22174;
			float localgetSmithJointGGXVisibilityTerm2496_g22174 = getSmithJointGGXVisibilityTerm( NdotL2496_g22174 , NdotV2496_g22174 , roughness2496_g22174 );
			float GGXVisibilityTerm2494_g22174 = localgetSmithJointGGXVisibilityTerm2496_g22174;
			float NdotH2495_g22174 = max( 0.0 , NdotAmbientH19817 );
			float roughness2495_g22174 = roughness2729_g22174;
			float localgetGGXTerm2495_g22174 = getGGXTerm( NdotH2495_g22174 , roughness2495_g22174 );
			float GGXTerm2494_g22174 = localgetGGXTerm2495_g22174;
			float NdotL2494_g22174 = temp_output_2490_0_g22174;
			float LdotH2494_g22174 = max( 0.0 , AmbientLdotAmbientH19902 );
			float roughness2494_g22174 = roughness2729_g22174;
			float3 specColor2494_g22174 = SpecColor2715_g22174;
			half3 localAmbient2505_g22174 = Ambient();
			float AmbientShadows2824_g22174 = break2823_g22174.z;
			float3 lightcolor2494_g22174 = ( localAmbient2505_g22174 * AmbientShadows2824_g22174 );
			float specularTermToggle2494_g22174 = SpecularHighlightToggle2498_g22174;
			float3 localcalcSpecularTerm2494_g22174 = calcSpecularTerm( GGXVisibilityTerm2494_g22174 , GGXTerm2494_g22174 , NdotL2494_g22174 , LdotH2494_g22174 , roughness2494_g22174 , specColor2494_g22174 , lightcolor2494_g22174 , specularTermToggle2494_g22174 );
			float3 SHSpecular2509_g22174 = localcalcSpecularTerm2494_g22174;
			float3 ifLocalVar2672_g22174 = 0;
			if( GrayscaledLight19806 > 0.0 )
				ifLocalVar2672_g22174 = localcalcSpecularTerm2305_g22174;
			else if( GrayscaledLight19806 == 0.0 )
				ifLocalVar2672_g22174 = SHSpecular2509_g22174;
			float4 temp_output_1727_0_g22168 = max( float4( 0,0,0,0 ) , VertexLightNdLNONMAX21088 );
			float4 ifLocalVar2136_g22165 = 0;
			UNITY_BRANCH 
			if( 1.0 > ToggleSteps66_g22165 )
				ifLocalVar2136_g22165 = ToonRampTexVertexLightLuminanced352_g22165;
			else if( 1.0 == ToggleSteps66_g22165 )
				ifLocalVar2136_g22165 = VertexLightNdLStepped2141_g22165;
			float4 NdLVertexLightsShadows24300 = ifLocalVar2136_g22165;
			float4 temp_cast_264 = (-max( SpecShadowMaskPower286_g22168 , -0.99 )).xxxx;
			float temp_output_2387_0_g22168 = ( saturate( SpecShadowMaskPower286_g22168 ) * 0.5 );
			float4 lerpResult2379_g22168 = lerp( ( ( NdLVertexLightsShadows24300 - temp_cast_264 ) / ( 1.0 - -max( SpecShadowMaskPower286_g22168 , -0.99 ) ) ) , float4( 1,1,1,1 ) , ( ( SpecShadowMaskPower286_g22168 * temp_output_2387_0_g22168 ) + temp_output_2387_0_g22168 ));
			float4 temp_output_2378_0_g22168 = saturate( lerpResult2379_g22168 );
			float4 ifLocalVar1924_g22168 = 0;
			UNITY_BRANCH 
			if( 1.0 > SpecShadowMaskVar279_g22168 )
				ifLocalVar1924_g22168 = float4(1,1,1,1);
			else if( 1.0 == SpecShadowMaskVar279_g22168 )
				ifLocalVar1924_g22168 = temp_output_1727_0_g22168;
			else if( 1.0 < SpecShadowMaskVar279_g22168 )
				ifLocalVar1924_g22168 = temp_output_2378_0_g22168;
			float4 VertexLightsNdLGGX1923_g22168 = ifLocalVar1924_g22168;
			float4 VertexLightShadows24307 = VertexLightsNdLGGX1923_g22168;
			float4 break2546_g22174 = VertexLightShadows24307;
			float NdotL2576_g22174 = break2546_g22174.x;
			float NdotV2576_g22174 = CorrectedNdotV2507_g22174;
			float roughness2576_g22174 = roughness2729_g22174;
			float localgetSmithJointGGXVisibilityTerm2576_g22174 = getSmithJointGGXVisibilityTerm( NdotL2576_g22174 , NdotV2576_g22174 , roughness2576_g22174 );
			float GGXVisibilityTerm2521_g22174 = localgetSmithJointGGXVisibilityTerm2576_g22174;
			float4 break346_g21922 = FourLightPosX0WorldPos286_g21922;
			float4 break277_g21922 = FourLightPosY0WorldPos291_g21922;
			float4 break300_g21922 = FourLightPosZ0WorldPos325_g21922;
			float3 appendResult358_g21922 = (float3(break346_g21922.x , break277_g21922.x , break300_g21922.x));
			float3 normalizeResult292_g21922 = normalize( appendResult358_g21922 );
			float3 normalizeResult289_g21922 = ASESafeNormalize( ( ase_worldViewDir + normalizeResult292_g21922 ) );
			float dotResult318_g21922 = dot( WorldNormals20_g21922 , normalizeResult289_g21922 );
			float VLNdotHOne21094 = dotResult318_g21922;
			float NdotH2569_g22174 = max( VLNdotHOne21094 , 0.0 );
			float roughness2569_g22174 = roughness2729_g22174;
			float localgetGGXTerm2569_g22174 = getGGXTerm( NdotH2569_g22174 , roughness2569_g22174 );
			float GGXTerm2521_g22174 = localgetGGXTerm2569_g22174;
			float NdotL2521_g22174 = break2546_g22174.x;
			float dotResult339_g21922 = dot( normalizeResult292_g21922 , normalizeResult289_g21922 );
			float VLLdotHOne21102 = dotResult339_g21922;
			float LdotH2521_g22174 = max( VLLdotHOne21102 , 0.0 );
			float roughness2521_g22174 = roughness2729_g22174;
			float3 specColor2521_g22174 = SpecColor2715_g22174;
			float3 localLightColorZero2834_g22174 = LightColorZero();
			float4 break2841_g22174 = VertexLightAtten21183;
			float3 lightcolor2521_g22174 = ( localLightColorZero2834_g22174 * break2841_g22174.x );
			float specularTermToggle2521_g22174 = SpecularHighlightToggle2498_g22174;
			float3 localcalcSpecularTerm2521_g22174 = calcSpecularTerm( GGXVisibilityTerm2521_g22174 , GGXTerm2521_g22174 , NdotL2521_g22174 , LdotH2521_g22174 , roughness2521_g22174 , specColor2521_g22174 , lightcolor2521_g22174 , specularTermToggle2521_g22174 );
			float NdotL2601_g22174 = break2546_g22174.y;
			float NdotV2601_g22174 = CorrectedNdotV2507_g22174;
			float roughness2601_g22174 = roughness2729_g22174;
			float localgetSmithJointGGXVisibilityTerm2601_g22174 = getSmithJointGGXVisibilityTerm( NdotL2601_g22174 , NdotV2601_g22174 , roughness2601_g22174 );
			float GGXVisibilityTerm2609_g22174 = localgetSmithJointGGXVisibilityTerm2601_g22174;
			float3 appendResult321_g21922 = (float3(break346_g21922.y , break277_g21922.y , break300_g21922.y));
			float3 normalizeResult308_g21922 = normalize( appendResult321_g21922 );
			float3 normalizeResult285_g21922 = ASESafeNormalize( ( ase_worldViewDir + normalizeResult308_g21922 ) );
			float dotResult298_g21922 = dot( WorldNormals20_g21922 , normalizeResult285_g21922 );
			float VLNdotHTwo21096 = dotResult298_g21922;
			float NdotH2600_g22174 = max( VLNdotHTwo21096 , 0.0 );
			float roughness2600_g22174 = roughness2729_g22174;
			float localgetGGXTerm2600_g22174 = getGGXTerm( NdotH2600_g22174 , roughness2600_g22174 );
			float GGXTerm2609_g22174 = localgetGGXTerm2600_g22174;
			float NdotL2609_g22174 = break2546_g22174.y;
			float dotResult354_g21922 = dot( normalizeResult308_g21922 , normalizeResult285_g21922 );
			float VLLdotHTwo21103 = dotResult354_g21922;
			float LdotH2609_g22174 = max( VLLdotHTwo21103 , 0.0 );
			float roughness2609_g22174 = roughness2729_g22174;
			float3 specColor2609_g22174 = SpecColor2715_g22174;
			float3 localLightColorZOne2835_g22174 = LightColorZOne();
			float3 lightcolor2609_g22174 = ( localLightColorZOne2835_g22174 * break2841_g22174.y );
			float specularTermToggle2609_g22174 = SpecularHighlightToggle2498_g22174;
			float3 localcalcSpecularTerm2609_g22174 = calcSpecularTerm( GGXVisibilityTerm2609_g22174 , GGXTerm2609_g22174 , NdotL2609_g22174 , LdotH2609_g22174 , roughness2609_g22174 , specColor2609_g22174 , lightcolor2609_g22174 , specularTermToggle2609_g22174 );
			float NdotL2618_g22174 = break2546_g22174.z;
			float NdotV2618_g22174 = CorrectedNdotV2507_g22174;
			float roughness2618_g22174 = roughness2729_g22174;
			float localgetSmithJointGGXVisibilityTerm2618_g22174 = getSmithJointGGXVisibilityTerm( NdotL2618_g22174 , NdotV2618_g22174 , roughness2618_g22174 );
			float GGXVisibilityTerm2626_g22174 = localgetSmithJointGGXVisibilityTerm2618_g22174;
			float3 appendResult355_g21922 = (float3(break346_g21922.z , break277_g21922.z , break300_g21922.z));
			float3 normalizeResult334_g21922 = normalize( appendResult355_g21922 );
			float3 normalizeResult327_g21922 = ASESafeNormalize( ( ase_worldViewDir + normalizeResult334_g21922 ) );
			float dotResult313_g21922 = dot( WorldNormals20_g21922 , normalizeResult327_g21922 );
			float VLNdotHThree21098 = dotResult313_g21922;
			float NdotH2617_g22174 = max( VLNdotHThree21098 , 0.0 );
			float roughness2617_g22174 = roughness2729_g22174;
			float localgetGGXTerm2617_g22174 = getGGXTerm( NdotH2617_g22174 , roughness2617_g22174 );
			float GGXTerm2626_g22174 = localgetGGXTerm2617_g22174;
			float NdotL2626_g22174 = break2546_g22174.z;
			float dotResult288_g21922 = dot( normalizeResult334_g21922 , normalizeResult327_g21922 );
			float VLLdotHThree21104 = dotResult288_g21922;
			float LdotH2626_g22174 = max( VLLdotHThree21104 , 0.0 );
			float roughness2626_g22174 = roughness2729_g22174;
			float3 specColor2626_g22174 = SpecColor2715_g22174;
			float3 localLightColorZTwo2833_g22174 = LightColorZTwo();
			float3 lightcolor2626_g22174 = ( localLightColorZTwo2833_g22174 * break2841_g22174.z );
			float specularTermToggle2626_g22174 = SpecularHighlightToggle2498_g22174;
			float3 localcalcSpecularTerm2626_g22174 = calcSpecularTerm( GGXVisibilityTerm2626_g22174 , GGXTerm2626_g22174 , NdotL2626_g22174 , LdotH2626_g22174 , roughness2626_g22174 , specColor2626_g22174 , lightcolor2626_g22174 , specularTermToggle2626_g22174 );
			float NdotL2635_g22174 = break2546_g22174.w;
			float NdotV2635_g22174 = CorrectedNdotV2507_g22174;
			float roughness2635_g22174 = roughness2729_g22174;
			float localgetSmithJointGGXVisibilityTerm2635_g22174 = getSmithJointGGXVisibilityTerm( NdotL2635_g22174 , NdotV2635_g22174 , roughness2635_g22174 );
			float GGXVisibilityTerm2643_g22174 = localgetSmithJointGGXVisibilityTerm2635_g22174;
			float3 appendResult278_g21922 = (float3(break346_g21922.w , break277_g21922.w , break300_g21922.w));
			float3 normalizeResult281_g21922 = normalize( appendResult278_g21922 );
			float3 normalizeResult319_g21922 = ASESafeNormalize( ( ase_worldViewDir + normalizeResult281_g21922 ) );
			float dotResult314_g21922 = dot( WorldNormals20_g21922 , normalizeResult319_g21922 );
			float VLNdotHFour21100 = dotResult314_g21922;
			float NdotH2634_g22174 = max( VLNdotHFour21100 , 0.0 );
			float roughness2634_g22174 = roughness2729_g22174;
			float localgetGGXTerm2634_g22174 = getGGXTerm( NdotH2634_g22174 , roughness2634_g22174 );
			float GGXTerm2643_g22174 = localgetGGXTerm2634_g22174;
			float NdotL2643_g22174 = break2546_g22174.w;
			float dotResult362_g21922 = dot( normalizeResult281_g21922 , normalizeResult319_g21922 );
			float VLLdotHFour21105 = dotResult362_g21922;
			float LdotH2643_g22174 = max( VLLdotHFour21105 , 0.0 );
			float roughness2643_g22174 = roughness2729_g22174;
			float3 specColor2643_g22174 = SpecColor2715_g22174;
			float3 localLightColorZThree2840_g22174 = LightColorZThree();
			float3 lightcolor2643_g22174 = ( localLightColorZThree2840_g22174 * break2841_g22174.w );
			float specularTermToggle2643_g22174 = SpecularHighlightToggle2498_g22174;
			float3 localcalcSpecularTerm2643_g22174 = calcSpecularTerm( GGXVisibilityTerm2643_g22174 , GGXTerm2643_g22174 , NdotL2643_g22174 , LdotH2643_g22174 , roughness2643_g22174 , specColor2643_g22174 , lightcolor2643_g22174 , specularTermToggle2643_g22174 );
			#ifdef UNITY_PASS_FORWARDBASE
				float3 staticSwitch2764_g22174 = ( localcalcSpecularTerm2521_g22174 + localcalcSpecularTerm2609_g22174 + localcalcSpecularTerm2626_g22174 + localcalcSpecularTerm2643_g22174 );
			#else
				float3 staticSwitch2764_g22174 = float3( 0,0,0 );
			#endif
			#ifdef VERTEXLIGHT_ON
				float3 staticSwitch2765_g22174 = staticSwitch2764_g22174;
			#else
				float3 staticSwitch2765_g22174 = float3( 0,0,0 );
			#endif
			float3 VertexLightGGXPBRMetallicWF2533_g22174 = staticSwitch2765_g22174;
			float3 specularTerm2404_g22174 = ( ifLocalVar2672_g22174 + VertexLightGGXPBRMetallicWF2533_g22174 );
			float NdotV2404_g22174 = CorrectedNdotV2507_g22174;
			float3 specColor2404_g22174 = SpecColor2715_g22174;
			float roughness2404_g22174 = roughness2729_g22174;
			float oneMinusReflectivity2404_g22174 = OneMinusReflectivity2718_g22174;
			float localGetSpecCubeDimensions1255_g22174 = ( 0.0 );
			float testW1255_g22174 = 0;
			float testH1255_g22174 = 0;
			{
			#ifndef SHADER_TARGET_SURFACE_ANALYSIS
			unity_SpecCube0.GetDimensions(testW1255_g22174,testH1255_g22174);
			#endif
			}
			float3 indirectNormal2316_g22174 = WorldNormals305_g22174;
			Unity_GlossyEnvironmentData g2316_g22174 = UnityGlossyEnvironmentSetup( SmoothnessTex290_g22174, data.worldViewDir, indirectNormal2316_g22174, float3(0,0,0));
			float3 indirectSpecular2316_g22174 = UnityGI_IndirectSpecular( data, 1.0, indirectNormal2316_g22174, g2316_g22174 );
			float3 ase_worldReflection = WorldReflectionVector( i, float3( 0, 0, 1 ) );
			float3 ifLocalVar45_g22174 = 0;
			if( 1.0 > IgnoreNormalsToggle323_g22174 )
				ifLocalVar45_g22174 = WorldReflectionVector( i , normals2353 );
			else if( 1.0 == IgnoreNormalsToggle323_g22174 )
				ifLocalVar45_g22174 = ase_worldReflection;
			half3 linearRgb1631_g22165 = ( ase_lightColor.rgb * staticSwitch1035_g22165 );
			half localgetLinearRgbToLuminance1631_g22165 = getLinearRgbToLuminance( linearRgb1631_g22165 );
			half3 linearRgb1630_g22165 = ( lerpResult1661_g22165 * AmbientLightBoosted1782_g22165 );
			half localgetLinearRgbToLuminance1630_g22165 = getLinearRgbToLuminance( linearRgb1630_g22165 );
			#ifdef UNITY_PASS_FORWARDBASE
				float staticSwitch1995_g22165 = localgetLinearRgbToLuminance1433_g22165;
			#else
				float staticSwitch1995_g22165 = 0.0;
			#endif
			#ifdef VERTEXLIGHT_ON
				float staticSwitch1994_g22165 = staticSwitch1995_g22165;
			#else
				float staticSwitch1994_g22165 = 0.0;
			#endif
			float LuminancedVertexLights1991_g22165 = staticSwitch1994_g22165;
			float LuminancedLight21855 = ( ( localgetLinearRgbToLuminance1631_g22165 + localgetLinearRgbToLuminance1630_g22165 ) + LuminancedVertexLights1991_g22165 );
			float3 BakedCubemap1524_g22174 = ( (SAMPLE_TEXTURECUBE_LOD( _Cubemap, sampler_Cubemap, ifLocalVar45_g22174, ( ( 1.0 - SmoothnessTex290_g22174 ) * 10.0 ) )).rgb * saturate( LuminancedLight21855 ) );
			float3 ifLocalVar2337_g22174 = 0;
			if( testW1255_g22174 >= 16 )
				ifLocalVar2337_g22174 = indirectSpecular2316_g22174;
			else
				ifLocalVar2337_g22174 = BakedCubemap1524_g22174;
			float3 ifLocalVar2338_g22174 = 0;
			UNITY_BRANCH 
			if( 1.0 > _WorkflowSwitch )
				ifLocalVar2338_g22174 = ifLocalVar2337_g22174;
			else if( 1.0 == _WorkflowSwitch )
				ifLocalVar2338_g22174 = BakedCubemap1524_g22174;
			else if( 1.0 < _WorkflowSwitch )
				ifLocalVar2338_g22174 = indirectSpecular2316_g22174;
			float AmbientOcclusion16840 = lerpResult54_g22165;
			float AO2783_g22174 = AmbientOcclusion16840;
			float3 indirectspecular2404_g22174 = ( ifLocalVar2338_g22174 * AO2783_g22174 );
			float smoothness2404_g22174 = SmoothnessTex290_g22174;
			float perceptualRoughness2404_g22174 = perceptualRoughness2761_g22174;
			float3 localcalcSpecularBase2404_g22174 = calcSpecularBase( specularTerm2404_g22174 , NdotV2404_g22174 , specColor2404_g22174 , roughness2404_g22174 , oneMinusReflectivity2404_g22174 , indirectspecular2404_g22174 , smoothness2404_g22174 , perceptualRoughness2404_g22174 );
			float3 SpecularReflections316_g22174 = localcalcSpecularBase2404_g22174;
			float3 temp_output_25130_0 = ( SpecularReflections316_g22174 * temp_output_80_0_g22174 );
			float3 CubemapReflections10644 = ( temp_output_25130_0 * temp_output_21533_0 );
			float3 worldSpaceViewDir449_g22227 = WorldSpaceViewDir( float4( 0,0,0,1 ) );
			float3 normalizeResult803_g22227 = ASESafeNormalize( worldSpaceViewDir449_g22227 );
			float3 ifLocalVar906_g22227 = 0;
			if( 1.0 > _MatcapViewDir )
				ifLocalVar906_g22227 = ase_worldViewDir;
			else if( 1.0 == _MatcapViewDir )
				ifLocalVar906_g22227 = normalizeResult803_g22227;
			float3 _Vector0 = float3(0,1,0);
			float dotResult9_g22227 = dot( ifLocalVar906_g22227 , _Vector0 );
			float3 normalizeResult13_g22227 = normalize( ( _Vector0 - ( dotResult9_g22227 * ifLocalVar906_g22227 ) ) );
			float3 normalizeResult19_g22227 = normalize( cross( ifLocalVar906_g22227 , normalizeResult13_g22227 ) );
			float3 normalizeResult16_g22227 = normalize( ase_worldNormal );
			float3 ifLocalVar20_g22227 = 0;
			if( 1.0 > _IgnoreNormalsMatcap )
				ifLocalVar20_g22227 = worldnormals2351;
			else if( 1.0 == _IgnoreNormalsMatcap )
				ifLocalVar20_g22227 = normalizeResult16_g22227;
			float dotResult22_g22227 = dot( normalizeResult19_g22227 , ifLocalVar20_g22227 );
			float dotResult23_g22227 = dot( normalizeResult13_g22227 , ifLocalVar20_g22227 );
			float2 appendResult25_g22227 = (float2(dotResult22_g22227 , dotResult23_g22227));
			float2 MatcapUV215_g22227 = ( ( appendResult25_g22227 * 0.5 ) + 0.5 );
			float ReflectionR1Intensity224_g22227 = _ReflectionR1Intensity;
			float3 temp_output_45_0_g22227 = ( ( (SAMPLE_TEXTURE2D_LOD( _MatcapR1, sampler_trilinear_repeat, MatcapUV215_g22227, ( ( 1.0 - _MatcapR1smoothness ) * 10.0 ) )).rgb * (_MatcapR1Color).rgb * ( _MatcapR1Color.a * MatcapR1Blending703_g22227 ) ) * ReflectionR1Intensity224_g22227 );
			#ifdef UNITY_PASS_FORWARDADD
				float3 staticSwitch2216_g22165 = ( ase_lightColor.rgb * ase_lightAtten );
			#else
				float3 staticSwitch2216_g22165 = ase_lightColor.rgb;
			#endif
			half3 linearRgb2203_g22165 = staticSwitch2216_g22165;
			half localgetLinearRgbToLuminance2203_g22165 = getLinearRgbToLuminance( linearRgb2203_g22165 );
			half3 linearRgb2202_g22165 = AmbientLightBoosted1782_g22165;
			half localgetLinearRgbToLuminance2202_g22165 = getLinearRgbToLuminance( linearRgb2202_g22165 );
			float LuminancedLightNoShadows25046 = ( localgetLinearRgbToLuminance2203_g22165 + localgetLinearRgbToLuminance2202_g22165 + LuminancedVertexLights1991_g22165 );
			float MatcapLighting825_g22227 = saturate( LuminancedLightNoShadows25046 );
			float3 MainTex207_g22227 = MainTexSaturate2197;
			float AmbientOcclusion834_g22227 = AmbientOcclusion16840;
			float3 PreClampFinalLight21212 = ( ForFinalLightCalculation993_g22165 + DiffuseVertexLighting354_g22165 );
			float3 MatcapLightingRGB901_g22227 = PreClampFinalLight21212;
			float3 ifLocalVar59_g22227 = 0;
			if( 1.0 > _MatcapR1Mode )
				ifLocalVar59_g22227 = ( temp_output_45_0_g22227 * MatcapLighting825_g22227 * MainTex207_g22227 * AmbientOcclusion834_g22227 );
			else if( 1.0 == _MatcapR1Mode )
				ifLocalVar59_g22227 = ( temp_output_45_0_g22227 * MatcapLightingRGB901_g22227 );
			else if( 1.0 < _MatcapR1Mode )
				ifLocalVar59_g22227 = ( ( 1.0 - temp_output_45_0_g22227 ) * MatcapLightingRGB901_g22227 );
			float3 lerpResult60_g22227 = lerp( float3( 1,1,1 ) , MainTex207_g22227 , _ReflectionR1Tint);
			float3 ifLocalVar427_g22227 = 0;
			UNITY_BRANCH 
			if( _MatcapR1Toggle > 0 )
				ifLocalVar427_g22227 = ( ifLocalVar59_g22227 * lerpResult60_g22227 * ( ReflectionR1Intensity224_g22227 * ReflectionMaskR199_g22227 ) );
			float3 MatcapR1calc419_g22227 = ( ifLocalVar427_g22227 * temp_output_570_0_g22227 );
			float ReflectionG2Intensity298_g22227 = _ReflectionG2Intensity;
			float3 temp_output_277_0_g22227 = ( ( (SAMPLE_TEXTURE2D_LOD( _MatcapG2, sampler_trilinear_repeat, MatcapUV215_g22227, ( ( 1.0 - _MatcapG2smoothness ) * 10.0 ) )).rgb * (_MatcapG2Color).rgb * ( _MatcapG2Color.a * MatcapG2Blending706_g22227 ) ) * ReflectionG2Intensity298_g22227 );
			float3 ifLocalVar262_g22227 = 0;
			if( 1.0 > _MatcapG2Mode )
				ifLocalVar262_g22227 = ( temp_output_277_0_g22227 * MatcapLighting825_g22227 * MainTex207_g22227 * AmbientOcclusion834_g22227 );
			else if( 1.0 == _MatcapG2Mode )
				ifLocalVar262_g22227 = ( temp_output_277_0_g22227 * MatcapLightingRGB901_g22227 );
			else if( 1.0 < _MatcapG2Mode )
				ifLocalVar262_g22227 = ( ( 1.0 - temp_output_277_0_g22227 ) * MatcapLightingRGB901_g22227 );
			float3 lerpResult254_g22227 = lerp( float3( 1,1,1 ) , MainTex207_g22227 , _ReflectionG2Tint);
			float3 ifLocalVar437_g22227 = 0;
			UNITY_BRANCH 
			if( _MatcapG2Toggle > 0 )
				ifLocalVar437_g22227 = ( ifLocalVar262_g22227 * lerpResult254_g22227 * ( ReflectionG2Intensity298_g22227 * ReflectionMaskG200_g22227 ) );
			float3 MatcapG2calc420_g22227 = ( ifLocalVar437_g22227 * temp_output_573_0_g22227 );
			float ReflectionB3Intensity300_g22227 = _ReflectionB3Intensity;
			float3 temp_output_361_0_g22227 = ( ( (SAMPLE_TEXTURE2D_LOD( _MatcapB3, sampler_trilinear_repeat, MatcapUV215_g22227, ( ( 1.0 - _MatcapB3smoothness ) * 10.0 ) )).rgb * (_MatcapB3Color).rgb * ( _MatcapB3Color.a * MatcapB3Blending708_g22227 ) ) * ReflectionB3Intensity300_g22227 );
			float3 ifLocalVar348_g22227 = 0;
			if( 1.0 > _MatcapB3Mode )
				ifLocalVar348_g22227 = ( temp_output_361_0_g22227 * MatcapLighting825_g22227 * MainTex207_g22227 * AmbientOcclusion834_g22227 );
			else if( 1.0 == _MatcapB3Mode )
				ifLocalVar348_g22227 = ( temp_output_361_0_g22227 * MatcapLightingRGB901_g22227 );
			else if( 1.0 < _MatcapB3Mode )
				ifLocalVar348_g22227 = ( ( 1.0 - temp_output_361_0_g22227 ) * MatcapLightingRGB901_g22227 );
			float3 lerpResult340_g22227 = lerp( float3( 1,1,1 ) , MainTex207_g22227 , _ReflectionB3Tint);
			float3 ifLocalVar438_g22227 = 0;
			UNITY_BRANCH 
			if( _MatcapB3Toggle > 0 )
				ifLocalVar438_g22227 = ( ifLocalVar348_g22227 * lerpResult340_g22227 * ( ReflectionB3Intensity300_g22227 * ReflectionMaskB201_g22227 ) );
			float3 MatcapB3calc421_g22227 = ( ifLocalVar438_g22227 * temp_output_576_0_g22227 );
			float ReflectionA4Intensity302_g22227 = _ReflectionA4Intensity;
			float3 temp_output_402_0_g22227 = ( ( (SAMPLE_TEXTURE2D_LOD( _MatcapA4, sampler_trilinear_repeat, MatcapUV215_g22227, ( ( 1.0 - _MatcapA4smoothness ) * 10.0 ) )).rgb * (_MatcapA4Color).rgb * ( _MatcapA4Color.a * MatcapA4Blending710_g22227 ) ) * ReflectionA4Intensity302_g22227 );
			float3 ifLocalVar389_g22227 = 0;
			if( 1.0 > _MatcapA4Mode )
				ifLocalVar389_g22227 = ( temp_output_402_0_g22227 * MatcapLighting825_g22227 * MainTex207_g22227 * AmbientOcclusion834_g22227 );
			else if( 1.0 == _MatcapA4Mode )
				ifLocalVar389_g22227 = ( temp_output_402_0_g22227 * MatcapLightingRGB901_g22227 );
			else if( 1.0 < _MatcapA4Mode )
				ifLocalVar389_g22227 = ( ( 1.0 - temp_output_402_0_g22227 ) * MatcapLightingRGB901_g22227 );
			float3 lerpResult381_g22227 = lerp( float3( 1,1,1 ) , MainTex207_g22227 , _ReflectionA4Tint);
			float3 ifLocalVar439_g22227 = 0;
			UNITY_BRANCH 
			if( _MatcapA4Toggle > 0 )
				ifLocalVar439_g22227 = ( ifLocalVar389_g22227 * lerpResult381_g22227 * ( ReflectionA4Intensity302_g22227 * ReflectionMaskA202_g22227 ) );
			float3 MatcapA4calc422_g22227 = ( ifLocalVar439_g22227 * temp_output_579_0_g22227 );
			float3 Matcap24108 = ( MatcapR1calc419_g22227 + MatcapG2calc420_g22227 + MatcapB3calc421_g22227 + MatcapA4calc422_g22227 );
			float DirectionToggle135_g22344 = _RimDirectionToggle;
			float RimShape196_g22344 = pow( ( 1.0 - saturate( ( NdotV19803 + _RimOffset ) ) ) , max( _RimPower , ( 1E-06 + 1E-06 ) ) );
			float3 WorldNormals127_g22344 = worldnormals2351;
			float fresnelNdotV93_g22344 = dot( WorldNormals127_g22344, ase_worldViewDir );
			float fresnelNode93_g22344 = ( _RimFresnelBias + _RimFresnelScale * pow( 1.0 - fresnelNdotV93_g22344, _RimFresnelPower ) );
			float Fresnel171_g22344 = fresnelNode93_g22344;
			float ifLocalVar274_g22344 = 0;
			if( 1.0 > DirectionToggle135_g22344 )
				ifLocalVar274_g22344 = RimShape196_g22344;
			else if( 1.0 == DirectionToggle135_g22344 )
				ifLocalVar274_g22344 = Fresnel171_g22344;
			float temp_output_2_0_g22345 = _RimTint;
			float temp_output_3_0_g22345 = ( 1.0 - temp_output_2_0_g22345 );
			float3 appendResult7_g22345 = (float3(temp_output_3_0_g22345 , temp_output_3_0_g22345 , temp_output_3_0_g22345));
			float3 temp_output_35_0_g22344 = ( ( MainTexSaturate2197 * temp_output_2_0_g22345 ) + appendResult7_g22345 );
			float UVSwitchProp340_g22344 = _RimMaskUVSwitch;
			float2 UV0340_g22344 = i.uv_texcoord;
			float2 UV1340_g22344 = i.uv2_texcoord2;
			float2 UV2340_g22344 = i.uv3_texcoord3;
			float2 UV3340_g22344 = i.uv4_texcoord4;
			float2 localUVSwitch340_g22344 = UVSwitch( UVSwitchProp340_g22344 , UV0340_g22344 , UV1340_g22344 , UV2340_g22344 , UV3340_g22344 );
			float4 tex2DNode296_g22344 = SAMPLE_TEXTURE2D( _RimMask, sampler_trilinear_repeat, ( ( _RimMask_ST.xy * localUVSwitch340_g22344 ) + _RimMask_ST.zw ) );
			float ifLocalVar104_g22344 = 0;
			if( 1.0 > _RimLightMaskinv )
				ifLocalVar104_g22344 = tex2DNode296_g22344.g;
			else if( 1.0 == _RimLightMaskinv )
				ifLocalVar104_g22344 = ( 1.0 - tex2DNode296_g22344.g );
			float FinalMask165_g22344 = ifLocalVar104_g22344;
			float ifLocalVar181_g22344 = 0;
			if( 1.0 > DirectionToggle135_g22344 )
				ifLocalVar181_g22344 = RimShape196_g22344;
			else if( 1.0 == DirectionToggle135_g22344 )
				ifLocalVar181_g22344 = Fresnel171_g22344;
			float3 indirectNormal184_g22344 = WorldNormals127_g22344;
			Unity_GlossyEnvironmentData g184_g22344 = UnityGlossyEnvironmentSetup( _RimSpecLightsmoothness, data.worldViewDir, indirectNormal184_g22344, float3(0,0,0));
			float3 indirectSpecular184_g22344 = UnityGI_IndirectSpecular( data, AmbientOcclusion16840, indirectNormal184_g22344, g184_g22344 );
			float3 ifLocalVar203_g22344 = 0;
			if( 1.0 == _RimSpecToggle )
				ifLocalVar203_g22344 = ( saturate( ifLocalVar181_g22344 ) * indirectSpecular184_g22344 );
			float3 temp_output_189_0_g22344 = ( ( ( PreClampFinalLight21212 * saturate( ifLocalVar274_g22344 ) ) * temp_output_35_0_g22344 * (_RimColor).rgb * FinalMask165_g22344 ) + ( ifLocalVar203_g22344 * temp_output_35_0_g22344 * FinalMask165_g22344 * _RimOpacity ) );
			float3 switchResult252_g22344 = (((i.ASEVFace>0)?(temp_output_189_0_g22344):(float3( 0,0,0 ))));
			float3 switchResult253_g22344 = (((i.ASEVFace>0)?(float3( 0,0,0 )):(temp_output_189_0_g22344)));
			float3 ifLocalVar251_g22344 = 0;
			if( 1.0 > _RimFaceCulling )
				ifLocalVar251_g22344 = temp_output_189_0_g22344;
			else if( 1.0 == _RimFaceCulling )
				ifLocalVar251_g22344 = switchResult252_g22344;
			else if( 1.0 < _RimFaceCulling )
				ifLocalVar251_g22344 = switchResult253_g22344;
			float3 ifLocalVar6009 = 0;
			if( 1.0 == _RimToggle )
				ifLocalVar6009 = ifLocalVar251_g22344;
			float3 rimlight2221 = ( ifLocalVar6009 * ( _RimToggle * ToggleAdvanced21493 ) );
			float2 uv_Flipbook = i.uv_texcoord * _Flipbook_ST.xy + _Flipbook_ST.zw;
			float cos16_g22340 = cos( ( _RotateFlipbook * UNITY_PI ) );
			float sin16_g22340 = sin( ( _RotateFlipbook * UNITY_PI ) );
			float2 rotator16_g22340 = mul( uv_Flipbook - float2( 0.5,0.5 ) , float2x2( cos16_g22340 , -sin16_g22340 , sin16_g22340 , cos16_g22340 )) + float2( 0.5,0.5 );
			float2 _Vector3 = float2(0,0);
			float2 temp_output_6_0_g22340 = ( 1.0 - float2( 1,1 ) );
			float2 temp_output_7_0_g22340 = ( 2.0 + float2( 0,0 ) );
			float2 _Vector2 = float2(1,1);
			float2 temp_output_19_0_g22340 = (_Vector3 + (rotator16_g22340 - ( _Vector3 + ( temp_output_6_0_g22340 / temp_output_7_0_g22340 ) )) * (_Vector2 - _Vector3) / (( _Vector2 - ( temp_output_6_0_g22340 / temp_output_7_0_g22340 ) ) - ( _Vector3 + ( temp_output_6_0_g22340 / temp_output_7_0_g22340 ) )));
			float temp_output_4_0_g22342 = (float)_Columns;
			float temp_output_5_0_g22342 = (float)_Rows;
			float2 appendResult7_g22342 = (float2(temp_output_4_0_g22342 , temp_output_5_0_g22342));
			float totalFrames39_g22342 = ( temp_output_4_0_g22342 * temp_output_5_0_g22342 );
			float2 appendResult8_g22342 = (float2(totalFrames39_g22342 , temp_output_5_0_g22342));
			float mulTime26_g22340 = _Time.y * (float)_Speed;
			float clampResult42_g22342 = clamp( 0.0 , 0.0001 , ( totalFrames39_g22342 - 1.0 ) );
			float temp_output_35_0_g22342 = frac( ( ( fmod( mulTime26_g22340 , (float)(float)_MaxFrames ) + clampResult42_g22342 ) / totalFrames39_g22342 ) );
			float2 appendResult29_g22342 = (float2(temp_output_35_0_g22342 , ( 1.0 - temp_output_35_0_g22342 )));
			float2 temp_output_15_0_g22342 = ( ( temp_output_19_0_g22340 / appendResult7_g22342 ) + ( floor( ( appendResult8_g22342 * appendResult29_g22342 ) ) / appendResult7_g22342 ) );
			float2 break20_g22340 = temp_output_19_0_g22340;
			float A1_g22341 = floor( max( break20_g22340.x , break20_g22340.y ) );
			float B1_g22341 = floor( ( 1.0 - min( break20_g22340.x , break20_g22340.y ) ) );
			float localASEOr1_g22341 = ASEOr( A1_g22341 , B1_g22341 );
			float3 PreFinalLight9087 = ( temp_output_1997_0_g22165 + temp_output_782_0_g22165 );
			float3 lerpResult43_g22340 = lerp( PreFinalLight9087 , FinalLight2205 , _FlipbookTint);
			float3 ifLocalVar2969 = 0;
			if( _FlipbookToggle == 1.0 )
				ifLocalVar2969 = ( ( (SAMPLE_TEXTURE2D( _Flipbook, sampler_Flipbook, temp_output_15_0_g22342 )).rgb * ( 1.0 - localASEOr1_g22341 ) ) * (_FlipbookColor).rgb * _FlipbookColor.a * lerpResult43_g22340 );
			float3 Flipbook2584 = ( ifLocalVar2969 * ( _FlipbookToggle * ToggleAdvanced21493 ) );
			float temp_output_142_0_g22343 = GrayscaledLight19806;
			float3 WorldNormals72_g22343 = worldnormals2351;
			float3 temp_cast_269 = (1.0).xxx;
			float UVSwitchProp449_g22343 = _SSSThicknessMapUVSwitch;
			float2 UV0449_g22343 = i.uv_texcoord;
			float2 UV1449_g22343 = i.uv2_texcoord2;
			float2 UV2449_g22343 = i.uv3_texcoord3;
			float2 UV3449_g22343 = i.uv4_texcoord4;
			float2 localUVSwitch449_g22343 = UVSwitch( UVSwitchProp449_g22343 , UV0449_g22343 , UV1449_g22343 , UV2449_g22343 , UV3449_g22343 );
			float3 temp_output_349_0_g22343 = (SAMPLE_TEXTURE2D( _SSSThicknessMap, sampler_trilinear_repeat, ( ( _SSSThicknessMap_ST.xy * localUVSwitch449_g22343 ) + _SSSThicknessMap_ST.zw ) )).rgb;
			float3 ifLocalVar52_g22343 = 0;
			if( 1.0 > _SSSThicknessinv )
				ifLocalVar52_g22343 = temp_output_349_0_g22343;
			else if( 1.0 == _SSSThicknessinv )
				ifLocalVar52_g22343 = ( 1.0 - temp_output_349_0_g22343 );
			float3 ifLocalVar426_g22343 = 0;
			if( 1.0 > _SSSMapMode )
				ifLocalVar426_g22343 = temp_cast_269;
			else if( 1.0 == _SSSMapMode )
				ifLocalVar426_g22343 = ifLocalVar52_g22343;
			float3 ThicknessMapMod432_g22343 = ifLocalVar426_g22343;
			float3 break435_g22343 = ThicknessMapMod432_g22343;
			float SubsurfaceDistortionModifier73_g22343 = ( _SubsurfaceDistortionModifier * break435_g22343.z );
			float dotResult9_g22343 = dot( -( ase_worldlightDir + ( WorldNormals72_g22343 * SubsurfaceDistortionModifier73_g22343 ) ) , ase_worldViewDir );
			float SSSPower74_g22343 = ( _SSSPower * break435_g22343.y );
			float SSSIntensity75_g22343 = ( _SSSScale * break435_g22343.x );
			float temp_output_428_0_g22343 = ( saturate( pow( saturate( dotResult9_g22343 ) , SSSPower74_g22343 ) ) * SSSIntensity75_g22343 );
			float3 temp_cast_270 = (temp_output_428_0_g22343).xxx;
			float3 MainTex76_g22343 = MainTexSaturate2197;
			float SSSTint77_g22343 = _SSSTint;
			float3 lerpResult40_g22343 = lerp( temp_cast_270 , ( temp_output_428_0_g22343 * MainTex76_g22343 ) , SSSTint77_g22343);
			#ifdef UNITY_PASS_FORWARDADD
				float staticSwitch248_g22343 = ase_lightAtten;
			#else
				float staticSwitch248_g22343 = 0.0;
			#endif
			float3 TangentNormals2171_g22165 = normals2353;
			float3 temp_cast_271 = (ase_lightAtten).xxx;
			float dotResult2167_g22165 = dot( TangentNormals2171_g22165 , temp_cast_271 );
			float SSSTangentNormalAtten2169_g22165 = ( ( dotResult2167_g22165 * ( NdLHalfingControl704_g22165 + 0.5 ) ) + ( ( ( ( 1.0 - NdLHalfingControl704_g22165 ) * 0.5 ) - 0.25 ) + ( RampOffset167_g22165 - 0.5 ) ) );
			float2 temp_cast_272 = (SSSTangentNormalAtten2169_g22165).xx;
			float3 SSSToonRampTexATTEN2175_g22165 = (SAMPLE_TEXTURE2D( _ToonRamp, sampler_linear_clamp, temp_cast_272 )).rgb;
			#ifdef DIRECTIONAL
				float3 staticSwitch2152_g22165 = SSSToonRampTexATTEN2175_g22165;
			#else
				float3 staticSwitch2152_g22165 = _egg;
			#endif
			float3 SSSDirectionalAttenuationRamp2190_g22165 = staticSwitch2152_g22165;
			half3 linearRgb2154_g22165 = SSSDirectionalAttenuationRamp2190_g22165;
			half localgetLinearRgbToLuminance2154_g22165 = getLinearRgbToLuminance( linearRgb2154_g22165 );
			#ifdef DIRECTIONAL
				float staticSwitch2192_g22165 = SSSTangentNormalAtten2169_g22165;
			#else
				float staticSwitch2192_g22165 = 1.0;
			#endif
			float SSSDirectionalAttenuationSteps2194_g22165 = staticSwitch2192_g22165;
			float SSSShadowCasterSteps2195_g22165 = saturate( ( floor( ( SSSDirectionalAttenuationSteps2194_g22165 * Steps30_g22165 ) ) / ( Steps30_g22165 - 1 ) ) );
			float ifLocalVar2197_g22165 = 0;
			UNITY_BRANCH 
			if( 1.0 > ToggleSteps66_g22165 )
				ifLocalVar2197_g22165 = localgetLinearRgbToLuminance2154_g22165;
			else if( 1.0 == ToggleSteps66_g22165 )
				ifLocalVar2197_g22165 = SSSShadowCasterSteps2195_g22165;
			float SSSAtten25037 = ifLocalVar2197_g22165;
			#ifdef DIRECTIONAL
				float staticSwitch120_g22343 = SSSAtten25037;
			#else
				float staticSwitch120_g22343 = staticSwitch248_g22343;
			#endif
			float3 temp_output_56_0_g22343 = ( ase_lightColor.rgb * staticSwitch120_g22343 );
			float3 localambientDir58_g22343 = ambientDir();
			float dotResult92_g22343 = dot( -( localambientDir58_g22343 + ( WorldNormals72_g22343 * SubsurfaceDistortionModifier73_g22343 ) ) , ase_worldViewDir );
			float temp_output_427_0_g22343 = ( saturate( pow( saturate( dotResult92_g22343 ) , SSSPower74_g22343 ) ) * SSSIntensity75_g22343 );
			float3 temp_cast_273 = (temp_output_427_0_g22343).xxx;
			float3 lerpResult98_g22343 = lerp( temp_cast_273 , ( temp_output_427_0_g22343 * MainTex76_g22343 ) , SSSTint77_g22343);
			half3 localAmbient319_g22343 = Ambient();
			float3 ifLocalVar351_g22343 = 0;
			if( temp_output_142_0_g22343 > 0.0 )
				ifLocalVar351_g22343 = ( lerpResult40_g22343 * temp_output_56_0_g22343 );
			else if( temp_output_142_0_g22343 == 0.0 )
				ifLocalVar351_g22343 = ( lerpResult98_g22343 * localAmbient319_g22343 );
			float3 temp_cast_274 = (1.0).xxx;
			float3 ifLocalVar424_g22343 = 0;
			if( 1.0 > _SSSMapMode )
				ifLocalVar424_g22343 = ifLocalVar52_g22343;
			else if( 1.0 == _SSSMapMode )
				ifLocalVar424_g22343 = temp_cast_274;
			float3 ThicknessMapColor431_g22343 = ifLocalVar424_g22343;
			half3 linearRgb390_g22343 = ase_lightColor.rgb;
			half localgetLinearRgbToLuminance390_g22343 = getLinearRgbToLuminance( linearRgb390_g22343 );
			half3 linearRgb391_g22343 = localAmbient319_g22343;
			half localgetLinearRgbToLuminance391_g22343 = getLinearRgbToLuminance( linearRgb391_g22343 );
			float3 ifLocalVar388_g22343 = 0;
			if( temp_output_142_0_g22343 > 0.0 )
				ifLocalVar388_g22343 = ( lerpResult40_g22343 * staticSwitch120_g22343 * localgetLinearRgbToLuminance390_g22343 );
			else if( temp_output_142_0_g22343 == 0.0 )
				ifLocalVar388_g22343 = ( lerpResult98_g22343 * localgetLinearRgbToLuminance391_g22343 );
			float3 SSSColor84_g22343 = (_SSSColor).rgb;
			float3 ifLocalVar389_g22343 = 0;
			if( temp_output_142_0_g22343 > 0.0 )
				ifLocalVar389_g22343 = ( lerpResult40_g22343 * temp_output_56_0_g22343 );
			else if( temp_output_142_0_g22343 == 0.0 )
				ifLocalVar389_g22343 = ( lerpResult98_g22343 * localAmbient319_g22343 );
			float3 ifLocalVar6012 = 0;
			if( 1.0 > _SSSSetting )
				ifLocalVar6012 = ( ifLocalVar351_g22343 * ThicknessMapColor431_g22343 );
			else if( 1.0 == _SSSSetting )
				ifLocalVar6012 = ( ifLocalVar388_g22343 * ThicknessMapColor431_g22343 * SSSColor84_g22343 );
			else if( 1.0 < _SSSSetting )
				ifLocalVar6012 = ( ifLocalVar389_g22343 * ThicknessMapColor431_g22343 * SSSColor84_g22343 );
			float3 SSS2928 = ifLocalVar6012;
			float3 temp_output_295_0 = ( ( SpecularHighlight2223 * ( ToggleAdvanced21493 * _SpecularToggle ) ) + CubemapReflections10644 + Matcap24108 + rimlight2221 + Flipbook2584 + ( SSS2928 * ( ToggleAdvanced21493 * _SSSToggle ) ) );
			float UVSwitchProp24656 = _EmissionUVSwitch;
			float2 UV024656 = i.uv_texcoord;
			float2 UV124656 = i.uv2_texcoord2;
			float2 UV224656 = i.uv3_texcoord3;
			float2 UV324656 = i.uv4_texcoord4;
			float2 localUVSwitch24656 = UVSwitch( UVSwitchProp24656 , UV024656 , UV124656 , UV224656 , UV324656 );
			float2 EmissionUVSwitch24640 = ( ( _Emission_ST.xy * localUVSwitch24656 ) + _Emission_ST.zw );
			float3 lerpResult21858 = lerp( float3( 1,1,1 ) , MainTexSaturate2197 , _EmissionTint);
			float3 lerpResult21851 = lerp( ( (SAMPLE_TEXTURE2D( _Emission, sampler_MainTex, EmissionUVSwitch24640 )).rgb * (_EmissionColor).rgb * lerpResult21858 ) , float3( 0,0,0 ) , saturate( ( _EmissionLightscale * LuminancedLight21855 ) ));
			#ifdef UNITY_PASS_FORWARDADD
				float3 staticSwitch24414 = float3( 0,0,0 );
			#else
				float3 staticSwitch24414 = lerpResult21851;
			#endif
			float3 BasicEmission5690 = staticSwitch24414;
			c.rgb = ( FinalBase25138 + temp_output_295_0 + BasicEmission5690 );
			c.a = FinalAlphaOut25135;
			clip( ( max( ( 1.0 - ( ( color905_g22378 - MaterialzeMask165_g22378 ) * 10.0 ) ) , temp_cast_229 ) - temp_cast_234 ).r - _MaskClipValue );
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
			float mulTime7_g22351 = _Time.y * _NoiseSpeed;
			float UVSwitchProp79_g22351 = _NoiseTextureUVSwitch;
			float2 UV079_g22351 = i.uv_texcoord;
			float2 UV179_g22351 = i.uv2_texcoord2;
			float2 UV279_g22351 = i.uv3_texcoord3;
			float2 UV379_g22351 = i.uv4_texcoord4;
			float2 localUVSwitch79_g22351 = UVSwitch( UVSwitchProp79_g22351 , UV079_g22351 , UV179_g22351 , UV279_g22351 , UV379_g22351 );
			float2 NoiseTextureUVSwitch90_g22351 = ( ( _NoiseTexture_ST.xy * localUVSwitch79_g22351 ) + _NoiseTexture_ST.zw );
			float2 panner17_g22351 = ( mulTime7_g22351 * _NoiseVectorXY + NoiseTextureUVSwitch90_g22351);
			float mulTime4_g22351 = _Time.y * _NoiseSpeed;
			float2 panner12_g22351 = ( ( mulTime4_g22351 * 2.179 ) * _NoiseVectorXY + ( 1.0 - NoiseTextureUVSwitch90_g22351 ));
			float mulTime16_g22351 = _Time.y * _Emiossionscrollspeed;
			float UVSwitchProp80_g22351 = _EmissionscrollUVSwitch;
			float2 UV080_g22351 = i.uv_texcoord;
			float2 UV180_g22351 = i.uv2_texcoord2;
			float2 UV280_g22351 = i.uv3_texcoord3;
			float2 UV380_g22351 = i.uv4_texcoord4;
			float2 localUVSwitch80_g22351 = UVSwitch( UVSwitchProp80_g22351 , UV080_g22351 , UV180_g22351 , UV280_g22351 , UV380_g22351 );
			float2 EmissionscrollUVSwitch88_g22351 = ( ( _Emissionscroll_ST.xy * localUVSwitch80_g22351 ) + _Emissionscroll_ST.zw );
			float2 panner21_g22351 = ( mulTime16_g22351 * _VectorXY + EmissionscrollUVSwitch88_g22351);
			float4 Emissionsscrollcolor5894 = _EmissionscrollColor;
			float UVSwitchProp24658 = _EmissionScrollMaskUVSwitch;
			float2 UV024658 = i.uv_texcoord;
			float2 UV124658 = i.uv2_texcoord2;
			float2 UV224658 = i.uv3_texcoord3;
			float2 UV324658 = i.uv4_texcoord4;
			float2 localUVSwitch24658 = UVSwitch( UVSwitchProp24658 , UV024658 , UV124658 , UV224658 , UV324658 );
			float2 EmissionScrollMaskUVSwitch24639 = ( ( _EmissionScrollMask_ST.xy * localUVSwitch24658 ) + _EmissionScrollMask_ST.zw );
			float4 tex2DNode5677 = SAMPLE_TEXTURE2D( _EmissionScrollMask, sampler_MainTex, EmissionScrollMaskUVSwitch24639 );
			float local_AudioTextureDimensions287_g22346 = ( 0.0 );
			float w287_g22346 = 0;
			float h287_g22346 = 0;
			{
			#ifndef SHADER_TARGET_SURFACE_ANALYSIS
			_AudioTexture.GetDimensions(w287_g22346, h287_g22346);
			#endif
			}
			float temp_output_840_0_g22346 = ( w287_g22346 + _AudioLinkSwitch );
			float A1_g22350 = (( temp_output_840_0_g22346 >= 33.0 && temp_output_840_0_g22346 <= 35.0 ) ? 1.0 :  0.0 );
			float B1_g22350 = (( temp_output_840_0_g22346 >= 129.0 && temp_output_840_0_g22346 <= 131.0 ) ? 1.0 :  0.0 );
			float localASEOr1_g22350 = ASEOr( A1_g22350 , B1_g22350 );
			float AudioTextureCheck808_g22346 = localASEOr1_g22350;
			float mulTime5_g22346 = _Time.y * _ESSpeed;
			float locallongIF931_g22346 = ( 0.0 );
			float2 Out931_g22346 = float2( 0,0 );
			float3 _DefaultTangentVector = float3(0,0,1);
			float UVSwitchProp88_g21919 = _NormalMapUVSwitch;
			float2 UV088_g21919 = i.uv_texcoord;
			float2 UV188_g21919 = i.uv2_texcoord2;
			float2 UV288_g21919 = i.uv3_texcoord3;
			float2 UV388_g21919 = i.uv4_texcoord4;
			float2 localUVSwitch88_g21919 = UVSwitch( UVSwitchProp88_g21919 , UV088_g21919 , UV188_g21919 , UV288_g21919 , UV388_g21919 );
			float UVSwitchProp107_g21919 = _SecondaryNormalMaskUVSwitch;
			float2 UV0107_g21919 = i.uv_texcoord;
			float2 UV1107_g21919 = i.uv2_texcoord2;
			float2 UV2107_g21919 = i.uv3_texcoord3;
			float2 UV3107_g21919 = i.uv4_texcoord4;
			float2 localUVSwitch107_g21919 = UVSwitch( UVSwitchProp107_g21919 , UV0107_g21919 , UV1107_g21919 , UV2107_g21919 , UV3107_g21919 );
			float4 tex2DNode65_g21919 = SAMPLE_TEXTURE2D( _SecondaryNormalMask, sampler_trilinear_repeat, ( ( _SecondaryNormalMask_ST.xy * localUVSwitch107_g21919 ) + _SecondaryNormalMask_ST.zw ) );
			float3 lerpResult63_g21919 = lerp( _DefaultTangentVector , UnpackScaleNormal( SAMPLE_TEXTURE2D( _NormalMap, sampler_trilinear_repeat, ( ( _NormalMap_ST.xy * localUVSwitch88_g21919 ) + _NormalMap_ST.zw ) ), _NormalScale ) , tex2DNode65_g21919.a);
			float UVSwitchProp98_g21919 = _SecondaryNormalUVSwitch;
			float2 UV098_g21919 = i.uv_texcoord;
			float2 UV198_g21919 = i.uv2_texcoord2;
			float2 UV298_g21919 = i.uv3_texcoord3;
			float2 UV398_g21919 = i.uv4_texcoord4;
			float2 localUVSwitch98_g21919 = UVSwitch( UVSwitchProp98_g21919 , UV098_g21919 , UV198_g21919 , UV298_g21919 , UV398_g21919 );
			float3 lerpResult58_g21919 = lerp( _DefaultTangentVector , UnpackScaleNormal( SAMPLE_TEXTURE2D( _SecondaryNormal, sampler_trilinear_repeat, ( ( _SecondaryNormal_ST.xy * localUVSwitch98_g21919 ) + _SecondaryNormal_ST.zw ) ), _SecondaryNormalScale ) , tex2DNode65_g21919.g);
			float3 temp_output_54_0_g21919 = BlendNormals( lerpResult63_g21919 , lerpResult58_g21919 );
			float localOutlineSwitch127_g21919 = ( 0.0 );
			float3 true127_g21919 = temp_output_54_0_g21919;
			float3 false127_g21919 = -temp_output_54_0_g21919;
			float3 Out0127_g21919 = float3( 0,0,0 );
			{
			#ifdef FOROUTLINE
			Out0127_g21919 = true127_g21919; //Outline
			#else
			Out0127_g21919 = false127_g21919; //Not Outline
			#endif
			#define FOROUTLINE
			}
			float3 switchResult121_g21919 = (((i.ASEVFace>0)?(temp_output_54_0_g21919):(Out0127_g21919)));
			float3 normalizeResult52_g21919 = normalize( switchResult121_g21919 );
			float3 normals2353 = normalizeResult52_g21919;
			float3 ifLocalVar112_g22346 = 0;
			if( 1.0 > _IgnoreNormalsESv2 )
				ifLocalVar112_g22346 = normals2353;
			else if( 1.0 == _IgnoreNormalsESv2 )
				ifLocalVar112_g22346 = float3(0,0,0);
			float3 break437_g22346 = ifLocalVar112_g22346;
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float3 ase_vertexNormal = mul( unity_WorldToObject, float4( ase_worldNormal, 0 ) );
			ase_vertexNormal = normalize( ase_vertexNormal );
			float2 appendResult113_g22346 = (float2(( break437_g22346.x + ase_vertexNormal.x ) , ( break437_g22346.y + ase_vertexNormal.y )));
			float2 VertexNormal244_g22346 = appendResult113_g22346;
			float2 VertexNormal931_g22346 = VertexNormal244_g22346;
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = Unity_SafeNormalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 newWorldNormal50_g21919 = (WorldNormalVector( i , temp_output_54_0_g21919 ));
			float localOutlineSwitch128_g21919 = ( 0.0 );
			float3 true128_g21919 = newWorldNormal50_g21919;
			float3 false128_g21919 = -newWorldNormal50_g21919;
			float3 Out0128_g21919 = float3( 0,0,0 );
			{
			#ifdef FOROUTLINE
			Out0128_g21919 = true128_g21919; //Outline
			#else
			Out0128_g21919 = false128_g21919; //Not Outline
			#endif
			#define FOROUTLINE
			}
			float3 switchResult119_g21919 = (((i.ASEVFace>0)?(newWorldNormal50_g21919):(Out0128_g21919)));
			float3 normalizeResult53_g21919 = normalize( switchResult119_g21919 );
			float3 worldnormals2351 = normalizeResult53_g21919;
			float3 normalizeResult85_g22346 = normalize( ase_worldNormal );
			float3 ifLocalVar86_g22346 = 0;
			if( 1.0 > _IgnoreNormalsESv2 )
				ifLocalVar86_g22346 = worldnormals2351;
			else if( 1.0 == _IgnoreNormalsESv2 )
				ifLocalVar86_g22346 = normalizeResult85_g22346;
			float fresnelNdotV58_g22346 = dot( ifLocalVar86_g22346, ase_worldViewDir );
			float fresnelNode58_g22346 = ( 0.0 + 1.0 * pow( 1.0 - fresnelNdotV58_g22346, 5.0 ) );
			float FresnelCamera248_g22346 = fresnelNode58_g22346;
			float FresnelCamera931_g22346 = FresnelCamera248_g22346;
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float4 transform187_g22346 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float2 appendResult209_g22346 = (float2(( transform187_g22346.x + break437_g22346.x ) , ( transform187_g22346.y + break437_g22346.y )));
			float2 VertextoWorldPos251_g22346 = appendResult209_g22346;
			float2 VertextoWorldPos931_g22346 = VertextoWorldPos251_g22346;
			float mulTime62_g22346 = _Time.y * _ESVoronoiSpeed;
			float time64_g22346 = mulTime62_g22346;
			float2 voronoiSmoothId64_g22346 = 0;
			float2 coords64_g22346 = VertextoWorldPos251_g22346 * _ESVoronoiScale;
			float2 id64_g22346 = 0;
			float2 uv64_g22346 = 0;
			float voroi64_g22346 = voronoi64_g22346( coords64_g22346, time64_g22346, id64_g22346, uv64_g22346, 0, voronoiSmoothId64_g22346 );
			float Voronoi255_g22346 = voroi64_g22346;
			float Voronoi931_g22346 = Voronoi255_g22346;
			float2 appendResult482_g22346 = (float2(( break437_g22346.x + i.uv_texcoord.x ) , ( break437_g22346.y + i.uv_texcoord.y )));
			float2 VertexUV481_g22346 = appendResult482_g22346;
			float2 VertexUV931_g22346 = VertexUV481_g22346;
			{
			if (_ESRenderMethod == 0)
				Out931_g22346 = VertexNormal931_g22346;
			else if (_ESRenderMethod == 1)
				Out931_g22346 = FresnelCamera931_g22346;
			else if (_ESRenderMethod == 2)
				Out931_g22346 = VertextoWorldPos931_g22346;
			else if (_ESRenderMethod == 3)
				Out931_g22346 = Voronoi931_g22346;
			else if (_ESRenderMethod == 4)
				Out931_g22346 = VertexUV931_g22346;
			}
			float dotResult6_g22346 = dot( Out931_g22346 , _ESCoordinates );
			float ifLocalVar728_g22346 = 0;
			if( 1.0 > AudioTextureCheck808_g22346 )
				ifLocalVar728_g22346 = _ESSize;
			else if( 1.0 == AudioTextureCheck808_g22346 )
				ifLocalVar728_g22346 = 1.0;
			float ifLocalVar732_g22346 = 0;
			if( 1.0 > AudioTextureCheck808_g22346 )
				ifLocalVar732_g22346 = _ESSharpness;
			float temp_output_18_0_g22346 = saturate( ( ( pow( ( 1.0 - ( frac( ( ( mulTime5_g22346 + _ESScrollOffset ) - dotResult6_g22346 ) ) / ifLocalVar728_g22346 ) ) , ( 1.0 - ifLocalVar732_g22346 ) ) + 1E-06 ) + _ESLevelOffset ) );
			float2 break742_g22346 = tex2DNode5677.rg;
			float ESMaskR738_g22346 = break742_g22346.x;
			int Band697_g22346 = (int)i.uv_texcoord.y;
			float ifLocalVar846_g22346 = 0;
			if( 32.0 == w287_g22346 )
				ifLocalVar846_g22346 = 32.0;
			else if( 32.0 < w287_g22346 )
				ifLocalVar846_g22346 = _AudioLinkBandHistory;
			float VectorCalculation793_g22346 = temp_output_18_0_g22346;
			float temp_output_845_0_g22346 = ( ifLocalVar846_g22346 * VectorCalculation793_g22346 );
			float Delay697_g22346 = temp_output_845_0_g22346;
			float localAudioLinkLerp697_g22346 = AudioLinkLerp( Band697_g22346 , Delay697_g22346 );
			int Band702_g22346 = (int)( i.uv_texcoord.y + 1.0 );
			float Delay702_g22346 = temp_output_845_0_g22346;
			float localAudioLinkLerp702_g22346 = AudioLinkLerp( Band702_g22346 , Delay702_g22346 );
			int Band706_g22346 = (int)( i.uv_texcoord.y + 2.0 );
			float Delay706_g22346 = temp_output_845_0_g22346;
			float localAudioLinkLerp706_g22346 = AudioLinkLerp( Band706_g22346 , Delay706_g22346 );
			int Band710_g22346 = (int)( i.uv_texcoord.y + 3.0 );
			float Delay710_g22346 = temp_output_845_0_g22346;
			float localAudioLinkLerp710_g22346 = AudioLinkLerp( Band710_g22346 , Delay710_g22346 );
			float AudioLinkV1V2Bands800_g22346 = ( ( ( localAudioLinkLerp697_g22346 * _AudioBandIntensity.x ) + ( localAudioLinkLerp702_g22346 * _AudioBandIntensity.y ) + ( localAudioLinkLerp706_g22346 * _AudioBandIntensity.z ) + ( localAudioLinkLerp710_g22346 * _AudioBandIntensity.w ) ) * ESMaskR738_g22346 );
			float2 appendResult608_g22346 = (float2(_WaveformCoordinates.x , _WaveformCoordinates.y));
			float2 appendResult609_g22346 = (float2(_WaveformCoordinates.z , _WaveformCoordinates.w));
			float2 uv_TexCoord606_g22346 = i.uv_texcoord * appendResult608_g22346 + appendResult609_g22346;
			float cos593_g22346 = cos( ( _WaveformRotation * UNITY_PI ) );
			float sin593_g22346 = sin( ( _WaveformRotation * UNITY_PI ) );
			float2 rotator593_g22346 = mul( uv_TexCoord606_g22346 - float2( 0.5,0.5 ) , float2x2( cos593_g22346 , -sin593_g22346 , sin593_g22346 , cos593_g22346 )) + float2( 0.5,0.5 );
			float2 _Vec001a = float2(0,0);
			float2 temp_output_581_0_g22346 = ( 1.0 - float2( 1,1 ) );
			float2 temp_output_589_0_g22346 = ( 2.0 + float2( 0,0 ) );
			float2 _Vec111a = float2(1,1);
			float2 temp_output_587_0_g22346 = (_Vec001a + (rotator593_g22346 - ( _Vec001a + ( temp_output_581_0_g22346 / temp_output_589_0_g22346 ) )) * (_Vec111a - _Vec001a) / (( _Vec111a - ( temp_output_581_0_g22346 / temp_output_589_0_g22346 ) ) - ( _Vec001a + ( temp_output_581_0_g22346 / temp_output_589_0_g22346 ) )));
			float2 uv564_g22346 = temp_output_587_0_g22346;
			float thickness564_g22346 = _WaveformThickness;
			float2 localAudioLinkWaveformsample564_g22346 = AudioLinkWaveformsample( uv564_g22346 , thickness564_g22346 );
			float cos869_g22346 = cos( ( _WaveformRotation * UNITY_PI ) );
			float sin869_g22346 = sin( ( _WaveformRotation * UNITY_PI ) );
			float2 rotator869_g22346 = mul( uv_TexCoord606_g22346 - float2( 0.5,0.5 ) , float2x2( cos869_g22346 , -sin869_g22346 , sin869_g22346 , cos869_g22346 )) + float2( 0.5,0.5 );
			float2 break905_g22346 = rotator869_g22346;
			float2 appendResult906_g22346 = (float2(break905_g22346.x , ( 1.0 - break905_g22346.y )));
			float2 _Vector2a = float2(0,0);
			float2 temp_output_866_0_g22346 = ( 1.0 - float2( 1,1 ) );
			float2 temp_output_855_0_g22346 = ( 2.0 + float2( 0,0 ) );
			float2 _Vector0a = float2(1,1);
			float2 temp_output_871_0_g22346 = (_Vector2a + (appendResult906_g22346 - ( _Vector2a + ( temp_output_866_0_g22346 / temp_output_855_0_g22346 ) )) * (_Vector0a - _Vector2a) / (( _Vector0a - ( temp_output_866_0_g22346 / temp_output_855_0_g22346 ) ) - ( _Vector2a + ( temp_output_866_0_g22346 / temp_output_855_0_g22346 ) )));
			float2 uv881_g22346 = temp_output_871_0_g22346;
			float thickness881_g22346 = _WaveformThickness;
			float2 localAudioLinkWaveformsample881_g22346 = AudioLinkWaveformsample( uv881_g22346 , thickness881_g22346 );
			float2 break874_g22346 = temp_output_871_0_g22346;
			float A1_g22349 = floor( max( break874_g22346.x , break874_g22346.y ) );
			float B1_g22349 = floor( ( 1.0 - min( break874_g22346.x , break874_g22346.y ) ) );
			float localASEOr1_g22349 = ASEOr( A1_g22349 , B1_g22349 );
			float2 uv922_g22346 = temp_output_587_0_g22346;
			float2 localAudioLinkWaveformsampleMirror922_g22346 = AudioLinkWaveformsampleMirror( uv922_g22346 );
			float ifLocalVar924_g22346 = 0;
			if( 1.0 > _AudioLinkWaveformMirrorToggle )
				ifLocalVar924_g22346 = localAudioLinkWaveformsample564_g22346.y;
			else if( 1.0 == _AudioLinkWaveformMirrorToggle )
				ifLocalVar924_g22346 = max( localAudioLinkWaveformsample564_g22346.y , ( localAudioLinkWaveformsample881_g22346.y * ( 1.0 - localASEOr1_g22349 ) ) );
			else if( 1.0 < _AudioLinkWaveformMirrorToggle )
				ifLocalVar924_g22346 = localAudioLinkWaveformsampleMirror922_g22346.y;
			float2 break571_g22346 = temp_output_587_0_g22346;
			float A1_g22347 = floor( max( break571_g22346.x , break571_g22346.y ) );
			float B1_g22347 = floor( ( 1.0 - min( break571_g22346.x , break571_g22346.y ) ) );
			float localASEOr1_g22347 = ASEOr( A1_g22347 , B1_g22347 );
			float4 break419_g22346 = _AudioLinkColor;
			float ESMaskG743_g22346 = break742_g22346.y;
			float AudioLinkV2Waveform801_g22346 = ( ( ifLocalVar924_g22346 * ( 1.0 - localASEOr1_g22347 ) ) * break419_g22346.a * ESMaskG743_g22346 );
			float ifLocalVar600_g22346 = 0;
			if( 2.0 > _AudioLinkSwitch )
				ifLocalVar600_g22346 = AudioLinkV1V2Bands800_g22346;
			else if( 2.0 == _AudioLinkSwitch )
				ifLocalVar600_g22346 = AudioLinkV2Waveform801_g22346;
			else if( 2.0 < _AudioLinkSwitch )
				ifLocalVar600_g22346 = ( AudioLinkV1V2Bands800_g22346 + AudioLinkV2Waveform801_g22346 );
			float mulTime301_g22346 = _Time.y * _AudioHueSpeed;
			float3 hsvTorgb3_g22348 = HSVToRGB( float3(mulTime301_g22346,1.0,1.0) );
			float3 ifLocalVar416_g22346 = 0;
			if( _AudioHueSpeed > 0.0 )
				ifLocalVar416_g22346 = ( hsvTorgb3_g22348 * max( max( break419_g22346.r , break419_g22346.g ) , break419_g22346.b ) );
			else if( _AudioHueSpeed == 0.0 )
				ifLocalVar416_g22346 = (_AudioLinkColor).rgb;
			float3 ifLocalVar289_g22346 = 0;
			if( 1.0 > AudioTextureCheck808_g22346 )
				ifLocalVar289_g22346 = ( temp_output_18_0_g22346 * (Emissionsscrollcolor5894).rgb * ESMaskR738_g22346 );
			else if( 1.0 == AudioTextureCheck808_g22346 )
				ifLocalVar289_g22346 = ( ifLocalVar600_g22346 * ifLocalVar416_g22346 );
			float2 uv_TanTexture = i.uv_texcoord * _TanTexture_ST.xy + _TanTexture_ST.zw;
			float UVSwitchProp24660 = _MainTexUVSwitch;
			float2 UV024660 = i.uv_texcoord;
			float2 UV124660 = i.uv2_texcoord2;
			float2 UV224660 = i.uv3_texcoord3;
			float2 UV324660 = i.uv4_texcoord4;
			float2 localUVSwitch24660 = UVSwitch( UVSwitchProp24660 , UV024660 , UV124660 , UV224660 , UV324660 );
			float2 MainTexUVSwitch24638 = ( ( _MainTex_ST.xy * localUVSwitch24660 ) + _MainTex_ST.zw );
			float4 tex2DNode43 = SAMPLE_TEXTURE2D( _MainTex, sampler_MainTex, MainTexUVSwitch24638 );
			float4 blendOpSrc166_g352 = SAMPLE_TEXTURE2D( _TanTexture, sampler_trilinear_repeat, uv_TanTexture );
			float4 blendOpDest166_g352 = tex2DNode43;
			float4 lerpBlendMode166_g352 = lerp(blendOpDest166_g352,( blendOpSrc166_g352 * blendOpDest166_g352 ),_TanPower);
			float4 temp_output_163_0_g353 = ( saturate( lerpBlendMode166_g352 ));
			float2 uv3_ColorDiffuseSampler = i.uv3_texcoord3 * _ColorDiffuseSampler_ST.xy + _ColorDiffuseSampler_ST.zw;
			float4 tex2DNode36_g353 = SAMPLE_TEXTURE2D( _ColorDiffuseSampler, sampler_trilinear_repeat, uv3_ColorDiffuseSampler );
			float4 lerpResult191_g353 = lerp( temp_output_163_0_g353 , float4( 0,0,0,0 ) , ( 1.0 - tex2DNode36_g353.a ));
			float MaskRed100_g353 = tex2DNode36_g353.r;
			float temp_output_25_0_g363 = 0.0;
			float temp_output_25_0_g368 = ( temp_output_25_0_g363 + 0.2 );
			float temp_output_22_0_g368 = step( MaskRed100_g353 , temp_output_25_0_g368 );
			float temp_output_22_0_g363 = step( MaskRed100_g353 , temp_output_25_0_g363 );
			float temp_output_25_0_g361 = ( temp_output_25_0_g368 + 0.2 );
			float temp_output_22_0_g361 = step( MaskRed100_g353 , temp_output_25_0_g361 );
			float temp_output_25_0_g360 = ( temp_output_25_0_g361 + 0.2 );
			float temp_output_22_0_g360 = step( MaskRed100_g353 , temp_output_25_0_g360 );
			float temp_output_25_0_g364 = ( temp_output_25_0_g360 + 0.2 );
			float temp_output_22_0_g364 = step( MaskRed100_g353 , temp_output_25_0_g364 );
			float MaskGreen103_g353 = tex2DNode36_g353.g;
			float temp_output_25_0_g356 = 0.0;
			float temp_output_25_0_g354 = ( temp_output_25_0_g356 + 0.2 );
			float temp_output_22_0_g354 = step( MaskGreen103_g353 , temp_output_25_0_g354 );
			float temp_output_22_0_g356 = step( MaskGreen103_g353 , temp_output_25_0_g356 );
			float temp_output_25_0_g358 = ( temp_output_25_0_g354 + 0.2 );
			float temp_output_22_0_g358 = step( MaskGreen103_g353 , temp_output_25_0_g358 );
			float temp_output_25_0_g355 = ( temp_output_25_0_g358 + 0.2 );
			float temp_output_22_0_g355 = step( MaskGreen103_g353 , temp_output_25_0_g355 );
			float temp_output_25_0_g366 = ( temp_output_25_0_g355 + 0.2 );
			float temp_output_22_0_g366 = step( MaskRed100_g353 , temp_output_25_0_g366 );
			float MaskBlue102_g353 = tex2DNode36_g353.b;
			float temp_output_25_0_g357 = 0.0;
			float temp_output_25_0_g365 = ( temp_output_25_0_g357 + 0.2 );
			float temp_output_22_0_g365 = step( MaskBlue102_g353 , temp_output_25_0_g365 );
			float temp_output_22_0_g357 = step( MaskBlue102_g353 , temp_output_25_0_g357 );
			float temp_output_25_0_g367 = ( temp_output_25_0_g365 + 0.2 );
			float temp_output_22_0_g367 = step( MaskBlue102_g353 , temp_output_25_0_g367 );
			float temp_output_25_0_g362 = ( temp_output_25_0_g367 + 0.2 );
			float temp_output_22_0_g362 = step( MaskBlue102_g353 , temp_output_25_0_g362 );
			float temp_output_25_0_g359 = ( temp_output_25_0_g362 + 0.2 );
			float temp_output_22_0_g359 = step( MaskBlue102_g353 , temp_output_25_0_g359 );
			float4 ColorOutPut161_g353 = ( ( ( ( temp_output_22_0_g368 - temp_output_22_0_g363 ) * _RedMaskColor1 ) + ( _RedMaskColor2 * ( temp_output_22_0_g361 - temp_output_22_0_g368 ) ) + ( _RedMaskColor3 * ( temp_output_22_0_g360 - temp_output_22_0_g361 ) ) + ( _RedMaskColor4 * ( temp_output_22_0_g364 - temp_output_22_0_g360 ) ) ) + ( ( ( temp_output_22_0_g354 - temp_output_22_0_g356 ) * _GreenMaskColor1 ) + ( _GreenMaskColor2 * ( temp_output_22_0_g358 - temp_output_22_0_g354 ) ) + ( _GreenMaskColor3 * ( temp_output_22_0_g355 - temp_output_22_0_g358 ) ) + ( _GreenMaskColor4 * ( temp_output_22_0_g366 - temp_output_22_0_g355 ) ) ) + ( ( ( temp_output_22_0_g365 - temp_output_22_0_g357 ) * _BlueMaskColor1 ) + ( _BlueMaskColor2 * ( temp_output_22_0_g367 - temp_output_22_0_g365 ) ) + ( _BlueMaskColor3 * ( temp_output_22_0_g362 - temp_output_22_0_g367 ) ) + ( _BlueMaskColor4 * ( temp_output_22_0_g359 - temp_output_22_0_g362 ) ) ) );
			float4 lerpResult158_g353 = lerp( temp_output_163_0_g353 , ColorOutPut161_g353 , _CustomDiffuseLerp);
			float4 lerpResult194_g353 = lerp( temp_output_163_0_g353 , ( lerpResult191_g353 + lerpResult158_g353 ) , _CustomDiffuseLerp);
			float2 uv_TexCoord130_g369 = i.uv_texcoord * _LipScale + _LipPosition;
			float cos133_g369 = cos( ( _LipRotation * UNITY_PI ) );
			float sin133_g369 = sin( ( _LipRotation * UNITY_PI ) );
			float2 rotator133_g369 = mul( uv_TexCoord130_g369 - float2( 0.5,0.5 ) , float2x2( cos133_g369 , -sin133_g369 , sin133_g369 , cos133_g369 )) + float2( 0.5,0.5 );
			float2 _Vec001 = float2(0,0);
			float2 temp_output_119_0_g369 = ( 1.0 - float2( 1,1 ) );
			float2 temp_output_120_0_g369 = ( 2.0 + float2( 0,0 ) );
			float2 _Vec111 = float2(1,1);
			float4 tex2DNode99_g369 = SAMPLE_TEXTURE2D( _MakeupLipThick, sampler_trilinear_repeat, (_Vec001 + (rotator133_g369 - ( _Vec001 + ( temp_output_119_0_g369 / temp_output_120_0_g369 ) )) * (_Vec111 - _Vec001) / (( _Vec111 - ( temp_output_119_0_g369 / temp_output_120_0_g369 ) ) - ( _Vec001 + ( temp_output_119_0_g369 / temp_output_120_0_g369 ) ))) );
			float4 temp_cast_11 = (( _MakeupLipThickness * tex2DNode99_g369.a )).xxxx;
			float4 color112_g369 = IsGammaSpace() ? float4(0,0,0,0) : float4(0,0,0,0);
			float4 ifLocalVar115_g369 = 0;
			if( _ToggleMakeup > 0.0 )
				ifLocalVar115_g369 = temp_cast_11;
			else if( _ToggleMakeup == 0.0 )
				ifLocalVar115_g369 = color112_g369;
			float4 lerpResult102_g369 = lerp( float4( lerpResult194_g353.rgb , 0.0 ) , tex2DNode99_g369 , ifLocalVar115_g369);
			float2 uv_MakeupMask = i.uv_texcoord * _MakeupMask_ST.xy + _MakeupMask_ST.zw;
			float4 tex2DNode39_g369 = SAMPLE_TEXTURE2D( _MakeupMask, sampler_trilinear_repeat, uv_MakeupMask );
			float MakeUpGreen42_g369 = tex2DNode39_g369.g;
			float4 MakeUpGreenOut66_g369 = ( _MakeUpGreenColor * MakeUpGreen42_g369 );
			float MakeUpBlue45_g369 = tex2DNode39_g369.b;
			float4 MajkeUpBlueOut73_g369 = ( _MakeUpBlueColor * MakeUpBlue45_g369 );
			float MakeUpRed41_g369 = tex2DNode39_g369.r;
			float MakeUpLipThickness101_g369 = tex2DNode99_g369.a;
			float lerpResult104_g369 = lerp( MakeUpRed41_g369 , MakeUpLipThickness101_g369 , _MakeupLipThickness);
			float4 MajkeUpRedOut71_g369 = ( _MakeUpRedColor * lerpResult104_g369 );
			float MakeUPRedAlpha63_g369 = ( lerpResult104_g369 * _MakeUpRedPower );
			float MakeUpGreenAlpha60_g369 = ( MakeUpGreen42_g369 * _MakeUpGreenPower );
			float MakeUPBlueAlpha61_g369 = ( MakeUpBlue45_g369 * _MakeUpBluePower );
			float MakeUpAlphaMask76_g369 = ( MakeUPRedAlpha63_g369 + MakeUpGreenAlpha60_g369 + MakeUPBlueAlpha61_g369 );
			float4 temp_cast_12 = (MakeUpAlphaMask76_g369).xxxx;
			float4 ifLocalVar113_g369 = 0;
			if( _ToggleMakeup > 0.0 )
				ifLocalVar113_g369 = temp_cast_12;
			else if( _ToggleMakeup == 0.0 )
				ifLocalVar113_g369 = color112_g369;
			float4 lerpResult84_g369 = lerp( lerpResult102_g369 , ( MakeUpGreenOut66_g369 + MajkeUpBlueOut73_g369 + MajkeUpRedOut71_g369 ) , ifLocalVar113_g369);
			float2 uv_HairTexture = i.uv_texcoord * _HairTexture_ST.xy + _HairTexture_ST.zw;
			float4 tex2DNode108_g370 = SAMPLE_TEXTURE2D( _HairTexture, sampler_trilinear_repeat, uv_HairTexture );
			float4 blendOpSrc136_g370 = tex2DNode108_g370;
			float4 blendOpDest136_g370 = _HairColor;
			float2 uv_HairMask = i.uv_texcoord * _HairMask_ST.xy + _HairMask_ST.zw;
			float4 HairMask28_g370 = SAMPLE_TEXTURE2D( _HairMask, sampler_trilinear_repeat, uv_HairMask );
			float4 ifLocalVar19_g370 = 0;
			if( _ToggleHair > 0.0 )
				ifLocalVar19_g370 = HairMask28_g370;
			float4 lerpResult15_g370 = lerp( lerpResult84_g369 , (( blendOpDest136_g370 > 0.5 ) ? ( 1.0 - 2.0 * ( 1.0 - blendOpDest136_g370 ) * ( 1.0 - blendOpSrc136_g370 ) ) : ( 2.0 * blendOpDest136_g370 * blendOpSrc136_g370 ) ) , ifLocalVar19_g370);
			float3 Texture18_g21920 = lerpResult15_g370.rgb;
			float grayscale5_g21920 = (Texture18_g21920.r + Texture18_g21920.g + Texture18_g21920.b) / 3;
			float UVSwitchProp257_g21920 = _HueMaskUVSwitch;
			float2 UV0257_g21920 = i.uv_texcoord;
			float2 UV1257_g21920 = i.uv2_texcoord2;
			float2 UV2257_g21920 = i.uv3_texcoord3;
			float2 UV3257_g21920 = i.uv4_texcoord4;
			float2 localUVSwitch257_g21920 = UVSwitch( UVSwitchProp257_g21920 , UV0257_g21920 , UV1257_g21920 , UV2257_g21920 , UV3257_g21920 );
			float2 temp_output_252_0_g21920 = ( ( _HueMask_ST.xy * localUVSwitch257_g21920 ) + _HueMask_ST.zw );
			float HueMaskG53_g21920 = SAMPLE_TEXTURE2D( _HueMask, sampler_point_clamp, temp_output_252_0_g21920 ).g;
			float ifLocalVar218_g21920 = 0;
			if( 1.0 > _ToggleHueTexforSpeed )
				ifLocalVar218_g21920 = 1.0;
			else if( 1.0 == _ToggleHueTexforSpeed )
				ifLocalVar218_g21920 = HueMaskG53_g21920;
			float mulTime2_g21920 = _Time.y * ( _HueShiftSpeed * ifLocalVar218_g21920 );
			float3 hsvTorgb3_g21921 = HSVToRGB( float3(( mulTime2_g21920 + _HueShiftRandomizer ),1.0,1.0) );
			float3 ifLocalVar13_g21920 = 0;
			if( ( _HueShiftSpeed + _HueShiftRandomizer ) > 0.0 )
				ifLocalVar13_g21920 = ( grayscale5_g21920 * hsvTorgb3_g21921 );
			else if( ( _HueShiftSpeed + _HueShiftRandomizer ) == 0.0 )
				ifLocalVar13_g21920 = Texture18_g21920;
			float HueMaskR52_g21920 = SAMPLE_TEXTURE2D( _HueMask, sampler_trilinear_repeat, temp_output_252_0_g21920 ).r;
			float ifLocalVar9_g21920 = 0;
			if( 1.0 > _HueMaskinverter )
				ifLocalVar9_g21920 = HueMaskR52_g21920;
			else if( 1.0 == _HueMaskinverter )
				ifLocalVar9_g21920 = ( 1.0 - HueMaskR52_g21920 );
			float lerpResult15_g21920 = lerp( 0.0 , ifLocalVar9_g21920 , _HueShiftblend);
			float3 lerpResult16_g21920 = lerp( Texture18_g21920 , ifLocalVar13_g21920 , lerpResult15_g21920);
			float AlphaChannelMul9091 = ( tex2DNode43.a * _MainColor.a );
			clip( AlphaChannelMul9091 - _Cutout);
			float3 desaturateInitialColor626 = ( lerpResult16_g21920 * (_MainColor).rgb );
			float desaturateDot626 = dot( desaturateInitialColor626, float3( 0.299, 0.587, 0.114 ));
			float3 desaturateVar626 = lerp( desaturateInitialColor626, desaturateDot626.xxx, ( 1.0 - _Saturation ) );
			float3 MainTexSaturate2197 = desaturateVar626;
			float3 lerpResult369_g22346 = lerp( ifLocalVar289_g22346 , ( ifLocalVar289_g22346 * MainTexSaturate2197 ) , _EmissionscrollTint);
			float3 EmissionScrollV25670 = lerpResult369_g22346;
			float3 ifLocalVar5679 = 0;
			if( 1.0 == _EmissionScrollToggle )
				ifLocalVar5679 = ( ( ( (SAMPLE_TEXTURE2D( _NoiseTexture, sampler_trilinear_repeat, ( panner17_g22351 + 0.25 ) )).rgb * (SAMPLE_TEXTURE2D( _NoiseTexture, sampler_NoiseTexture, ( 1.0 - panner12_g22351 ) )).rgb ) * ( (SAMPLE_TEXTURE2D( _Emissionscroll, sampler_trilinear_repeat, panner21_g22351 )).rgb * (Emissionsscrollcolor5894).rgb ) ) * tex2DNode5677.r );
			else if( 1.0 < _EmissionScrollToggle )
				ifLocalVar5679 = EmissionScrollV25670;
			float ToggleAdvanced21493 = _COLORCOLOR;
			float3 temp_output_21539_0 = ( ifLocalVar5679 * ( saturate( _EmissionScrollToggle ) * ToggleAdvanced21493 ) );
			float3 Emissionscroll5684 = temp_output_21539_0;
			float3 WorldNormals20_g21922 = worldnormals2351;
			float dotResult442_g21922 = dot( WorldNormals20_g21922 , ase_worldViewDir );
			float NdotV19803 = dotResult442_g21922;
			float smoothstepResult10_g22166 = smoothstep( min( _ShadowRimSharpness , ( 1.0 - 1E-06 ) ) , 1.0 , ( NdotV19803 + _ShadowRimRange ));
			float lerpResult11_g22166 = lerp( 1.0 , smoothstepResult10_g22166 , _ShadowRimOpacity);
			float mulTime25_g22166 = _Time.y * _RimHueSpeed;
			float3 hsvTorgb3_g22167 = HSVToRGB( float3(mulTime25_g22166,1.0,1.0) );
			float3 temp_cast_16 = 1;
			float3 ifLocalVar27_g22166 = 0;
			if( _RimHueSpeed > 0.0 )
				ifLocalVar27_g22166 = hsvTorgb3_g22167;
			else if( _RimHueSpeed == 0.0 )
				ifLocalVar27_g22166 = temp_cast_16;
			float3 ifLocalVar8688 = 0;
			if( 1.0 == _RimSwitch )
				ifLocalVar8688 = ( ( ( 1.0 - lerpResult11_g22166 ) * (_EmissiveRimColor).rgb ) * ifLocalVar27_g22166 );
			float3 EmissiveRim8681 = ifLocalVar8688;
			float3 MainTexture145_g22378 = MainTexSaturate2197;
			float DissolveModifier30_g22441 = _DissolveObject31;
			float4 transform4_g22441 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22441 = transform4_g22441.y;
			float ifLocalVar46_g22441 = 0;
			if( _DissolveToggleDir31 > 0.0 )
				ifLocalVar46_g22441 = -Space24_g22441;
			else if( _DissolveToggleDir31 == 0.0 )
				ifLocalVar46_g22441 = Space24_g22441;
			float3 ase_objectScale = float3( length( unity_ObjectToWorld[ 0 ].xyz ), length( unity_ObjectToWorld[ 1 ].xyz ), length( unity_ObjectToWorld[ 2 ].xyz ) );
			float ObjectScale23_g22441 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22441 = exp2( _DissolveDensity );
			float temp_output_70_0_g22441 = ( ( (-1.0 + (DissolveModifier30_g22441 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22441 / ObjectScale23_g22441 ) ) * DissolveDensity67_g22441 );
			float DissolveOutputNoPattern286_g22441 = temp_output_70_0_g22441;
			float lerpResult91_g22441 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22441 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22441 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22441 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22441 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22441 );
			float DissolveModifier30_g22448 = _DissolveObject32;
			float4 transform4_g22448 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22448 = transform4_g22448.y;
			float ifLocalVar46_g22448 = 0;
			if( _DissolveToggleDir32 > 0.0 )
				ifLocalVar46_g22448 = -Space24_g22448;
			else if( _DissolveToggleDir32 == 0.0 )
				ifLocalVar46_g22448 = Space24_g22448;
			float ObjectScale23_g22448 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22448 = exp2( _DissolveDensity );
			float temp_output_70_0_g22448 = ( ( (-1.0 + (DissolveModifier30_g22448 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22448 / ObjectScale23_g22448 ) ) * DissolveDensity67_g22448 );
			float DissolveOutputNoPattern286_g22448 = temp_output_70_0_g22448;
			float lerpResult91_g22448 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22448 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22448 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22448 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22448 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22448 );
			float DissolveModifier30_g22447 = _DissolveObject33;
			float4 transform4_g22447 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22447 = transform4_g22447.y;
			float ifLocalVar46_g22447 = 0;
			if( _DissolveToggleDir33 > 0.0 )
				ifLocalVar46_g22447 = -Space24_g22447;
			else if( _DissolveToggleDir33 == 0.0 )
				ifLocalVar46_g22447 = Space24_g22447;
			float ObjectScale23_g22447 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22447 = exp2( _DissolveDensity );
			float temp_output_70_0_g22447 = ( ( (-1.0 + (DissolveModifier30_g22447 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22447 / ObjectScale23_g22447 ) ) * DissolveDensity67_g22447 );
			float DissolveOutputNoPattern286_g22447 = temp_output_70_0_g22447;
			float lerpResult91_g22447 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22447 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22447 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22447 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22447 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22447 );
			float DissolveModifier30_g22442 = _DissolveObject34;
			float4 transform4_g22442 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22442 = transform4_g22442.y;
			float ifLocalVar46_g22442 = 0;
			if( _DissolveToggleDir34 > 0.0 )
				ifLocalVar46_g22442 = -Space24_g22442;
			else if( _DissolveToggleDir34 == 0.0 )
				ifLocalVar46_g22442 = Space24_g22442;
			float ObjectScale23_g22442 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22442 = exp2( _DissolveDensity );
			float temp_output_70_0_g22442 = ( ( (-1.0 + (DissolveModifier30_g22442 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22442 / ObjectScale23_g22442 ) ) * DissolveDensity67_g22442 );
			float DissolveOutputNoPattern286_g22442 = temp_output_70_0_g22442;
			float lerpResult91_g22442 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22442 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22442 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22442 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22442 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22442 );
			float DissolveModifier30_g22430 = _DissolveObject35;
			float4 transform4_g22430 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22430 = transform4_g22430.y;
			float ifLocalVar46_g22430 = 0;
			if( _DissolveToggleDir35 > 0.0 )
				ifLocalVar46_g22430 = -Space24_g22430;
			else if( _DissolveToggleDir35 == 0.0 )
				ifLocalVar46_g22430 = Space24_g22430;
			float ObjectScale23_g22430 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22430 = exp2( _DissolveDensity );
			float temp_output_70_0_g22430 = ( ( (-1.0 + (DissolveModifier30_g22430 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22430 / ObjectScale23_g22430 ) ) * DissolveDensity67_g22430 );
			float DissolveOutputNoPattern286_g22430 = temp_output_70_0_g22430;
			float lerpResult91_g22430 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22430 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22430 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22430 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22430 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22430 );
			float DissolveModifier30_g22427 = _DissolveObject36;
			float4 transform4_g22427 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22427 = transform4_g22427.y;
			float ifLocalVar46_g22427 = 0;
			if( _DissolveToggleDir36 > 0.0 )
				ifLocalVar46_g22427 = -Space24_g22427;
			else if( _DissolveToggleDir36 == 0.0 )
				ifLocalVar46_g22427 = Space24_g22427;
			float ObjectScale23_g22427 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22427 = exp2( _DissolveDensity );
			float temp_output_70_0_g22427 = ( ( (-1.0 + (DissolveModifier30_g22427 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22427 / ObjectScale23_g22427 ) ) * DissolveDensity67_g22427 );
			float DissolveOutputNoPattern286_g22427 = temp_output_70_0_g22427;
			float lerpResult91_g22427 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22427 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22427 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22427 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22427 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22427 );
			float DissolveModifier30_g22435 = _DissolveObject37;
			float4 transform4_g22435 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22435 = transform4_g22435.y;
			float ifLocalVar46_g22435 = 0;
			if( _DissolveToggleDir37 > 0.0 )
				ifLocalVar46_g22435 = -Space24_g22435;
			else if( _DissolveToggleDir37 == 0.0 )
				ifLocalVar46_g22435 = Space24_g22435;
			float ObjectScale23_g22435 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22435 = exp2( _DissolveDensity );
			float temp_output_70_0_g22435 = ( ( (-1.0 + (DissolveModifier30_g22435 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22435 / ObjectScale23_g22435 ) ) * DissolveDensity67_g22435 );
			float DissolveOutputNoPattern286_g22435 = temp_output_70_0_g22435;
			float lerpResult91_g22435 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22435 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22435 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22435 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22435 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22435 );
			float DissolveModifier30_g22438 = _DissolveObject38;
			float4 transform4_g22438 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22438 = transform4_g22438.y;
			float ifLocalVar46_g22438 = 0;
			if( _DissolveToggleDir38 > 0.0 )
				ifLocalVar46_g22438 = -Space24_g22438;
			else if( _DissolveToggleDir38 == 0.0 )
				ifLocalVar46_g22438 = Space24_g22438;
			float ObjectScale23_g22438 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22438 = exp2( _DissolveDensity );
			float temp_output_70_0_g22438 = ( ( (-1.0 + (DissolveModifier30_g22438 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22438 / ObjectScale23_g22438 ) ) * DissolveDensity67_g22438 );
			float DissolveOutputNoPattern286_g22438 = temp_output_70_0_g22438;
			float lerpResult91_g22438 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22438 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22438 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22438 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22438 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22438 );
			float DissolveModifier30_g22456 = _DissolveObject39;
			float4 transform4_g22456 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22456 = transform4_g22456.y;
			float ifLocalVar46_g22456 = 0;
			if( _DissolveToggleDir39 > 0.0 )
				ifLocalVar46_g22456 = -Space24_g22456;
			else if( _DissolveToggleDir39 == 0.0 )
				ifLocalVar46_g22456 = Space24_g22456;
			float ObjectScale23_g22456 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22456 = exp2( _DissolveDensity );
			float temp_output_70_0_g22456 = ( ( (-1.0 + (DissolveModifier30_g22456 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22456 / ObjectScale23_g22456 ) ) * DissolveDensity67_g22456 );
			float DissolveOutputNoPattern286_g22456 = temp_output_70_0_g22456;
			float lerpResult91_g22456 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22456 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22456 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22456 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22456 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22456 );
			float DissolveModifier30_g22411 = _DissolveObject40;
			float4 transform4_g22411 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22411 = transform4_g22411.y;
			float ifLocalVar46_g22411 = 0;
			if( _DissolveToggleDir40 > 0.0 )
				ifLocalVar46_g22411 = -Space24_g22411;
			else if( _DissolveToggleDir40 == 0.0 )
				ifLocalVar46_g22411 = Space24_g22411;
			float ObjectScale23_g22411 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22411 = exp2( _DissolveDensity );
			float temp_output_70_0_g22411 = ( ( (-1.0 + (DissolveModifier30_g22411 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22411 / ObjectScale23_g22411 ) ) * DissolveDensity67_g22411 );
			float DissolveOutputNoPattern286_g22411 = temp_output_70_0_g22411;
			float lerpResult91_g22411 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22411 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22411 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22411 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22411 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22411 );
			float DissolveModifier30_g22458 = _DissolveObject21;
			float4 transform4_g22458 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22458 = transform4_g22458.y;
			float ifLocalVar46_g22458 = 0;
			if( _DissolveToggleDir21 > 0.0 )
				ifLocalVar46_g22458 = -Space24_g22458;
			else if( _DissolveToggleDir21 == 0.0 )
				ifLocalVar46_g22458 = Space24_g22458;
			float ObjectScale23_g22458 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22458 = exp2( _DissolveDensity );
			float temp_output_70_0_g22458 = ( ( (-1.0 + (DissolveModifier30_g22458 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22458 / ObjectScale23_g22458 ) ) * DissolveDensity67_g22458 );
			float DissolveOutputNoPattern286_g22458 = temp_output_70_0_g22458;
			float lerpResult91_g22458 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22458 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22458 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22458 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22458 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22458 );
			float DissolveModifier30_g22450 = _DissolveObject22;
			float4 transform4_g22450 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22450 = transform4_g22450.y;
			float ifLocalVar46_g22450 = 0;
			if( _DissolveToggleDir22 > 0.0 )
				ifLocalVar46_g22450 = -Space24_g22450;
			else if( _DissolveToggleDir22 == 0.0 )
				ifLocalVar46_g22450 = Space24_g22450;
			float ObjectScale23_g22450 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22450 = exp2( _DissolveDensity );
			float temp_output_70_0_g22450 = ( ( (-1.0 + (DissolveModifier30_g22450 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22450 / ObjectScale23_g22450 ) ) * DissolveDensity67_g22450 );
			float DissolveOutputNoPattern286_g22450 = temp_output_70_0_g22450;
			float lerpResult91_g22450 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22450 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22450 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22450 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22450 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22450 );
			float DissolveModifier30_g22451 = _DissolveObject23;
			float4 transform4_g22451 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22451 = transform4_g22451.y;
			float ifLocalVar46_g22451 = 0;
			if( _DissolveToggleDir23 > 0.0 )
				ifLocalVar46_g22451 = -Space24_g22451;
			else if( _DissolveToggleDir23 == 0.0 )
				ifLocalVar46_g22451 = Space24_g22451;
			float ObjectScale23_g22451 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22451 = exp2( _DissolveDensity );
			float temp_output_70_0_g22451 = ( ( (-1.0 + (DissolveModifier30_g22451 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22451 / ObjectScale23_g22451 ) ) * DissolveDensity67_g22451 );
			float DissolveOutputNoPattern286_g22451 = temp_output_70_0_g22451;
			float lerpResult91_g22451 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22451 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22451 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22451 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22451 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22451 );
			float DissolveModifier30_g22396 = _DissolveObject24;
			float4 transform4_g22396 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22396 = transform4_g22396.y;
			float ifLocalVar46_g22396 = 0;
			if( _DissolveToggleDir24 > 0.0 )
				ifLocalVar46_g22396 = -Space24_g22396;
			else if( _DissolveToggleDir24 == 0.0 )
				ifLocalVar46_g22396 = Space24_g22396;
			float ObjectScale23_g22396 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22396 = exp2( _DissolveDensity );
			float temp_output_70_0_g22396 = ( ( (-1.0 + (DissolveModifier30_g22396 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22396 / ObjectScale23_g22396 ) ) * DissolveDensity67_g22396 );
			float DissolveOutputNoPattern286_g22396 = temp_output_70_0_g22396;
			float lerpResult91_g22396 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22396 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22396 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22396 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22396 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22396 );
			float DissolveModifier30_g22436 = _DissolveObject25;
			float4 transform4_g22436 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22436 = transform4_g22436.y;
			float ifLocalVar46_g22436 = 0;
			if( _DissolveToggleDir25 > 0.0 )
				ifLocalVar46_g22436 = -Space24_g22436;
			else if( _DissolveToggleDir25 == 0.0 )
				ifLocalVar46_g22436 = Space24_g22436;
			float ObjectScale23_g22436 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22436 = exp2( _DissolveDensity );
			float temp_output_70_0_g22436 = ( ( (-1.0 + (DissolveModifier30_g22436 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22436 / ObjectScale23_g22436 ) ) * DissolveDensity67_g22436 );
			float DissolveOutputNoPattern286_g22436 = temp_output_70_0_g22436;
			float lerpResult91_g22436 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22436 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22436 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22436 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22436 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22436 );
			float DissolveModifier30_g22437 = _DissolveObject26;
			float4 transform4_g22437 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22437 = transform4_g22437.y;
			float ifLocalVar46_g22437 = 0;
			if( _DissolveToggleDir26 > 0.0 )
				ifLocalVar46_g22437 = -Space24_g22437;
			else if( _DissolveToggleDir26 == 0.0 )
				ifLocalVar46_g22437 = Space24_g22437;
			float ObjectScale23_g22437 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22437 = exp2( _DissolveDensity );
			float temp_output_70_0_g22437 = ( ( (-1.0 + (DissolveModifier30_g22437 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22437 / ObjectScale23_g22437 ) ) * DissolveDensity67_g22437 );
			float DissolveOutputNoPattern286_g22437 = temp_output_70_0_g22437;
			float lerpResult91_g22437 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22437 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22437 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22437 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22437 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22437 );
			float DissolveModifier30_g22459 = _DissolveObject27;
			float4 transform4_g22459 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22459 = transform4_g22459.y;
			float ifLocalVar46_g22459 = 0;
			if( _DissolveToggleDir27 > 0.0 )
				ifLocalVar46_g22459 = -Space24_g22459;
			else if( _DissolveToggleDir27 == 0.0 )
				ifLocalVar46_g22459 = Space24_g22459;
			float ObjectScale23_g22459 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22459 = exp2( _DissolveDensity );
			float temp_output_70_0_g22459 = ( ( (-1.0 + (DissolveModifier30_g22459 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22459 / ObjectScale23_g22459 ) ) * DissolveDensity67_g22459 );
			float DissolveOutputNoPattern286_g22459 = temp_output_70_0_g22459;
			float lerpResult91_g22459 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22459 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22459 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22459 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22459 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22459 );
			float DissolveModifier30_g22424 = _DissolveObject28;
			float4 transform4_g22424 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22424 = transform4_g22424.y;
			float ifLocalVar46_g22424 = 0;
			if( _DissolveToggleDir28 > 0.0 )
				ifLocalVar46_g22424 = -Space24_g22424;
			else if( _DissolveToggleDir28 == 0.0 )
				ifLocalVar46_g22424 = Space24_g22424;
			float ObjectScale23_g22424 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22424 = exp2( _DissolveDensity );
			float temp_output_70_0_g22424 = ( ( (-1.0 + (DissolveModifier30_g22424 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22424 / ObjectScale23_g22424 ) ) * DissolveDensity67_g22424 );
			float DissolveOutputNoPattern286_g22424 = temp_output_70_0_g22424;
			float lerpResult91_g22424 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22424 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22424 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22424 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22424 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22424 );
			float DissolveModifier30_g22422 = _DissolveObject29;
			float4 transform4_g22422 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22422 = transform4_g22422.y;
			float ifLocalVar46_g22422 = 0;
			if( _DissolveToggleDir29 > 0.0 )
				ifLocalVar46_g22422 = -Space24_g22422;
			else if( _DissolveToggleDir29 == 0.0 )
				ifLocalVar46_g22422 = Space24_g22422;
			float ObjectScale23_g22422 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22422 = exp2( _DissolveDensity );
			float temp_output_70_0_g22422 = ( ( (-1.0 + (DissolveModifier30_g22422 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22422 / ObjectScale23_g22422 ) ) * DissolveDensity67_g22422 );
			float DissolveOutputNoPattern286_g22422 = temp_output_70_0_g22422;
			float lerpResult91_g22422 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22422 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22422 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22422 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22422 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22422 );
			float DissolveModifier30_g22440 = _DissolveObject30;
			float4 transform4_g22440 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22440 = transform4_g22440.y;
			float ifLocalVar46_g22440 = 0;
			if( _DissolveToggleDir30 > 0.0 )
				ifLocalVar46_g22440 = -Space24_g22440;
			else if( _DissolveToggleDir30 == 0.0 )
				ifLocalVar46_g22440 = Space24_g22440;
			float ObjectScale23_g22440 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22440 = exp2( _DissolveDensity );
			float temp_output_70_0_g22440 = ( ( (-1.0 + (DissolveModifier30_g22440 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22440 / ObjectScale23_g22440 ) ) * DissolveDensity67_g22440 );
			float DissolveOutputNoPattern286_g22440 = temp_output_70_0_g22440;
			float lerpResult91_g22440 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22440 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22440 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22440 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22440 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22440 );
			float DissolveModifier30_g22454 = _DissolveObject11;
			float4 transform4_g22454 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22454 = transform4_g22454.y;
			float ifLocalVar46_g22454 = 0;
			if( _DissolveToggleDir11 > 0.0 )
				ifLocalVar46_g22454 = -Space24_g22454;
			else if( _DissolveToggleDir11 == 0.0 )
				ifLocalVar46_g22454 = Space24_g22454;
			float ObjectScale23_g22454 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22454 = exp2( _DissolveDensity );
			float temp_output_70_0_g22454 = ( ( (-1.0 + (DissolveModifier30_g22454 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22454 / ObjectScale23_g22454 ) ) * DissolveDensity67_g22454 );
			float DissolveOutputNoPattern286_g22454 = temp_output_70_0_g22454;
			float lerpResult91_g22454 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22454 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22454 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22454 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22454 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22454 );
			float DissolveModifier30_g22460 = _DissolveObject12;
			float4 transform4_g22460 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22460 = transform4_g22460.y;
			float ifLocalVar46_g22460 = 0;
			if( _DissolveToggleDir12 > 0.0 )
				ifLocalVar46_g22460 = -Space24_g22460;
			else if( _DissolveToggleDir12 == 0.0 )
				ifLocalVar46_g22460 = Space24_g22460;
			float ObjectScale23_g22460 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22460 = exp2( _DissolveDensity );
			float temp_output_70_0_g22460 = ( ( (-1.0 + (DissolveModifier30_g22460 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22460 / ObjectScale23_g22460 ) ) * DissolveDensity67_g22460 );
			float DissolveOutputNoPattern286_g22460 = temp_output_70_0_g22460;
			float lerpResult91_g22460 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22460 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22460 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22460 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22460 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22460 );
			float DissolveModifier30_g22428 = _DissolveObject13;
			float4 transform4_g22428 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22428 = transform4_g22428.y;
			float ifLocalVar46_g22428 = 0;
			if( _DissolveToggleDir13 > 0.0 )
				ifLocalVar46_g22428 = -Space24_g22428;
			else if( _DissolveToggleDir13 == 0.0 )
				ifLocalVar46_g22428 = Space24_g22428;
			float ObjectScale23_g22428 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22428 = exp2( _DissolveDensity );
			float temp_output_70_0_g22428 = ( ( (-1.0 + (DissolveModifier30_g22428 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22428 / ObjectScale23_g22428 ) ) * DissolveDensity67_g22428 );
			float DissolveOutputNoPattern286_g22428 = temp_output_70_0_g22428;
			float lerpResult91_g22428 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22428 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22428 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22428 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22428 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22428 );
			float DissolveModifier30_g22431 = _DissolveObject14;
			float4 transform4_g22431 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22431 = transform4_g22431.y;
			float ifLocalVar46_g22431 = 0;
			if( _DissolveToggleDir14 > 0.0 )
				ifLocalVar46_g22431 = -Space24_g22431;
			else if( _DissolveToggleDir14 == 0.0 )
				ifLocalVar46_g22431 = Space24_g22431;
			float ObjectScale23_g22431 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22431 = exp2( _DissolveDensity );
			float temp_output_70_0_g22431 = ( ( (-1.0 + (DissolveModifier30_g22431 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22431 / ObjectScale23_g22431 ) ) * DissolveDensity67_g22431 );
			float DissolveOutputNoPattern286_g22431 = temp_output_70_0_g22431;
			float lerpResult91_g22431 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22431 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22431 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22431 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22431 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22431 );
			float DissolveModifier30_g22457 = _DissolveObject15;
			float4 transform4_g22457 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22457 = transform4_g22457.y;
			float ifLocalVar46_g22457 = 0;
			if( _DissolveToggleDir15 > 0.0 )
				ifLocalVar46_g22457 = -Space24_g22457;
			else if( _DissolveToggleDir15 == 0.0 )
				ifLocalVar46_g22457 = Space24_g22457;
			float ObjectScale23_g22457 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22457 = exp2( _DissolveDensity );
			float temp_output_70_0_g22457 = ( ( (-1.0 + (DissolveModifier30_g22457 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22457 / ObjectScale23_g22457 ) ) * DissolveDensity67_g22457 );
			float DissolveOutputNoPattern286_g22457 = temp_output_70_0_g22457;
			float lerpResult91_g22457 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22457 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22457 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22457 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22457 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22457 );
			float DissolveModifier30_g22455 = _DissolveObject16;
			float4 transform4_g22455 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22455 = transform4_g22455.y;
			float ifLocalVar46_g22455 = 0;
			if( _DissolveToggleDir16 > 0.0 )
				ifLocalVar46_g22455 = -Space24_g22455;
			else if( _DissolveToggleDir16 == 0.0 )
				ifLocalVar46_g22455 = Space24_g22455;
			float ObjectScale23_g22455 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22455 = exp2( _DissolveDensity );
			float temp_output_70_0_g22455 = ( ( (-1.0 + (DissolveModifier30_g22455 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22455 / ObjectScale23_g22455 ) ) * DissolveDensity67_g22455 );
			float DissolveOutputNoPattern286_g22455 = temp_output_70_0_g22455;
			float lerpResult91_g22455 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22455 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22455 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22455 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22455 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22455 );
			float DissolveModifier30_g22439 = _DissolveObject17;
			float4 transform4_g22439 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22439 = transform4_g22439.y;
			float ifLocalVar46_g22439 = 0;
			if( _DissolveToggleDir17 > 0.0 )
				ifLocalVar46_g22439 = -Space24_g22439;
			else if( _DissolveToggleDir17 == 0.0 )
				ifLocalVar46_g22439 = Space24_g22439;
			float ObjectScale23_g22439 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22439 = exp2( _DissolveDensity );
			float temp_output_70_0_g22439 = ( ( (-1.0 + (DissolveModifier30_g22439 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22439 / ObjectScale23_g22439 ) ) * DissolveDensity67_g22439 );
			float DissolveOutputNoPattern286_g22439 = temp_output_70_0_g22439;
			float lerpResult91_g22439 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22439 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22439 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22439 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22439 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22439 );
			float DissolveModifier30_g22446 = _DissolveObject18;
			float4 transform4_g22446 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22446 = transform4_g22446.y;
			float ifLocalVar46_g22446 = 0;
			if( _DissolveToggleDir18 > 0.0 )
				ifLocalVar46_g22446 = -Space24_g22446;
			else if( _DissolveToggleDir18 == 0.0 )
				ifLocalVar46_g22446 = Space24_g22446;
			float ObjectScale23_g22446 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22446 = exp2( _DissolveDensity );
			float temp_output_70_0_g22446 = ( ( (-1.0 + (DissolveModifier30_g22446 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22446 / ObjectScale23_g22446 ) ) * DissolveDensity67_g22446 );
			float DissolveOutputNoPattern286_g22446 = temp_output_70_0_g22446;
			float lerpResult91_g22446 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22446 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22446 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22446 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22446 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22446 );
			float DissolveModifier30_g22443 = _DissolveObject19;
			float4 transform4_g22443 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22443 = transform4_g22443.y;
			float ifLocalVar46_g22443 = 0;
			if( _DissolveToggleDir19 > 0.0 )
				ifLocalVar46_g22443 = -Space24_g22443;
			else if( _DissolveToggleDir19 == 0.0 )
				ifLocalVar46_g22443 = Space24_g22443;
			float ObjectScale23_g22443 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22443 = exp2( _DissolveDensity );
			float temp_output_70_0_g22443 = ( ( (-1.0 + (DissolveModifier30_g22443 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22443 / ObjectScale23_g22443 ) ) * DissolveDensity67_g22443 );
			float DissolveOutputNoPattern286_g22443 = temp_output_70_0_g22443;
			float lerpResult91_g22443 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22443 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22443 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22443 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22443 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22443 );
			float DissolveModifier30_g22445 = _DissolveObject20;
			float4 transform4_g22445 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22445 = transform4_g22445.y;
			float ifLocalVar46_g22445 = 0;
			if( _DissolveToggleDir20 > 0.0 )
				ifLocalVar46_g22445 = -Space24_g22445;
			else if( _DissolveToggleDir20 == 0.0 )
				ifLocalVar46_g22445 = Space24_g22445;
			float ObjectScale23_g22445 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22445 = exp2( _DissolveDensity );
			float temp_output_70_0_g22445 = ( ( (-1.0 + (DissolveModifier30_g22445 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22445 / ObjectScale23_g22445 ) ) * DissolveDensity67_g22445 );
			float DissolveOutputNoPattern286_g22445 = temp_output_70_0_g22445;
			float lerpResult91_g22445 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22445 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22445 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22445 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22445 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22445 );
			float DissolveModifier30_g22390 = _DissolveObject1;
			float4 transform4_g22390 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22390 = transform4_g22390.y;
			float ifLocalVar46_g22390 = 0;
			if( _DissolveToggleDir1 > 0.0 )
				ifLocalVar46_g22390 = -Space24_g22390;
			else if( _DissolveToggleDir1 == 0.0 )
				ifLocalVar46_g22390 = Space24_g22390;
			float ObjectScale23_g22390 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22390 = exp2( _DissolveDensity );
			float temp_output_70_0_g22390 = ( ( (-1.0 + (DissolveModifier30_g22390 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22390 / ObjectScale23_g22390 ) ) * DissolveDensity67_g22390 );
			float DissolveOutputNoPattern286_g22390 = temp_output_70_0_g22390;
			float lerpResult91_g22390 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22390 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22390 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22390 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22390 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22390 );
			float DissolveModifier30_g22429 = _DissolveObject2;
			float4 transform4_g22429 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22429 = transform4_g22429.y;
			float ifLocalVar46_g22429 = 0;
			if( _DissolveToggleDir2 > 0.0 )
				ifLocalVar46_g22429 = -Space24_g22429;
			else if( _DissolveToggleDir2 == 0.0 )
				ifLocalVar46_g22429 = Space24_g22429;
			float ObjectScale23_g22429 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22429 = exp2( _DissolveDensity );
			float temp_output_70_0_g22429 = ( ( (-1.0 + (DissolveModifier30_g22429 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22429 / ObjectScale23_g22429 ) ) * DissolveDensity67_g22429 );
			float DissolveOutputNoPattern286_g22429 = temp_output_70_0_g22429;
			float lerpResult91_g22429 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22429 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22429 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22429 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22429 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22429 );
			float DissolveModifier30_g22452 = _DissolveObject3;
			float4 transform4_g22452 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22452 = transform4_g22452.y;
			float ifLocalVar46_g22452 = 0;
			if( _DissolveToggleDir3 > 0.0 )
				ifLocalVar46_g22452 = -Space24_g22452;
			else if( _DissolveToggleDir3 == 0.0 )
				ifLocalVar46_g22452 = Space24_g22452;
			float ObjectScale23_g22452 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22452 = exp2( _DissolveDensity );
			float temp_output_70_0_g22452 = ( ( (-1.0 + (DissolveModifier30_g22452 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22452 / ObjectScale23_g22452 ) ) * DissolveDensity67_g22452 );
			float DissolveOutputNoPattern286_g22452 = temp_output_70_0_g22452;
			float lerpResult91_g22452 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22452 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22452 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22452 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22452 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22452 );
			float DissolveModifier30_g22432 = _DissolveObject4;
			float4 transform4_g22432 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22432 = transform4_g22432.y;
			float ifLocalVar46_g22432 = 0;
			if( _DissolveToggleDir4 > 0.0 )
				ifLocalVar46_g22432 = -Space24_g22432;
			else if( _DissolveToggleDir4 == 0.0 )
				ifLocalVar46_g22432 = Space24_g22432;
			float ObjectScale23_g22432 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22432 = exp2( _DissolveDensity );
			float temp_output_70_0_g22432 = ( ( (-1.0 + (DissolveModifier30_g22432 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22432 / ObjectScale23_g22432 ) ) * DissolveDensity67_g22432 );
			float DissolveOutputNoPattern286_g22432 = temp_output_70_0_g22432;
			float lerpResult91_g22432 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22432 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22432 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22432 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22432 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22432 );
			float DissolveModifier30_g22449 = _DissolveObject5;
			float4 transform4_g22449 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22449 = transform4_g22449.y;
			float ifLocalVar46_g22449 = 0;
			if( _DissolveToggleDir5 > 0.0 )
				ifLocalVar46_g22449 = -Space24_g22449;
			else if( _DissolveToggleDir5 == 0.0 )
				ifLocalVar46_g22449 = Space24_g22449;
			float ObjectScale23_g22449 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22449 = exp2( _DissolveDensity );
			float temp_output_70_0_g22449 = ( ( (-1.0 + (DissolveModifier30_g22449 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22449 / ObjectScale23_g22449 ) ) * DissolveDensity67_g22449 );
			float DissolveOutputNoPattern286_g22449 = temp_output_70_0_g22449;
			float lerpResult91_g22449 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22449 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22449 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22449 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22449 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22449 );
			float DissolveModifier30_g22434 = _DissolveObject6;
			float4 transform4_g22434 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22434 = transform4_g22434.y;
			float ifLocalVar46_g22434 = 0;
			if( _DissolveToggleDir6 > 0.0 )
				ifLocalVar46_g22434 = -Space24_g22434;
			else if( _DissolveToggleDir6 == 0.0 )
				ifLocalVar46_g22434 = Space24_g22434;
			float ObjectScale23_g22434 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22434 = exp2( _DissolveDensity );
			float temp_output_70_0_g22434 = ( ( (-1.0 + (DissolveModifier30_g22434 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22434 / ObjectScale23_g22434 ) ) * DissolveDensity67_g22434 );
			float DissolveOutputNoPattern286_g22434 = temp_output_70_0_g22434;
			float lerpResult91_g22434 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22434 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22434 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22434 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22434 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22434 );
			float DissolveModifier30_g22453 = _DissolveObject7;
			float4 transform4_g22453 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22453 = transform4_g22453.y;
			float ifLocalVar46_g22453 = 0;
			if( _DissolveToggleDir7 > 0.0 )
				ifLocalVar46_g22453 = -Space24_g22453;
			else if( _DissolveToggleDir7 == 0.0 )
				ifLocalVar46_g22453 = Space24_g22453;
			float ObjectScale23_g22453 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22453 = exp2( _DissolveDensity );
			float temp_output_70_0_g22453 = ( ( (-1.0 + (DissolveModifier30_g22453 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22453 / ObjectScale23_g22453 ) ) * DissolveDensity67_g22453 );
			float DissolveOutputNoPattern286_g22453 = temp_output_70_0_g22453;
			float lerpResult91_g22453 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22453 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22453 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22453 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22453 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22453 );
			float DissolveModifier30_g22444 = _DissolveObject8;
			float4 transform4_g22444 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22444 = transform4_g22444.y;
			float ifLocalVar46_g22444 = 0;
			if( _DissolveToggleDir8 > 0.0 )
				ifLocalVar46_g22444 = -Space24_g22444;
			else if( _DissolveToggleDir8 == 0.0 )
				ifLocalVar46_g22444 = Space24_g22444;
			float ObjectScale23_g22444 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22444 = exp2( _DissolveDensity );
			float temp_output_70_0_g22444 = ( ( (-1.0 + (DissolveModifier30_g22444 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22444 / ObjectScale23_g22444 ) ) * DissolveDensity67_g22444 );
			float DissolveOutputNoPattern286_g22444 = temp_output_70_0_g22444;
			float lerpResult91_g22444 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22444 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22444 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22444 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22444 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22444 );
			float DissolveModifier30_g22426 = _DissolveObject9;
			float4 transform4_g22426 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22426 = transform4_g22426.y;
			float ifLocalVar46_g22426 = 0;
			if( _DissolveToggleDir9 > 0.0 )
				ifLocalVar46_g22426 = -Space24_g22426;
			else if( _DissolveToggleDir9 == 0.0 )
				ifLocalVar46_g22426 = Space24_g22426;
			float ObjectScale23_g22426 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22426 = exp2( _DissolveDensity );
			float temp_output_70_0_g22426 = ( ( (-1.0 + (DissolveModifier30_g22426 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22426 / ObjectScale23_g22426 ) ) * DissolveDensity67_g22426 );
			float DissolveOutputNoPattern286_g22426 = temp_output_70_0_g22426;
			float lerpResult91_g22426 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22426 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22426 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22426 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22426 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22426 );
			float DissolveModifier30_g22425 = _DissolveObject10;
			float4 transform4_g22425 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22425 = transform4_g22425.y;
			float ifLocalVar46_g22425 = 0;
			if( _DissolveToggleDir10 > 0.0 )
				ifLocalVar46_g22425 = -Space24_g22425;
			else if( _DissolveToggleDir10 == 0.0 )
				ifLocalVar46_g22425 = Space24_g22425;
			float ObjectScale23_g22425 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22425 = exp2( _DissolveDensity );
			float temp_output_70_0_g22425 = ( ( (-1.0 + (DissolveModifier30_g22425 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22425 / ObjectScale23_g22425 ) ) * DissolveDensity67_g22425 );
			float DissolveOutputNoPattern286_g22425 = temp_output_70_0_g22425;
			float lerpResult91_g22425 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22425 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22425 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22425 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22425 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22425 );
			float DissolveModifier30_g22433 = _GlobalDissolve;
			float4 transform4_g22433 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Space24_g22433 = transform4_g22433.y;
			float ifLocalVar46_g22433 = 0;
			if( _GlobalDissolveDir > 0.0 )
				ifLocalVar46_g22433 = -Space24_g22433;
			else if( _GlobalDissolveDir == 0.0 )
				ifLocalVar46_g22433 = Space24_g22433;
			float ObjectScale23_g22433 = max( ase_objectScale.y , 1.0 );
			float DissolveDensity67_g22433 = exp2( _DissolveDensity );
			float temp_output_70_0_g22433 = ( ( (-1.0 + (DissolveModifier30_g22433 - _DissolveRemapMin) * (1.0 - -1.0) / (_DissolveRemapMax - _DissolveRemapMin)) + ( ifLocalVar46_g22433 / ObjectScale23_g22433 ) ) * DissolveDensity67_g22433 );
			float DissolveOutputNoPattern286_g22433 = temp_output_70_0_g22433;
			float lerpResult91_g22433 = lerp( 0.0 , saturate( DissolveOutputNoPattern286_g22433 ) , saturate( ( 1.0 - ( DissolveOutputNoPattern286_g22433 - _EmissiveScale ) ) ));
			float3 ifLocalVar258_g22433 = 0;
			if( _ToggleDissolveEmission == 1.0 )
				ifLocalVar258_g22433 = ( saturate( ( MainTexture145_g22378 + 0.5 ) ) * (_EmissiveDissolveColor).rgb * lerpResult91_g22433 );
			float3 GlobalEmissiveDissolve939_g22378 = ifLocalVar258_g22433;
			float3 EmissiveDissolve21891 = ( ( ifLocalVar258_g22441 + ifLocalVar258_g22448 + ifLocalVar258_g22447 + ifLocalVar258_g22442 + ifLocalVar258_g22430 + ifLocalVar258_g22427 + ifLocalVar258_g22435 + ifLocalVar258_g22438 + ifLocalVar258_g22456 + ifLocalVar258_g22411 ) + ( ifLocalVar258_g22458 + ifLocalVar258_g22450 + ifLocalVar258_g22451 + ifLocalVar258_g22396 + ifLocalVar258_g22436 + ifLocalVar258_g22437 + ifLocalVar258_g22459 + ifLocalVar258_g22424 + ifLocalVar258_g22422 + ifLocalVar258_g22440 ) + ( ifLocalVar258_g22454 + ifLocalVar258_g22460 + ifLocalVar258_g22428 + ifLocalVar258_g22431 + ifLocalVar258_g22457 + ifLocalVar258_g22455 + ifLocalVar258_g22439 + ifLocalVar258_g22446 + ifLocalVar258_g22443 + ifLocalVar258_g22445 ) + ( ifLocalVar258_g22390 + ifLocalVar258_g22429 + ifLocalVar258_g22452 + ifLocalVar258_g22432 + ifLocalVar258_g22449 + ifLocalVar258_g22434 + ifLocalVar258_g22453 + ifLocalVar258_g22444 + ifLocalVar258_g22426 + ifLocalVar258_g22425 ) + GlobalEmissiveDissolve939_g22378 );
			o.Emission = ( Emissionscroll5684 + EmissiveRim8681 + EmissiveDissolve21891 );
		}

		ENDCG
		CGPROGRAM
		#pragma only_renderers d3d11 glcore gles3 vulkan nomrt 
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows exclude_path:deferred noambient novertexlights nolightmap  nodynlightmap nodirlightmap nometa vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			AlphaToMask Off
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 5.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float4 customPack1 : TEXCOORD1;
				float4 customPack2 : TEXCOORD2;
				float3 customPack3 : TEXCOORD3;
				float3 customPack4 : TEXCOORD4;
				float4 tSpace0 : TEXCOORD5;
				float4 tSpace1 : TEXCOORD6;
				float4 tSpace2 : TEXCOORD7;
				half4 color : COLOR0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.customPack1.zw = customInputData.uv2_texcoord2;
				o.customPack1.zw = v.texcoord1;
				o.customPack2.xy = customInputData.uv3_texcoord3;
				o.customPack2.xy = v.texcoord2;
				o.customPack2.zw = customInputData.uv4_texcoord4;
				o.customPack2.zw = v.texcoord3;
				o.customPack3.xyz = customInputData.vertexToFrag2250_g22168;
				o.customPack4.xyz = customInputData.vertexToFrag2251_g22168;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				o.color = v.color;
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				surfIN.uv2_texcoord2 = IN.customPack1.zw;
				surfIN.uv3_texcoord3 = IN.customPack2.xy;
				surfIN.uv4_texcoord4 = IN.customPack2.zw;
				surfIN.vertexToFrag2250_g22168 = IN.customPack3.xyz;
				surfIN.vertexToFrag2251_g22168 = IN.customPack4.xyz;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.worldRefl = -worldViewDir;
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				surfIN.vertexColor = IN.color;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				UnityGI gi;
				UNITY_INITIALIZE_OUTPUT( UnityGI, gi );
				o.Alpha = LightingStandardCustomLighting( o, worldViewDir, gi ).a;
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Standard"
	CustomEditor "MorisMaterialInspector"
}
/*ASEBEGIN
Version=18935
-1846;105;1858;858;2801.707;1775.041;2.500608;True;False
Node;AmplifyShaderEditor.CommentaryNode;24651;-3552,-1008;Inherit;False;1592.821;638.8351;;25;24640;24639;24638;24632;24623;24564;24667;24666;24665;24664;24663;24662;24661;24660;24659;24658;24657;24656;24655;24654;24653;24652;24674;24675;24676;UV Switch;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;24564;-3488,-960;Inherit;False;Property;_MainTexUVSwitch;Main Tex UV Switch;1327;1;[Enum];Create;True;0;4;UV0;0;UV1;1;UV2;2;UV3;3;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;24665;-3216,-784;Inherit;False;1;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexCoordVertexDataNode;24667;-3216,-896;Inherit;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexCoordVertexDataNode;24657;-3216,-672;Inherit;False;2;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexCoordVertexDataNode;24663;-3216,-560;Inherit;False;3;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CustomExpressionNode;24660;-2992,-960;Inherit;False;if (UVSwitchProp == 0)$	return UV0@$else if (UVSwitchProp == 1)$	return UV1@$else if (UVSwitchProp == 2)$	return UV2@$else$	return UV3@;2;Create;5;True;UVSwitchProp;FLOAT;0;In;;Inherit;False;True;UV0;FLOAT2;0,0;In;;Inherit;False;True;UV1;FLOAT2;0,0;In;;Inherit;False;True;UV2;FLOAT2;0,0;In;;Inherit;False;True;UV3;FLOAT2;0,0;In;;Inherit;False;UVSwitch;False;False;0;;False;5;0;FLOAT;0;False;1;FLOAT2;0,0;False;2;FLOAT2;0,0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureTransformNode;24666;-2768,-960;Inherit;False;43;False;1;0;SAMPLER2D;;False;2;FLOAT2;0;FLOAT2;1
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24659;-2512,-960;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;24652;-2384,-960;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;24638;-2256,-960;Inherit;False;MainTexUVSwitch;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.CommentaryNode;972;-2106,-128;Inherit;False;1803.727;433.9317;;13;24641;626;9091;9090;2197;297;627;411;21780;296;43;25140;25141;Main Texture;0,0.1310344,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;24641;-2080,-48;Inherit;False;24638;MainTexUVSwitch;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;43;-1872,-80;Inherit;True;Property;_MainTex;Main Tex;1704;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;296;-1872,112;Inherit;False;Property;_MainColor;Main Color;1703;0;Create;True;0;0;0;False;0;False;1,1,1,1;0,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FunctionNode;25179;-1623.64,-238.7991;Inherit;False;Rithrin Fluff;1152;;1;4b9c8babc7b61594aad7398dcf6244d6;0;1;3;FLOAT4;0,0,0,0;False;2;COLOR;0;COLOR;65
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9090;-1408,192;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;24742;-1568,-80;Inherit;False;Hue Shift;1547;;21920;ba913d8caaf7acd4a97eca4685e47654;0;1;17;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;13946;-1918,512;Inherit;False;691.5416;280.4319;;2;2353;2351;Normals;0.5019608,0.5019608,1,1;0;0
Node;AmplifyShaderEditor.FunctionNode;25026;-1856,608;Inherit;False;Normals and World Normals;1562;;21919;a9b4a0b5166a58041907936d7f327add;0;0;2;FLOAT3;0;FLOAT3;14
Node;AmplifyShaderEditor.RegisterLocalVarNode;9091;-1280,192;Inherit;False;AlphaChannelMul;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;25140;-1568,0;Inherit;False;Property;_Cutout;Cutout;1521;0;Create;True;0;0;0;False;0;False;0.5;0.5;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;2351;-1456,576;Inherit;False;worldnormals;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;19836;-1920,-1536;Inherit;False;2055.645;1173.099;;36;21172;21174;21093;21091;21085;21173;21086;21095;21099;21087;21089;21171;21101;21097;21090;21092;21104;21103;21102;21094;21100;21098;21105;21096;21183;21088;19806;19802;19902;19804;19817;21432;19801;19820;19803;19798;Utilities;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;411;-1264,48;Inherit;False;Property;_Saturation;Saturation;1702;0;Create;True;0;0;0;False;0;False;1;1;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.ClipNode;25141;-1264,-80;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ComponentMaskNode;21780;-1680,112;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.OneMinusNode;627;-992,48;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;297;-1072,-80;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;19798;-1872,-1488;Inherit;False;2351;worldnormals;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DesaturateOpNode;626;-800,-80;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.FunctionNode;25050;-1600,-1488;Inherit;False;Utilities;-1;;21922;9d926cf50a172564e8b49022db15d05a;0;1;19;FLOAT3;0,0,0;False;40;FLOAT;0;FLOAT;14;FLOAT;17;FLOAT;15;FLOAT;16;FLOAT;60;FLOAT;62;FLOAT;63;FLOAT;64;FLOAT;65;FLOAT;13;FLOAT;446;FLOAT;18;FLOAT;114;FLOAT4;372;FLOAT4;373;FLOAT4;374;FLOAT4;375;FLOAT4;397;FLOAT4;376;FLOAT3;377;FLOAT3;378;FLOAT3;379;FLOAT3;380;FLOAT3;382;FLOAT3;384;FLOAT3;386;FLOAT3;388;FLOAT;381;FLOAT;383;FLOAT;385;FLOAT;387;FLOAT;389;FLOAT;390;FLOAT;391;FLOAT;392;FLOAT3;393;FLOAT3;394;FLOAT3;395;FLOAT3;396
Node;AmplifyShaderEditor.CommentaryNode;14567;0,-256;Inherit;False;1247.051;1106.631;;19;16840;25037;24300;25046;21855;24075;9087;2205;21212;14546;14547;14557;14556;19839;19840;21201;19117;14551;21199;Lighting;1,0.9782155,0.759434,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;2197;-544,-80;Inherit;False;MainTexSaturate;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21183;-672,-1168;Inherit;False;VertexLightAtten;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;19801;-1104,-1488;Inherit;False;NdotL;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;19820;-1104,-1248;Inherit;False;NdotAmbientL;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;2353;-1456,688;Inherit;False;normals;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21088;-672,-1248;Inherit;False;VertexLightNdLNONMAX;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;19840;32,32;Inherit;False;19820;NdotAmbientL;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21201;32,112;Inherit;False;21183;VertexLightAtten;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;19117;32,-208;Inherit;False;2353;normals;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;19839;32,-48;Inherit;False;19801;NdotL;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21199;32,192;Inherit;False;21088;VertexLightNdLNONMAX;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;14551;32,-128;Inherit;False;2197;MainTexSaturate;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;2895;-1920,3584;Inherit;False;1042.624;425.8316;;8;19832;8681;8688;2907;8684;21078;8678;23961;Shadow Rim;0,0,0,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;19803;-1104,-1008;Inherit;False;NdotV;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;25092;352,-32;Inherit;False;Lighting;1476;;22165;f969bc2898d94ca4199b47c9b757495b;0;6;1258;FLOAT3;0,0,1;False;150;FLOAT3;0,0,0;False;1264;FLOAT;0;False;1263;FLOAT;0;False;1306;FLOAT4;0,0,0,0;False;1305;FLOAT4;0,0,0,0;False;12;FLOAT3;0;FLOAT3;198;FLOAT3;1309;FLOAT3;2205;FLOAT;210;FLOAT;1989;FLOAT;211;FLOAT;1361;FLOAT;2199;FLOAT;638;FLOAT;2155;FLOAT4;2137
Node;AmplifyShaderEditor.RegisterLocalVarNode;14557;960,272;Inherit;False;AmbientRamp;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;19806;-1104,-848;Inherit;False;GrayscaledLight;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;14556;960,80;Inherit;False;DirectLightRamp;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21432;-1104,-928;Inherit;False;NdotVCorr;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;19832;-1904,3712;Inherit;False;19803;NdotV;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;21392;8624,624;Inherit;False;891.5;452.2001;Kaj Optimizer;4;21368;21544;22130;25088;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;24623;-3488,-736;Inherit;False;Property;_EmissionScrollMaskUVSwitch;Emission Scroll Mask UV Switch;1325;1;[Enum];Create;True;0;4;UV0;0;UV1;1;UV2;2;UV3;3;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;19802;-1104,-1408;Inherit;False;NdotH;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;19804;-1104,-1328;Inherit;False;LdotH;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;19817;-1104,-1168;Inherit;False;NdotAmbientH;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;19902;-1104,-1088;Inherit;False;AmbientLdotAmbientH;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;1083;-1920,2304;Inherit;False;1839.251;1088.966;;33;8524;2223;21159;21148;21162;21161;21160;21164;21156;21158;21165;21163;21155;21154;21157;21147;24166;21572;20971;19845;5950;20970;19819;19854;20969;19818;2479;19815;19816;24302;24307;24841;24953;Specular Highlights;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;24300;960,752;Inherit;False;NdLVertexLightsShadows;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;20970;-1632,2448;Inherit;False;14556;DirectLightRamp;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;24302;-1904,2448;Inherit;False;24300;NdLVertexLightsShadows;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;19816;-1424,2688;Inherit;False;19817;NdotAmbientH;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;19845;-1632,2608;Inherit;False;21432;NdotVCorr;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;21078;-1440,3824;Inherit;False;Constant;_Float22;Float 22;192;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;8678;-1456,3664;Inherit;False;Property;_RimSwitch;Rim Switch;1720;1;[Enum];Create;True;0;2;Shadow;0;Emissive;1;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;19854;-1440,2608;Inherit;False;19804;LdotH;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21572;-1376,2768;Inherit;False;19806;GrayscaledLight;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;20969;-1632,2768;Inherit;False;19902;AmbientLdotAmbientH;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;19819;-1632,2688;Inherit;False;19820;NdotAmbientL;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureTransformNode;24664;-2768,-736;Inherit;False;5677;False;1;0;SAMPLER2D;;False;2;FLOAT2;0;FLOAT2;1
Node;AmplifyShaderEditor.GetLocalVarNode;5950;-1632,2368;Inherit;False;2351;worldnormals;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;19818;-1632,2528;Inherit;False;19801;NdotL;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21148;-1904,2528;Inherit;False;21088;VertexLightNdLNONMAX;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;2479;-1424,2368;Inherit;False;2197;MainTexSaturate;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.FunctionNode;23961;-1712,3712;Inherit;False;Shadow Emissive Rim;1662;;22166;4fa91309dca2f3c428e54f87b3f4adf3;0;1;34;FLOAT;0;False;2;FLOAT;0;FLOAT3;22
Node;AmplifyShaderEditor.CustomExpressionNode;24658;-2992,-736;Inherit;False;if (UVSwitchProp == 0)$	return UV0@$else if (UVSwitchProp == 1)$	return UV1@$else if (UVSwitchProp == 2)$	return UV2@$else$	return UV3@;2;Create;5;True;UVSwitchProp;FLOAT;0;In;;Inherit;False;True;UV0;FLOAT2;0,0;In;;Inherit;False;True;UV1;FLOAT2;0,0;In;;Inherit;False;True;UV2;FLOAT2;0,0;In;;Inherit;False;True;UV3;FLOAT2;0,0;In;;Inherit;False;UVSwitch;False;False;0;;False;5;0;FLOAT;0;False;1;FLOAT2;0,0;False;2;FLOAT2;0,0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;20971;-1408,2448;Inherit;False;14557;AmbientRamp;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;24632;-3488,-544;Inherit;False;Property;_EmissionUVSwitch;Emission UV Switch;1326;1;[Enum];Create;True;0;4;UV0;0;UV1;1;UV2;2;UV3;3;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;19815;-1456,2528;Inherit;False;19802;NdotH;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;21544;8672,784;Inherit;False;784.9004;263.6;If Optimizer is toggled, make sure nothing is accidentally activated when its set to Off;6;21525;21524;21493;21482;23968;23969;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;24953;-1824,2368;Inherit;False;2353;normals;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;21482;8704,944;Inherit;False;Property;_COLORCOLOR;Toggle Advanced;1711;0;Create;False;0;0;0;False;1;ToggleUI;False;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureTransformNode;24662;-2768,-544;Inherit;False;5683;False;1;0;SAMPLER2D;;False;2;FLOAT2;0;FLOAT2;1
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24653;-2512,-736;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;21524;8704,848;Inherit;False;Property;_COLORADDSUBDIFF;Cubemap Toggle;1714;0;Create;False;0;0;0;True;1;ToggleUI;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CustomExpressionNode;24656;-2992,-544;Inherit;False;if (UVSwitchProp == 0)$	return UV0@$else if (UVSwitchProp == 1)$	return UV1@$else if (UVSwitchProp == 2)$	return UV2@$else$	return UV3@;2;Create;5;True;UVSwitchProp;FLOAT;0;In;;Inherit;False;True;UV0;FLOAT2;0,0;In;;Inherit;False;True;UV1;FLOAT2;0,0;In;;Inherit;False;True;UV2;FLOAT2;0,0;In;;Inherit;False;True;UV3;FLOAT2;0,0;In;;Inherit;False;UVSwitch;False;False;0;;False;5;0;FLOAT;0;False;1;FLOAT2;0,0;False;2;FLOAT2;0,0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.FunctionNode;25081;-1072,2448;Inherit;False;Specular Highlights;1629;;22168;f01d465b622765446a78e511544ff258;0;28;2254;FLOAT3;0,0,0;False;44;FLOAT3;0,0,0;False;49;FLOAT3;0,0,0;False;1922;FLOAT4;0,0,0,0;False;1665;FLOAT4;0,0,0,0;False;192;FLOAT;1;False;293;FLOAT;1;False;594;FLOAT;0;False;578;FLOAT;0;False;1737;FLOAT;0;False;634;FLOAT;0;False;596;FLOAT;0;False;579;FLOAT;0;False;671;FLOAT;0;False;1739;FLOAT;0;False;1666;FLOAT4;0,0,0,0;False;1671;FLOAT3;0,0,0;False;1672;FLOAT3;0,0,0;False;1673;FLOAT3;0,0,0;False;1674;FLOAT3;0,0,0;False;1675;FLOAT;0;False;1676;FLOAT;0;False;1677;FLOAT;0;False;1678;FLOAT;0;False;1679;FLOAT;0;False;1680;FLOAT;0;False;1681;FLOAT;0;False;1682;FLOAT;0;False;6;FLOAT3;0;FLOAT3;58;FLOAT3;125;FLOAT3;2152;FLOAT3;1894;FLOAT4;1933
Node;AmplifyShaderEditor.CommentaryNode;5629;587.2,-1664;Inherit;False;2706.611;1279.384;;19;24642;16905;5677;22191;5684;5690;21390;24414;21539;5679;21538;20987;23831;21537;21540;5676;5630;5675;5637;Emission;1,0.724138,0,1;0;0
Node;AmplifyShaderEditor.ConditionalIfNode;8684;-1264,3664;Inherit;False;False;5;0;FLOAT;1;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;16840;960,560;Inherit;False;AmbientOcclusion;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21525;8928,848;Inherit;False;ToggleCubemap;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21103;-160,-1088;Inherit;False;VLLdotHTwo;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24661;-2512,-544;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;24307;-512,2688;Inherit;False;VertexLightShadows;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21855;960,368;Inherit;False;LuminancedLight;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;10638;0,1392;Inherit;False;2229.355;721.6573;;32;10644;21523;22506;22474;21444;21533;21526;21529;19810;19809;2355;20976;5759;21556;24134;5954;21237;21236;21235;24167;19808;21249;21245;21246;21247;21243;24313;21250;21251;21244;21248;25129;Cubemap;0,0.5019608,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21493;8928,944;Inherit;False;ToggleAdvanced;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21096;-160,-1408;Inherit;False;VLNdotHTwo;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21104;-160,-1008;Inherit;False;VLLdotHThree;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21100;-160,-1248;Inherit;False;VLNdotHFour;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;24166;-512,2608;Inherit;False;PixelAmbientShadows;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;2907;-1104,3664;Inherit;False;ShadowRim;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21102;-160,-1168;Inherit;False;VLLdotHOne;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21098;-160,-1328;Inherit;False;VLNdotHThree;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21105;-160,-928;Inherit;False;VLLdotHFour;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21094;-160,-1488;Inherit;False;VLNdotHOne;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;24654;-2384,-736;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.CommentaryNode;5637;669,-1328;Inherit;False;946.7302;241.8046;Emission Scroll V1;3;22183;5894;5668;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;21244;32,1632;Inherit;False;21096;VLNdotHTwo;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21243;32,1568;Inherit;False;21094;VLNdotHOne;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21248;32,1888;Inherit;False;21103;VLLdotHTwo;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;10648;0,928;Inherit;False;1215.048;387.565;;8;24130;24112;24109;24108;21517;2615;3121;24379;Matcap;0,0.5019608,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;21250;32,2016;Inherit;False;21105;VLLdotHFour;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;24134;560,1440;Inherit;False;21855;LuminancedLight;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21529;1344,1584;Inherit;False;21493;ToggleAdvanced;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;5759;304,1600;Inherit;False;2197;MainTexSaturate;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;21251;32,1440;Inherit;False;24307;VertexLightShadows;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;21235;304,1840;Inherit;False;19820;NdotAmbientL;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21236;304,1920;Inherit;False;19902;AmbientLdotAmbientH;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21556;304,1680;Inherit;False;19806;GrayscaledLight;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;19809;480,1760;Inherit;False;19804;LdotH;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;24167;304,1440;Inherit;False;24166;PixelAmbientShadows;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;14547;576,-112;Inherit;False;2907;ShadowRim;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21526;1344,1664;Inherit;False;21525;ToggleCubemap;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;19810;304,1760;Inherit;False;21432;NdotVCorr;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21245;32,1696;Inherit;False;21098;VLNdotHThree;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21237;512,1840;Inherit;False;19817;NdotAmbientH;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;20976;528,1600;Inherit;False;16840;AmbientOcclusion;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;24313;32,1504;Inherit;False;21183;VertexLightAtten;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;2355;512,1520;Inherit;False;2353;normals;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;25046;960,464;Inherit;False;LuminancedLightNoShadows;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21212;960,-16;Inherit;False;PreClampFinalLight;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;5954;304,1520;Inherit;False;2351;worldnormals;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;21249;32,1952;Inherit;False;21104;VLLdotHThree;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21247;32,1824;Inherit;False;21102;VLLdotHOne;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21246;32,1760;Inherit;False;21100;VLNdotHFour;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;25172;-1057.293,-224.9516;Inherit;False;LipMatCap;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;19808;528,1680;Inherit;False;19802;NdotH;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;5668;704,-1264;Inherit;False;Property;_EmissionscrollColor;Emission scroll Color;1708;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,1;1,1,1,1;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;24639;-2256,-736;Inherit;False;EmissionScrollMaskUVSwitch;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;24655;-2384,-544;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;2615;32,992;Inherit;False;2351;worldnormals;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;3121;240,992;Inherit;False;2197;MainTexSaturate;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;24130;240,1072;Inherit;False;25046;LuminancedLightNoShadows;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;25130;848,1472;Inherit;False;Baked Cubemap and Ambient Reflections;1522;;22174;eb37bac9792209a4a9e79fa57ed77ae1;0;25;2903;SAMPLERSTATE;0;False;2821;FLOAT3;0,0,0;False;2817;FLOAT;0;False;2191;FLOAT3;0,0,0;False;86;FLOAT3;0,0,0;False;93;FLOAT3;0,0,0;False;89;FLOAT3;1,1,1;False;1847;FLOAT;1;False;2674;FLOAT;1;False;2409;FLOAT;0;False;2671;FLOAT;0;False;2411;FLOAT;0;False;2500;FLOAT;0;False;2501;FLOAT;0;False;2502;FLOAT;0;False;2545;FLOAT4;0,0,0,0;False;2842;FLOAT4;0,0,0,0;False;2590;FLOAT;0;False;2602;FLOAT;0;False;2619;FLOAT;0;False;2636;FLOAT;0;False;2543;FLOAT;0;False;2604;FLOAT;0;False;2621;FLOAT;0;False;2638;FLOAT;0;False;3;FLOAT3;0;FLOAT;2705;FLOAT;2927
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;14546;816,-208;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21533;1584,1616;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21517;256,1152;Inherit;False;21493;ToggleAdvanced;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;24112;32,1152;Inherit;False;16840;AmbientOcclusion;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;5675;636.8,-864;Inherit;False;1618.596;442.6833;Emission;19;16904;24643;5683;22203;22201;22204;22202;21851;21857;5687;8822;16903;21852;21858;5678;5685;21850;5682;21856;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;24379;32,1072;Inherit;False;21212;PreClampFinalLight;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;5894;928,-1264;Inherit;False;Emissionsscrollcolor;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;24640;-2256,-544;Inherit;False;EmissionUVSwitch;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;25173;308.365,864.3626;Inherit;False;25172;LipMatCap;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;24642;672,-1072;Inherit;False;24639;EmissionScrollMaskUVSwitch;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerStateNode;16905;768,-992;Inherit;False;0;0;0;2;43;None;1;0;SAMPLER2D;;False;1;SAMPLERSTATE;0
Node;AmplifyShaderEditor.CommentaryNode;5630;672,-1600;Inherit;False;1146.265;258.0206;Emission Scroll V2;5;5670;7379;7380;5661;20988;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;7380;976,-1536;Inherit;False;2351;worldnormals;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;5661;704,-1456;Inherit;False;2197;MainTexSaturate;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;5677;944,-1072;Inherit;True;Property;_EmissionScrollMask;Emission Scroll Mask;1709;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;24643;656,-800;Inherit;False;24640;EmissionUVSwitch;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.CommentaryNode;2964;-1920,1664;Inherit;False;1227.628;476.2062;;12;21176;21184;21177;21178;21179;9167;19837;24077;5929;2928;6012;5934;Subsurface Scattering;1,0.4009434,0.4009434,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;2205;960,-208;Inherit;False;FinalLight;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerStateNode;16904;688,-720;Inherit;False;0;0;0;1;43;None;1;0;SAMPLER2D;;False;1;SAMPLERSTATE;0
Node;AmplifyShaderEditor.CommentaryNode;50;-1937,1024;Inherit;False;1213.111;558.6198;;11;21494;2221;21476;6009;21509;2974;19824;21213;23876;9349;2436;Rim Light;1,1,0,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;25137;5019.1,-144;Inherit;False;1196.501;572.5001;Final Base;16;25150;25145;23967;25138;22467;24092;25104;25147;24093;25101;25149;25100;22508;24094;25099;21782;;1,1,1,1;0;0
Node;AmplifyShaderEditor.FunctionNode;25086;528,992;Inherit;False;Matcap;1249;;22227;9363e9c7b7d1f2d4ab0056224472b0d9;0;7;65;FLOAT3;0,0,0;False;72;FLOAT3;0,0,0;False;929;COLOR;0,0,0,0;False;900;FLOAT3;0,0,0;False;890;FLOAT;0;False;833;FLOAT;0;False;581;FLOAT;0;False;2;FLOAT3;0;FLOAT;647
Node;AmplifyShaderEditor.CommentaryNode;2564;0,2304;Inherit;False;1273.458;361.4141;;8;21518;22198;22199;2969;2968;21519;21520;2584;Flipbook;1,0,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;7379;800,-1536;Inherit;False;2353;normals;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ConditionalIfNode;22474;1728,1616;Inherit;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;20988;944,-1456;Inherit;False;5894;Emissionsscrollcolor;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;25122;5088,-592;Inherit;False;1308.8;422.1001;Reflectivity Alpha;13;25135;25111;25102;25134;25133;25132;25131;25120;25119;25118;25114;25117;25143;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;25037;960,656;Inherit;False;SSSAtten;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;9087;960,-112;Inherit;False;PreFinalLight;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;5685;864,-624;Inherit;False;Property;_EmissionColor;Emission Color;1706;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,0;0,0,0,1;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;5683;864,-816;Inherit;True;Property;_Emission;Emission;1707;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;black;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;24077;-1696,1920;Inherit;False;25037;SSSAtten;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;5678;1072,-544;Inherit;False;Property;_EmissionTint;Emission Tint;1710;0;Create;True;0;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;21850;1536,-592;Inherit;False;Property;_EmissionLightscale;Emission Lightscale;1728;0;Create;True;0;0;0;False;1;ToggleUI;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;2436;-1888,1200;Inherit;False;2351;worldnormals;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;5682;1104,-624;Inherit;False;2197;MainTexSaturate;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;22506;1904,1616;Inherit;False;CubemapLightAbsorbtion;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;19824;-1888,1360;Inherit;False;19803;NdotV;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;5929;-1696,1760;Inherit;False;2351;worldnormals;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;9167;-1696,1840;Inherit;False;2197;MainTexSaturate;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;21856;1536,-512;Inherit;False;21855;LuminancedLight;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;22199;32,2448;Inherit;False;9087;PreFinalLight;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;19837;-1648,2016;Inherit;False;19806;GrayscaledLight;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;9349;-1888,1280;Inherit;False;2197;MainTexSaturate;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;25145;5200,-80;Inherit;False;Property;_ModeCustom;Mode Custom;1727;0;Create;True;0;0;0;False;1;ToggleUI;False;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;22198;32,2528;Inherit;False;2205;FinalLight;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;21213;-1888,1120;Inherit;False;21212;PreClampFinalLight;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;24109;960,1072;Inherit;False;MatcapLightAbsorbtion;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;25133;5152,-288;Inherit;False;21525;ToggleCubemap;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;25132;5152,-368;Inherit;False;21493;ToggleAdvanced;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;25129;1440,1744;Inherit;False;OneMinusReflectivity;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;25151;1216,-1536;Inherit;False;Emission Scroll V2;1414;;22346;995e6dd10a2936e4a88f19546ea48650;0;5;70;FLOAT3;0,0,0;False;71;FLOAT3;0,0,1;False;30;FLOAT3;0,0,0;False;28;COLOR;1,1,1,1;False;725;FLOAT2;0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;23876;-1888,1440;Inherit;False;16840;AmbientOcclusion;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;25100;5280,176;Inherit;False;9091;AlphaChannelMul;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;25095;-1616,1168;Inherit;False;Rim Light;1577;;22344;6b1931f25cd84864d9988266c3b81246;0;5;271;FLOAT3;0,0,0;False;22;FLOAT3;0,0,0;False;33;FLOAT3;0,0,0;False;260;FLOAT;0;False;322;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;21858;1376,-640;Inherit;False;3;0;FLOAT3;1,1,1;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;5934;-1264,1728;Inherit;False;Property;_SSSSetting;SSS Setting;1712;1;[Enum];Create;True;0;3;Light Based;0;Color Based;1;Mixed;2;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;24732;288,2464;Inherit;False;Clamped Flipbook;1231;;22340;87fba7ae5c66562488908889b8d68bfa;0;2;51;FLOAT3;1,1,1;False;52;FLOAT3;1,1,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;24094;5280,336;Inherit;False;24109;MatcapLightAbsorbtion;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;2968;336,2384;Inherit;False;Property;_FlipbookToggle;Flipbook Toggle;1713;0;Create;False;0;2;Off;0;On;1;0;False;1;ToggleUI;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;8822;1152,-720;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ComponentMaskNode;16903;1152,-816;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;25131;5296,-448;Inherit;False;25129;OneMinusReflectivity;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21852;1760,-592;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;5670;1568,-1536;Inherit;False;EmissionScrollV2;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.FunctionNode;25039;-1424,1808;Inherit;False;Subsurface scattering;1609;;22343;b3a8a731faf6b9a4bbb4cf58bc679816;0;9;24;FLOAT3;0,0,0;False;41;FLOAT3;0,0,0;False;445;FLOAT;0;False;142;FLOAT;0;False;169;FLOAT3;0,0,0;False;176;FLOAT3;0,0,0;False;186;FLOAT3;0,0,0;False;194;FLOAT3;0,0,0;False;237;FLOAT4;0,0,0,0;False;3;FLOAT3;35;FLOAT3;36;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;21782;5232,16;Inherit;False;Property;_Mode;Mode;1726;1;[Enum];Create;True;0;4;Opaque;0;Cutout;1;Fade;2;Transparent;3;0;False;0;False;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;22508;5280,256;Inherit;False;22506;CubemapLightAbsorbtion;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;24752;1216,-1264;Inherit;False;Emission Scroll V1;1459;;22351;b2dc8236d7837514dbd2e9707c89d6c6;0;1;32;COLOR;1,1,1,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25134;5392,-336;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;25099;5280,96;Inherit;False;2205;FinalLight;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;21518;480,2560;Inherit;False;21493;ToggleAdvanced;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;2974;-1472,1088;Inherit;False;Property;_RimToggle;Rim Toggle;1718;0;Create;True;0;2;Off;0;On;1;0;False;1;ToggleUI;False;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;5676;1824,-1328;Inherit;False;Property;_EmissionScrollToggle;Emission Scroll Toggle;1719;1;[Enum];Create;True;0;3;Off;0;ES v1;1;ES v2;2;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;25149;5376,-32;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;3;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21494;-1536,1360;Inherit;False;21493;ToggleAdvanced;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;17083;6000,1408;Inherit;False;922.2013;441.0284;;3;21890;21892;21891;Dissolve;1,1,1,1;0;0
Node;AmplifyShaderEditor.ConditionalIfNode;6012;-1072,1760;Inherit;False;False;5;0;FLOAT;1;False;1;FLOAT;1;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CustomExpressionNode;24841;-544,2432;Inherit;False;UNITY_BRANCH$if (_SpecularSetting == 0)$	Out = Toon@$else if (_SpecularSetting == 1)$	Out = GGX@$else if (_SpecularSetting == 2)$	Out = Anisotropic@$else$	Out = GGXAnisotropic@;1;Call;5;True;Out;FLOAT3;0,0,0;Out;;Inherit;False;True;Toon;FLOAT3;0,0,0;In;;Inherit;False;True;GGX;FLOAT3;0,0,0;In;;Inherit;False;True;Anisotropic;FLOAT3;0,0,0;In;;Inherit;False;True;GGXAnisotropic;FLOAT3;0,0,0;In;;Inherit;False;longIF;False;False;0;;False;6;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;2;FLOAT;0;FLOAT3;2
Node;AmplifyShaderEditor.SaturateNode;21857;1888,-592;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;25147;5504,-80;Inherit;False;False;5;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;2969;544,2400;Inherit;False;False;5;0;FLOAT;1;False;1;FLOAT;1;False;2;FLOAT;0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;5687;1568,-816;Inherit;True;3;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21509;-1264,1280;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;6009;-1280,1104;Inherit;False;False;5;0;FLOAT;1;False;1;FLOAT;1;False;2;FLOAT;0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;21540;2320,-1232;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;25114;5552,-464;Inherit;False;3;0;FLOAT;1;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;23831;1696,-1264;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;25117;5504,-544;Inherit;False;9091;AlphaChannelMul;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;20987;1808,-1168;Inherit;False;5670;EmissionScrollV2;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;21537;2320,-1152;Inherit;False;21493;ToggleAdvanced;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25101;5488,96;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21519;752,2416;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;24093;5536,272;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;25104;5680,-80;Inherit;False;False;5;0;FLOAT;0;False;1;FLOAT;2;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21523;1760,1488;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21476;-1088,1104;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;2966;5088,640;Inherit;False;Property;_SpecularToggle;Specular Toggle;1716;0;Create;True;0;2;Off;0;On;1;0;False;1;ToggleUI;False;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21520;896,2416;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;2223;-304,2448;Inherit;False;SpecularHighlight;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;21753;5088,560;Inherit;False;21493;ToggleAdvanced;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;2928;-896,1760;Inherit;False;SSS;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25118;5728,-544;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;24092;5664,272;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;25119;5712,-448;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;5679;2080,-1328;Inherit;False;False;5;0;FLOAT;1;False;1;FLOAT;1;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;21890;6032,1552;Inherit;False;2197;MainTexSaturate;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;21760;5120,1088;Inherit;False;21493;ToggleAdvanced;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21538;2560,-1232;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;21851;2096,-816;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;5935;5120,1168;Inherit;False;Property;_SSSToggle;SSS Toggle;1715;0;Create;True;0;2;Off;0;On;1;0;False;1;ToggleUI;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;10644;1904,1488;Inherit;False;CubemapReflections;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;22467;5872,-80;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21761;5408,1120;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;2929;5168,1008;Inherit;False;2928;SSS;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StaticSwitch;24414;2272,-816;Inherit;False;Property;_Keyword0;Keyword 0;162;0;Create;True;0;0;0;False;0;False;0;0;0;False;UNITY_PASS_FORWARDADD;Toggle;2;Key0;Key1;Fetch;False;True;All;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;2584;1040,2416;Inherit;False;Flipbook;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21754;5392,592;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;25174;6266.394,1391.272;Inherit;False;RithrinDissolve;0;;22378;9de9fdc4865d5f14fa6bbaa8a4044166;0;1;147;FLOAT3;0,0,0;False;4;FLOAT3;506;COLOR;505;FLOAT3;504;FLOAT;945
Node;AmplifyShaderEditor.RegisterLocalVarNode;24108;960,992;Inherit;False;Matcap;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;2221;-944,1104;Inherit;False;rimlight;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21539;2704,-1328;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ConditionalIfNode;8688;-1264,3840;Inherit;False;False;5;0;FLOAT;1;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;25143;5808,-288;Inherit;False;Constant;_Float19;Float 19;82;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;25102;5728,-368;Inherit;False;9091;AlphaChannelMul;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;2224;5088,480;Inherit;False;2223;SpecularHighlight;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;25120;5872,-512;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21763;5584,1008;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;10647;5536,768;Inherit;False;24108;Matcap;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21755;5568,480;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;2222;5536,848;Inherit;False;2221;rimlight;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;5690;2576,-816;Inherit;False;BasicEmission;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;20979;5536,688;Inherit;False;10644;CubemapReflections;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;20980;5536,928;Inherit;False;2584;Flipbook;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;5684;3088,-1328;Inherit;False;Emissionscroll;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21891;6656,1616;Inherit;False;EmissiveDissolve;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ConditionalIfNode;25111;6032,-544;Inherit;False;False;5;0;FLOAT;0;False;1;FLOAT;2;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;25138;6032,-80;Inherit;False;FinalBase;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;8359;6944,416;Inherit;False;1625.112;461.4236;Custom Rendering Options;8;8374;8397;8385;8367;8366;8389;2919;21375;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;8681;-1104,3840;Inherit;False;EmissiveRim;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;25135;6208,-544;Inherit;False;FinalAlphaOut;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;25139;6128,624;Inherit;False;25138;FinalBase;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;22135;-2560,-128;Inherit;False;391;431.3881;Shader Optimizer Animated States;4;22137;22136;22138;25142;;0,0.1294118,1,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;295;5856,672;Inherit;True;6;6;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;20985;6288,304;Inherit;False;5684;Emissionscroll;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;22125;6000,1088;Inherit;False;557.6592;242.7808;;2;22126;24749;Dither;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;20982;6112,800;Inherit;False;5690;BasicEmission;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;8366;7248,480;Inherit;False;263;291;Blend Alpha;3;8364;8365;8363;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21892;6656,1696;Inherit;False;VertexOffsetDissolve;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;8389;7520,480;Inherit;False;568.8174;379.8966;Stencil;7;8391;8390;8393;8392;8396;8395;8394;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;8397;8112,480;Inherit;False;434.1172;215.0966;Depth;4;8399;8402;8401;8400;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;14722;8624,416;Inherit;False;356;178;Custom Inspector Settings;1;14723;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;20983;6288,464;Inherit;False;21891;EmissiveDissolve;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;8367;6976,480;Inherit;False;259;291;Blend RGB;3;8362;8361;8360;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;25152;7952,912;Inherit;False;613.6592;1141.172;Shader Fallback properties;17;25096;25170;25158;25167;25166;25159;25157;25155;25156;25164;25163;25162;25153;25168;25154;25161;25169;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;20986;6288,384;Inherit;False;8681;EmissiveRim;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;8365;7264,688;Inherit;False;Property;_BlendOpAlpha;Blend Op Alpha;1675;1;[Enum];Create;True;0;0;1;UnityEngine.Rendering.BlendOp;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;8391;7536,528;Inherit;False;Property;_StencilBufferReference;Stencil Buffer Reference;1693;0;Create;True;0;0;1;UnityEngine.Rendering.BlendMode;True;0;False;0;0;0;255;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;21762;5344,1008;Inherit;False;Property;_SSSToggle;SSS Toggle;125;0;Create;False;0;0;0;False;0;False;0;0;0;False;_SUNDISK_NONE;ToggleOff;2;OFF;ON;Create;True;False;All;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;8362;6992,688;Inherit;False;Property;_BlendOpRGB;Blend Op RGB;1677;1;[Enum];Create;True;0;0;1;UnityEngine.Rendering.BlendOp;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;21444;1472,1472;Inherit;False;Property;_COLORADDSUBDIFF;Cubemap Toggle;125;0;Create;False;0;0;0;False;0;False;0;0;0;False;_COLORADDSUBDIFF_ON;ToggleOff;2;OFF;ON;Create;True;False;All;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;21179;-1872,1936;Inherit;False;21173;VLDirFour;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StaticSwitch;21752;5328,480;Inherit;False;Property;_SpecularToggle;Specular Toggle;125;0;Create;False;0;0;0;False;0;False;0;0;0;False;_REQUIRE_UV2;ToggleOff;2;OFF;ON;Create;True;False;All;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.FunctionNode;24994;6272,1552;Inherit;False;Dissolve;1345;;22352;82730ad0d4bfc13408a9f4bccf476772;0;1;424;FLOAT3;0,0,0;False;4;FLOAT;0;FLOAT;310;FLOAT3;425;FLOAT3;426
Node;AmplifyShaderEditor.SimpleAddOpNode;14780;6352,640;Inherit;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;22126;6064,1184;Inherit;False;9091;AlphaChannelMul;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;24749;6272,1184;Inherit;False;Dither;1328;;22355;043df5c2e430b6241a679a3821a80d10;0;1;53;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;21375;8128,768;Inherit;False;Property;_MaskClipValue;Mask Clip Value;1725;0;Create;True;0;0;1;;False;0;False;0.5;0.5;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;25170;8304,1776;Inherit;False;Property;_Cutoff;_Cutoff;1686;1;[HideInInspector];Create;True;0;0;1;UnityEngine.Rendering.BlendMode;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.IntNode;23966;5088,720;Inherit;False;Property;_SpecularToggleAnimated;_SpecularToggle;1413;0;Create;False;0;0;0;True;1;ToggleUI;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.GetLocalVarNode;18112;6432,768;Inherit;False;21892;VertexOffsetDissolve;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;8524;-528,2352;Half;False;Property;_SpecularSetting;Specular Setting;1717;1;[Enum];Create;True;0;4;Toon;0;Unity Standard GGX;1;Anisotropic by James OHare;2;Anisotropic GGX;3;0;True;0;False;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;21390;2848,-1328;Inherit;False;Property;_COLORCOLOR2;Toggle Advanced;129;0;Create;False;0;0;0;False;0;False;0;0;0;True;_GLOSSYREFLECTIONS;Toggle;2;OFF;ON;Reference;21385;False;True;All;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21087;-672,-1408;Inherit;False;FourLightPosY0WorldPos;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;8394;7824,608;Inherit;False;Property;_StencilBufferPassFront;Stencil Buffer Pass Front;1698;1;[Enum];Create;True;0;0;1;UnityEngine.Rendering.StencilOp;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21184;-1872,2000;Inherit;False;21183;VertexLightAtten;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21091;-672,-928;Inherit;False;VLFinalTwo;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;8396;7824,768;Inherit;False;Property;_StencilBufferZFailFront;Stencil Buffer ZFail Front;1695;1;[Enum];Create;True;0;0;1;UnityEngine.Rendering.StencilOp;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;21368;8672,688;Inherit;False;Property;_ShaderOptimizerEnabled;Shader Optimizer Enabled;1230;0;Create;True;0;3;Basic;0;Advanced;1;Advanced Plus;2;0;True;1;ShaderOptimizerLockButton;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;8392;7536,608;Inherit;False;Property;_StencilBufferReadMask;Stencil Buffer Read Mask;1690;0;Create;True;0;0;1;UnityEngine.Rendering.BlendMode;True;0;False;255;255;0;255;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21090;-672,-1008;Inherit;False;VLFinalOne;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;8400;8128,608;Inherit;False;Property;_ZTestMode;ZTest Mode;1697;1;[Enum];Create;True;0;0;1;UnityEngine.Rendering.CompareFunction;True;0;False;4;4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;8363;7264,528;Inherit;False;Property;_SourceBlendAlpha;Source Blend Alpha;1699;1;[Enum];Create;True;0;0;1;UnityEngine.Rendering.BlendMode;True;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21161;-1632,3104;Inherit;False;21101;VLHalfVectorFour;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;14723;8672,480;Inherit;False;Property;_AdvancedExperimentalToggle;Advanced Experimental Toggle;1705;1;[Enum];Create;True;0;3;Basic;0;Advanced;1;Advanced Plus;2;0;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.IntNode;22191;1824,-1408;Inherit;False;Property;_EmissionScrollToggleAnimated;_EmissionScrollToggle;1401;0;Create;False;0;0;0;True;1;ToggleUI;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21097;-672,-608;Inherit;False;VLHalfVectorTwo;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.IntNode;23967;5104,16;Inherit;False;Property;_ModeAnimated;_Mode;1410;0;Create;False;0;0;0;True;1;ToggleUI;False;0;1;False;0;1;INT;0
Node;AmplifyShaderEditor.IntNode;23968;9152,848;Inherit;False;Property;_COLORADDSUBDIFFAnimated;_COLORADDSUBDIFF;1458;0;Create;False;0;0;0;True;1;ToggleUI;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;24075;960,176;Inherit;False;DirectionalAttenuation;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;8360;6992,528;Inherit;False;Property;_SourceBlendRGB;Source Blend RGB;1700;1;[Enum];Create;True;0;0;1;UnityEngine.Rendering.BlendMode;True;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21172;-160,-688;Inherit;False;VLDirThree;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21092;-672,-848;Inherit;False;VLFinalThree;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.IntNode;22204;1776,-688;Inherit;False;Property;_OptimizerExcludeEmission;OptimizerExcludeEmission;1400;0;Create;True;0;0;0;True;1;ToggleUI;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.RangedFloatNode;8364;7264,608;Inherit;False;Property;_DestinationBlendAlpha;Destination Blend Alpha;1676;1;[Enum];Create;True;0;0;1;UnityEngine.Rendering.BlendMode;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.IntNode;22137;-2496,0;Inherit;False;Property;_MainColorAnimated;_MainColor;1407;0;Create;False;0;0;0;True;1;ToggleUI;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.IntNode;24675;-3488,-656;Inherit;False;Property;_EmissionScrollMaskUVSwitchAnimated;_EmissionScrollMaskUVSwitch;1404;0;Create;False;0;0;0;True;1;ToggleUI;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21089;-672,-1088;Inherit;False;VertexLightAttenNdL;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21099;-672,-528;Inherit;False;VLHalfVectorThree;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;8399;8128,528;Inherit;False;Property;_ZWriteMode;ZWrite Mode;1722;1;[Enum];Create;True;0;2;Off;0;On;1;0;True;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;8361;6992,608;Inherit;False;Property;_DestinationBlendRGB;Destination Blend RGB;1678;1;[Enum];Create;True;0;0;1;UnityEngine.Rendering.BlendMode;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;2919;6992,784;Float;False;Property;_CullMode;Cull Mode;1701;1;[Enum];Create;True;0;0;1;UnityEngine.Rendering.CullMode;True;0;False;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.IntNode;23965;5120,1248;Inherit;False;Property;_SSSToggleAnimated;_SSSToggle;1403;0;Create;False;0;0;0;True;1;ToggleUI;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.IntNode;22202;2048,-608;Inherit;False;Property;_EmissionTintAnimated;_EmissionTint;1344;0;Create;False;0;0;0;True;1;ToggleUI;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21101;-672,-448;Inherit;False;VLHalfVectorFour;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;21176;-1872,1744;Inherit;False;21174;VLDirOne;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;8393;7536,688;Inherit;False;Property;_StencilBufferWriteMask;Stencil Buffer Write Mask;1689;0;Create;True;0;0;1;UnityEngine.Rendering.BlendMode;True;0;False;255;255;0;255;0;1;FLOAT;0
Node;AmplifyShaderEditor.IntNode;24676;-3488,-464;Inherit;False;Property;_EmissionUVSwitchAnimated;_EmissionUVSwitch;1405;0;Create;False;0;0;0;True;1;ToggleUI;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21165;-1376,3104;Inherit;False;21102;VLLdotHOne;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.IntNode;25150;5040,-80;Inherit;False;Property;_ModeCustomAnimated;_ModeCustom;1411;0;Create;False;0;0;0;True;1;ToggleUI;False;0;1;False;0;1;INT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21158;-1376,2976;Inherit;False;21098;VLNdotHThree;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21171;-160,-768;Inherit;False;VLDirTwo;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;8395;7824,688;Inherit;False;Property;_StencilBufferFailFront;Stencil Buffer Fail Front;1694;1;[Enum];Create;True;0;0;1;UnityEngine.Rendering.StencilOp;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21174;-160,-848;Inherit;False;VLDirOne;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21085;-672,-1488;Inherit;False;FourLightPosX0WorldPos;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.IntNode;24674;-3488,-880;Inherit;False;Property;_MainTexUVSwitchAnimated;_MainTexUVSwitch;1408;0;Create;False;0;0;0;True;1;ToggleUI;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21177;-1872,1808;Inherit;False;21171;VLDirTwo;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;24376;6544,400;Inherit;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;21163;-1376,3232;Inherit;False;21104;VLLdotHThree;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21178;-1872,1872;Inherit;False;21172;VLDirThree;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;8374;7136,784;Inherit;False;Property;_AlphatoCoverage;Alpha to Coverage;1723;1;[Enum];Create;True;0;2;Off;0;On;1;0;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.IntNode;22138;-2496,80;Inherit;False;Property;_SaturationAnimated;_Saturation;1412;0;Create;False;0;0;0;True;1;ToggleUI;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.IntNode;25142;-2496,160;Inherit;False;Property;_CutoutAnimated;_Cutout;1406;0;Create;False;0;0;0;True;1;ToggleUI;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.ColorNode;25096;8304,1856;Inherit;False;Property;_Color;_Color;1323;1;[HideInInspector];Create;True;0;0;0;True;0;False;1,1,1,1;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;21147;-1632,2848;Inherit;False;21183;VertexLightAtten;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;21164;-1376,3296;Inherit;False;21105;VLLdotHFour;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.IntNode;22183;1392,-1168;Inherit;False;Property;_EmissionscrollColorAnimated;_EmissionscrollColor;1402;0;Create;False;0;0;0;True;1;ToggleUI;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.IntNode;22201;2048,-688;Inherit;False;Property;_EmissionColorAnimated;_EmissionColor;1398;0;Create;False;0;0;0;True;1;ToggleUI;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.RangedFloatNode;25158;8304,1376;Inherit;False;Property;_OcclusionStrength;_OcclusionStrength;1681;1;[HideInInspector];Create;True;0;0;1;UnityEngine.Rendering.BlendMode;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;8390;7824,528;Inherit;False;Property;_StencilBufferComparison;Stencil Buffer Comparison;1696;1;[Enum];Create;True;0;0;1;UnityEngine.Rendering.CompareFunction;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;21385;6080,704;Inherit;False;Property;_COLORCOLOR;Toggle Advanced;129;0;Create;False;0;0;0;False;0;False;0;0;0;False;_COLORCOLOR_ON;ToggleOff;2;OFF;ON;Create;True;False;All;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;8401;8320,528;Inherit;False;Property;_DepthOffsetFactor;Depth Offset Factor;1691;0;Create;True;0;0;1;UnityEngine.Rendering.BlendMode;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21160;-1632,3040;Inherit;False;21099;VLHalfVectorThree;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21173;-160,-608;Inherit;False;VLDirFour;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21093;-672,-768;Inherit;False;VLFinalFour;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.IntNode;25088;9168,688;Inherit;False;Property;_IgnoreProjector;IgnoreProjector;1324;0;Create;True;0;0;0;True;1;OverrideTagToggle(IgnoreProjector);False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21162;-1376,3168;Inherit;False;21103;VLLdotHTwo;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.IntNode;22136;-2496,-80;Inherit;False;Property;_OptimizerExcludeMainSettings;OptimizerExcludeMainSettings;1409;0;Create;True;0;0;0;True;1;ToggleUI;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.SamplerNode;25161;8000,1168;Inherit;True;Property;_DetailNormalMap;_DetailNormalMap;1321;0;Create;True;0;0;0;True;1;HideInInspector;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.IntNode;23969;9152,944;Inherit;False;Property;_COLORCOLORAnimated;_COLORCOLOR;1457;0;Create;False;0;0;0;True;1;ToggleUI;False;0;1;False;0;1;INT;0
Node;AmplifyShaderEditor.RangedFloatNode;25159;8304,1296;Inherit;False;Property;_BumpScale;_BumpScale;1683;1;[HideInInspector];Create;True;0;0;1;UnityEngine.Rendering.BlendMode;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21154;-1632,2976;Inherit;False;21097;VLHalfVectorTwo;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;25136;6512,560;Inherit;False;25135;FinalAlphaOut;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;22130;8960,688;Inherit;False;Property;_LockTooltip;LockTooltip;1475;0;Create;True;0;0;0;False;1;HelpBox(3);False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;8402;8320,608;Inherit;False;Property;_DepthOffsetUnits;Depth Offset Units;1692;0;Create;True;0;0;1;UnityEngine.Rendering.BlendMode;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21095;-672,-688;Inherit;False;VLHalfVectorOne;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;21157;-1376,2848;Inherit;False;21094;VLNdotHOne;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;25166;8304,1456;Inherit;False;Property;_GlossyReflections;_GlossyReflections;1684;1;[HideInInspector];Create;True;0;0;1;UnityEngine.Rendering.BlendMode;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21159;-1376,3040;Inherit;False;21100;VLNdotHFour;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21086;-672,-1328;Inherit;False;FourLightPosZ0WorldPos;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;25157;8304,1216;Inherit;False;Property;_DetailNormalMapScale;_DetailNormalMapScale;1680;1;[HideInInspector];Create;True;0;0;1;UnityEngine.Rendering.BlendMode;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21156;-1376,2912;Inherit;False;21096;VLNdotHTwo;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;25164;8000,1744;Inherit;True;Property;_BumpMap;_BumpMap;1318;0;Create;True;0;0;0;True;1;HideInInspector;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;25169;8304,1696;Inherit;False;Property;_Glossiness;_Glossiness;1687;1;[HideInInspector];Create;True;0;0;1;UnityEngine.Rendering.BlendMode;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;25155;8304,1136;Inherit;False;Property;_DstBlend;_DstBlend;1679;2;[HideInInspector];[Enum];Create;True;0;0;1;UnityEngine.Rendering.BlendMode;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;25156;8304,1056;Inherit;False;Property;_SrcBlend;_SrcBlend;1688;2;[HideInInspector];[Enum];Create;True;0;0;1;UnityEngine.Rendering.BlendMode;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21155;-1632,2912;Inherit;False;21095;VLHalfVectorOne;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;25163;8000,1552;Inherit;True;Property;_EmissionMap;_EmissionMap;1319;0;Create;True;0;0;0;True;1;HideInInspector;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;25162;8000,1360;Inherit;True;Property;_DetailMask;_DetailMask;1320;0;Create;True;0;0;0;True;1;HideInInspector;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;25153;8000,976;Inherit;True;Property;_MetallicGlossMap;_MetallicGlossMap;1322;0;Create;True;0;0;0;True;1;HideInInspector;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;25168;8304,1616;Inherit;False;Property;_GlossMapScale;_GlossMapScale;1682;1;[HideInInspector];Create;True;0;0;1;UnityEngine.Rendering.BlendMode;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;25154;8304,976;Inherit;False;Property;_ZWrite;_ZWrite;1721;2;[HideInInspector];[Enum];Create;True;0;2;Off;0;On;1;0;True;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;25167;8304,1536;Inherit;False;Property;_SpecularHighlights;_SpecularHighlights;1685;1;[HideInInspector];Create;True;0;0;1;UnityEngine.Rendering.BlendMode;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.IntNode;22203;2032,-528;Inherit;False;Property;_EmissionLightscaleAnimated;_EmissionLightscale;1399;0;Create;False;0;0;0;True;1;ToggleUI;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.RangedFloatNode;8385;7344,784;Inherit;False;Property;_ColorMask;Color Mask;1724;1;[Enum];Create;True;0;0;1;UnityEngine.Rendering.ColorWriteMask;True;0;False;15;15;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;6720,416;Float;False;True;-1;7;MorisMaterialInspector;0;0;CustomLighting;Moriohs Shaders/Moris Toon Shader/Toon;False;False;False;False;True;True;True;True;True;False;True;False;False;False;False;False;False;False;False;False;False;Back;0;True;8399;0;True;8400;True;0;True;8401;0;True;8402;False;0;Custom;0.5;True;True;0;True;Opaque;;Geometry;ForwardOnly;5;d3d11;glcore;gles3;vulkan;nomrt;True;True;True;True;0;True;8385;True;0;True;8391;255;True;8392;255;True;8393;0;True;8390;0;True;8394;0;True;8395;0;True;8396;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;0;4;10;25;False;0.5;True;1;1;True;8360;0;True;8361;1;0;True;8363;0;True;8364;0;True;8362;0;True;8365;0;False;5E-06;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;Standard;-1;-1;-1;-1;1;IgnoreProjectorPlaceholder=True;False;0;0;True;2919;-1;0;True;21375;0;0;0;False;0.1;False;-1;0;True;8374;True;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;24660;0;24564;0
WireConnection;24660;1;24667;0
WireConnection;24660;2;24665;0
WireConnection;24660;3;24657;0
WireConnection;24660;4;24663;0
WireConnection;24659;0;24666;0
WireConnection;24659;1;24660;0
WireConnection;24652;0;24659;0
WireConnection;24652;1;24666;1
WireConnection;24638;0;24652;0
WireConnection;43;1;24641;0
WireConnection;25179;3;43;0
WireConnection;9090;0;43;4
WireConnection;9090;1;296;4
WireConnection;24742;17;25179;0
WireConnection;9091;0;9090;0
WireConnection;2351;0;25026;0
WireConnection;25141;0;24742;0
WireConnection;25141;1;9091;0
WireConnection;25141;2;25140;0
WireConnection;21780;0;296;0
WireConnection;627;0;411;0
WireConnection;297;0;25141;0
WireConnection;297;1;21780;0
WireConnection;626;0;297;0
WireConnection;626;1;627;0
WireConnection;25050;19;19798;0
WireConnection;2197;0;626;0
WireConnection;21183;0;25050;397
WireConnection;19801;0;25050;0
WireConnection;19820;0;25050;60
WireConnection;2353;0;25026;14
WireConnection;21088;0;25050;375
WireConnection;19803;0;25050;13
WireConnection;25092;1258;19117;0
WireConnection;25092;150;14551;0
WireConnection;25092;1264;19839;0
WireConnection;25092;1263;19840;0
WireConnection;25092;1306;21201;0
WireConnection;25092;1305;21199;0
WireConnection;14557;0;25092;211
WireConnection;19806;0;25050;18
WireConnection;14556;0;25092;210
WireConnection;21432;0;25050;446
WireConnection;19802;0;25050;14
WireConnection;19804;0;25050;15
WireConnection;19817;0;25050;62
WireConnection;19902;0;25050;64
WireConnection;24300;0;25092;2137
WireConnection;23961;34;19832;0
WireConnection;24658;0;24623;0
WireConnection;24658;1;24667;0
WireConnection;24658;2;24665;0
WireConnection;24658;3;24657;0
WireConnection;24658;4;24663;0
WireConnection;24653;0;24664;0
WireConnection;24653;1;24658;0
WireConnection;24656;0;24632;0
WireConnection;24656;1;24667;0
WireConnection;24656;2;24665;0
WireConnection;24656;3;24657;0
WireConnection;24656;4;24663;0
WireConnection;25081;2254;24953;0
WireConnection;25081;44;5950;0
WireConnection;25081;49;2479;0
WireConnection;25081;1922;24302;0
WireConnection;25081;1665;21148;0
WireConnection;25081;192;20970;0
WireConnection;25081;293;20971;0
WireConnection;25081;594;19818;0
WireConnection;25081;578;19815;0
WireConnection;25081;1737;19845;0
WireConnection;25081;634;19854;0
WireConnection;25081;596;19819;0
WireConnection;25081;579;19816;0
WireConnection;25081;671;20969;0
WireConnection;25081;1739;21572;0
WireConnection;8684;1;8678;0
WireConnection;8684;2;23961;0
WireConnection;8684;3;21078;0
WireConnection;16840;0;25092;638
WireConnection;21525;0;21524;0
WireConnection;21103;0;25050;390
WireConnection;24661;0;24662;0
WireConnection;24661;1;24656;0
WireConnection;24307;0;25081;1933
WireConnection;21855;0;25092;1361
WireConnection;21493;0;21482;0
WireConnection;21096;0;25050;383
WireConnection;21104;0;25050;391
WireConnection;21100;0;25050;387
WireConnection;24166;0;25081;1894
WireConnection;2907;0;8684;0
WireConnection;21102;0;25050;389
WireConnection;21098;0;25050;385
WireConnection;21105;0;25050;392
WireConnection;21094;0;25050;381
WireConnection;24654;0;24653;0
WireConnection;24654;1;24664;1
WireConnection;25046;0;25092;2199
WireConnection;21212;0;25092;1309
WireConnection;25172;0;25179;65
WireConnection;24639;0;24654;0
WireConnection;24655;0;24661;0
WireConnection;24655;1;24662;1
WireConnection;25130;2821;24167;0
WireConnection;25130;2817;24134;0
WireConnection;25130;86;5954;0
WireConnection;25130;93;2355;0
WireConnection;25130;89;5759;0
WireConnection;25130;1847;20976;0
WireConnection;25130;2674;21556;0
WireConnection;25130;2409;19808;0
WireConnection;25130;2671;19810;0
WireConnection;25130;2411;19809;0
WireConnection;25130;2500;21235;0
WireConnection;25130;2501;21237;0
WireConnection;25130;2502;21236;0
WireConnection;25130;2545;21251;0
WireConnection;25130;2842;24313;0
WireConnection;25130;2590;21243;0
WireConnection;25130;2602;21244;0
WireConnection;25130;2619;21245;0
WireConnection;25130;2636;21246;0
WireConnection;25130;2543;21247;0
WireConnection;25130;2604;21248;0
WireConnection;25130;2621;21249;0
WireConnection;25130;2638;21250;0
WireConnection;14546;0;25092;0
WireConnection;14546;1;14547;0
WireConnection;21533;0;21529;0
WireConnection;21533;1;21526;0
WireConnection;5894;0;5668;0
WireConnection;24640;0;24655;0
WireConnection;5677;1;24642;0
WireConnection;5677;7;16905;0
WireConnection;2205;0;14546;0
WireConnection;25086;65;2615;0
WireConnection;25086;72;3121;0
WireConnection;25086;929;25173;0
WireConnection;25086;900;24379;0
WireConnection;25086;890;24130;0
WireConnection;25086;833;24112;0
WireConnection;25086;581;21517;0
WireConnection;22474;0;21533;0
WireConnection;22474;2;25130;2705
WireConnection;25037;0;25092;2155
WireConnection;9087;0;25092;198
WireConnection;5683;1;24643;0
WireConnection;5683;7;16904;0
WireConnection;22506;0;22474;0
WireConnection;24109;0;25086;647
WireConnection;25129;0;25130;2927
WireConnection;25151;70;7380;0
WireConnection;25151;71;7379;0
WireConnection;25151;30;5661;0
WireConnection;25151;28;20988;0
WireConnection;25151;725;5677;0
WireConnection;25095;271;21213;0
WireConnection;25095;22;2436;0
WireConnection;25095;33;9349;0
WireConnection;25095;260;19824;0
WireConnection;25095;322;23876;0
WireConnection;21858;1;5682;0
WireConnection;21858;2;5678;0
WireConnection;24732;51;22199;0
WireConnection;24732;52;22198;0
WireConnection;8822;0;5685;0
WireConnection;16903;0;5683;0
WireConnection;21852;0;21850;0
WireConnection;21852;1;21856;0
WireConnection;5670;0;25151;0
WireConnection;25039;24;5929;0
WireConnection;25039;41;9167;0
WireConnection;25039;445;24077;0
WireConnection;25039;142;19837;0
WireConnection;24752;32;5894;0
WireConnection;25134;0;25132;0
WireConnection;25134;1;25133;0
WireConnection;25149;0;25145;0
WireConnection;6012;1;5934;0
WireConnection;6012;2;25039;35
WireConnection;6012;3;25039;36
WireConnection;6012;4;25039;0
WireConnection;24841;2;25081;0
WireConnection;24841;3;25081;58
WireConnection;24841;4;25081;125
WireConnection;24841;5;25081;2152
WireConnection;21857;0;21852;0
WireConnection;25147;0;25145;0
WireConnection;25147;3;25149;0
WireConnection;25147;4;21782;0
WireConnection;2969;0;2968;0
WireConnection;2969;3;24732;0
WireConnection;5687;0;16903;0
WireConnection;5687;1;8822;0
WireConnection;5687;2;21858;0
WireConnection;21509;0;2974;0
WireConnection;21509;1;21494;0
WireConnection;6009;1;2974;0
WireConnection;6009;3;25095;0
WireConnection;21540;0;5676;0
WireConnection;25114;1;25131;0
WireConnection;25114;2;25134;0
WireConnection;23831;0;24752;0
WireConnection;23831;1;5677;1
WireConnection;25101;0;25099;0
WireConnection;25101;1;25100;0
WireConnection;21519;0;2968;0
WireConnection;21519;1;21518;0
WireConnection;24093;0;22508;0
WireConnection;24093;1;24094;0
WireConnection;25104;0;25147;0
WireConnection;25104;2;25101;0
WireConnection;25104;3;25099;0
WireConnection;25104;4;25099;0
WireConnection;21523;0;25130;0
WireConnection;21523;1;21533;0
WireConnection;21476;0;6009;0
WireConnection;21476;1;21509;0
WireConnection;21520;0;2969;0
WireConnection;21520;1;21519;0
WireConnection;2223;0;24841;2
WireConnection;2928;0;6012;0
WireConnection;25118;0;25117;0
WireConnection;25118;1;25114;0
WireConnection;24092;0;24093;0
WireConnection;25119;0;25114;0
WireConnection;5679;1;5676;0
WireConnection;5679;3;23831;0
WireConnection;5679;4;20987;0
WireConnection;21538;0;21540;0
WireConnection;21538;1;21537;0
WireConnection;21851;0;5687;0
WireConnection;21851;2;21857;0
WireConnection;10644;0;21523;0
WireConnection;22467;0;25104;0
WireConnection;22467;2;24092;0
WireConnection;21761;0;21760;0
WireConnection;21761;1;5935;0
WireConnection;24414;1;21851;0
WireConnection;2584;0;21520;0
WireConnection;21754;0;21753;0
WireConnection;21754;1;2966;0
WireConnection;25174;147;21890;0
WireConnection;24108;0;25086;0
WireConnection;2221;0;21476;0
WireConnection;21539;0;5679;0
WireConnection;21539;1;21538;0
WireConnection;8688;1;8678;0
WireConnection;8688;3;23961;22
WireConnection;25120;0;25118;0
WireConnection;25120;1;25119;0
WireConnection;21763;0;2929;0
WireConnection;21763;1;21761;0
WireConnection;21755;0;2224;0
WireConnection;21755;1;21754;0
WireConnection;5690;0;24414;0
WireConnection;5684;0;21539;0
WireConnection;21891;0;25174;506
WireConnection;25111;0;25147;0
WireConnection;25111;2;25120;0
WireConnection;25111;3;25102;0
WireConnection;25111;4;25143;0
WireConnection;25138;0;22467;0
WireConnection;8681;0;8688;0
WireConnection;25135;0;25111;0
WireConnection;295;0;21755;0
WireConnection;295;1;20979;0
WireConnection;295;2;10647;0
WireConnection;295;3;2222;0
WireConnection;295;4;20980;0
WireConnection;295;5;21763;0
WireConnection;21892;0;25174;504
WireConnection;21762;1;2929;0
WireConnection;21444;1;25130;0
WireConnection;21752;1;2224;0
WireConnection;24994;424;21890;0
WireConnection;14780;0;25139;0
WireConnection;14780;1;295;0
WireConnection;14780;2;20982;0
WireConnection;24749;53;22126;0
WireConnection;21390;1;21539;0
WireConnection;21087;0;25050;373
WireConnection;21091;0;25050;378
WireConnection;21090;0;25050;377
WireConnection;21097;0;25050;384
WireConnection;24075;0;25092;1989
WireConnection;21172;0;25050;395
WireConnection;21092;0;25050;379
WireConnection;21089;0;25050;376
WireConnection;21099;0;25050;386
WireConnection;21101;0;25050;388
WireConnection;21171;0;25050;394
WireConnection;21174;0;25050;393
WireConnection;21085;0;25050;372
WireConnection;24376;0;20985;0
WireConnection;24376;1;20986;0
WireConnection;24376;2;20983;0
WireConnection;21385;1;295;0
WireConnection;21173;0;25050;396
WireConnection;21093;0;25050;380
WireConnection;21095;0;25050;382
WireConnection;21086;0;25050;374
WireConnection;0;2;24376;0
WireConnection;0;9;25136;0
WireConnection;0;10;25174;505
WireConnection;0;13;14780;0
WireConnection;0;11;18112;0
ASEEND*/
//CHKSM=C21BA61CB3ADD92A1BF3265817C5ACA203384EA7